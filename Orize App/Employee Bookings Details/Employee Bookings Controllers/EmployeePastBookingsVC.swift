//
//  EmployeePastBookingsVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 12/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class EmployeePastBookingsVC: UIViewController {
    
    //Outlets
    @IBOutlet private weak var logoAndRightButtonsView: UIView!
    @IBOutlet weak var staticBookingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //Variables
    var employeeBookingArray = [BookingDetails]()
    var coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    let pastBookingId = 1 //To See Past Booking Use 1

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getEmployeeBookings(coWorkerEmployeeId: coWorkerEmployeeId)
    }
    
    private func setupInitialView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: EmployeeBookingRegisteredXibs.EmployeeBookingsTblCell, bundle: nil), forCellReuseIdentifier: EmployeeBookingRegisteredXibs.EmployeeBookingsTblCell)
        
    }
    
    //MARK: IB-ACTIONS
    @IBAction func currentBookingsBtntapped(_ sender: UIButton) {
        //Displays Current Employee Booking VC - EmployeeBookingVc
        self.navigationController?.popViewController(animated: true)
    }

}

extension EmployeePastBookingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employeeBookingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeBookingRegisteredXibs.EmployeeBookingsTblCell, for: indexPath) as? EmployeeBookingsTblCell else {return UITableViewCell()}
        
        cell.configureCell(bookingDetails: employeeBookingArray[indexPath.row], delegate: self, bookingDetailInt: indexPath.row)
        return cell
    }
    
    
}

//MARK: Employee Booking Cell Protocol Implementation
extension EmployeePastBookingsVC: EmployeeBookingsTblCellDelegate {
    
    func viewButtonTapped(bookingDetails: BookingDetails, tag: Int) {
        guard let bookingDetailsVc = storyboard?.instantiateViewController(withIdentifier: "EmployeeBookingDetailsVC") as? EmployeeBookingDetailsVC else {return}
        
        bookingDetailsVc.bookingNumber = bookingDetails.bookingNo
        bookingDetailsVc.orderType = bookingDetails.type
        
        navigationController?.pushViewController(bookingDetailsVc, animated: true)
        
    }
    
    func cancelButtonTapped(bookingDetails: BookingDetails, tag: Int) {
        print(bookingDetails)
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension EmployeePastBookingsVC {
    
    func getEmployeeBookings(coWorkerEmployeeId: Int) {
        
        self.view.makeToastActivity(.center)
        
        EmployeeBookingServices.instance.getEmployeeBookings(coWorkerEmployeeId: coWorkerEmployeeId, bookingPeriodId: pastBookingId) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.employeeBookingArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        //print(self.employeeBookingArray)
                    } else {
                        //Failure
                        debugPrint(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
}
