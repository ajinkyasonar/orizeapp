//
//  AllVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 01/02/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol AllVCDelegate: class {
    
    func sendCartCountToRootVC(cartCount: Int)
    func passCategoryIndexToRootVC(categoryIndex: Int)
}

class AllVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    
    //Variables
    let employeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var allProductResultArray = [ProductDatum]()
    var productListArray = [ProductsListArr]()
    var searchAutoCompleteArray = [SearchAutocompleteDatum]()
    
    //Variables to get
    var storeCategoryId: Int!
    
    //Delegate
    weak var delegate: AllVCDelegate?
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getAllCategories(with: self.storeCategoryId)
        configureTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    private func configureTableView() {
        
        tableView.register(UINib(nibName: ConvinienceStoreRegisteredXibs.AllCategoryTblCell, bundle: nil), forCellReuseIdentifier: ConvinienceStoreRegisteredXibs.AllCategoryTblCell)
        tableView.delegate = self
        tableView.dataSource = self
        
        searchTextField.delegate = self
        
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        tableViewHeightConstraint.constant = tableView.contentSize.height
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        
    }
    
    
    
}

//MARK: ALL PRODUCT TABLE VIEW IMPLEMENTATION
extension AllVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allProductResultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConvinienceStoreRegisteredXibs.AllCategoryTblCell, for: indexPath) as? AllCategoryTblCell else {return UITableViewCell()}
        cell.configureCell(productData: allProductResultArray[indexPath.row], delegate: self, atIndex: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? AllCategoryTblCell else {return}
        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
    }
    
}

//MARK: ALL CATEGORY TABLEVIEW CELL DELEGATE IMPLEMENTATION -> For View Button Navigation index
extension AllVC: AllCategoryTblCellDelegate {
    
    func viewAllBtnTapped(_ atIndex: Int) {
        //Pass This Category + 1 to all root view controller and navigation to individual category
        let categoryIndexToPass = (atIndex + 1)
        delegate?.passCategoryIndexToRootVC(categoryIndex: categoryIndexToPass) //This goes to Root View Controller
    }
}




//MARK: COLLECTION VIEW IMPLEMENTATION
extension AllVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allProductResultArray[collectionView.tag].productsListArr.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConvinienceStoreRegisteredXibs.AllCategoryCollectionCell, for: indexPath) as? AllCategoryCollectionCell else {return UICollectionViewCell()}
        
        cell.configureCell(productListArray: allProductResultArray[collectionView.tag].productsListArr[indexPath.row], delegate: self, productIndexRow: indexPath.row, productIndexSection: collectionView.tag)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: (self.view.frame.width - 40)/3, height: 230)
        }else {
            return CGSize(width: (self.view.frame.width-30)/2, height: 230)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Showing Details View Controller
        let selectedItem = allProductResultArray[collectionView.tag].productsListArr[indexPath.row]
        guard let selectedProductVc = storyboard?.instantiateViewController(withIdentifier: "SelectedProductVC") as? SelectedProductVC else {return}
        selectedProductVc.selectedProductDetails = selectedItem
        selectedProductVc.delegate = self
        navigationController?.present(selectedProductVc, animated: true, completion: nil)
    }
    
    
}

//MARK: ALL Category Collection Cell Delegate Implementation
extension AllVC: AllCategoryCollectionCellDelegate {
    
    
    func addButtonTapped(productDetails: ProductsListArr, productCount: Int, productIndexRow productIndex: Int, productIndexSection: Int) {
        
        addRemoveStoreCartProduct(employeeId: employeeId, storeProductId: productDetails.productID, productQuantity: productCount)
        
    }
    
    func removeButtonTapped(productDetails: ProductsListArr, productCount: Int, productIndexRow productIndex: Int, productIndexSection: Int) {
        
        addRemoveStoreCartProduct(employeeId: employeeId, storeProductId: productDetails.productID, productQuantity: productCount)
    }
    
    
}


//MARK: SEARCH TEXT FIELD IMPLEMENTATION
extension AllVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        guard let allConvenienceSearchVc = storyboard?.instantiateViewController(withIdentifier: "ConvenienceAllCategorySearchVC") as? ConvenienceAllCategorySearchVC else {return}
        searchTextField.endEditing(true)
        allConvenienceSearchVc.convenienceSearchAutocompleteArray = self.searchAutoCompleteArray
        allConvenienceSearchVc.modalTransitionStyle = .crossDissolve
        allConvenienceSearchVc.modalPresentationStyle = .overCurrentContext
        allConvenienceSearchVc.delegate = self
        navigationController?.pushViewController(allConvenienceSearchVc, animated: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        textField.text = ""
        return true
        
    }
    
}

//MARK: FOOD SEARCH VIEW CONTROLLER DELEGATE
extension AllVC: ConvenienceAllCategorySearchVCDelegate {
    
    func passCategoryIndexToAllVC(categoryIndex: Int) {
        delegate?.passCategoryIndexToRootVC(categoryIndex: categoryIndex)
    }
    
    
}

//MARK: SELECTED PRODUCT VIEW CONTROLLER DELEGATE IMPLEMENTATION - WHEN TAPPED ON PRODUCT
extension AllVC: SelectedProductVCDelegate {
    
    func addRemoveConvenienceCartProduct(product: ProductsListArr, productQuantity: Int) {
        addRemoveStoreCartProduct(employeeId: employeeId, storeProductId: product.productID, productQuantity: productQuantity)
    }
    
}


//MARK: API IMPLEMENTATION HERE
extension AllVC {
    
    func getAllCategories(with storeCategoryId: Int) {
        
        allProductResultArray.removeAll()
        productListArray.removeAll()
        searchAutoCompleteArray.removeAll()
        self.view.makeToastActivity(.center)
        
        ConvenienceStoreService.instance.getConvenienceStoreProductsListWith(storeCategoryId: storeCategoryId) { (true, returnedResponse) in
            
            if let returnedResponse = returnedResponse {
                
                if returnedResponse.code == 0 {
                    //Success
                    //print(returnedResponse.results)
                    for values in returnedResponse.results {
                        self.allProductResultArray = values.productData
                        self.searchAutoCompleteArray = values.searchAutocompleteData
                        for values in self.allProductResultArray {
                            self.productListArray = values.productsListArr
                            //print(self.productListArray)
                        }
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.tableView.reloadData()
                            self.getEmployeeDetails(coWorkerEmployeeId: self.employeeId)
                        }
                    }
                }else {
                    //Failure
                    //print(returnedResponse.message)
                    self.view.hideToastActivity()
                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                }
                
            }
            
            
        }
        
    }
    
    //Add Remove All Store Cart Product API Implementation
    func addRemoveStoreCartProduct(employeeId: Int, storeProductId: Int, productQuantity: Int) {
        
        let params = ["co_works_employee_id": employeeId, "store_products_id": storeProductId, "qty": productQuantity]
        
        ServiceHelper.instance.GenericPostService(forUrl: ADD_REMOVE_STORE_CART_PRODUCT_URL, parameters: params) { (addRemoveResponse: ConvenienceStoreAddRemoveProduct) in
            
            DispatchQueue.main.async {
                self.view.makeToast(addRemoveResponse.message, duration: 1.0, position: .center)
                self.getEmployeeDetails(coWorkerEmployeeId: employeeId)
            }
            
        }
        
    }
    
    //Get EMPLOYEE DETAILS - This is To Get Total Cart Items with Employee Details
    func getEmployeeDetails(coWorkerEmployeeId: Int) {
        
        let params = ["co_works_employee_id": coWorkerEmployeeId]
        
        ServiceHelper.instance.GenericPostService(forUrl: GET_EMPLOYEE_DETAILS_URL, parameters: params) { (employeeDetails: EmployeeDetails) in
            
            if employeeDetails.code == 0 {
                //Send employee cart count to root view controller for cart display
                employeeDetails.results.forEach({ (detail) in
                    self.delegate?.sendCartCountToRootVC(cartCount: detail.itemsInCart)
                })
            }
            
        }
        
    }
    
}
