//
//  LaundryServices.swift
//  Orize App
//
//  Created by Apple on 15/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class LaundryServices {
    
    static let instance = LaundryServices()
    
    //MARK: GET LAUNDRY VENDER LIST SERVICE
    func getLaundryVenderList(completion: @escaping(LaundryVenderList?, Error?) -> ()) {
        
        guard let url = URL(string: GET_LAUNDRY_VENDERS_LIST_URL) else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                let decoder = JSONDecoder()
                guard let data = data else {return completion(nil, error)}
                
                do{
                    let returnedResponse = try decoder.decode(LaundryVenderList.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    print(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    //MARK: GET LAUNDRY VENDER PRODUCTS WITH VENDER CATEGORY ID
    
    func getVenderProductsWith(venderId id: Int, completion: @escaping(LaundryCategories?, Error?) -> Void) {
        
        guard let url = URL(string: GET_LAUNDRY_CATEGORIES_WITH_VENDER_ID) else {return}
        
        let postData = NSMutableData(data: "laundry_vendors_id=\(id)".data(using: .utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpBody = postData as Data
        urlRequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }
            else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(LaundryCategories.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    //MARK: ADD REMOVE LAUNDRY SERVICES FROM CART
    func addLaundryServices(coworkedId: Int, laundryProductId: Int, productQuantity: Int, laundryServiceId: Int, completion: @escaping(LaundryProductBooking?, Error?) -> ()) {
        
        guard let url = URL(string: Add_LAUNDRY_SERVICES_TO_CART) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coworkedId)".data(using: .utf8)!)
        postData.append("&laundry_products_id=\(laundryProductId)".data(using: .utf8)!)
        postData.append("&qty=\(productQuantity)".data(using: .utf8)!)
        postData.append("&laundry_service_id=\(laundryServiceId)".data(using: .utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }
            else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(LaundryProductBooking.self, from: data)
                    completion(returnedResponse, nil)
                    return
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
    }
    
    //MARK: REMOVE LAUNDRY SERVICE FROM CART API
    func removeLaundryServiceFromCart(cartId: Int, coworkedId: Int, completion: @escaping(LaundryProductBooking?, Error?) -> ()) {
        
        guard let url = URL(string: REMOVE_LAUNDRY_SERVICE_FROM_CART) else {return}
        
        let postData = NSMutableData(data: "cart_id=\(cartId)".data(using: .utf8)!)
        postData.append("&co_works_employee_id=\(coworkedId)".data(using: .utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }
            else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(LaundryProductBooking.self, from: data)
                    completion(returnedResponse, nil)
                    return
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
            
            
        }
        task.resume()
        
    }
    
    //MARK: REMOVE LAUNDRY PRODUCT FROM CART API
    func updateLaundryProductFromCart(coworkerId: Int, laundryProductId: Int, productQuantity: Int, completion:@escaping(LaundryProductBooking?, Error?)->()) {
        
        guard let url = URL(string: UPDATE_LAUNDRY_PRODUCT_FROM_CART) else {return}
        let postData = NSMutableData(data: "co_works_employee_id=\(coworkerId)".data(using: .utf8)!)
        postData.append("&laundry_products_id=\(laundryProductId)".data(using: .utf8)!)
        postData.append("&qty=\(productQuantity)".data(using: .utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if let error = error {
                completion(nil, error)
                return
            }
            else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(LaundryProductBooking.self, from: data)
                    completion(returnedResponse, nil)
                }
                catch{
                    completion(nil, error)
                }
            }
            
        }
        task.resume()
        
    }
    
    
}
