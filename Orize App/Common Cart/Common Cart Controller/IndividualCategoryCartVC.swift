//
//  FoodCourtCartVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 29/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class IndividualCategoryCartVC: UIViewController {
    
    //Outlets
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var emptyCartView: UIView!
    
    @IBOutlet weak var cartProductTblView: UITableView!
    @IBOutlet weak var cartProductTblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var summaryView: UIView!
    @IBOutlet weak var subtotalAmountLbl: UILabel!
    @IBOutlet weak var cgstAmountLbl: UILabel!
    @IBOutlet weak var sgstAmountLbl: UILabel!
    @IBOutlet weak var grandTotalAmountLbl: UILabel!
    
    //Add On Outlets
    @IBOutlet weak var addOnTableView: UITableView!
    @IBOutlet weak var addOnTblHeight: NSLayoutConstraint!
    
    //Variables
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var moduleBooking: ModuleBooking!
    var productsArray = [ProductCartList]()
    var categoryModuleId: Int!
    
    var totalBookingAmountValue = Int()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getEmployeeCartDetails(byEmployeeId: coWorkerEmployeeId)
        setupInitialView()
        //updateRefreshProductList(withEmployeeId: coWorkerEmployeeId)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupInitialView()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK: IB-ACTIONS
    @IBAction func continuePaymentBtnTapped(_ sender: UIButton) {
        
        //Go to Payment Selection View Controller
        guard let paymentSelectionVc = storyboard?.instantiateViewController(withIdentifier: "CommonCartGatewaySelectionVC") as? CommonCartGatewaySelectionVC else {return}
        paymentSelectionVc.totalCartAmountValue = self.totalBookingAmountValue
        self.navigationController?.pushViewController(paymentSelectionVc, animated: true)
    }
    
    
    
}

//MARK: INITIAL VIEW SETUP FUNCTIONS
extension IndividualCategoryCartVC {
    
    private func setupInitialView() {
        
        cartProductTblView.delegate = self
        cartProductTblView.dataSource = self
        
        //Register Cell Based on Module Name
        switch moduleBooking.moduleName {
            
        case .FOODCOURT:
            cartProductTblView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCartTblCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.FoodCartTblCell)
            
        case .CONVENIENCE_STORE:
            cartProductTblView.register(UINib(nibName: ConvinienceStoreRegisteredXibs.ConvenienceCartTblCell, bundle: nil), forCellReuseIdentifier: ConvinienceStoreRegisteredXibs.ConvenienceCartTblCell)
            
        case .LAUNDRY:
            cartProductTblView.register(UINib(nibName: "LaundryCartTblCell", bundle: nil), forCellReuseIdentifier: "LaundryCartTblCell")
            
        }
        
        //addOnTableView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCartAddonTblCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.FoodCartAddonTblCell)
        
        self.cartProductTblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        cartProductTblView.layer.removeAllAnimations()
        cartProductTblHeight.constant = cartProductTblView.contentSize.height
        //addOnTableView.layer.removeAllAnimations()
        //addOnTblHeight.constant = addOnTableView.contentSize.height
        
        self.updateViewConstraints()
        self.view.layoutIfNeeded()
        
    }
    
}


//MARK: TABLE VIEW IMPLEMENTATION
extension IndividualCategoryCartVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return moduleBooking.productCartList.count
        return productsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch moduleBooking.moduleName {
            
        case .FOODCOURT:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FoodCourtRegisteredXibs.FoodCartTblCell, for: indexPath) as? FoodCartTblCell else {return UITableViewCell()}
            
            //cell.configureCell(productDetails: moduleBooking.productCartList[indexPath.row], delegate: self, categoryInt: indexPath.section, productInt: indexPath.row)
            cell.configureCell(productDetails: productsArray.sorted()[indexPath.row], delegate: self, categoryInt: indexPath.section, productInt: indexPath.row)
            return cell
            
        case .CONVENIENCE_STORE:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ConvinienceStoreRegisteredXibs.ConvenienceCartTblCell, for: indexPath) as? ConvenienceCartTblCell else {return UITableViewCell()}
            
            cell.configureCell(productDetails: productsArray.sorted()[indexPath.row], delegate: self, productIndex: indexPath.row)
            
            return cell
            
        case .LAUNDRY:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LaundryCartTblCell", for: indexPath) as? LaundryCartTblCell else {return UITableViewCell()}
            
            cell.configureCell(productDetails: productsArray.sorted()[indexPath.row], delegate: self, productIndex: indexPath.row)
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: FOOD COURT TABLE CELL DELEGATE IMPLEMENTATION
extension IndividualCategoryCartVC: FoodCartTblCellDelegate {
    
    
    func preBookingEditButtonTapped(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        
        //This Is To Navigate To Pre Booking SELECTION VIEW CONTROLLER
        guard let preBookingSelectionVc = FOODCOURT_STORYBOARD.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
        preBookingSelectionVc.editProductDetails = productDetails
        self.navigationController?.pushViewController(preBookingSelectionVc, animated: true)
    }
    
    
    func addButtonTappedFoodCart(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        let foodCourtProductId = productDetails.productID
        //print(foodCourtProductId)
        //addRemoveFoodCourtProduct(coWorkerEmployeeId: coWorkerEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: ProductQuantity)
        addRemoveFoodCourtItemService(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: ProductQuantity, subscriptionDate: "", deliveryTime: "")
    }
    
    func removeButtonTappedFoodCart(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        let foodCourtProductId = productDetails.productID
        //print(foodCourtProductId)
        //addRemoveFoodCourtProduct(coWorkerEmployeeId: coWorkerEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: ProductQuantity)
        addRemoveFoodCourtItemService(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: ProductQuantity, subscriptionDate: "", deliveryTime: "")
    }
    
}

//MARK: CONVENIENCE CART TABLE CELL DELEGATE IMPLEMENTATION
extension IndividualCategoryCartVC: ConvenienceCartTblCellDelegate {
    
    
    func addButtonTappedConvCart(productDetails: ProductCartList, productIndex: Int, productQuantity: Int) {
        addRemoveProductsFromConvenienceCart(coWorkersEmployeeId: coWorkerEmployeeId, storeProductId: productDetails.productID, productQuantity: productQuantity)
    }
    
    func removeButtonTappedConvCart(productDetails: ProductCartList, productIndex: Int, productQuantity: Int) {
        addRemoveProductsFromConvenienceCart(coWorkersEmployeeId: coWorkerEmployeeId, storeProductId: productDetails.productID, productQuantity: productQuantity)
    }
    
    
}

//MARK: LAUNDRY CART TABLE VIEW CELL DELEGATE IMPLEMENTATION
extension IndividualCategoryCartVC: LaundryCartTblCellDelegate {
    
    
    func editBtnTapped(at index: Int, productDetails: ProductCartList) {
        guard let laundrySelectionvc = LAUNDRY_STORYBOARD.instantiateViewController(withIdentifier: "LaundryProductSelectionVC") as? LaundryProductSelectionVC else {return}
        laundrySelectionvc.editLaundryProduct = productDetails
        laundrySelectionvc.modalTransitionStyle = .crossDissolve
        laundrySelectionvc.modalPresentationStyle = .overCurrentContext
        laundrySelectionvc.delegate = self
        navigationController?.present(laundrySelectionvc, animated: true)
    }
    
}

//MARK: LaundryProductSelectionVCDelegate IMPLEMENTATION

extension IndividualCategoryCartVC: LaundryProductSelectionVCDelegate {
  
    func laundryPopupDismissed() {
        getEmployeeCartDetails(byEmployeeId: coWorkerEmployeeId)
    }
 
}


//MARK: API IMPLEMENTATIONS TO ADD REMOVE PRODUCTS
extension IndividualCategoryCartVC {
    
    //MARK: ADD REMOVE FOOD PRODUCTS API IMPLEMENTATION
    
    func addRemoveFoodCourtItemService(coWorkersEmployeeId: Int, foodCourtProductId: Int, productQuantity: Int, subscriptionDate: String, deliveryTime: String) {
        
        
        FoodCourtServices.instance.addRemoveFoodcourtCartItem(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: productQuantity, subscriptionDate: subscriptionDate, deliveryTime: deliveryTime) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        DispatchQueue.main.async {
                            self.getEmployeeCartDetails(byEmployeeId: coWorkersEmployeeId)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //MARK: ADD REMOVE PRODUCTS FROM CONVENIENCE CART API SERVICE
    
    func addRemoveProductsFromConvenienceCart(coWorkersEmployeeId: Int, storeProductId: Int, productQuantity: Int) {
        
        let params = ["co_works_employee_id": coWorkersEmployeeId, "store_products_id": storeProductId, "qty": productQuantity]
        
        ServiceHelper.instance.GenericPostService(forUrl: ADD_REMOVE_STORE_CART_PRODUCT_URL, parameters: params) { (returnedResponse: ConvenienceStoreAddRemoveProduct) in
            
            if returnedResponse.code != 0 {
                //Failure
                DispatchQueue.main.async {
                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                }
            } else {
                //Success
                DispatchQueue.main.async {
                    self.getEmployeeCartDetails(byEmployeeId: coWorkersEmployeeId)
                }
            }
            
        }
        
    }
    
}

//MARK: COMMON API SERVICE IMPLEMENTATION
extension IndividualCategoryCartVC {
    
    //MARK: SERVICE TO GET ALL CART ITEMS
    func getEmployeeCartDetails(byEmployeeId employeeId: Int) {
        
        productsArray.removeAll()
        
        CommonCartServices.instance.getAllCartList(byEmployeeId: employeeId) { (returnedResponse, error) in
            
            if error != nil {
                //Error
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        returnedResponse.results.forEach({ (data) in
                            if let matchedModuleId = data.data.enumerated().first(where: {$0.element.moduleID == self.categoryModuleId}) {
                                
                                self.productsArray = data.data[matchedModuleId.offset].productCartList
                                self.totalBookingAmountValue = data.allTotalamount
                                
                                DispatchQueue.main.async {
                                    self.mainContentView.isHidden = false
                                    self.emptyCartView.isHidden = true
                                    
                                    let index = data.data[matchedModuleId.offset]
                                    
                                    self.configureSummaryView(subTotalAmount: index.subtotal, totalAmount: index.totalamount, sGstAmount: index.totalSgstAmount ?? 0.0, cGSTAmount: index.totalCgstAmount ?? 0.0, actualAmount: index.actualAmount ?? 0)
                                    
                                }
                                
                            }else {
                                //No Module Id Found and Show Empty Cart
                                self.productsArray.removeAll()
                                DispatchQueue.main.async {
                                    self.mainContentView.isHidden = true
                                    self.emptyCartView.isHidden = false
                                }
                            }
                        })
                        //print(self.productsArray)
                        
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            DispatchQueue.main.async {
                self.cartProductTblView.reloadData()
            }
            
        }
        
    }
    
}


//MARK: ADDITIONAL FUNCTIONS

extension IndividualCategoryCartVC {
    
    // MARK: SET SUMMARY DETAILS IN VIEW
    
    private func configureSummaryView(subTotalAmount: Int, totalAmount: Int, sGstAmount: Double, cGSTAmount: Double, actualAmount: Int) {
        
        self.summaryView.layer.cornerRadius = 5
        self.summaryView.layer.borderWidth = 0.2
        self.summaryView.layer.borderColor = UIColor.black.cgColor
        
        self.subtotalAmountLbl.text = "\(rupee) \(subTotalAmount)"
        self.cgstAmountLbl.text = "\(rupee) \(cGSTAmount)"
        self.sgstAmountLbl.text = "\(rupee) \(sGstAmount)"
        self.grandTotalAmountLbl.text = "\(rupee) \(totalAmount)"
        
    }
    
    
}
