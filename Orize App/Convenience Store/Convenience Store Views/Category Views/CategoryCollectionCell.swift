//
//  FruitsCollectionCell.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 01/02/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol CategoryCollectionCellDelegate : class {
    
    func addProductBtnTapped(productData: ProductsListArr, productIndex: Int, productCount: Int)
    
    func RemoveProductBtnTapped(productData: ProductsListArr, productIndex: Int, productCount: Int)
}

class CategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet private weak var plusButton: UIButton!
    @IBOutlet private weak var minusButton: UIButton!
    @IBOutlet private weak var counterLabel: UILabel!
    @IBOutlet private weak var cellMainView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    
    //Delegate
    private weak var delegate: CategoryCollectionCellDelegate?
    private var productIndex: Int!
    private var productData: ProductsListArr!
    
    var counter = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellMainView.layer.cornerRadius = 5
        cellMainView.layer.borderColor = UIColor.black.cgColor
        cellMainView.layer.borderWidth = 0.3
    }
    
    func configureCategoryCell(productData: ProductsListArr, delegate: CategoryCollectionCellDelegate, productIndex: Int) {
        
        self.delegate = delegate
        self.productData = productData
        self.productIndex = productIndex
        
        self.productImage.sd_setImage(with: URL(string: productData.productPhoto))
        self.productNameLbl.text = productData.productName
        self.productPriceLbl.text = "Rs. \(productData.productPrice)"
        
        counter = 0
        counterLabel.text = "\(counter)"
    }
    
    @IBAction func addProductBtnTapped(_ sender: UIButton) {
        
        if counter < productData.productAllowedQtyInCart {
            counter += 1
            counterLabel.text = "\(counter)"
            delegate?.addProductBtnTapped(productData: self.productData, productIndex: self.productIndex, productCount: counter)
        }else {
            self.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
        }
        
        
        
    }
    
    @IBAction func removeProductBtnTapped(_ sender: UIButton) {
        
        if counter > 0 {
            counter -= 1
            counterLabel.text = "\(counter)"
            delegate?.addProductBtnTapped(productData: self.productData, productIndex: self.productIndex, productCount: counter)
        }else {
            self.makeToast("Lowest Count", duration: 1.0, position: .bottom)
        }
        
    }
    
}
