//
//  FindCarpoolTableCell.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 12/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol FindCarpoolTableCellDelegate {
    func requestCarpoolBtnTapped(_ tag: Int)
}

class FindCarpoolTableCell: UITableViewCell {
    
    @IBOutlet weak var locationNameLbl: UILabel!
    @IBOutlet weak var seatsCountlbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timingLbl: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    var delegate: FindCarpoolTableCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        innerView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
//    func configureCell(data: FindCarpoolDataModel) {
//        locationNameLbl.text = data.locationName
//        seatsCountlbl.text = data.seatsCount
//        dateLbl.text = data.date
//        timingLbl.text = data.time
//    }
    
    func configureCell(data: ListAllVehiclesResult) {
        
        if data.availableSeats > 1 {
            seatsCountlbl.text = "\(data.availableSeats) seats"
        }else {
            seatsCountlbl.text = "\(data.availableSeats) seat"
        }
        
        locationNameLbl.text = data.destination
        dateLbl.text = data.travelDateApp
        timingLbl.text = data.travelTimeApp
        
    }
    
    @IBAction func requestCarpoolBtnTapped(_ sender: UIButton) {
        delegate?.requestCarpoolBtnTapped(self.tag)
    }

}
