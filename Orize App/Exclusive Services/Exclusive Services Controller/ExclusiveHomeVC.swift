//
//  ExclusiveHomeVC.swift
//  Orize App
//
//  Created by Apple on 12/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class ExclusiveHomeVC: UIViewController {
    
    //MARK: IB-Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var eventsCollectionView: UICollectionView!
    @IBOutlet weak var servicesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        
        searchTextField.layer.borderWidth = 0.7
        searchTextField.layer.borderColor = UIColor.darkGray.cgColor
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        eventsCollectionView.delegate = self
        eventsCollectionView.dataSource = self
        eventsCollectionView.register(UINib(nibName: ExclusiveServicesXibs.EventCollectionCell, bundle: nil), forCellWithReuseIdentifier: ExclusiveServicesXibs.EventCollectionCell)
        
        servicesTableView.delegate = self
        servicesTableView.dataSource = self
        servicesTableView.register(UINib(nibName: ExclusiveServicesXibs.ExclusiveServicesTblCell, bundle: nil), forCellReuseIdentifier: ExclusiveServicesXibs.ExclusiveServicesTblCell)
        
        
    }
    
    @IBAction func searchBarBtnTapped(_ sender: UIButton) {
        print("Search Bar Button Tapped")
    }
    
}

//MARK: COLLECTION VIEW IMPLEMENTATION
extension ExclusiveHomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return eventArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExclusiveServicesXibs.EventCollectionCell, for: indexPath) as? EventCollectionCell else {return UICollectionViewCell()}
        
        cell.configureCell(delegate: self, eventDetail: eventArray[indexPath.item], selectedIndex: indexPath.item)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.bounds.width
        let collectionViewHeight = collectionView.bounds.height
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            return CGSize(width: (collectionViewWidth) / 2, height: collectionViewHeight)
            
        }else {
            
            return CGSize(width: (collectionViewWidth - 30), height: collectionViewHeight)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

//MARK: TABLE VIEW IMPLEMENTATION
extension ExclusiveHomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exclusiveServicesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExclusiveServicesXibs.ExclusiveServicesTblCell) as? ExclusiveServicesTblCell else {return UITableViewCell()}
        
        cell.configureCell(delegate: self, serviceDetail: exclusiveServicesArray[indexPath.row], selectedIndex: indexPath.row)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}



//MARK: TEXT FIELD DELEGATE IMPLEMENTATION - SEARCH TEXT FIELD
extension ExclusiveHomeVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        //SEARCH TEXT FIELD SEARCHING IMPLEMENTATION GOES HERE
        
    }
    
}

//MARK: EventCollectionCell Delegate Implementation
extension ExclusiveHomeVC: EventCollectionCellDelegate {
    
    
    func didTapEventImage(eventDetail: Event, at index: Int) {
        //Todo: Go to Details View Controller passing selected event
        //print("Event Details: \(eventDetail) & index: \(index)")
        
        guard let detailsVc = storyboard?.instantiateViewController(withIdentifier: "ExclusiveEventDetailsVC") as? ExclusiveEventDetailsVC else {return}
        detailsVc.selectedEvent = eventDetail
        navigationController?.pushViewController(detailsVc, animated: true)
    }
    
    
    func didTapJoinEventBtn(eventDetail: Event, at index: Int) {
        //Todo: This should navigate to a page where user can read about the selected event
        print("Event Details: \(eventDetail) & index: \(index)")
    }
    
}

//MARK: ExclusiveServicesTblCell Delegate Implementation
extension ExclusiveHomeVC: ExclusiveServicesTblCellDelegate {
    
    func didTapViewButton(selectedServiceDetail detail: ExclusiveServices, at index: Int) {
        //Todo - Go to exclusive service details screen
        //print("Selected Service Detail: \(detail) & selected Index: \(index)")
        
        guard let exclusiveDetailVc = storyboard?.instantiateViewController(withIdentifier: "ExclusiveServiceDetailVC") as? ExclusiveServiceDetailVC else {return}
        exclusiveDetailVc.exclusiveServiceDetail = detail
        self.navigationController?.pushViewController(exclusiveDetailVc, animated: true)
        
    }
    
}
