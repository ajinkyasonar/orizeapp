//
//  Login.swift
//  Orize App
//
//  Created by Aditya Infotech on 29/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let login = try? newJSONDecoder().decode(Login.self, from: jsonData)

import Foundation

struct Login: Codable {
    let code: Int
    let status, message: String
    let results: [LoginResult]
}

struct LoginResult: Codable {
    let coWorksEmployeeID: Int
    let code, fullName: String
    
    enum CodingKeys: String, CodingKey {
        case coWorksEmployeeID = "co_works_employee_id"
        case code
        case fullName = "full_name"
    }
}

