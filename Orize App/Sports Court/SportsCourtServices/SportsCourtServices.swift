//
//  SportsCourtServices.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class SportsCourtServices {
    static let instance = SportsCourtServices()
    
    
    //MARK: SERVICE TO GET SPORTS TYPE LIST
    func getSportsTypeListService(completion: @escaping(SportsTypeList?, Error?)->()) {
        
        guard let url = URL(string: GET_SPORTS_TYPE_LIST_URL) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedSportsTypeResponse = try decoder.decode(SportsTypeList.self, from: data)
                    completion(returnedSportsTypeResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
            }.resume()
        
    }
    
    //MARK: GET SPORTS COURT TIMINGS API SERVICE
    func getSportsTimings(withSportsTypeId typeId: Int, coWorkersEmployeeId: Int, completion:@escaping(SportsTimings?, Error?)->()) {
        
        guard let url = URL(string: GET_SPORTS_TIMINGS_URL) else {return}
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&sports_type_id=\(typeId)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, result, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedSportsTimingsResponse = try decoder.decode(SportsTimings.self, from: data)
                    completion(returnedSportsTimingsResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
        
    }
    
    //MARK: BOOK SPORTS BOOKING API SERVICE
    
    func bookSportsCourtService(coWorkersEmployeeId: Int, sportsTypeId: Int, bookingDate: String, sportsTimingsId: Int, isChallenge: Int, paymentGatewayResponse: String, paymentGateway: Int, bookingAmount: Int, secondBookDiscount: Int, completion:@escaping(SportsCourtBooking?, Error?) ->()) {
        
        guard let url = URL(string: SPORTS_COURT_BOOKING_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&sports_type_id=\(sportsTypeId)".data(using: String.Encoding.utf8)!)
        postData.append("&booking_date=\(bookingDate)".data(using: String.Encoding.utf8)!)
        postData.append("&sports_timings_id=\(sportsTimingsId)".data(using: String.Encoding.utf8)!)
        postData.append("&is_challenge=\(isChallenge)".data(using: String.Encoding.utf8)!)
        postData.append("&payment_gateway_response=\(paymentGatewayResponse)".data(using: String.Encoding.utf8)!)
        postData.append("&payment_gateway=\(paymentGateway)".data(using: String.Encoding.utf8)!)
        postData.append("&booking_amount=\(bookingAmount)".data(using: String.Encoding.utf8)!)
        postData.append("&second_book_discount=\(secondBookDiscount)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedBookingResponse = try decoder.decode(SportsCourtBooking.self, from: data)
                    completion(returnedBookingResponse, nil)
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
            }
            
            
            }.resume()
        
    }
    
    //MARK: REQUEST CHALLENGE API SERVICE
    
    func sportsCourtRequestChallengeService(sportsCourtBookingId: Int, coWorkersEmployeeId: Int, bookingDate: String, paymentGatewayResponse: String, paymentGateway: Int, bookingAmount: Int, secondBookDiscount: Int, completion:@escaping(SportsCourtChallenge?, Error?) ->()) {
        
        guard let url = URL(string: SPORTS_COURT_REQUEST_CHALLENGE_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "sports_court_booking_id=\(sportsCourtBookingId)".data(using: String.Encoding.utf8)!)
        postData.append("&co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&booking_date=\(bookingDate)".data(using: String.Encoding.utf8)!)
        postData.append("&payment_gateway_response=\(paymentGatewayResponse)".data(using: String.Encoding.utf8)!)
        postData.append("&payment_gateway=\(paymentGateway)".data(using: String.Encoding.utf8)!)
        postData.append("&booking_amount=\(bookingAmount)".data(using: String.Encoding.utf8)!)
        postData.append("&second_book_discount=\(secondBookDiscount)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedChallengeResponse = try decoder.decode(SportsCourtChallenge.self, from: data)
                    completion(returnedChallengeResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            
            }.resume()
        
    }
    
    //MARK: GET CHALLENG REQUEST LIST BY EMPLOYEE ID SERVICE API
    
    func getChallengeRequestList(coWorkersEmployeeId: Int, completion: @escaping(ChallengeRequestList?, Error?) ->()) {
        
        guard let url = URL(string: GET_CHALLENGE_REQUEST_LIST_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(ChallengeRequestList.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            
            }.resume()
        
    }
    
    //MARK: ACCEPT DECLINE & CANCEL CHALLENGE SERVICE API
    
    func acceptRejectAndCancelSportsCourtChallengeService(coWorkersEmployeeId: Int, sportsCourtChallengesId: Int, status: Int, completion: @escaping(ChallengeBookingStatus?, Error?) ->()) {
        
        guard let url = URL(string: ACCEPT_DECLINE_CANCEL_CHALLENGE_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&sports_court_challenges_id=\(sportsCourtChallengesId)".data(using: String.Encoding.utf8)!)
        postData.append("&status=\(status)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(ChallengeBookingStatus.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            
            }.resume()
        
    }
    
    
}
