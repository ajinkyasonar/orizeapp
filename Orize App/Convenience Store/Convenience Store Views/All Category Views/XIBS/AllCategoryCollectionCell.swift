//
//  AllCategoryCollectionCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol AllCategoryCollectionCellDelegate: class {
    func addButtonTapped(productDetails: ProductsListArr, productCount: Int, productIndexRow: Int, productIndexSection: Int)
    
    func removeButtonTapped(productDetails: ProductsListArr, productCount: Int, productIndexRow: Int, productIndexSection: Int)
}

class AllCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    @IBOutlet weak var counterLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        elementView.layer.cornerRadius = 5
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
    }
    
    //Delegate
    weak var delegate: AllCategoryCollectionCellDelegate?
    private var productDetail: ProductsListArr!
    private var productIndexRow: Int!
    private var productIndexSection: Int!
    
    var counter = 0
    
    
    func configureCell(productListArray: ProductsListArr, delegate: AllCategoryCollectionCellDelegate, productIndexRow: Int, productIndexSection: Int) {
        
        self.delegate = delegate
        self.productDetail = productListArray
        self.productIndexRow = productIndexRow
        self.productIndexSection = productIndexSection
        
        guard let imageUrl = URL(string: productListArray.productPhoto) else {return}
        productImage.sd_setImage(with: imageUrl)
        productNameLbl.text = productListArray.productName
        productPriceLbl.text = "Rs. \(productListArray.productPrice)"
        
        counter = 0
        counterLbl.text = "\(counter)"
        
        
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        guard let product = productDetail else {return}
        
        if counter < product.productAllowedQtyInCart {
            counter += 1
            counterLbl.text = "\(counter)"
            delegate?.addButtonTapped(productDetails: product, productCount: counter, productIndexRow: productIndexRow, productIndexSection: productIndexSection)
        }else {
            self.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
        }
        
    }
    
    @IBAction func removeBtnTapped(_ sender: UIButton) {
        
        guard let product = productDetail else {return}
        
        if counter > 0 {
            counter -= 1
            counterLbl.text = "\(counter)"
            delegate?.removeButtonTapped(productDetails: product, productCount: counter, productIndexRow: productIndexRow, productIndexSection: productIndexSection)
        }else {
            self.makeToast("Lowest Count", duration: 1.0, position: .bottom)
        }
        
    }

}
