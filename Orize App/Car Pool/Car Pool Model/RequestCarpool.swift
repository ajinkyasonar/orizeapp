//
//  RequestCarpool.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let requestCarpool = try? newJSONDecoder().decode(RequestCarpool.self, from: jsonData)

import Foundation

struct RequestCarpool: Codable {
    let code: Int
    let status, message: String
    let results: [RequestCarpoolResult]
}

struct RequestCarpoolResult: Codable {
    let carPoolVehicleRequestID: Int
    let driverName, vehicleNo, destination, travelDateApp: String
    let travelTimeApp, remarks: String
    
    enum CodingKeys: String, CodingKey {
        case carPoolVehicleRequestID = "car_pool_vehicle_request_id"
        case driverName = "driver_name"
        case vehicleNo = "vehicle_no"
        case destination
        case travelDateApp = "travel_date_app"
        case travelTimeApp = "travel_time_app"
        case remarks
    }
}

