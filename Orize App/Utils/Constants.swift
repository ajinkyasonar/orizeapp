//
//  LoginConstants.swift
//  Orize App
//
//  Created by Aditya Infotech on 17/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation
import UIKit

//MARK: SYMBOLS
let rupee = "\u{20B9}"

//Identify Device Type

enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad // iPad style UI
}

var USERNAME = ""
var PASSWORD = ""
var FIREBASE_TOKEN = ""


//Gateways Keys
struct GatewayKeys {
    static let RAZORPAY_KEY = "rzp_live_KvANnEUQVYMHwR"
    
    //"rzp_live_KvANnEUQVYMHwR" Live Key Razorpay
    //rzp_test_IoJQhB7I0uwStu
}

struct CCAvenueCredentials {
    
    static let MERCHANT_ID = "220393"
    static let ACCESS_CODE = "AVKU85GE62CG28UKGC"
    static let ACCOUNT_NAME = "ORIZE PROPERTY MANGEMENT PRIVATE LIMITED"
    static let RSA_URL = "http://13.234.40.117/dev/ccavenue/GetRSA.php"
    static let RESPONSE_HANDLER_URL = "http://13.234.40.117/dev/ccavenue/ccavResponseHandler.php"
    
}

//MARK: FONTS

struct Fonts {
    static let montserratRegular = "MontserratRegular"
    static let montserratSemiBold = "MontserratSemiBold"
    static let montserratLight = "MontserratLight"
}

//Auto Login Supported Functionality
var IS_REMEMBER_SELECTED = "isRememberedSelected"

//UserDefault Variables
var CO_WORKERS_EMPLOYEE_ID = "co_worker_employee_Id"
var Co_Worker_EMPLOYEE_NAME = "employeeName"


let BASE_URL = "http://13.234.40.117/dev/index.php/Webservice/"

//Prolif Server Base URl - "https://prolifiquetech.in/qhospitality/index.php/Webservice/"
//Amazon Server Base Url = "http://13.234.40.117/qhospitality/index.php/Webservice/"
//Development Server Url = "http://13.234.40.117/dev/index.php/Webservice/"

//Login URLS
let LOGIN_URL = BASE_URL + "employeelogin"
let GET_EMPLOYEE_DETAILS_URL = BASE_URL + "co_works_employee_details"

//Home Page URLS
let GET_BANNER_LIST_URL = BASE_URL + "get_banner_list"
let GET_HOMEPAGE_SERVICES_LIST_URL = BASE_URL + "get_services_list"
let GET_HOMEPAGE_FAVOURITES_LIST_URL = BASE_URL + "employee_favourites_modules"
let ADD_REMOVE_FAVOURITES_URL = BASE_URL + "add_remove_favourites_modules"

//Convinence Store URLS
let CONVENIENCE_STORE_CATEGORY_LIST_URL = BASE_URL + "get_store_category_list"
let CONVENIENCE_STORE_PRODUCT_LIST_URL = BASE_URL + "get_products_list_for_store"
let ADD_REMOVE_STORE_CART_PRODUCT_URL = BASE_URL + "add_remove_store_cart"

//LIST CARPOOL URLS
let LIST_YOUR_CAR_URL = BASE_URL + "add_vehicles"
let LIST_YOUR_CAR_UPDATE_URL = BASE_URL + "update_vehicles"
let LIST_YOUR_CAR_CANCEL_URL = BASE_URL + "remove_vehicles"

//FIND & REQUEST CARPOOL URLS
let FIND_CARPOOL_GET_ALL_VEHICLES_LIST_URL = BASE_URL + "get_all_vehicles_list"
let REQUEST_CARPOOL_URL = BASE_URL + "request_carpool"
let CANCEL_CARPOOL_URL = BASE_URL + "request_carpool_cancel"
let UPDATE_CARPOOL_URL = BASE_URL + "request_carpool_update"

//EMPLOYEE CARPOOL LISTING -> Your Carpool Listing
let EMPLOYEE_CARPOOL_LISTING_URL = BASE_URL + "vehicles_list"
let REMOVE_VEHICLE_URL = BASE_URL + "remove_vehicles"


//MARK: SPORTS COURT MODULE
let GET_SPORTS_TYPE_LIST_URL = BASE_URL + "get_sports_type_list"
let GET_SPORTS_TIMINGS_URL = BASE_URL + "get_sports_timings_sports_type_id"
let SPORTS_COURT_BOOKING_URL = BASE_URL + "sports_court_booking"
let SPORTS_COURT_REQUEST_CHALLENGE_URL = BASE_URL + "sports_court_challenge_request"
let GET_CHALLENGE_REQUEST_LIST_URL = BASE_URL + "get_challenge_request_list_by_emp"
let ACCEPT_DECLINE_CANCEL_CHALLENGE_URL = BASE_URL + "change_sports_court_challenge_status"

//MARK: EMPLOYEE BOOKINGS DETAILS MODULE
let GET_EMPLOYEE_BOOKING_LIST_URL = BASE_URL + "get_employee_bookings"
let GET_EMPLOYEE_BOOKINGS_BY_ORDER_ID = BASE_URL + "get_employee_bookings_by_order_id"
let CANCEL_CHANGE_EMPLOYEE_BOOKING_ALL_ORDER_URL = BASE_URL + "change_all_order_status"

//Individual Category Product Cancellation Urls -> Employee Bookings
let CHANGE_FOODCOURT_ORDER_PRODUCT_STATUS = BASE_URL + "change_food_court_order_product_status"
let CHANGE_STORE_ORDER_PRODUCT_STATUS = BASE_URL + "change_store_order_product_status"


//MARK: FOOD COURT MODULE
let GET_FOODCOURT_CATEGORIES_LIST_URL = BASE_URL + "get_food_court_category_list"
let GET_FOODCOURT_PRODUCT_LIST_URL = BASE_URL + "get_products_list_for_foodcourt"
let ADD_REMOVE_FOODCOURT_CART_ITEM_URL = BASE_URL + "add_remove_food_court_cart"
let GET_FOODCOURT_ORDER_STATUS_LIST_URL = BASE_URL + "get_food_court_order_status_list"
let FOODCOURT_TRACK_ORDER_URL = BASE_URL + "food_court_track_order"

//MARK: FOOD COURT -> MODIFICATIONS
let GET_FOODCOURT_VENDER_LIST_URL = BASE_URL + "get_food_court_vendor_list"
let GET_FOODCOURT_VENDER_PRODUCTS_URL = BASE_URL + "get_food_court_categoryname_list"

//MARK: LAUNDRY MODULE URLS
let GET_LAUNDRY_VENDERS_LIST_URL = BASE_URL + "get_laundry_vendor_list"
let GET_LAUNDRY_CATEGORIES_WITH_VENDER_ID = BASE_URL + "get_laundry_categoryname_list"
let Add_LAUNDRY_SERVICES_TO_CART = BASE_URL + "add_remove_laundry_service_cart"
let REMOVE_LAUNDRY_SERVICE_FROM_CART = BASE_URL + "remove_laundry_service_from_cart"
let UPDATE_LAUNDRY_PRODUCT_FROM_CART = BASE_URL + "update_laundry_product_cart"

//MARK: COMMON CART MODULE
let GET_MODULE_LIST_URL = BASE_URL + "get_module_list"
let GET_ALL_CART_LIST_URL = BASE_URL + "get_all_cart_list_by_employee_id"
let PLACE_ALL_ORDERS_URL = BASE_URL + "place_all_order"


//RazorPay Common Urls
let GENERATE_RAZORPAY_ORDER_ID_URL = BASE_URL + "razorpayorderid"


//Storyboards
let LOGIN_STORYBOARD = UIStoryboard(name: "Login", bundle: nil)
let CONVENIENCE_STORE_STORYBOARD = UIStoryboard(name: "ConvenienceStore", bundle: nil)
let HOMEPAGE_STORYBOARD = UIStoryboard(name: "HomePage", bundle: nil)
let SPORTS_COURT_STORYBOARD = UIStoryboard(name: "SportsCourt", bundle: nil)
let CAR_POOL_STORYBOARD = UIStoryboard(name: "Carpool", bundle: nil)
let COMMOM_ELEMENT_STORYBOARD = UIStoryboard(name: "CommonElement", bundle: nil)
let EMPLOYEE_BOOKINGS_STORYBOARD = UIStoryboard(name: "EmployeeBookings", bundle: nil)
let FOODCOURT_STORYBOARD = UIStoryboard(name: "FoodCourt", bundle: nil)
let COMMONCART_STORYBOARD = UIStoryboard(name: "CommonCart", bundle: nil)
let AMPHITHEATRE_STORYBOARD = UIStoryboard(name: "AmphiTheatre", bundle: nil)
let ACCESS_CONTROL_STORYBOARD = UIStoryboard(name: "AccessControl", bundle: nil)
let EXCLUSIVE_SERVICE_STORYBOARD = UIStoryboard(name: "Exclusive", bundle: nil)
let LAUNDRY_STORYBOARD = UIStoryboard(name: "Laundry", bundle: nil)
let HEALTH_SPA_STORYBOARD = UIStoryboard(name: "HealthSpa", bundle: nil)


//XIB REGISTRAATIONS
struct ConvinienceStoreRegisteredXibs {
    static let AllCategoryTblCell = "AllCategoryTblCell"
    static let AllCategoryCollectionCell = "AllCategoryCollectionCell"
    static let ConvenienceCartTblCell = "ConvenienceCartTblCell"
    static let AllConvenienceSearchTblCell = "AllConvenienceSearchTblCell"
    
}

struct FoodCourtRegisteredXibs {
    static let AllFoodCategoryTblCell = "AllFoodCategoryTblCell"
    static let AllFoodCategoryCollCell = "AllFoodCategoryCollCell"
    static let IndividualProductCatCell = "IndividualProductCatCell"
    static let FoodCartTblCell = "FoodCartTblCell"
    static let FoodCartAddonTblCell = "FoodCartAddonTblCell"
    static let Food_AllCategory_SearchTblCell = "Food_AllCategory_SearchTblCell"
    static let FoodCourtPrebookingCollCell = "FoodCourtPrebookingCollCell"
    static let FoodCourtOrderTrackingCell = "FoodCourtOrderTrackingCell"
}

struct HomePageRegisteredXibs {
    static let SelectedFavouriteCollCell = "SelectedFavouriteCollCell"
}

struct EmployeeBookingRegisteredXibs {
    static let EmployeeBookingsTblCell = "EmployeeBookingsTblCell"
    static let EmployeeBookingsDetailsTblCell = "EmployeeBookingsDetailsTblCell"
}

struct CommonCartRegisteredXibs {
    static let AllCartTblCell = "AllCartTblCell"
    static let AllCartTableHeaderCell = "AllCartTableHeaderCell"
    static let AllCartTableFooterCell = "AllCartTableFooterCell"
}

struct ExclusiveServicesXibs {
    static let EventCollectionCell = "EventCollectionCell"
    static let ExclusiveServicesTblCell = "ExclusiveServicesTblCell"
    static let ExclusiveServiceImagesCollCell = "ExclusiveServiceImagesCollCell"
    static let ExclusiveServiceAddonTblCell = "ExclusiveServiceAddonTblCell"
}

struct LaundryXibs {
    static let LaundryCategoryTblCell = "LaundryCategoryTblCell"
    static let LaundryCategoryCollCell = "LaundryCategoryCollCell"
    static let LaundryServiceTblCell = "LaundryServiceTblCell"
}

struct AmphiTheatreXibs {
    static let AmphiAllEventsTblCell = "AmphiAllEventsTblCell"
    static let AmphiCartEventTblCell = "AmphiCartEventTblCell"
    static let AmphiPassTblCell = "AmphiPassTblCell"
}
