//
//  EmployeeBookingsTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 17/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol EmployeeBookingsTblCellDelegate : class {
    func viewButtonTapped(bookingDetails: BookingDetails, tag: Int)
    func cancelButtonTapped(bookingDetails: BookingDetails, tag: Int)
}

class EmployeeBookingsTblCell: UITableViewCell {
    
    
    @IBOutlet private weak var outerView: UIView!
    @IBOutlet private weak var orderNumberLbl: UILabel!
    @IBOutlet private weak var dateLbl: UILabel!
    @IBOutlet private weak var timeLbl: UILabel!
    @IBOutlet private weak var totalAmountLbl: UILabel!
    @IBOutlet private weak var cancelButton: UIButton!
    
    weak var delegate: EmployeeBookingsTblCellDelegate?
    private var bookingDetail: BookingDetails!
    private var bookingDetailInt: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        outerView.layer.cornerRadius = 10
    }
    
    func configureCell(bookingDetails: BookingDetails, delegate: EmployeeBookingsTblCellDelegate, bookingDetailInt: Int) {
        
        self.delegate = delegate
        self.bookingDetail = bookingDetails
        self.bookingDetailInt = bookingDetailInt
        
        orderNumberLbl.text = "Order No - \(bookingDetails.bookingNo)"
        dateLbl.text = bookingDetails.orderDate
        timeLbl.text = bookingDetails.orderTime
        totalAmountLbl.text = "\(rupee) \(bookingDetails.price)"
        
        cancelButton.isHidden = bookingDetails.isCancellable == false ? true : false
    }
    
    @IBAction func viewBtnTapped(_ sender: UIButton) {
        delegate?.viewButtonTapped(bookingDetails: bookingDetail, tag: bookingDetailInt)
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        delegate?.cancelButtonTapped(bookingDetails: bookingDetail, tag: bookingDetailInt)
    }
    
}
