//
//  AccessControlVC.swift
//  Orize App
//
//  Created by Apple on 26/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class AccessControlVC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manageAccessControl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //manageAccessControl()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    private func manageAccessControl() {
        
        let application = UIApplication.shared
        let accessControlAppPath = "spectrableapp://"
        let accessControlApplicationUrl = URL(string: accessControlAppPath)
        
        let accessControlAppStoreUrl = URL(string: "https://apps.apple.com/in/app/spectra-access-card/id1375399735")
        
        guard let installedAppUrl = accessControlApplicationUrl else {return}
        guard let storeAppUrl = accessControlAppStoreUrl else {return}
        
        if application.canOpenURL(installedAppUrl) {
            
            application.open(installedAppUrl, options: [:], completionHandler: nil)
            
        }else {
            
            application.open(storeAppUrl)
        }
    }

}
