//
//  ExclusiveServices.swift
//  Orize App
//
//  Created by Apple on 12/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

struct ExclusiveServices {
    let serviceImages: [String]
    let serviceName: String
    let serviceDescription: String
    let servicePrice: Int
    let serviceTimings: [String]
}

var exclusiveServicesArray = [
    
    ExclusiveServices(serviceImages:
        
        ["http://13.234.40.117/qhospitality/images/exclusive/conference3-min.jpg",
         "http://13.234.40.117/qhospitality/images/exclusive/conference1-min.jpg",
         "http://13.234.40.117/qhospitality/images/exclusive/conference2-min.jpg",
         "http://13.234.40.117/qhospitality/images/exclusive/conference5-min.jpg"
        
        ], serviceName: "Executive Board Room", serviceDescription: "Book the exclusive board room for important meetings", servicePrice: 2000, serviceTimings: [
            
            "8.00 am - 9.00 am", "9.00 am - 10.00 am", "01.30 pm - 3.30 pm", "5.00 pm - 7.00 pm",
            "7.00 pm - 9.30 pm"
        ])
    
]

struct AddOnItems {
    let name: String
    let description: String
    let price: Int
}

var addOnItemsArray = [
    
    AddOnItems(name: "Mineral Water", description: "Premium quality packaged drinking water", price: 80),
    
    AddOnItems(name: "Veg Meal Box", description: "1 Dal, 3 Rotis, Steam Rice, Raita / Salad / Dessert", price: 199),
    
    AddOnItems(name: "Non Veg Meal Box", description: "Chicken Gravy, 3 Rotis, Steam Rice, Ratia / Salad / Dessert", price: 299)

]
