//
//  EmployeeDetails.swift
//  Orize App
//
//  Created by Aditya Infotech on 08/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//


//   let employeeDetails = try? newJSONDecoder().decode(EmployeeDetails.self, from: jsonData)

import Foundation

// MARK: - EmployeeDetails
struct EmployeeDetails: Codable {
    let code: Int
    let status, message: String
    let results: [Details]
}

// MARK: - Result
struct Details: Codable {
    let coWorksEmployeeID: Int
    let code, fullName, address: String
    let landlinePhone: String? = ""
    let mobileno, emailid: String
    let status: Int
    let statustext: String
    let itemsInCart: Int
    
    enum CodingKeys: String, CodingKey {
        case coWorksEmployeeID = "co_works_employee_id"
        case code
        case fullName = "full_name"
        case address
        case landlinePhone = "landline_phone"
        case mobileno, emailid, status, statustext
        case itemsInCart = "items_in_cart"
    }
}
