//
//  AmphiPassTblCell.swift
//  Orize App
//
//  Created by Apple on 13/09/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class AmphiPassTblCell: UITableViewCell {
    
    @IBOutlet weak var passTypeLbl: UILabel!
    @IBOutlet weak var passPriceLbl: UILabel!
    @IBOutlet weak var passQuantityCounterLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(with passDetails: PassDetails) {
        passTypeLbl.text = passDetails.passType
        passPriceLbl.text = "\(rupee) \(passDetails.passPrice)"
    }
    
}
