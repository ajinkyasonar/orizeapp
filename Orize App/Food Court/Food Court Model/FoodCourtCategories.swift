//
//  FoodCourtCategories.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let foodCourtCategories = try? newJSONDecoder().decode(FoodCourtCategories.self, from: jsonData)

import Foundation

// MARK: - FoodCourtCategories
struct FoodCourtCategories: Codable {
    let code: Int
    let status, message: String
    let results: [FoodCourtCategoriesDetails]
}

// MARK: - Result
struct FoodCourtCategoriesDetails: Codable {
    let foodCourtCategoryID: Int
    let name: String
    let description: String
    let isPreBookingCategory: Int
    
    enum CodingKeys: String, CodingKey {
        case foodCourtCategoryID = "food_court_category_id"
        case name
        case description = "description"
        case isPreBookingCategory = "is_prebooking_category"
    }
}

