//
//  ConvenienceStoreVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 31/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import CarbonKit

class ConvenienceStoreVC: UIViewController {
    
    //Outlets
    @IBOutlet private weak var logoAndRightButtonsView: UIView!
    @IBOutlet private weak var topCartBtn: UIButton!
    
    //Storage Variables
    var convenienceStoreCategoryListArray = [ConvenienceStoreResult]()
    
    //Variables
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var items = [String]()
    let employeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)

    override func viewDidLoad() {
        super.viewDidLoad()
        getConvenienceStoreCategoryList()
        
    }
    
    //MARK: ACTIONS HERE
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func goToCartBtnTapped(_ sender: UIButton) {
        //Go To Common Cart View Controller
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        self.navigationController?.pushViewController(commonCartVc, animated: true)
    }

}

//MARK: CARBON KIT IMPLEMENTATION
extension ConvenienceStoreVC: CarbonTabSwipeNavigationDelegate {
    
    func configureCarbonKit() {
        
        
        items = convenienceStoreCategoryListArray.map({$0.name})
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: logoAndRightButtonsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 50))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        
        
        //Appearance of Menu
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        carbonTabSwipeNavigation?.setNormalColor(UIColor.white)
        carbonTabSwipeNavigation?.setSelectedColor(UIColor.white)
        carbonTabSwipeNavigation?.setIndicatorColor(UIColor.white)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            carbonTabSwipeNavigation?.setTabExtraWidth(30)
            carbonTabSwipeNavigation?.setTabBarHeight(60)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
        }else {
            carbonTabSwipeNavigation?.setTabBarHeight(50)
            carbonTabSwipeNavigation?.setTabExtraWidth(20)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
        }
        
    }
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        let storeCategoryId = self.convenienceStoreCategoryListArray[index].storeCategoryID
        
        switch index {
        case 0:
            guard let allVc = storyboard?.instantiateViewController(withIdentifier: "AllVC") as? AllVC else {return UIViewController()}
            allVc.storeCategoryId = storeCategoryId
            allVc.delegate = self
            return allVc

        default:
            guard let categoriesVc = storyboard?.instantiateViewController(withIdentifier: "CategoriesVC") as? CategoriesVC else {return UIViewController()}
            categoriesVc.storeCategoryId = storeCategoryId
            categoriesVc.delegate = self
            return categoriesVc
        }
    }
    
}

//MARK: ALL VIEW CONTROLLER & INDIVIDUAL CATEGORY VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension ConvenienceStoreVC: AllVCDelegate, CategoriesVCDelegate {
    
    func sendCartCountToRootVC(cartCount: Int) {
        
        DispatchQueue.main.async {
            if cartCount != 0 {
                self.topCartBtn.setTitle("\(cartCount)", for: .normal)
            } else {
                self.topCartBtn.setTitle("", for: .normal)
            }
        }
        
    }
    
    //NAvigate To Individual Category on View All Btn Tap
    func passCategoryIndexToRootVC(categoryIndex: Int) {
        //Navigate to Individual Category
        carbonTabSwipeNavigation?.setCurrentTabIndex(UInt(categoryIndex), withAnimation: true)
    }
    
}




//MARK: API IMPLEMENTATIONS
extension ConvenienceStoreVC {
    
    func getConvenienceStoreCategoryList() {
        
        ConvenienceStoreService.instance.getConvenienceStoreCategoryList { (returnedResponse, error) in
            
            if error != nil {
                
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.convenienceStoreCategoryListArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.configureCarbonKit()
                            self.getEmployeeDetails(coWorkerEmployeeId: self.employeeId)
                        }
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Get EMPLOYEE DETAILS - This is To Get Total Cart Items with Employee Details
    func getEmployeeDetails(coWorkerEmployeeId: Int) {
        
        let params = ["co_works_employee_id": coWorkerEmployeeId]
        
        ServiceHelper.instance.GenericPostService(forUrl: GET_EMPLOYEE_DETAILS_URL, parameters: params) { (employeeDetails: EmployeeDetails) in
            
            if employeeDetails.code == 0 {
                //Send employee cart count to root view controller for cart display
                employeeDetails.results.forEach({ (detail) in
                    DispatchQueue.main.async {
                        if detail.itemsInCart != 0 {
                            self.topCartBtn.setTitle("\(detail.itemsInCart)", for: .normal)
                        } else {
                            self.topCartBtn.setTitle("", for: .normal)
                        }
                    }
                })
            }
            
        }
        
    }
    
}
