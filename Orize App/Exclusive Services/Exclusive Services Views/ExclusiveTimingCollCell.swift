//
//  ExclusiveTimingCollCell.swift
//  Orize App
//
//  Created by Apple on 14/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class ExclusiveTimingCollCell: UICollectionViewCell {
    
    @IBOutlet private weak var timingLabel: UILabel!
    @IBOutlet private weak var elementView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var isSelected: Bool {
        
        didSet {
            
            if self.isSelected {
                
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.timingLabel.textColor = UIColor.white
                self.elementView.backgroundColor = #colorLiteral(red: 0.01629221998, green: 0.3044524491, blue: 0.4406405687, alpha: 1)
            }
            else {
                
                self.transform = CGAffineTransform.identity
                self.timingLabel.textColor = UIColor.black
                self.elementView.backgroundColor = UIColor.white
            }
            
        }
        
    }
    
    func configureCell(eventDetail: ExclusiveServices, at index: Int) {
        self.timingLabel.text = eventDetail.serviceTimings[index]
    }
    
}
