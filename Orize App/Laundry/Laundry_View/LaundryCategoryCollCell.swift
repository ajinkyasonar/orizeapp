//
//  LaundryCategoryCollCell.swift
//  Orize App
//
//  Created by Apple on 20/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import Toast_Swift

protocol LaundryCategoryCollCellDelegate: class {

    func didTapAddBtn(at index: Int, productDetails: LaundryProducts, productQuantity: Int)

    func didTapRemoveBtn(at index: Int, productDetails: LaundryProducts, productQuantity: Int)

}

class LaundryCategoryCollCell: UICollectionViewCell {

    @IBOutlet weak var elementView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productQuantityLbl: UILabel!

    var counter = 0

    weak var delegate: LaundryCategoryCollCellDelegate?
    private var selectedProductDetail: LaundryProducts!
    private var selectedProductIndex: Int!

    override func awakeFromNib() {
        super.awakeFromNib()

        elementView.layer.cornerRadius = 5
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
    }

    func configureCell(productDetails: LaundryProducts, delegate: LaundryCategoryCollCellDelegate, selectedProductIndex: Int) {
        
        self.delegate = delegate
        self.selectedProductDetail = productDetails
        self.selectedProductIndex = selectedProductIndex


        guard let imageUrl = URL(string: productDetails.productPhoto) else {return}

        productImage.sd_setImage(with: imageUrl, completed: .none)
        productNameLbl.text = productDetails.productName

        counter = 0
        productQuantityLbl.text = "\(counter)"

    }


    @IBAction func addBtnTapped(_ sender: UIButton) {

        if counter < 10 {
            counter += 1
            productQuantityLbl.text = "\(counter)"
            delegate?.didTapAddBtn(at: selectedProductIndex, productDetails: selectedProductDetail, productQuantity: counter)
        } else {

            self.makeToast("Max Quantity Reached", duration: 1.0, position: .center)
        }

    }

    @IBAction func removeBtnTapped(_ sender: UIButton) {

        if counter > 0 {
            counter -= 1
            productQuantityLbl.text = "\(counter)"
            delegate?.didTapRemoveBtn(at: selectedProductIndex, productDetails: selectedProductDetail, productQuantity: counter)
        } else {

            self.makeToast("Lowest Quantity Reached", duration: 1.0, position: .center)
        }

    }

}
