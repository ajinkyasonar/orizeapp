//
//  FoodCourtCart.swift
//  Orize App
//
//  Created by Aditya Infotech on 29/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let foodCourtCart = try? newJSONDecoder().decode(FoodCourtCart.self, from: jsonData)

import Foundation

// MARK: - FoodCourtCart
struct AddRemoveFoodCourtCart: Codable {
    let code: Int
    let status, message: String
    let results: [FoodCourt_CartResult]
}

// MARK: - Result
struct FoodCourt_CartResult: Codable {
    let subtotal, tax, totalamount: Int
    let productCartList: [ProductCartList]
    
    enum CodingKeys: String, CodingKey {
        case subtotal, tax, totalamount
        case productCartList = "product_cart_list"
    }
}
