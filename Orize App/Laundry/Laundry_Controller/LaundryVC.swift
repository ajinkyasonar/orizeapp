//
//  LaundryVC.swift
//  Orize App
//
//  Created by Apple on 20/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import CarbonKit
import Toast_Swift
import SDWebImage

protocol LaundryVCDelegate: class {
    func reloadLaundryData()
}

class LaundryVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var logoAndRightButtonsView: UIView!
    @IBOutlet weak var cartButton: UIButton!
    
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var items = [String]()
    let coworkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    var venderList = [LaundryVender]()
    
    weak var delegate: LaundryVCDelegate?
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getEmployeeDetails(employeeId: coworkerEmployeeId)
        delegate?.reloadLaundryData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    private func setupInitialView() {
        getLaundryVenderList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCartCountNotification), name: .UPDATE_CART_COUNT_NOTIFICATION, object: nil)
        
    }
    
    @objc func updateCartCountNotification(notification: NSNotification) {
        getEmployeeDetails(employeeId: coworkerEmployeeId)
    }
    
    //MARK: IB-ACTIONS
    @IBAction func backBtnTapped(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cartBtnTapped(_ sender: UIButton) {
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        self.navigationController?.pushViewController(commonCartVc, animated: true)
    }
    
}

extension LaundryVC: CarbonTabSwipeNavigationDelegate {
    
    private func configureCarbonKit() {
        
        for allValues in self.venderList {
            items.append(allValues.name)
        }
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: logoAndRightButtonsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 50))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view , attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        
        
        //Appearance of Menu
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        carbonTabSwipeNavigation?.setNormalColor(UIColor.white)
        carbonTabSwipeNavigation?.setSelectedColor(UIColor.white)
        carbonTabSwipeNavigation?.setIndicatorColor(UIColor.white)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            carbonTabSwipeNavigation?.setTabExtraWidth(30)
            carbonTabSwipeNavigation?.setTabBarHeight(60)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
        }else {
            carbonTabSwipeNavigation?.setTabBarHeight(50)
            carbonTabSwipeNavigation?.setTabExtraWidth(20)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
        }
        
        
    }
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        
        guard let laundryVendervc = storyboard?.instantiateViewController(withIdentifier: "LaundryVenderVC") as? LaundryVenderVC else {return UIViewController()}
        
        self.delegate = laundryVendervc
        laundryVendervc.venderCategoryId = venderList[index].laundryVendorID
        
        return laundryVendervc
        
    }
    
    
}

//MARK: API IMPLEMENTATION
extension LaundryVC {
    
    func getLaundryVenderList() {
        
        self.view.makeToastActivity(.center)
        
        LaundryServices.instance.getLaundryVenderList { [unowned self] (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 0.2, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        self.venderList = returnedResponse.results
                        //print(returnedResponse.results)
                        
                        DispatchQueue.main.async {
                            self.configureCarbonKit()
                            self.view.hideToastActivity()
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                DispatchQueue.main.async {
                    debugPrint(error.localizedDescription)
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count
                                DispatchQueue.main.async {
                                    
                                    if detail.itemsInCart != 0 {
                                        self.cartButton.setTitle("\(detail.itemsInCart)", for: .normal)
                                    } else {
                                        self.cartButton.setTitle("", for: .normal)
                                    }
                                    
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
