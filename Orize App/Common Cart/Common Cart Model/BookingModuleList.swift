//
//  EmployeeBookingsModuleList.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let employeeBookingsModuleList = try? newJSONDecoder().decode(EmployeeBookingsModuleList.self, from: jsonData)

import Foundation

struct BookingModuleList: Codable {
    let code: Int
    let status, message: String
    let results: [ModuleList]
}

struct ModuleList: Codable {
    let moduleID: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case moduleID = "module_id"
        case name
    }
}

