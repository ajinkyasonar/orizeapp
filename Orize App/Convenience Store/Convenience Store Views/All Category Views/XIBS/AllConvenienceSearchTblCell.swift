//
//  AllConvenienceSearchTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 15/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class AllConvenienceSearchTblCell: UITableViewCell {
    
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productCategoryLbl: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(searchData: SearchAutocompleteDatum) {
        
        guard let url = URL(string: searchData.categoryPhoto) else {return}
        productImage.sd_setImage(with: url)
        productNameLbl.text = searchData.name
        productCategoryLbl.text = searchData.categoryName
        
    }
    
}
