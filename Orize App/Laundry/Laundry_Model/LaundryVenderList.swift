//
//  LaundryVenderList.swift
//  Orize App
//
//  Created by Apple on 15/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// MARK: - LaundryVenderList
struct LaundryVenderList: Codable {
    let code: Int
    let status, message: String
    let results: [LaundryVender]
}

// MARK: - Result
struct LaundryVender: Codable {
    let laundryVendorID: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case laundryVendorID = "laundry_vendor_id"
        case name
    }
}

