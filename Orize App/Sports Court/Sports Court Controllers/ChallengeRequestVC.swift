//
//  ChallengeRequestVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 11/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class ChallengeRequestVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var topCustomNavigationView: UIView!
    
    @IBOutlet weak var incomingBtn: UIButton!
    @IBOutlet weak var sentBtn: UIButton!
    
    @IBOutlet weak var challengeRequestTableView: UITableView!
    
    //Variables
    var challengeRequestArray = [ChallengeRequestListResult]()
    var incomingChallegesArray = [RequestDetails]()
    var sentChallengesArray = [RequestDetails]()
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //Flags
    var checkButtonTag = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    func initialSetup() {
        
        getChallengeList(coWorkerEmployeeId: coWorkerEmployeeId)
        topCustomNavigationView.layer.borderWidth = 0.4
        topCustomNavigationView.layer.borderColor = UIColor.black.cgColor
        
        defaultSelectIncomingButtonOnViewLoad()
        
        challengeRequestTableView.delegate = self
        challengeRequestTableView.dataSource = self
    }
    
    //MARK: ACTIONS PERFORMED HERE
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func incomingOrSentBtnTapped(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            
            incomingBtn.backgroundColor = UIColor.white
            incomingBtn.layer.borderWidth = 2
            incomingBtn.layer.borderColor = #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
            
            sentBtn.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.5)
            sentBtn.layer.borderWidth = 0
            sentBtn.layer.borderColor = UIColor.clear.cgColor
            
            checkButtonTag = sender.tag
            
            challengeRequestTableView.reloadData()
            
            break
            
        case 2:
            
            sentBtn.backgroundColor = UIColor.white
            sentBtn.layer.borderWidth = 2
            sentBtn.layer.borderColor = #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
            
            incomingBtn.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.5)
            incomingBtn.layer.borderWidth = 0
            incomingBtn.layer.borderColor = UIColor.clear.cgColor
            
            checkButtonTag = sender.tag
            
            challengeRequestTableView.reloadData()
            
            break
            
        default:
            return
            
        }
        
        
    }
    
}

//MARK: TABLE VIEW IMPLEMENTATION
extension ChallengeRequestVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return challengeRequestArray.count
        
        if checkButtonTag == 1 {
            return incomingChallegesArray.count
        } else {
            return sentChallengesArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch checkButtonTag {
        case 1:
            
            guard let cell = challengeRequestTableView.dequeueReusableCell(withIdentifier: "IncomingChallengeTblCell", for: indexPath) as? IncomingChallengeTblCell else {return UITableViewCell()}
            cell.configureCell(data: incomingChallegesArray[indexPath.row])
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
            
        default:
            
            guard let cell = challengeRequestTableView.dequeueReusableCell(withIdentifier: "SentChallengeTblCell", for: indexPath) as? SentChallengeTblCell else {return UITableViewCell()}
            cell.configureCell(data: sentChallengesArray[indexPath.row])
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}

//MARK: INCOMING CELL DELEGATE IMPLEMENTATIONS
extension ChallengeRequestVC: IncomingChallengeTblCellDelegate {
    
    //Continue here... (1-Cancel/Decline,2-Accept)
    
    func didTapAcceptButton(_ tag: Int) {
        
        let sportsCourtChallengesId = incomingChallegesArray[tag].sportsCourtChallengesID
        let acceptRequestStatus = 2
        //print(sportsCourtChallengesId)
        acceptDeclineCancel(coWorkersEmployeeId: coWorkerEmployeeId, sportsCourtChallengesId: sportsCourtChallengesId, status: acceptRequestStatus)
    }
    
    func didTapDeclineButton(_ tag: Int) {
        let sportsCourtChallengesId = incomingChallegesArray[tag].sportsCourtChallengesID
        let declineRequestStatus = 1
        //print(sportsCourtChallengesId)
        acceptDeclineCancel(coWorkersEmployeeId: coWorkerEmployeeId, sportsCourtChallengesId: sportsCourtChallengesId, status: declineRequestStatus)
    }
    
    
}

extension ChallengeRequestVC: SentChallengeTblCellDelegate {
    
    func didTapCancelChallengeRequestBtn(_ tag: Int) {
        let sportsCourtChallengesId = sentChallengesArray[tag].sportsCourtChallengesID
        let cancelRequestStatus = 1
        //print(sportsCourtChallengesId)
        acceptDeclineCancel(coWorkersEmployeeId: coWorkerEmployeeId, sportsCourtChallengesId: sportsCourtChallengesId, status: cancelRequestStatus)
    }
    
}



//MARK: ADDITIONAL METHODS AND TASKS
extension ChallengeRequestVC {
    
    func defaultSelectIncomingButtonOnViewLoad() {
        
        incomingBtn.tag = 1
        incomingBtn.backgroundColor = UIColor.white
        incomingBtn.layer.borderWidth = 2
        incomingBtn.layer.borderColor = #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
        
        sentBtn.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.5)
        sentBtn.layer.borderWidth = 0
        sentBtn.layer.borderColor = UIColor.clear.cgColor
        
        checkButtonTag = incomingBtn.tag
        
        challengeRequestTableView.reloadData()
        
    }
    
}

//MARK: API IMPLEMENTATION
extension ChallengeRequestVC {
    
    //MARK: GET CHALLENGE LIST SERVICE
    func getChallengeList(coWorkerEmployeeId: Int) {
        
        self.view.makeToastActivity(.center)
        
        SportsCourtServices.instance.getChallengeRequestList(coWorkersEmployeeId: coWorkerEmployeeId) { (returnedResonse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResonse = returnedResonse {
                    
                    if returnedResonse.code == 0 {
                        //Success
                        self.challengeRequestArray = returnedResonse.results
                        //print(returnedResonse.results)
                        
                        for values in self.challengeRequestArray {
                            self.incomingChallegesArray = values.incoming
                            self.sentChallengesArray = values.sent
                            
                            //print("Incoming Challenge Array: \(values.incoming)")
                            //print("Sent Challenge Array: \(values.sent)")
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.challengeRequestTableView.reloadData()
                        }
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
                
            }
            
        }
        
    }
    
    //MARK: ACCEPT, DECLINE & CANCEL CHALLENGE REQUEST - Status (1-Cancel/Decline,2-Accept)
    
    func acceptDeclineCancel(coWorkersEmployeeId: Int, sportsCourtChallengesId: Int, status: Int) {
        
        self.view.makeToastActivity(.center)
        
        SportsCourtServices.instance.acceptRejectAndCancelSportsCourtChallengeService(coWorkersEmployeeId: coWorkersEmployeeId, sportsCourtChallengesId: sportsCourtChallengesId, status: status) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
}
