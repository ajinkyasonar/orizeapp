//
//  PlaceAllOrders.swift
//  Orize App
//
//  Created by Aditya Infotech on 03/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

struct PlaceAllOrders: Codable {
    let code: Int
    let status, message: String
}
