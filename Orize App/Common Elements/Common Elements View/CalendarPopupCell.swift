//
//  CalendarPopupCell.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 18/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarPopupCell: JTAppleCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedView.isHidden = true
    }
    
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var selectedView: UIView!
    
}
