//
//  FoodOrderTrackingVC.swift
//  Orize App
//
//  Created by Apple on 24/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class FoodOrderTrackingVC: UIViewController {
    
    //OUTLETS
    
    @IBOutlet weak var orderTableView: UITableView!
    @IBOutlet weak var orderTableViewHeightConstraint: NSLayoutConstraint!
    
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var foodCourtOrderDetailId: Int!
    var orderDetails: OrderDetails!
    var orderStatusList = [OrderStatusList]()
    
    //Order Detail Outlets
    @IBOutlet weak var foodProductItemNameLbl: UILabel!
    @IBOutlet weak var productQuantityLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    var currentOrderId = Int()
    var currentOrderStatus = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        
    }
    
    private func setupInitialView() {
        foodCourtTrackOrder(coWorkerEmployeeId: coWorkerEmployeeId, foodCourtOrderDetailId: foodCourtOrderDetailId)
        
        orderTableView.delegate = self
        orderTableView.dataSource = self
        orderTableView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCourtOrderTrackingCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.FoodCourtOrderTrackingCell)
        orderTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        foodProductItemNameLbl.text = orderDetails.name
        productQuantityLbl.text = "x \(orderDetails.quantity ?? -1)"
        productPriceLbl.text = "\(rupee) \(orderDetails.price)"
        
        guard let quantity = orderDetails.quantity else {return}
        let totalPrice = orderDetails.price * quantity
        totalAmountLbl.text = "\(rupee) \(totalPrice)"
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        orderTableView.layer.removeAllAnimations()
        orderTableViewHeightConstraint.constant = orderTableView.contentSize.height
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        
    }
    
    //MARK: IB-ACTIONS IMPLEMENTATION
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}

//MARK: TABLE VIEW IMPLEMENTATION
extension FoodOrderTrackingVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderStatusList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FoodCourtRegisteredXibs.FoodCourtOrderTrackingCell) as? FoodCourtOrderTrackingCell else {return UITableViewCell()}
        cell.configureCell(orderStatusList: orderStatusList[indexPath.row], currentOrderStatusId: currentOrderId, indexpathCount: indexPath.row)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}


//MARK: API SERVICE IMPLEMENTATIONS
extension FoodOrderTrackingVC {
    
    func getFoodCourtOrderStatus() {
        
        FoodCourtServices.instance.getFoodCourtOrderStatusList { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.orderStatusList = returnedResponse.results
                        DispatchQueue.main.async {
                            self.orderTableView.reloadData()
                        }
                    }else {
                        //Failure
                        debugPrint(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
    }
    
    func foodCourtTrackOrder(coWorkerEmployeeId: Int, foodCourtOrderDetailId: Int) {
        
        FoodCourtServices.instance.foodCourtTrackOrder(coWorkerEmployeeId: coWorkerEmployeeId, foodCourtOrderDetailsId: foodCourtOrderDetailId) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        returnedResponse.results.forEach({ (detail) in
                            self.currentOrderId = detail.id
                            self.currentOrderStatus = detail.status
                        })
                        DispatchQueue.main.async {
                            self.getFoodCourtOrderStatus()
                        }
                    }else {
                        //Failure
                        debugPrint(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
            
        }
    }
    
}
