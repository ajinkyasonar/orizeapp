//
//  CustomSearchBarTextField.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 31/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class CustomSearchbarTextField: UITextField {
    
    private var padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    func setupView() {
        let placeholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray.cgColor])
        self.attributedPlaceholder = placeholder
    }
    
}
