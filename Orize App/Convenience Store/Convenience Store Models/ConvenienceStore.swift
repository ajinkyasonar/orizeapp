//
//  ConvenienceStore.swift
//  Orize App
//
//  Created by Aditya Infotech on 29/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let convenienceStore = try? newJSONDecoder().decode(ConvenienceStore.self, from: jsonData)

import Foundation

struct ConvenienceStore: Codable {
    let code: Int
    let status, message: String
    let results: [ConvenienceStoreResult]
}

struct ConvenienceStoreResult: Codable {
    let storeCategoryID: Int
    let name, description: String
    
    enum CodingKeys: String, CodingKey {
        case storeCategoryID = "store_category_id"
        case name, description
    }
}


