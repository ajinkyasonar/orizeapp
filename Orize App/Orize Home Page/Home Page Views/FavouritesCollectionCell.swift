//
//  FavouritesCollectionCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 06/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class FavouritesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var favouritesImage: UIImageView!
    @IBOutlet weak var favouritesTitleLbl: UILabel!
    
    func configureCell(favouriteService: FavouriteService) {
        guard let imageurl = URL(string: favouriteService.photo) else {return}
        favouritesImage.sd_setImage(with: imageurl)
        favouritesTitleLbl.text = favouriteService.name
    }
    
}
