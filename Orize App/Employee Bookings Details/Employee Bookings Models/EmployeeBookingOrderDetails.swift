//
//  EmployeeBookingOrderDetails.swift
//  Orize App
//
//  Created by Aditya Infotech on 19/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let employeeBookingOrderDetails = try? newJSONDecoder().decode(EmployeeBookingOrderDetails.self, from: jsonData)

import Foundation

// MARK: - EmployeeBookingOrderDetails
struct EmployeeBookingOrderDetails: Codable {
    let code: Int
    let status, message: String
    let results: [OrderDetailsResult]
}

// MARK: - Result
struct OrderDetailsResult: Codable {
    let moduleName: String
    let moduleId: Int?
    let data: [OrderDetails]
    
    enum CodingKeys: String, CodingKey {
        case moduleName = "module_name"
        case moduleId = "module_id"
        case data
    }
}

// MARK: - Datum
struct OrderDetails: Codable {
    let id: Int
    let orderMasterId: Int?
    let foodCourtOrderDetailId: Int?
    let bookingNo, orderDate, name: String
    let price: Int
    let quantity: Int?
    let productId: Int?
    let createdDate: String
    let type: BookingType
    let isCancellable: Bool
    let photo: String
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderMasterId = "order_master_id"
        case foodCourtOrderDetailId = "food_court_order_details_id"
        case bookingNo = "booking_no"
        case orderDate = "order_date"
        case name, price
        case createdDate = "created_date"
        case type
        case isCancellable = "is_cancellable"
        case photo = "photo"
        case quantity = "qty"
        case productId = "product_id"
        case description = "description"
    }
    
    
}

enum BookingType: String, Codable {
    case SportsBooking = "sportsbooking"
    case FoodCourt = "foodcourt"
    case ConvenienceStore = "store"
}
