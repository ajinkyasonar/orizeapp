//
//  FoodCourtOrderTrackingCell.swift
//  Orize App
//
//  Created by Apple on 24/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class FoodCourtOrderTrackingCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var circleView: UIView!
    @IBOutlet private weak var lineView: UIView!
    @IBOutlet private weak var orderStatusLbl: UILabel!
    @IBOutlet private weak var trackArrowImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(orderStatusList: OrderStatusList, currentOrderStatusId: Int, indexpathCount: Int) {
        orderStatusLbl.text = orderStatusList.name
        lineView.isHidden = orderStatusList.id == 5 ? true : false
        
        if (orderStatusList.id <= currentOrderStatusId) {
            circleView.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
            lineView.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
            
            //Hides Arrow on Last Status else it keep unhidden
            if orderStatusList.id == 5 {
                trackArrowImage.isHidden = true
            }else {
              trackArrowImage.isHidden = false
            }
            
        } else {
            
            circleView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            lineView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            trackArrowImage.isHidden = true
        }
    }
    
}
