//
//  LaundryProductBooking.swift
//  Orize App
//
//  Created by Apple on 29/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//
import Foundation

// MARK: - LaundryProductBooking
struct LaundryProductBooking: Codable {
    let code: Int
    let status, message: String
    let results: [LaundryProductBookingResult]
}

// MARK: - Result
struct LaundryProductBookingResult: Codable {
    let subtotal: Int
    let tax: Double
    let totalamount: Int
    let bookedLaundryProductList: [BookedLaundryProduct]
    
    enum CodingKeys: String, CodingKey {
        case subtotal, tax, totalamount
        case bookedLaundryProductList = "product_cart_list"
    }
}

// MARK: - ProductCartList
struct BookedLaundryProduct: Codable {
    let productID: Int
    let productName: String
    let productPhoto: String
    let productDescription: String
    let productQty, productAllowedQtyInCart, productPrice: Int
    let laundryService: [LaundryServiceList]
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case productName = "product_name"
        case productPhoto = "product_photo"
        case productDescription = "product_description"
        case productQty = "product_qty"
        case productAllowedQtyInCart = "product_allowed_qty_in_cart"
        case productPrice = "product_price"
        case laundryService = "laundry_service"
    }
}

