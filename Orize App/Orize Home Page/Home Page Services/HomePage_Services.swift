//
//  HomePage_Services.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class HomePage_Services {
    static let instance = HomePage_Services()
    
    //MARK: Banner List API Service
    func getBannerListService(completion: @escaping(HomePageBanner?, Error?) ->()) {
        
        guard let url = URL(string: GET_BANNER_LIST_URL) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                do{
                    let returnedResponse = try decoder.decode(HomePageBanner.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
    }
    
    //MARK: GET SERVICES LIST
    func getHomepageServices(withCoworkerId id: Int, completion:@escaping(HomePageServicesList?, Error?) ->()) {
        
        guard let url = URL(string: GET_HOMEPAGE_SERVICES_LIST_URL) else {return}
        let postData = NSMutableData(data: "co_works_employee_id=\(id)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 20)
        urlRequest.httpBody = postData as Data
        urlRequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(HomePageServicesList.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
        }
        task.resume()
        
    }
    
    //MARK: GET FAVOURITES LIST SERVICE
    func getHomepageFavouritesService(withCoworkerId id: Int, completion:@escaping(HomePageFavouriteList?, Error?) ->()) {
        
        guard let url = URL(string: GET_HOMEPAGE_FAVOURITES_LIST_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(id)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpBody = postData as Data
        urlRequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(HomePageFavouriteList.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
        
    }
    
    //MARK: ADD REMOVE FAVOURITES SERVICES
    func addRemoveFavourites(coworkerId: Int, moduleIds: String, completion:@escaping(AddRemoveFavourites?, Error?) ->()) {
        
        guard let url = URL(string: ADD_REMOVE_FAVOURITES_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coworkerId)".data(using: String.Encoding.utf8)!)
        postData.append("&module_id=\(moduleIds)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpBody = postData as Data
        urlRequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(AddRemoveFavourites.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
        
    }
    
}
