//
//  ExclusiveServiceAddonTblCell.swift
//  Orize App
//
//  Created by Apple on 16/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol ExclusiveServiceAddonTblCellDelegate: class {
    
    func didTapAddButton(addOnItem item: AddOnItems, at index: Int, itemCount: Int)
    
    func didTapRemoveButton(addOnItem item: AddOnItems, at index: Int, itemCount: Int)
}

class ExclusiveServiceAddonTblCell: UITableViewCell {
    
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productDescriptionLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    @IBOutlet private weak var productQuantityLbl: UILabel!
    @IBOutlet private weak var addProductBtn: UIButton!
    @IBOutlet private weak var removeProductBtn: UIButton!
    
    private weak var delegate: ExclusiveServiceAddonTblCellDelegate?
    private var selectedAddonItem: AddOnItems!
    private var selectedAddonItemIndex: Int!
    
    var counter = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(delegate: ExclusiveServiceAddonTblCellDelegate, addonDetail: AddOnItems, at index: Int) {
        
        self.delegate = delegate
        self.selectedAddonItem = addonDetail
        self.selectedAddonItemIndex = index
        
        productNameLbl.text = addonDetail.name
        productDescriptionLbl.text = addonDetail.description
        productPriceLbl.text = "\(rupee) \(addonDetail.price)"
        counter = 0
        productQuantityLbl.text = "\(counter)"
        
    }
    
    //TODO: ADD PRODUCT ACTION
    @IBAction func addProductBtnTapped(_ sender: UIButton) {
        print("Add Product Button Tapped")
        
        if counter >= 10 {
            
            self.makeToast("Maximum quantity of 10 reached", duration: 1.0, position: .center)
        }
        else {
            counter += 1
            productQuantityLbl.text = "\(counter)"
            delegate?.didTapAddButton(addOnItem: selectedAddonItem, at: selectedAddonItemIndex, itemCount: counter)
        }
        
    }
    
    //TODO: REMOVE PRODUCT ACTION
    @IBAction func removeProductBtnTapped(_ sender: UIButton) {
        
        if counter > 0 {
            
            counter -= 1
            productQuantityLbl.text = "\(counter)"
            delegate?.didTapRemoveButton(addOnItem: selectedAddonItem, at: selectedAddonItemIndex, itemCount: counter)
        }
        else {
            
            self.makeToast("Product quantity is already lowest", duration: 1.0, position: .center)
        }
        
        print("Remove Product Button Tapped")
    }
    
}
