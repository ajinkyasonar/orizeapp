//
//  Homepage_Banner.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let homePageBanner = try? newJSONDecoder().decode(HomePageBanner.self, from: jsonData)

import Foundation

// MARK: - HomePageBanner
struct HomePageBanner: Codable {
    let code: Int
    let status, message: String
    let results: [Banner]
}

// MARK: - Result
struct Banner: Codable {
    let bannerID: Int
    let filenameurl: String
    
    enum CodingKeys: String, CodingKey {
        case bannerID = "banner_id"
        case filenameurl
    }
}
