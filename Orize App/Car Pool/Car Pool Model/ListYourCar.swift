//
//  ListYourCarModel.swift
//  Orize App
//
//  Created by Aditya Infotech on 19/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//


// To parse the JSON, add this file to your project and do:
//
//   let listYourCar = try? newJSONDecoder().decode(ListYourCar.self, from: jsonData)

import Foundation

struct ListYourCar: Codable {
    let code: Int
    let status, message: String
    let results: [ListYourCarResult]
}

struct ListYourCarResult: Codable {
    let carPoolVehicleTravelID: Int
    let driverName, vehicleNo, destination, travelDateApp: String
    let travelDateOriginal, travelTimeApp, travelTimeOriginal: String
    let numberOfSeats: Int
    let isSaveAllowed: Int
    let remarks, parkingLevel: String
    
    enum CodingKeys: String, CodingKey {
        case carPoolVehicleTravelID = "car_pool_vehicle_travel_id"
        case driverName = "driver_name"
        case vehicleNo = "vehicle_no"
        case destination
        case travelDateApp = "travel_date_app"
        case travelDateOriginal = "travel_date_original"
        case travelTimeApp = "travel_time_app"
        case travelTimeOriginal = "travel_time_original"
        case numberOfSeats = "number_of_seats"
        case isSaveAllowed = "is_save_allowed"
        case parkingLevel = "parking_level"
        case remarks
    }
}
