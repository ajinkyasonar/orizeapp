//
//  AmphiAllEventsTblCell.swift
//  Orize App
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class AmphiAllEventsTblCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventMessageLbl: UILabel!
    @IBOutlet weak var playingSlotLbl: UILabel!
    @IBOutlet weak var eventDescriptionLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        eventImage.layer.cornerRadius = 10
    }
    
    func configureCell(event: AmphiEvent) {
        
        guard let eventImageUrl = URL(string: event.eventImage) else {return}
        eventImage.sd_setImage(with: eventImageUrl, completed: .none)
        
        eventNameLbl.text = event.eventName
        eventMessageLbl.text = event.eventMessage
        playingSlotLbl.text = event.playingSlot
        eventDescriptionLbl.text = event.eventDescription
        
    }
    
}
