//
//  EmployeeCarpoolListingCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 22/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol EmployeeCarpoolListingCellDelegate {
    
    func deleteButtonTapped(_ tag: Int)
    func editButtonTapped(_ tag: Int)
    
}

class EmployeeCarpoolListingCell: UITableViewCell {
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var vehicleNumberLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    var delegate: EmployeeCarpoolListingCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        innerView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func configureCell(data: EmployeeCarpoolListingResult) {
        
        destinationLbl.text = data.destination
        vehicleNumberLbl.text = data.vehicleNo
        dateLbl.text = data.travelDateApp
        timeLbl.text = data.travelTimeApp
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        delegate?.deleteButtonTapped(self.tag)
    }
    
    @IBAction func editDetailsButtonTapped(_ sender: UIButton) {
        delegate?.editButtonTapped(self.tag)
    }

}
