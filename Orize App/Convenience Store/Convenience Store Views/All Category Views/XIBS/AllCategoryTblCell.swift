//
//  AllCategoryTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol AllCategoryTblCellDelegate: class {
    
    func viewAllBtnTapped(_ atIndex: Int)
}

class AllCategoryTblCell: UITableViewCell {
    
    @IBOutlet private weak var staticCategoryNameView: UIView!
    @IBOutlet private weak var categoryNameLbl: UILabel!
    @IBOutlet private weak var productCollectionView: UICollectionView!
    
    private weak var delegate: AllCategoryTblCellDelegate?
    private var productCategoryIndex: Int!
    private var productData: ProductDatum!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(productData: ProductDatum, delegate: AllCategoryTblCellDelegate, atIndex: Int) {
        
        self.delegate = delegate
        self.productData = productData
        self.productCategoryIndex = atIndex
        
        categoryNameLbl.text = productData.storeCategoryName
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        
        productCollectionView.register(UINib(nibName: ConvinienceStoreRegisteredXibs.AllCategoryCollectionCell, bundle: nil), forCellWithReuseIdentifier: ConvinienceStoreRegisteredXibs.AllCategoryCollectionCell)
        
        productCollectionView.delegate = dataSourceDelegate
        productCollectionView.dataSource = dataSourceDelegate
        productCollectionView.tag = row
        productCollectionView.reloadData()
    }
    
    @IBAction func viewAllBtnTapped(_ sender: UIButton) {
        delegate?.viewAllBtnTapped(productCollectionView.tag)
    }
    
}
