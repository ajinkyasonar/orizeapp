//
//  LaundryProductsVC.swift
//  Orize App
//
//  Created by Apple on 21/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class LaundryVenderVC: UIViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var venderTableView: UITableView!
    
    //Received Variables
    var venderCategoryId: Int!
    
    var laundryCategoryListArr = [LaundryCategoryData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    func setupInitialUI() {
        getVenderProductsWith(venderId: venderCategoryId)
        
        venderTableView.delegate = self
        venderTableView.dataSource = self
        venderTableView.register(UINib(nibName: LaundryXibs.LaundryCategoryTblCell, bundle: nil), forCellReuseIdentifier: LaundryXibs.LaundryCategoryTblCell)
        
    }
    
    //MARK: IB-ACTION
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK: TABLE VIIEW IMPLEMENTATION
extension LaundryVenderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laundryCategoryListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LaundryCategoryTblCell") as? LaundryCategoryTblCell else {return UITableViewCell()}
        
        cell.configureCell(categoryData: laundryCategoryListArr[indexPath.row], delegate: self, selectedCategoryIndex: indexPath.row)
        
        cell.setupCollectionViewDataSoruceDelegate(dataSoruceDelegate: self, forRow: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
}

//MARK: LAUNDRY CATEGORY TABLE CELL DELEGATE IMPLEMENTATION
extension LaundryVenderVC: LaundryCategoryTblCellDelegate {
    
    func didTapViewAllButton(at index: Int, categoryData: LaundryCategoryData) {
        //Navigate to All Laundru Products View Controller
        guard let laundryProductsvc = storyboard?.instantiateViewController(withIdentifier: "LaundryProductsVC") as? LaundryProductsVC else {return}
        laundryProductsvc.laundryCategoryData = categoryData
        navigationController?.pushViewController(laundryProductsvc, animated: true)
        
        //print(categoryData.laundryCategoryName)
    }
    
}

//MARK: LAUNDRY PRODUCT COLLECTION VIEW IMPLEMENTATION
extension LaundryVenderVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return laundryCategoryListArr[collectionView.tag].productsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LaundryCategoryCollCell", for: indexPath) as? LaundryCategoryCollCell else {return UICollectionViewCell()}
        
        let products = laundryCategoryListArr[collectionView.tag].productsListArr[indexPath.item]
        
        cell.configureCell(productDetails: products, delegate: self, selectedProductIndex: indexPath.item)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let cellWidth = (width - 10) / 2
        let cellHeight = cellWidth * 1.2
        return CGSize(width: cellWidth, height: cellHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedProduct = laundryCategoryListArr[collectionView.tag].productsListArr[indexPath.item]
        guard let productSelectionvc = storyboard?.instantiateViewController(withIdentifier: "LaundryProductSelectionVC") as? LaundryProductSelectionVC else {return}
        productSelectionvc.laundryProduct = selectedProduct
        productSelectionvc.modalPresentationStyle = .overCurrentContext
        productSelectionvc.modalTransitionStyle = .crossDissolve
        navigationController?.present(productSelectionvc, animated: true)
    }
    
}

//MARK: Laundry Product Category Collection Cell Delegate Implementation
extension LaundryVenderVC: LaundryCategoryCollCellDelegate {
    
    func didTapAddBtn(at index: Int, productDetails: LaundryProducts, productQuantity: Int) {
        print(productDetails)
    }
    
    func didTapRemoveBtn(at index: Int, productDetails: LaundryProducts, productQuantity: Int) {
        print(productDetails)
    }
    
}

//MARK: RELOAD DATA WITH HELP OF CARBON KIT ROOT CONTROLLE DELEGATE
extension LaundryVenderVC: LaundryVCDelegate {
    
    func reloadLaundryData() {
        getVenderProductsWith(venderId: venderCategoryId)
        venderTableView.reloadData()
    }
    
}



//MARK: API SERVICES IMPLEMENTATION
extension LaundryVenderVC {
    
    func getVenderProductsWith(venderId id: Int) {
        
        view.makeToastActivity(.center)
        
        LaundryServices.instance.getVenderProductsWith(venderId: id) { [unowned self] (returnedResponse, error) in
            
            if error != nil {
                
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            }
            else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        self.laundryCategoryListArr.removeAll()
                        //Success
                        
                        for data in returnedResponse.results {
                            self.laundryCategoryListArr = data.productData
                        }
                        
                        DispatchQueue.main.async {
                            self.venderTableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
