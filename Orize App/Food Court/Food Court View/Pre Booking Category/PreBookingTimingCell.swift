//
//  PreBookingTimingCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 10/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class PreBookingTimingCell: UICollectionViewCell {
    
    @IBOutlet private weak var timingLabel: UILabel!
    @IBOutlet private weak var elementView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override var isSelected: Bool {
        
        didSet {
            
            if self.isSelected
            {
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                //self.contentView.backgroundColor = UIColor.red
                self.timingLabel.textColor = UIColor.white
                self.elementView.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
                
            }
            else
            {
                self.transform = CGAffineTransform.identity
                //self.contentView.backgroundColor = UIColor.gray
                self.timingLabel.textColor = UIColor.black
                self.elementView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
            }

        }
        
    }
    
    func configureCell(deliveryTimings: FoodCourtDeliveryTimeList) {
        
        timingLabel.text = deliveryTimings.deliveryTime
    }
    
}
