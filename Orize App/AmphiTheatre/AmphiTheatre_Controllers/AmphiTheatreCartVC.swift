//
//  AmphiTheatreCartVC.swift
//  Orize App
//
//  Created by Apple on 13/09/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class AmphiTheatreCartVC: UIViewController {
    
    //IB_Outlets
    @IBOutlet weak var selectedEventsTblView: UITableView!
    @IBOutlet weak var selecteEventTblViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addOnItemsTblView: UITableView!
    @IBOutlet weak var addOnItemsTblViewHeightConstraint: NSLayoutConstraint!
    
    var selectedAmphiEvent: AmphiEvent!
    var passTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    fileprivate func initialSetup() {
        
        //Selected Event Table View
        selectedEventsTblView.delegate = self
        selectedEventsTblView.dataSource = self
        selectedEventsTblView.register(UINib(nibName: AmphiTheatreXibs.AmphiCartEventTblCell, bundle: nil), forCellReuseIdentifier: AmphiTheatreXibs.AmphiCartEventTblCell)
        selectedEventsTblView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        //Add On Table View
        addOnItemsTblView.delegate = self
        addOnItemsTblView.dataSource = self
        addOnItemsTblView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCartAddonTblCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.FoodCartAddonTblCell)
        addOnItemsTblView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        
    }
    
    //MARK: IB-ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Automatic Table View Height Management
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        selectedEventsTblView.layer.removeAllAnimations()
        selecteEventTblViewHeightConstraint.constant = selectedEventsTblView.contentSize.height
        addOnItemsTblView.layer.removeAllAnimations()
        addOnItemsTblViewHeightConstraint.constant = addOnItemsTblView.contentSize.height
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }

}

// MARK: Table View Delegate & Data Source Implementation
extension AmphiTheatreCartVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AmphiTheatreXibs.AmphiCartEventTblCell) as? AmphiCartEventTblCell else {return UITableViewCell()}
        
        guard let selectedAmphiEvent = selectedAmphiEvent else {return UITableViewCell()}
        
        cell.configureCell(with: selectedAmphiEvent)
        
        cell.passArray = selectedAmphiEvent.passDetails
            
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
