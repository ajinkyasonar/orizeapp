//
//  LaundryVenderCategory.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 20/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let laundryCategories = try? newJSONDecoder().decode(LaundryCategories.self, from: jsonData)

import Foundation

// MARK: - LaundryCategories
struct LaundryCategories: Codable {
    let code: Int
    let status, message: String
    let results: [LaundryResult]
}

// MARK: - Result
struct LaundryResult: Codable {
    let vendorsName: String
    let productData: [LaundryCategoryData]
    
    enum CodingKeys: String, CodingKey {
        case vendorsName = "vendors_name"
        case productData = "product_data"
    }
}

// MARK: - ProductDatum
struct LaundryCategoryData: Codable {
    let laundryCategoryID: Int
    let laundryCategoryName: String
    let productsListArr: [LaundryProducts]
    
    enum CodingKeys: String, CodingKey {
        case laundryCategoryID = "laundry_category_id"
        case laundryCategoryName = "laundry_category_name"
        case productsListArr = "products_list_arr"
    }
}

// MARK: - ProductsListArr
struct LaundryProducts: Codable {
    let productID: Int
    let laundryCategoryName: String?
    let productName: String
    let productPhoto: String
    let productUnit: String?
    let productDescription: String
    let productAllowedQtyInCart: Int
    let laundryServiceList: [LaundryServiceList]
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case laundryCategoryName = "laundry_category_name"
        case productName = "product_name"
        case productPhoto = "product_photo"
        case productUnit = "product_unit"
        case productDescription = "product_description"
        case productAllowedQtyInCart = "product_allowed_qty_in_cart"
        case laundryServiceList = "laundry_service_list"
    }
}

// MARK: - LaundryServiceList
class LaundryServiceList: Codable {
    
    let laundryServiceID: Int
    var clothTypeID: String? = ""
    var clothTypeName: String? = ""
    let laundryServiceName: String
    var laundryServicePrice: Int? = 0
    let cartId: Int?
    let productQty: Int?
    var isSelected = false
    
    init(laundryServiceId: Int, clothTypeId: String?, clothTypeName: String?, laundryServiceName: String, laundryServicePrice: Int?, cartId: Int?, productQuantity: Int?, isSelected: Bool) {
        
        self.laundryServiceID = laundryServiceId
        self.clothTypeID = clothTypeId
        self.clothTypeName = clothTypeName
        self.laundryServiceName = laundryServiceName
        self.laundryServicePrice = laundryServicePrice
        self.cartId = cartId
        self.productQty = productQuantity
        self.isSelected = isSelected
        
    }
    
    
    enum CodingKeys: String, CodingKey {
        case laundryServiceID = "laundry_service_id"
        case clothTypeID = "cloth_type_id"
        case clothTypeName = "cloth_type_name"
        case laundryServiceName = "laundry_service_name"
        case laundryServicePrice = "laundry_service_price"
        case cartId = "cart_id"
        case productQty = "product_qty"
    }
}
