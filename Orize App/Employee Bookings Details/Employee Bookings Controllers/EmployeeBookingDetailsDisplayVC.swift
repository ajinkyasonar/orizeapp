//
//  EmployeeBookingDetailsAllVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class EmployeeBookingDetailsDisplayVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var moduleIndex: Int! //Test
    var bookingNumber: String! //Test
    var orderType: String! //Test
    let employeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var orderDetailsArray = [OrderDetails]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getEmployeeBookingDetails(withBookingId: bookingNumber, coWorkerEmployeeId: employeeId, type: orderType)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: EmployeeBookingRegisteredXibs.EmployeeBookingsDetailsTblCell, bundle: nil), forCellReuseIdentifier: EmployeeBookingRegisteredXibs.EmployeeBookingsDetailsTblCell)
    }

}

extension EmployeeBookingDetailsDisplayVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeBookingRegisteredXibs.EmployeeBookingsDetailsTblCell, for: indexPath) as? EmployeeBookingsDetailsTblCell else {return UITableViewCell()}
        
        cell.configureCell(orderDetails: orderDetailsArray[indexPath.row], delegate: self, orderDetailTag: indexPath.row)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
}

extension EmployeeBookingDetailsDisplayVC: EmployeeBookingDetailsTblCellDelegate {
    
    
    func trackItemsButtonTapped(orderDetails: OrderDetails, orderDetailTag: Int) {
        //print(orderDetails)
        guard let foodTrackingVc = FOODCOURT_STORYBOARD.instantiateViewController(withIdentifier: "FoodOrderTrackingVC") as? FoodOrderTrackingVC else {return}
        foodTrackingVc.foodCourtOrderDetailId = orderDetails.foodCourtOrderDetailId
        foodTrackingVc.orderDetails = orderDetails
        foodTrackingVc.modalPresentationStyle = .overCurrentContext
        foodTrackingVc.modalTransitionStyle = .crossDissolve
        navigationController?.present(foodTrackingVc, animated: true, completion: nil)
        
    }
    
    func cancelOrderButtonTapped(orderDetails: OrderDetails, orderDetailTag: Int) {
        let cancelOrderStatusId = 3
        //This API Has To Be Replaced with Change Order Product Status Based on Type
        
        switch orderDetails.type {
            
        case .FoodCourt:
            changeFoodCourtProductOrderStatus(orderMasterId: orderDetails.orderMasterId ?? -1, productId: orderDetails.productId ?? -1, orderStatusId: cancelOrderStatusId)
            
        case .ConvenienceStore:
            changeConvenienceProductOrderStatus(orderMasterId: orderDetails.orderMasterId ?? -1, productId: orderDetails.productId ?? -1, orderStatusId: cancelOrderStatusId)
            
        default:
            return
        }
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension EmployeeBookingDetailsDisplayVC {
    
    //MARK: API SERVICE FOR CHANGEING FOODCOURT PRODUCT ORDER STATUS
    func changeFoodCourtProductOrderStatus(orderMasterId: Int, productId: Int, orderStatusId: Int) {
        
        let params = ["food_court_order_master_id": orderMasterId, "food_court_product_id": productId, "order_status": orderStatusId]
        
        ServiceHelper.instance.GenericPostService(forUrl: CHANGE_FOODCOURT_ORDER_PRODUCT_STATUS, parameters: params) { (productStatusChange: ProductStatusChange) in
            
            if productStatusChange.code == 0 {
                //Success
                DispatchQueue.main.async {
                    self.getEmployeeBookingDetails(withBookingId: self.bookingNumber, coWorkerEmployeeId: self.employeeId, type: self.orderType)
                    self.view.makeToast(productStatusChange.message, duration: 2.0, position: .bottom)
                }
            }else {
                //Failure
                DispatchQueue.main.async {
                  self.view.makeToast(productStatusChange.message, duration: 2.0, position: .bottom)
                }
                
            }
            
        }
        
    }
    
    //MARK: API SERVICE FOR CHANGING CONVENIENCE STORE PRODUCT ORDER STATUS
    func changeConvenienceProductOrderStatus(orderMasterId: Int, productId: Int, orderStatusId: Int) {
        
        let params = ["food_court_order_master_id": orderMasterId, "food_court_product_id": productId, "order_status": orderStatusId]
        
        ServiceHelper.instance.GenericPostService(forUrl: CHANGE_STORE_ORDER_PRODUCT_STATUS, parameters: params) { (productStatusChange: ProductStatusChange) in
            
            if productStatusChange.code == 0 {
                //Success
                DispatchQueue.main.async {
                    self.getEmployeeBookingDetails(withBookingId: self.bookingNumber, coWorkerEmployeeId: self.employeeId, type: self.orderType)
                    self.view.makeToast(productStatusChange.message, duration: 2.0, position: .bottom)
                }
            }else {
                //Failure
                DispatchQueue.main.async {
                    self.view.makeToast(productStatusChange.message, duration: 2.0, position: .bottom)
                }
                
            }
            
        }
        
    }
    
    //MARK: GET EMPLOYEE DETAILS API SERVICE
    func getEmployeeBookingDetails(withBookingId bookingId : String, coWorkerEmployeeId: Int, type: String) {
        
        self.view.makeToastActivity(.center)
        
        EmployeeBookingServices.instance.getEmployeeBookingDetails(byBookingNumber: bookingId, coworkerEmployeeId: coWorkerEmployeeId, andType: type) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                    print(error.debugDescription)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.orderDetailsArray = returnedResponse.results[self.moduleIndex].data
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        
                    }else {
                        //Failure
                        self.view.hideToastActivity()
                        self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
}
