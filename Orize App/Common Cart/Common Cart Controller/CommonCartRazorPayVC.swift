//
//  CommonCartRazorPayVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 03/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Razorpay
import Toast_Swift

class CommonCartRazorPayVC: UIViewController, RazorpayPaymentCompletionProtocol {
    
    //Variables
    var razorpay: Razorpay!
    var totalAmount = Double()
    var imageStr = String()
    var isFromPaymetSuccessfull = false
    var orderId = ""
    
    //Variables Required
    var coWrokersEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var razorPayKey = GatewayKeys.RAZORPAY_KEY
    var cartTotalAmountValue: Int!
    var paymentMode = 2
    var paymentGatewayValue: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalAmount = Double(cartTotalAmountValue) * 100
        
        imageStr = "https://img.etimg.com/thumb/msid-66686984,width-300,imgsize-12508,resizemode-4/razorpay.jpg"
        
        razorpay = Razorpay.initWithKey(razorPayKey, andDelegate: self)
        
        showPaymentForm(amount: Double(totalAmount), image: imageStr, name: "Orize", description: "Cart Total Amount", orderId: orderId)
        
        
    }
    
    
    func showPaymentForm(amount: Double, image:String, name: String, description: String, orderId: String)
    {
        
        let options: [String:Any] = [
            "amount" : amount, //mandatory in paise
            "description": description,
            "image": image,
            "currency": "INR",
            "name": name,
            "order_id": orderId
            ]
        
        razorpay.open(options)
    }
    
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle:UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        { action -> Void in
            
            self.navigationController?.popViewController(animated: true)
        })
        self.present(alertController, animated: true, completion: nil)
        
        isFromPaymetSuccessfull = false
        
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        self.view.makeToastActivity(.center)
        
        let alertController = UIAlertController(title: "Message", message: "Payment Successful", preferredStyle:UIAlertController.Style.alert)//Payment Id \(payment_id)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        { action -> Void in
            
            //print("Is Challenge Value \(self.isChallenge)")
            
            self.isFromPaymetSuccessfull = true
            
            //Call Place ALL Orders API
            self.placeAllOrders(coWorkerId: self.coWrokersEmployeeId, paymentMode: self.paymentMode, razorpayResponse: payment_id, razorpayTag: self.paymentGatewayValue)
            
        })
        
        self.present(alertController, animated: true, completion: nil)
        
    }

}

//MARK: API SERVICE IMPLEMENTATION
extension CommonCartRazorPayVC {
    
    func placeAllOrders(coWorkerId: Int, paymentMode: Int, razorpayResponse: String, razorpayTag: Int) {
        
        self.view.makeToastActivity(.center)
        
        CommonCartServices.instance.placeALLOrdersService(coWorkerId: coWorkerId, paymentMode: paymentMode, gatewayResponse: razorpayResponse, gatewayValue: razorpayTag) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    DispatchQueue.main.async {
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        self.view.hideToastActivity()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                    }
                }
                
            }
            
        }
        
    }
    
}
