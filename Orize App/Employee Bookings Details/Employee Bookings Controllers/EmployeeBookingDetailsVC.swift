//
//  EmployeeBookingDetailsVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 19/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift
import CarbonKit

class EmployeeBookingDetailsVC: UIViewController {
    
    //Outlets
    @IBOutlet private weak var logoAndRightButtonsView: UIView!
    
    //Required Variables
    let coWrokerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var bookingNumber: String!
    var orderType: String!
    
    //Variables
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var items = [String]()
    
    var orderDetailsResultArray = [OrderDetailsResult]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getEmployeeBookingDetails(withBookingId: bookingNumber, coWorkerEmployeeId: coWrokerEmployeeId, type: orderType)
        
    }
    
    //IB - Actions
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK: Carbon Kit Implementation
extension EmployeeBookingDetailsVC: CarbonTabSwipeNavigationDelegate {
    
    private func configureCarbonKit() {
        
        items = orderDetailsResultArray.map({$0.moduleName})
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        carbonTabSwipeNavigation?.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints([
            
            NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: logoAndRightButtonsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 50),
            
            NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0)
            
            ])
        
        //Appearance of Menu
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        carbonTabSwipeNavigation?.setNormalColor(UIColor.white)
        carbonTabSwipeNavigation?.setSelectedColor(UIColor.white)
        carbonTabSwipeNavigation?.setIndicatorColor(UIColor.white)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            carbonTabSwipeNavigation?.setTabExtraWidth(30)
            carbonTabSwipeNavigation?.setTabBarHeight(60)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
        }else {
            carbonTabSwipeNavigation?.setTabBarHeight(50)
            carbonTabSwipeNavigation?.setTabExtraWidth(20)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
        }
        
    }
    
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        //let moduleId = orderDetailsResultArray[index].moduleId
        
        guard let EmployeeBookingDisplayVc = storyboard?.instantiateViewController(withIdentifier: "EmployeeBookingDetailsAllVC") as? EmployeeBookingDetailsDisplayVC else {return UIViewController()}
        
        //EmployeeBookingDisplayVc.moduleId = moduleId
        EmployeeBookingDisplayVc.moduleIndex = index //Test
        EmployeeBookingDisplayVc.bookingNumber = bookingNumber
        EmployeeBookingDisplayVc.orderType = orderType
        
        return EmployeeBookingDisplayVc
        
    }
    
    
    
}


//MARK: API IMPLEMENTATION
extension EmployeeBookingDetailsVC {
    
    func getEmployeeBookingDetails(withBookingId bookingId : String, coWorkerEmployeeId: Int, type: String) {
        
        self.view.makeToastActivity(.center)
        
        EmployeeBookingServices.instance.getEmployeeBookingDetails(byBookingNumber: bookingId, coworkerEmployeeId: coWorkerEmployeeId, andType: type) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                    print(error.debugDescription)
                    self.view.hideToastActivity()
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.orderDetailsResultArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.configureCarbonKit()
                            self.view.hideToastActivity()
                        }
                        //print(self.orderDetailsResultArray)
                        
                    }else {
                        //Failure
                        self.view.hideToastActivity()
                        self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
}
