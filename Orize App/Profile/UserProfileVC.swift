//
//  UserProfileVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 18/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class UserProfileVC: UIViewController {
    
    @IBOutlet weak var employeeImage: UIImageView!
    @IBOutlet weak var employeeNameLbl: UILabel!
    @IBOutlet weak var setProfileImageBtn: UIButton!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        
    }
    
    //MARK: SET UP INITIAL VIEw
    private func setupInitialView() {
        
        employeeNameLbl.text = defaults.string(forKey: Co_Worker_EMPLOYEE_NAME)
        setupRoundedImage(imageView: employeeImage)
        
        if employeeImage.image != nil {
            setProfileImageBtn.setImage(nil, for: .normal)
        }else {
            setProfileImageBtn.setImage(UIImage(named: "camera"), for: .normal)
        }
    }
    
    //MARK:
    private func setupRoundedImage(imageView: UIImageView) {
        imageView.layer.borderWidth = 1.0
        imageView.layer.cornerRadius = imageView.frame.width/2
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
    }
    
    
    @IBAction func logoutBtnTapped(_ sender: UIButton) {
        logoutUser()
    }
    
    @IBAction func setProfileImageBtnTapped(sender: UIButton) {
        //This will open camera gallery option to set profile image
        print("Camera Button Tapped")
       imageSelectionAction()
        
    }
    
    //MARK: IMAGE SELECTION ACTION FOR PROFILE IMAGE
    private func imageSelectionAction() {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }else {
                //Camera is Not Available
                self.view.makeToast("There is some issue with your camera", duration: 2.0, position: .bottom)
            }
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
}

extension UserProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {return}
        employeeImage.image = selectedImage
        setProfileImageBtn.setImage(nil, for: .normal)
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
