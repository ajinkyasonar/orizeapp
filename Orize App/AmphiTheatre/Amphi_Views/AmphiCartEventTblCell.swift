//
//  AmphiCartEventTblCell.swift
//  Orize App
//
//  Created by Apple on 13/09/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class AmphiCartEventTblCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventMessageLbl: UILabel!
    @IBOutlet weak var playingSlotLbl: UILabel!
    @IBOutlet weak var eventDescriptionLbl: UILabel!
    @IBOutlet weak var passTableView: UITableView!
    @IBOutlet weak var passTableViewHeightConstraint: NSLayoutConstraint!
    
    var passArray: [PassDetails]! {
        
        didSet {
            
            passTableView.register(UINib(nibName: AmphiTheatreXibs.AmphiPassTblCell, bundle: nil), forCellReuseIdentifier: AmphiTheatreXibs.AmphiPassTblCell)
            passTableView.delegate = self
            passTableView.dataSource = self
            passTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
            passTableView.reloadData()
            
        }
        
    }
    
    //MARK: Manage Table View Height Based on Content
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        passTableView.layer.removeAllAnimations()
        passTableViewHeightConstraint.constant = passTableView.contentSize.height
        updateConstraints()
        layoutIfNeeded()
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        eventImage.layer.cornerRadius = 10
    }
    
    func configureCell(with event: AmphiEvent) {
        
        guard let eventImageUrl = URL(string: event.eventImage) else {return}
        eventImage.sd_setImage(with: eventImageUrl, completed: .none)
        
        eventNameLbl.text = event.eventName
        eventMessageLbl.text = event.eventMessage
        playingSlotLbl.text = event.playingSlot
        eventDescriptionLbl.text = event.eventDescription
        
    }
    
}

//MARK: TableView Delegate & Data Source
extension AmphiCartEventTblCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AmphiPassTblCell") as? AmphiPassTblCell else {return UITableViewCell()}
        
        cell.configureCell(with: passArray[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
