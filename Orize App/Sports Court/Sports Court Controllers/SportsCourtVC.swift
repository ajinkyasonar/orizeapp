//
//  SportsCourtVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 08/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class SportsCourtVC: UIViewController {
    
    @IBOutlet weak var topCustomNavigationView: UIView!
    @IBOutlet weak var allElementsStackView: UIStackView!
    
    @IBOutlet weak var sportSelectionCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sportSelectionCollectionView: UICollectionView!
    @IBOutlet weak var challengeRequestBtn: UIButton!
    
    //Variables
    var sportsTypeListResultArray = [SportsTypeListResult]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
    }
    
    func setupInitialUI() {
        
        topCustomNavigationView.layer.borderWidth = 0.4
        topCustomNavigationView.layer.borderColor = UIColor.black.cgColor
        challengeRequestBtn.layer.cornerRadius = 5
        
        getSportsListTypeService()
        sportSelectionCollectionView.delegate = self
        sportSelectionCollectionView.dataSource = self
        
        self.sportSelectionCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    //MARK: ACTIONS PERFORMED HERE
    @IBAction func navigationBackButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        sportSelectionCollectionView.layer.removeAllAnimations()
        sportSelectionCollectionViewHeightConstraint.constant = sportSelectionCollectionView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        
    }
    
    @IBAction func challengeRequestButtonTapped(_ sender: UIButton) {
        
        guard let viewcontroller = storyboard?.instantiateViewController(withIdentifier: "ChallengeRequestVC") as? ChallengeRequestVC else {return}
        //self.present(viewcontroller, animated: true, completion: nil)
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }

}

//MARK: COLLECTION VIEW IMPLEMENTATION
extension SportsCourtVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sportsTypeListResultArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = sportSelectionCollectionView.dequeueReusableCell(withReuseIdentifier: "SportSelectionCollectionCell", for: indexPath) as? SportSelectionCollectionCell else {return UICollectionViewCell()}
        cell.configureCell(data: sportsTypeListResultArray[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2 + 20, height: collectionViewSize/2 - 30)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "SelectedSportBookingVC") as? SelectedSportBookingVC else {return}
        let individualSelectedItem = sportsTypeListResultArray[indexPath.row]
        viewController.gameImageUrl = individualSelectedItem.photo
        viewController.gameTitleString = individualSelectedItem.name
        viewController.sportsTypeId = individualSelectedItem.sportsTypeID
        //self.navigationController?.present(viewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//MARK: API IMPLEMENTATION HERE
extension SportsCourtVC {
    
    //GET SPORTS LIST SERVICE
    func getSportsListTypeService() {
        
        self.view.makeToastActivity(.center)
        
        SportsCourtServices.instance.getSportsTypeListService { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        self.sportsTypeListResultArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.sportSelectionCollectionView.reloadData()
                            self.view.hideToastActivity()
                        }
                    } else {
                        //Failure
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                            self.view.hideToastActivity()
                        })
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
