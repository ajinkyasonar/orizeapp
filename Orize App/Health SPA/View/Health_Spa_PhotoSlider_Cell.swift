//
//  Health_Spa_PhotoSlider_Cell.swift
//  Orize App
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class Health_Spa_PhotoSlider_Cell: UICollectionViewCell {
    
    @IBOutlet weak var sliderImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(image: SliderImage) {
        guard let imageUrl = URL(string: image.imageUrl) else {return}
        sliderImage.sd_setImage(with: imageUrl, completed: nil)
    }

}
