//
//  AllEventsVC.swift
//  Orize App
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class AllEventsVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var eventTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()        
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        
        eventTableView.delegate = self
        eventTableView.dataSource = self
        eventTableView.register(UINib(nibName: AmphiTheatreXibs.AmphiAllEventsTblCell, bundle: nil), forCellReuseIdentifier: AmphiTheatreXibs.AmphiAllEventsTblCell)
        
    }

}

extension AllEventsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amphiEventsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AmphiTheatreXibs.AmphiAllEventsTblCell) as? AmphiAllEventsTblCell else {return UITableViewCell()}
        
        cell.configureCell(event: amphiEventsData[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /* Todo: Open the Payment check out event page taking the selected
         event data to the check out controller
         */
        
        let selectedEvent = amphiEventsData[indexPath.row]
        
        guard let amphiCartVc = storyboard?.instantiateViewController(withIdentifier: "AmphiTheatreCartVC") as? AmphiTheatreCartVC else {return}
        amphiCartVc.selectedAmphiEvent = selectedEvent
        navigationController?.pushViewController(amphiCartVc, animated: true)
        
    }
    
}
