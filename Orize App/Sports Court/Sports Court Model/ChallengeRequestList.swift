//
//  ChallengeRequestList.swift
//  Orize App
//
//  Created by Aditya Infotech on 08/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// To parse the JSON, add this file to your project and do:
//
//   let challengeRequestList = try? newJSONDecoder().decode(ChallengeRequestList.self, from: jsonData)

import Foundation

struct ChallengeRequestList: Codable {
    let code: Int
    let status, message: String
    let results: [ChallengeRequestListResult]
}

struct ChallengeRequestListResult: Codable {
    let incoming, sent: [RequestDetails]
}

struct RequestDetails: Codable {
    let sportsCourtChallengesID: Int
    let sportsTypeName, bookingDate, bookingTime: String
    let challengerName: String?
    let contactNumber: String
    let playersCapacity: Int
    let requestStatus: String
    let isCancellable: Bool?
    
    enum CodingKeys: String, CodingKey {
        case sportsCourtChallengesID = "sports_court_challenges_id"
        case sportsTypeName = "sports_type_name"
        case bookingDate = "booking_date"
        case bookingTime = "booking_time"
        case challengerName = "challenger_name"
        case contactNumber = "contact_number"
        case playersCapacity = "players_capacity"
        case requestStatus = "request_status"
        case isCancellable = "is_cancellable"
    }
}


//MARK: ACCEPT, DECLINE & CANCEL CHALLENGE REQUEST MODEL
struct ChallengeBookingStatus: Codable {
    let code: Int
    let status, message: String
}
