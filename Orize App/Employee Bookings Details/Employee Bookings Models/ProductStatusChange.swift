//
//  ProductStatusChange.swift
//  Orize App
//
//  Created by Apple on 23/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

struct ProductStatusChange: Codable {
    
    let code: Int
    let status: String
    let message: String
    
}
