//
//  LaundryCartTblCell.swift
//  Orize App
//
//  Created by Apple on 30/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol LaundryCartTblCellDelegate: class {
    func editBtnTapped(at index: Int, productDetails: ProductCartList)
}

class LaundryCartTblCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var servicesNameLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    
    weak var delegate: LaundryCartTblCellDelegate?
    private var productIndex: Int!
    private var productDetails: ProductCartList!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        elementView.layer.cornerRadius = 10
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
        
        productImage.layer.cornerRadius = 10
        productImage.layer.borderColor = UIColor.black.cgColor
        productImage.layer.borderWidth = 0.3
        
    }
    
    func configureCell(productDetails: ProductCartList, delegate: LaundryCartTblCellDelegate, productIndex: Int) {
        
        self.delegate = delegate
        self.productDetails = productDetails
        self.productIndex = productIndex
        
        guard let productImageUrl = URL(string: productDetails.productPhoto) else {return}
        productImage.sd_setImage(with: productImageUrl)
        
        productNameLbl.text = productDetails.productName
        productPriceLbl.text = "\(rupee) \(productDetails.productPrice)"
        
        guard let laundryServices = productDetails.laundryServicesList else {return}
        var selectedServices = [String]()
        
        for service in laundryServices {
            guard let cartid = service.cartId else {return}
            if cartid > 0 {
                selectedServices.append(service.laundryServiceName)
            }
        }
        
        servicesNameLbl.text = selectedServices.joined(separator: ", ")
        //print("Cart Laundry Selected Services: \(selectedServices.joined(separator: ","))")
        
        
    }
    
    @IBAction func editBtnTapped(_ sender: UIButton) {
        delegate?.editBtnTapped(at: productIndex, productDetails: productDetails)
    }
    
}
