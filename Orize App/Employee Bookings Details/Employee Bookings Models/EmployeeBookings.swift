//
//  EmployeeBookings.swift
//  Orize App
//
//  Created by Aditya Infotech on 18/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let employeeBookings = try? newJSONDecoder().decode(EmployeeBookings.self, from: jsonData)

import Foundation

// MARK: - EmployeeBookings
struct EmployeeBookings: Codable {
    let code: Int
    let status, message: String
    let results: [BookingDetails]
}

// MARK: - Result
struct BookingDetails: Codable {
    let id: Int
    let bookingNo, orderDate, orderTime: String
    let price: Int
    let createdDate, type: String
    let isCancellable: Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case bookingNo = "booking_no"
        case orderDate = "order_date"
        case orderTime = "order_time"
        case price
        case createdDate = "created_date"
        case type
        case isCancellable = "is_cancellable"
    }
}


//MARK: EMPLOYEE BOOKING ORDER CANCELLATION MODEL

struct EmployeeBookingCancelOrder: Codable {
    
    let code: Int
    let status: String
    let message: String
    
}
