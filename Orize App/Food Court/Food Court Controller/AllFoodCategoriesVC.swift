//
//  AllFoodCategoriesVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol AllFoodCategoriesVCDelegate: class {
    
    func passCategoryIndexToRootVC(categoryIndex: Int)
    func updateFoodCartCountInRootVC(itemCount: Int)
}

class VenderVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    
    //Variables
    let coWorkersEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var venderCategoryId: Int!
    var isSearching = false
    var allCategoryNames = [ProductData]()
    var allCategoryNamesFilterArray = [ProductData]()
    var searchAutoCompleteArray = [SearchAutocompleteData]()
    
    //Delegate
    weak var delegate: AllFoodCategoriesVCDelegate?
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupInitialTableView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        allCategoryNamesFilterArray = allCategoryNames
        //setupInitialTableView()
        
    }
    
    func setupInitialTableView() {
        
        guard let venderCategoryId = venderCategoryId else {return}
        getProductList(withVenderCategoryId: venderCategoryId)
        
        tableView.register(UINib(nibName: FoodCourtRegisteredXibs.AllFoodCategoryTblCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.AllFoodCategoryTblCell)
        
        tableView.delegate = self
        tableView.dataSource = self
        searchTextField.delegate = self
        
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        tableViewHeightConstraint.constant = tableView.contentSize.height
        self.updateViewConstraints()
        self.view.layoutIfNeeded()
        
    }
    
}

//MARK: TABLE VIEW IMPLEMENTATION
extension VenderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            return allCategoryNamesFilterArray.count
        } else {
            return allCategoryNames.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FoodCourtRegisteredXibs.AllFoodCategoryTblCell) as? AllFoodCategoryTblCell else {return UITableViewCell()}
        
        cell.configureCell(productData: allCategoryNames[indexPath.row], viewButtonIndex: indexPath.row, delegate: self)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let isPrebooking = allCategoryNames[indexPath.row].isPrebookingCategory
        return isPrebooking == 1 ? 320 : 340
       
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? AllFoodCategoryTblCell else {return}
        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
    }
    
    
    
}

//MARK: ALL FOOD CATEGORY CELL DELEGATE IMPLEMENTATION
extension VenderVC: AllFoodCategoryTblCellDelegate {
    
    func viewAllButtonTapped(atIndex: Int, productDataDetails: ProductData) {
        //Todo: Check if the category exist in the category array or else stop navigation
        let categoryIndex = (atIndex + 1)
        delegate?.passCategoryIndexToRootVC(categoryIndex: categoryIndex)
    }
    
}


//MARK: COLLECTION VIEW IMPLEMENTATION
extension VenderVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let count = allCategoryNames[collectionView.tag].productsListArr.count
        
        return count >= 4 ? 4 : count
        
        //return allCategoryNames[collectionView.tag].productsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let product = allCategoryNames[collectionView.tag].productsListArr[indexPath.item]
        
        if product.isPrebookingCategory == 1 {
            //Show Pre Booking Category Cell
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell, for: indexPath) as? FoodCourtPrebookingCollCell else {return UICollectionViewCell()}
            
            cell.configureCell(product: product, delegate: self , productIndex: indexPath.item)
            
            return cell
            
        } else {
            //Show Normal Food Product Cell
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodCourtRegisteredXibs.AllFoodCategoryCollCell, for: indexPath) as? AllFoodCategoryCollCell else {return UICollectionViewCell()}
            
            cell.configureCell(product: product, categoryInt: collectionView.tag, productInt: indexPath.item, delegate: self)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.width
        let cellWidth = (width - 5) / 2
        let cellHeight = (cellWidth * 1.3)
        return CGSize(width: cellWidth, height: cellHeight)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItem = allCategoryNames[collectionView.tag].productsListArr[indexPath.row]
        
        guard let foodProductVc = storyboard?.instantiateViewController(withIdentifier: "FoodItemDetailVC") as? FoodItemDetailVC else {return}
        foodProductVc.delegate = self
        foodProductVc.selectedFoodProductDetails = selectedItem
        foodProductVc.modalPresentationStyle = .overCurrentContext
        foodProductVc.modalTransitionStyle = .crossDissolve
        navigationController?.present(foodProductVc, animated: true, completion: nil)
        
        
    }
    
    
}

//MARK: SEARCH TEXT FIELD IMPLEMENTATION
extension VenderVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //tableView.scrollToRow(at: IndexPath(row: 3, section: 0), at: .middle, animated: true)
        
        
         //MARK: OLD CODE FOR ALL SEARCH
         guard let foodSearchVc = storyboard?.instantiateViewController(withIdentifier: "FoodSearchVC") as? FoodSearchVC else {return}
         searchTextField.endEditing(true)
         foodSearchVc.modalTransitionStyle = .crossDissolve
         foodSearchVc.modalPresentationStyle = .overCurrentContext
         foodSearchVc.delegate = self
         navigationController?.pushViewController(foodSearchVc, animated: false)
        
        

    }
    
    
}

//MARK: Food Prebooking Cell Delegate
extension VenderVC: FoodCourtPrebookingCollCellDelegate {
    
    func didTapBookButton(productDetail: FoodCourtProductsDetails, atTag tag: Int) {
        
        guard let preBookSelectionVc = storyboard?.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
        preBookSelectionVc.productDetails = productDetail
        self.navigationController?.pushViewController(preBookSelectionVc, animated: true)
        
    }

}


//MARK: ALL FOOD COLLECTION DELEGATE
extension VenderVC: AllFoodCategoryCollCellActionDelegate {
    
    
    func addBtnTapped(product: FoodCourtProductsDetails, categoryInt: Int, productInt: Int, productQuantity: Int) {
        
        add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: product.productID, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
        
    }
    
    func minusBtnTapped(product: FoodCourtProductsDetails, categoryInt: Int, productInt: Int, productQuantity: Int) {
        
        add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: product.productID, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
        
    }
    
}

//MARK: FOOD SEARCH VIEW CONTROLLER DELEGATE
extension VenderVC: FoodSearchVCDelegate {
    
    func passVendorIndexToAllVC(categoryIndex: Int) {
        delegate?.passCategoryIndexToRootVC(categoryIndex: categoryIndex)
    }
    
    
}

//MARK: FOOD ITEM DETAIL VIEW CONTROLLER DELEGATE
extension VenderVC: FoodItemDetailVCDelegate {
    
    func addRemoveFoodCourtCartProduct(product: FoodCourtProductsDetails, productQuantity: Int) {
        add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: product.productID, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
    }
}


//MARK: API IMPLEMENTATIONS HERE
extension VenderVC {
    
    
    func getProductList(withVenderCategoryId venderCategoryId: Int) {
        
        allCategoryNames.removeAll()
        searchAutoCompleteArray.removeAll()
        
        self.view.makeToastActivity(.center)
        
        FoodCourtServices.instance.getFoodCourtVenderDetails(with: venderCategoryId) { (returnedResponse, error) in
            
            if error != nil {
                
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        
                        for values in returnedResponse.results {
                            self.allCategoryNames = values.productData
                            self.searchAutoCompleteArray = values.searchAutocompleteData
                        }
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        
                    } else {
                        //Failure
                        print(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    //ADD REMOVE FOODCOURT CART API
    
    func add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: Int, foodCourtProductId: Int, productQuantity: Int, subscriptionDate: String, deliveryTime: String) {
        
        FoodCourtServices.instance.addRemoveFoodcourtCartItem(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: productQuantity, subscriptionDate: subscriptionDate, deliveryTime: deliveryTime) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        DispatchQueue.main.async {
                            //Update Count on Cart
                            self.getEmployeeDetails(employeeId: coWorkersEmployeeId)
                        }
                    }else {
                        //Failure
                        print(returnedResponse.message)
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process
                                DispatchQueue.main.async {
                                    self.delegate?.updateFoodCartCountInRootVC(itemCount: detail.itemsInCart)
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                   self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}


//MARK: PREVIOUS CODE

/*

 func getProductList(withfoodCourtCategoryId foodCourtCategoryId: Int) {
 
 allCategoryNames.removeAll()
 searchAutoCompleteArray.removeAll()
 
 self.view.makeToastActivity(.center)
 
 FoodCourtServices.instance.getProductListForFoodCourt(withfoodCourtCategoryId: foodCourtCategoryId) { (returnedresponse, error) in
 
 
 if error != nil {
 DispatchQueue.main.async {
 self.view.hideToastActivity()
 self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
 }
 }else {
 
 if let returnedresponse = returnedresponse {
 
 if returnedresponse.code == 0 {
 //Success
 //print(returnedresponse.results)
 for values in returnedresponse.results {
 //print(values.productData.count)
 self.allCategoryNames = values.productData
 self.searchAutoCompleteArray = values.searchAutocompleteData
 
 }
 
 DispatchQueue.main.async {
 self.tableView.reloadData()
 self.view.hideToastActivity()
 
 }
 
 }else {
 //Failure
 DispatchQueue.main.async {
 self.view.makeToast(returnedresponse.message, duration: 2.0, position: .bottom)
 }
 print(returnedresponse.message)
 }
 
 
 }
 
 }
 
 }
 
 }

 */
