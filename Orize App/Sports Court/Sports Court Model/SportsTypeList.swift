//
//  SportsTypeList.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let sportsTypeList = try? newJSONDecoder().decode(SportsTypeList.self, from: jsonData)

import Foundation

struct SportsTypeList: Codable {
    let code: Int
    let status, message: String
    let results: [SportsTypeListResult]
}

struct SportsTypeListResult: Codable {
    let sportsTypeID: Int
    let photo: String
    let name, description: String
    
    enum CodingKeys: String, CodingKey {
        case sportsTypeID = "sports_type_id"
        case photo, name, description
    }
}

