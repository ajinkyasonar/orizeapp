//
//  HealthSpaSelectionVC.swift
//  Orize App
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import FSCalendar

class HealthSpaSelectionVC: UIViewController {
    
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var pageView: UIPageControl!
    @IBOutlet weak var accessTextLbl: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var passTableView: UITableView!
    @IBOutlet weak var passTableViewHeightConstraint: NSLayoutConstraint!
    
    //Variables
    var timer = Timer()
    var counter = 0
    let formatter = DateFormatter()
    
    var selectedPassesArray = [HealthSpaPass]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        passTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        passTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    private func initialSetup() {
        photoCollectionView.delegate = self
        photoCollectionView.dataSource = self
        photoCollectionView.register(UINib(nibName: "Health_Spa_PhotoSlider_Cell", bundle: nil), forCellWithReuseIdentifier: "Health_Spa_PhotoSlider_Cell")
        
        pageView.numberOfPages = sliderImageArr.count
        pageView.currentPage = 0
        
        DispatchQueue.main.async {
          self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.slideImage), userInfo: nil, repeats: true)
        }
        
        accessTextLbl.text = "When would you like to access the health & fitness facilities?"
        
        setupCalendar()
        
        passTableView.delegate = self
        passTableView.dataSource = self
        passTableView.register(UINib(nibName: "Health_Spa_Pass_Cell", bundle: nil), forCellReuseIdentifier: "Health_Spa_Pass_Cell")
        passTableView.layer.borderWidth = 0.4
        passTableView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        passTableView.layer.cornerRadius = 8
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        passTableView.layer.removeAllAnimations()
        passTableViewHeightConstraint.constant = passTableView.contentSize.height
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    private func setupCalendar() {
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        formatter.locale = .current
        
        
        calendar.delegate = self
        calendar.dataSource = self
        calendar.scope = .month
        calendar.appearance.headerMinimumDissolvedAlpha = 0.0
    }
    
    //IB-ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueBtnTapped(_ sender: UIButton) {
        if selectedPassesArray.count > 0 {
            guard let paymentDetailvc = storyboard?.instantiateViewController(withIdentifier: "HealthSpaPaymentDetailsVC") as? HealthSpaPaymentDetailsVC else {return}
            paymentDetailvc.purchasedPasses = selectedPassesArray
            navigationController?.pushViewController(paymentDetailvc, animated: true)
        }
        else {
            print("There are no items to proceed payment")
        }
        
    }

}

extension HealthSpaSelectionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderImageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Health_Spa_PhotoSlider_Cell", for: indexPath) as? Health_Spa_PhotoSlider_Cell else {return UICollectionViewCell()}
        
        cell.configureCell(image: sliderImageArr[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.frame.width
        let cellHeight = collectionView.frame.height
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}

//MARK: CALENDAR DELEGATE & DATA SOURCE IMPLEMENTATION
extension HealthSpaSelectionVC: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        guard let endDate = Calendar.current.date(byAdding: .month, value: 1, to: Date(), wrappingComponents: false) else {return Date()}
        return endDate
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let selectedDate = formatter.string(from: date)
        print("Selected Date is: \(selectedDate)")
    }
    
    
}

extension HealthSpaSelectionVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return healthSpaPassesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Health_Spa_Pass_Cell", for: indexPath) as? Health_Spa_Pass_Cell else {return UITableViewCell()}
        
        cell.configureCell(passDetails: healthSpaPassesArray[indexPath.row], delegate: self, selectedIndex: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}

extension HealthSpaSelectionVC: Health_Spa_Pass_CellDelegate {
    
    func addPassButtonTapped(at index: Int, passDetails: HealthSpaPass) {
        if passDetails.unitCount < 5 {
            passDetails.unitCount += 1
        }
        
        if selectedPassesArray.contains(where: {$0.timePeriod == passDetails.timePeriod}) {
            return
        }else {
            selectedPassesArray.append(passDetails)
        }
        //print(selectedPassesArray)
    }
    
    func removePassButtonTapped(at index: Int, passDetails: HealthSpaPass) {
        if passDetails.unitCount > 0 {
            passDetails.unitCount -= 1
        }
        
        if passDetails.unitCount == 0 {
            if let index = selectedPassesArray.lastIndex(where: {$0.timePeriod == passDetails.timePeriod}) {
                selectedPassesArray.remove(at: index)
            }
            //print(selectedPassesArray)
        }
        
        
    }
    
}


//MARK: IMAGE SLIDER IMPLEMENTATION LOGIC
extension HealthSpaSelectionVC {
    
    @objc func slideImage() {
        
        if counter < sliderImageArr.count {
            let index = IndexPath(item: counter, section: 0)
            self.photoCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageView.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath(item: counter, section: 0)
            self.photoCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageView.currentPage = counter
            counter = 1
        }
        
    }
    
}
