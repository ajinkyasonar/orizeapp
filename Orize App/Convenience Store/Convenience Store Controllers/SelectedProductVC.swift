//
//  SelectedProductVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 21/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import Toast_Swift

protocol SelectedProductVCDelegate: class {
    
    func addRemoveConvenienceCartProduct(product: ProductsListArr, productQuantity: Int)
}

class SelectedProductVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productDescriptionLbl: UILabel!
    @IBOutlet weak var counterDisplayLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!

    //Variables
    var selectedProductDetails: ProductsListArr!
    var counter = 0
    
    //Delegate
    weak var delegate: SelectedProductVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    private func setupInitialView() {
        
        guard let imageUrl = URL(string: selectedProductDetails.productPhoto) else {return}
        imageView.sd_setImage(with: imageUrl)
        productNameLbl.text = selectedProductDetails.productName
        productPriceLbl.text = "\(rupee) \(selectedProductDetails.productPrice)"
        productDescriptionLbl.text = selectedProductDetails.productDescription
    }
    
    
    //Actions
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func hiddenViewBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        
        if counter == selectedProductDetails.productAllowedQtyInCart {
            
            self.view.makeToast("Maximum Count Reached", duration: 1.0, position: .center)
            
        }else {
            
            counter += 1
            counterDisplayLbl.text = "\(counter)"
        }
        
        delegate?.addRemoveConvenienceCartProduct(product: selectedProductDetails, productQuantity: counter)
    }
    
    @IBAction func removeBtnTapped(_ sender: UIButton) {
        
        if counter <= 0 {
            
            self.view.makeToast("Lowest Count Reached", duration: 1.0, position: .center)
            
        }else {
            
            counter -= 1
            counterDisplayLbl.text = "\(counter)"
            
        }
        
        delegate?.addRemoveConvenienceCartProduct(product: selectedProductDetails, productQuantity: counter)
        
    }

}
