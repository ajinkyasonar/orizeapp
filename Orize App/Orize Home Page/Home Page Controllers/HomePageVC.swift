//
//  HomePageVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 05/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import Toast_Swift

class HomePageVC: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var logoAndRightButtonTop: UIView!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var imageSliderCollectionView: UICollectionView!
    @IBOutlet weak var sliderPageView: UIPageControl!
    
    //Favourite Outlets
    @IBOutlet weak var favouritesStackView: UIStackView!
    @IBOutlet weak var yourFavouritesAndEditView: UIView!
    @IBOutlet weak var favouritesOuterViewForCollectionView: UIView!
    @IBOutlet weak var favouritesCollectionView: UICollectionView!
    @IBOutlet weak var favouriteEditButton: UIButton!
    
    //Services Outlets
    @IBOutlet weak var servicesStackView: UIStackView!
    @IBOutlet weak var servicesAndViewAllView: UIView!
    @IBOutlet weak var servicesOuterViewForCollectionView: UIView!
    @IBOutlet weak var servicesCollectionView: UICollectionView!
    
    //Storage Variables
    var sliderImageArray = [Banner]()
    var servicesArray = [Services]()
    var favouritesArray = [FavouriteService]()
    
    //MARK: VARIABLES
    var timer = Timer()
    var counter = 0
    let coWorkerId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInitialView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setupInitialView() {
        
        getSliderBanners()
        getServicesList(withCoworkerId: coWorkerId)
        getFavouritesList(withCoworkerId: coWorkerId)
        
        imageSliderCollectionView.delegate = self
        imageSliderCollectionView.dataSource = self
        
        //Favourites Collection View
        favouritesCollectionView.delegate = self
        favouritesCollectionView.dataSource = self
        favouritesOuterViewForCollectionView.dropShadow()
        favouritesCollectionView.layer.cornerRadius = 5
        favouritesCollectionView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.6)
        favouritesCollectionView.layer.borderWidth = 1.5
        
        //Services Collection View
        servicesCollectionView.delegate = self
        servicesCollectionView.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getFavouritesList(withCoworkerId: coWorkerId)
        getEmployeeDetails(employeeId: coWorkerId)
    }
    
    //MARK: IB-ACTIONS PERFORMED HERE
    
    @IBAction func favouriteEditBtnTapped(_ sender: UIButton) {
        guard let favouriteSelectionVc = storyboard?.instantiateViewController(withIdentifier: "FavouriteServicesSelectionVC") as? FavouriteServicesSelectionVC else {return}
        favouriteSelectionVc.servicesArray = self.servicesArray
        self.navigationController?.pushViewController(favouriteSelectionVc, animated: true)
    }
    
    @IBAction func goToCartBtnTapped(sender: UIButton) {
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        self.navigationController?.pushViewController(commonCartVc, animated: true)
    }
    
    
}

extension HomePageVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
            
        case imageSliderCollectionView:
            return sliderImageArray.count
        case favouritesCollectionView:
            return favouritesArray.count
        case servicesCollectionView:
            return servicesArray.count
        default:
            return 0
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
            
        case imageSliderCollectionView:
            guard let cell = imageSliderCollectionView.dequeueReusableCell(withReuseIdentifier: "ImageSliderCollectionCell", for: indexPath) as? ImageSliderCollectionCell else {return UICollectionViewCell()}
            cell.configureCell(banner: sliderImageArray[indexPath.item])
            return cell
            
        case favouritesCollectionView:
            guard let cell = favouritesCollectionView.dequeueReusableCell(withReuseIdentifier: "FavouritesCollectionCell", for: indexPath) as? FavouritesCollectionCell else {return UICollectionViewCell()}
            cell.configureCell(favouriteService: favouritesArray[indexPath.item])
            return cell
            
        case servicesCollectionView:
            guard let cell = servicesCollectionView.dequeueReusableCell(withReuseIdentifier: "ServicesCollectionViewCell", for: indexPath) as? ServicesCollectionViewCell else {return UICollectionViewCell()}
            cell.configureCell(service: servicesArray[indexPath.row])
            return cell
            
        default:
            return UICollectionViewCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch collectionView {
        case imageSliderCollectionView:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case imageSliderCollectionView:
            let size = imageSliderCollectionView.frame.size
            return CGSize(width: size.width, height: size.height)
        case favouritesCollectionView:
            let size = favouritesCollectionView.frame.size
            return CGSize(width: size.width/2, height: size.height)
        case servicesCollectionView:
            let size = servicesCollectionView.frame.size
            return CGSize(width: 150, height: size.height)
        default:
            return CGSize(width: 0, height: 0)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        switch collectionView {
        case servicesCollectionView:
            return 20.0
        default:
            return 0.0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
            
        case servicesCollectionView:
            
            switch servicesArray[indexPath.item].moduleID {
                
            case 1:
                
                guard let convenienceStoreViewController = CONVENIENCE_STORE_STORYBOARD.instantiateViewController(withIdentifier: "ConvenienceStoreVC") as? ConvenienceStoreVC else {return}
                self.navigationController?.pushViewController(convenienceStoreViewController, animated: true)
                break
                
            case 3:
                
                guard let carpoolViewController = CAR_POOL_STORYBOARD.instantiateViewController(withIdentifier: "CarpoolVC") as? CarpoolVC else {return}
                self.navigationController?.pushViewController(carpoolViewController, animated: true)
                break
                
            case 5:
                
                guard let sportsCourtViewController = SPORTS_COURT_STORYBOARD.instantiateViewController(withIdentifier: "SportsCourtVC") as? SportsCourtVC else {return}
                self.navigationController?.pushViewController(sportsCourtViewController, animated: true)
                break
                
            case 6:
                
                guard let foodCourtViewController = FOODCOURT_STORYBOARD.instantiateViewController(withIdentifier: "FoodCourtVC") as? FoodCourtVC else {return}
                navigationController?.pushViewController(foodCourtViewController, animated: true)
                break
                
            case 7:
                
                guard let exclusiveServiceViewController = EXCLUSIVE_SERVICE_STORYBOARD.instantiateViewController(withIdentifier: "ExclusiveHomeVC") as? ExclusiveHomeVC else {return}
                navigationController?.pushViewController(exclusiveServiceViewController, animated: true)
                break
                
            case 8:
                
                guard let laundryViewController = LAUNDRY_STORYBOARD.instantiateViewController(withIdentifier: "LaundryVC") as? LaundryVC else {return}
                navigationController?.pushViewController(laundryViewController, animated: true)
                break
                
            case 9:
                
                guard let amphitheatreViewController = AMPHITHEATRE_STORYBOARD.instantiateViewController(withIdentifier: "AmphiTheatreVC") as? AmphiTheatreVC else {return}
                navigationController?.pushViewController(amphitheatreViewController, animated: true)
                
            case 10:
                
                guard let healthSpaViewController = HEALTH_SPA_STORYBOARD.instantiateViewController(withIdentifier: "HealthSpaSelectionVC") as? HealthSpaSelectionVC else {return}
                navigationController?.pushViewController(healthSpaViewController, animated: true)
                
            default:
                return
                
            }
            
        case favouritesCollectionView:
            
            switch favouritesArray[indexPath.item].moduleID {
                
            case 1:
                
                guard let convenienceStoreViewController = CONVENIENCE_STORE_STORYBOARD.instantiateViewController(withIdentifier: "ConvenienceStoreVC") as? ConvenienceStoreVC else {return}
                self.navigationController?.pushViewController(convenienceStoreViewController, animated: true)
                break
                
            case 3:
                
                guard let carpoolViewController = CAR_POOL_STORYBOARD.instantiateViewController(withIdentifier: "CarpoolVC") as? CarpoolVC else {return}
                self.navigationController?.pushViewController(carpoolViewController, animated: true)
                break
                
            case 5:
                
                guard let sportsCourtViewController = SPORTS_COURT_STORYBOARD.instantiateViewController(withIdentifier: "SportsCourtVC") as? SportsCourtVC else {return}
                self.navigationController?.pushViewController(sportsCourtViewController, animated: true)
                break
                
            case 6:
                
                guard let foodCourtViewController = FOODCOURT_STORYBOARD.instantiateViewController(withIdentifier: "FoodCourtVC") as? FoodCourtVC else {return}
                navigationController?.pushViewController(foodCourtViewController, animated: true)
                break
                
            case 7:
                
                guard let exclusiveServiceViewController = EXCLUSIVE_SERVICE_STORYBOARD.instantiateViewController(withIdentifier: "ExclusiveHomeVC") as? ExclusiveHomeVC else {return}
                navigationController?.pushViewController(exclusiveServiceViewController, animated: true)
                break
                
            case 8:
                
                guard let laundryViewController = LAUNDRY_STORYBOARD.instantiateViewController(withIdentifier: "LaundryVC") as? LaundryVC else {return}
                navigationController?.pushViewController(laundryViewController, animated: true)
                break
                
            case 9:
                
                guard let amphitheatreViewController = AMPHITHEATRE_STORYBOARD.instantiateViewController(withIdentifier: "AmphiTheatreVC") as? AmphiTheatreVC else {return}
                navigationController?.pushViewController(amphitheatreViewController, animated: true)
                
            case 10:
                
                guard let healthSpaViewController = HEALTH_SPA_STORYBOARD.instantiateViewController(withIdentifier: "HealthSpaSelectionVC") as? HealthSpaSelectionVC else {return}
                navigationController?.pushViewController(healthSpaViewController, animated: true)
                
            default:
                return
                
            }
            
            
        default:
            return
        }
        
        
        
    }
    
    
    
    
}

//MARK: OTHER REQUIRED METHODS
extension HomePageVC {
    
    @objc func changeSliderImages() {
        
        if counter < sliderImageArray.count
        {
            let index = IndexPath.init(item: counter, section: 0)
            imageSliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            sliderPageView.currentPage = counter
            counter += 1
        }
        else
        {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            imageSliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            sliderPageView.currentPage = counter
            counter = 1
        }
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension HomePageVC {
    
    //MARK: GET BANNERS LIST
    func getSliderBanners() {
        
        self.view.makeToastActivity(.center)
        
        HomePage_Services.instance.getBannerListService { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        
                        self.sliderImageArray = returnedResponse.results
                        
                        DispatchQueue.main.async {
                            self.imageSliderCollectionView.reloadData()
                            self.sliderPageView.numberOfPages = self.sliderImageArray.count
                            self.sliderPageView.currentPage = 0
                            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeSliderImages), userInfo: nil, repeats: true)
                            self.view.hideToastActivity()
                        }
                        
                    }else {
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
        }
    }
    
    //MARK: GET SERVICES LIST
    func getServicesList(withCoworkerId id: Int) {
        
        self.view.makeToastActivity(.center)
        
        HomePage_Services.instance.getHomepageServices(withCoworkerId: id) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                    
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.servicesArray = returnedResponse.results
                        //print(self.servicesArray)
                        DispatchQueue.main.async {
                            self.servicesCollectionView.reloadData()
                            self.servicesOuterViewForCollectionView.isHidden = self.servicesArray.isEmpty == true ? true : false
                            self.view.hideToastActivity()
                        }
                        
                        
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                            
                        }
                    }
                }
                
            }
            
        }
    }
    
    //MARK: GET FAVOURITES LIST SERVICE
    func getFavouritesList(withCoworkerId id: Int) {
        
        self.view.makeToastActivity(.center)
        
        HomePage_Services.instance.getHomepageFavouritesService(withCoworkerId: id) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.favouritesArray = returnedResponse.results
                        
                        DispatchQueue.main.async {
                            self.favouritesCollectionView.reloadData()
                            self.favouritesOuterViewForCollectionView.isHidden = self.favouritesArray.isEmpty == true ? true : false
                            //Favourite Edit Button Check
                            var favEditButtonTitle = ""
                            favEditButtonTitle = self.favouritesArray.isEmpty == true ? "Add Favourites" : "Edit Favourites"
                            self.favouriteEditButton.setTitle(favEditButtonTitle, for: .normal)
                            self.view.hideToastActivity()
                        }
                        
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
    }
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                DispatchQueue.main.async {
                    debugPrint(error.localizedDescription)
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count
                                DispatchQueue.main.async {
                                    
                                    if detail.itemsInCart != 0 {
                                        self.cartButton.setTitle("\(detail.itemsInCart)", for: .normal)
                                    } else {
                                        self.cartButton.setTitle("", for: .normal)
                                    }
                                    
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
}
