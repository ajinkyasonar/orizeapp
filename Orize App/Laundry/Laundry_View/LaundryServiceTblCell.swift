//
//  LaundryServiceTblCell.swift
//  Orize App
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol LaundryServiceTblCellDelegate: class {
    func didTapAddRemoveServiceButton(at index: Int, serviceDetails: LaundryServiceList)
}

class LaundryServiceTblCell: UITableViewCell {
    
    @IBOutlet weak var addRemoveBtn: UIButton!
    @IBOutlet weak var serviceNameLbl: UILabel!
    @IBOutlet weak var servicePriceLbl: UILabel!
    
    weak var delegate: LaundryServiceTblCellDelegate?
    private var selectedLaundryService: LaundryServiceList!
    private var selectedLaundryServiceIndex: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(laundryService: LaundryServiceList, delegate: LaundryServiceTblCellDelegate, selectedServiceIndex: Int) {
        
        self.selectedLaundryService = laundryService
        self.delegate = delegate
        self.selectedLaundryServiceIndex = selectedServiceIndex
        
        serviceNameLbl.text = laundryService.laundryServiceName
        servicePriceLbl.text = "\(rupee) \(laundryService.laundryServicePrice ?? 0)"
        
        /*
         if laundryService.cartId == nil {
         addRemoveBtn.setImage(UIImage(named: "plus"), for: .normal)
         }
         else {
         if let cartId = laundryService.cartId {
         if cartId == 0 {
         addRemoveBtn.setImage(UIImage(named: "plus"), for: .normal)
         }else {
         addRemoveBtn.setImage(UIImage(named: "minus"), for: .normal)
         }
         }
         }
         */
        
        
       
         if laundryService.isSelected {
         addRemoveBtn.setImage(UIImage(named: "minus"), for: .normal)
         }
         else {
         addRemoveBtn.setImage(UIImage(named: "plus"), for: .normal)
         }
        
        
        
        
    }
    
    
    @IBAction func addRemoveLaundryServices(_ sender: UIButton) {
        delegate?.didTapAddRemoveServiceButton(at: selectedLaundryServiceIndex, serviceDetails: selectedLaundryService)
        
    }
    
}
