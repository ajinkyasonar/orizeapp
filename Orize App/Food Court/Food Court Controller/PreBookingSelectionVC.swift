//
//  PreBookingSelectionVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 09/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import FSCalendar
import Toast_Swift

class PreBookingSelectionVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendarModeToggleBtn: UIButton!
    
    @IBOutlet weak var topCartButton: UIButton!
    @IBOutlet weak var productQuantityLbl: UILabel!
    @IBOutlet weak var foodItemNameLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    
    //Timings Outlets
    @IBOutlet weak var timingCollectionView: UICollectionView!
    @IBOutlet weak var timingCollectionViewHeightConstraint: NSLayoutConstraint!
    
    
    //Variables
    let coworkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var productDetails: FoodCourtProductsDetails!
    var searchProductDetails: SearchAutocompleteData?
    var editProductDetails: ProductCartList!
    var dateStringArray = [String]()
    let formatter = DateFormatter()
    var productQuantityCounter = 0
    var selectedDatesJsonString = ""
    var selectedDeliveryTime = ""
    var isDeliverTimeSelected = false
    
    var selectedSubscriptionDates = [String]()
    var selectedDatesDictArr = [[String: Any]]()
    
    //Array For Multiple Time Slot Selection
    var timeSlotSelectionarray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Check Previous Selected Dates if matched then selected on Collection View
        if editProductDetails != nil {
            
            for timeSlots in timeSlotSelectionarray {
                
                if let index = editProductDetails.foodCourtDeliveryTimeList?.lastIndex(where: {$0.deliveryTime == timeSlots}) {
                    //print(index)
                    timingCollectionView.selectItem(at: IndexPath(item: index, section: 0), animated: false, scrollPosition: .top)
                }
                
            }
            
        }
        
    }
    
    private func initialSetup() {
        
        calendar.delegate = self
        calendar.dataSource = self
        
        timingCollectionView.delegate = self
        timingCollectionView.dataSource = self
        timingCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        timingCollectionView.allowsMultipleSelection = true
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        calendar.appearance.headerMinimumDissolvedAlpha = 0.0
        calendar.appearance.eventDefaultColor = UIColor.green
        
        calendar.scope = .month
        calendarModeToggleBtn.setTitle("Weekly Mode", for: .normal)
        
        //Checks which array is nil
        //Checks normal navigation
        if productDetails != nil {
            dateStringArray = productDetails.foodCourtDeliveryDatesList.compactMap({$0.date})
            
            foodItemNameLbl.text = "Selected Food Item: \(productDetails.productName)"
            productPriceLbl.text = "Price: \(rupee) \(productDetails.productPriceWithTax)"
        }
        //Checks Searched Product
        else if searchProductDetails != nil {
            
            guard let searchProductDetails = searchProductDetails, let searchProductdateList = searchProductDetails.foodCourtDeliveryDatesList else {return}
            
            dateStringArray = searchProductdateList.compactMap({$0.date})
            
            foodItemNameLbl.text = "Selected Food Item: \(searchProductDetails.name)"
            productPriceLbl.text = "Price: \(rupee) \(searchProductDetails.productPriceWithTax ?? 0)"
            
        }
            //Checks edit product
        else {
            dateStringArray = editProductDetails.foodCourtDeliveryDatesList?.compactMap({$0.date}) ?? [""]
            productQuantityCounter = editProductDetails.productQty
            
            //Add Previous Subscribed Dates into selectedSubscriptionDates
            selectedSubscriptionDates = editProductDetails.subscriptionDate?.compactMap({$0.subscriptionDate}) ?? [""]
            
            //Test Previous Selected Timings
            timeSlotSelectionarray = editProductDetails.deliveryTime?.components(separatedBy: ",") ?? []
            
            //Manages Previous Dates that are subscribed by user and marks them selected in calendar
            selectedSubscriptionDates.forEach { (date) in
                let previousDates = formatter.date(from: date)
                calendar.select(previousDates)
            }
            
            foodItemNameLbl.text = "Selected Food Item: \(editProductDetails.productName)"
            productPriceLbl.text = "Price: \(rupee) \(editProductDetails.productPriceWithTax ?? 0)"
        }
        
        productQuantityLbl.text = "\(productQuantityCounter)"
        
        getEmployeeDetails(employeeId: coworkerEmployeeId)
        
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        timingCollectionView.layer.removeAllAnimations()
        timingCollectionViewHeightConstraint.constant = timingCollectionView.contentSize.height
        self.updateViewConstraints()
        self.view.layoutIfNeeded()
    }
    
    
    //MARK: IB-ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func cartBtnTapped(_ sender: UIButton) {
        guard let cartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        self.navigationController?.pushViewController(cartVc, animated: true)
    }
    
    @IBAction func doneBtnTapped(_ sender: UIButton) {
        //If product array is nil then goes to editproduct array
        //Here API Calls are done.
        
        selectedDatesJsonString = getSelectedDatesJson()
        selectedDeliveryTime = timeSlotSelectionarray.joined(separator: ",")
        
        if productDetails != nil {
            
            if (selectedSubscriptionDates.count > 0 && timeSlotSelectionarray.count > 0 && productQuantityCounter > 0) {
                
                add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coworkerEmployeeId, foodCourtProductId: productDetails.productID, productQuantity: productQuantityCounter, subscriptionDate: selectedDatesJsonString, deliveryTime: selectedDeliveryTime)
                
            }else {
                self.view.makeToast("Dates, Time slot selection & Quantity required", duration: 2.0, position: .bottom)
            }
            
        }
            //Search Product Logic
        else if searchProductDetails != nil {
            
            if (selectedSubscriptionDates.count > 0 && timeSlotSelectionarray.count > 0 && productQuantityCounter > 0) {
                
                add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coworkerEmployeeId, foodCourtProductId: searchProductDetails?.id ?? 0, productQuantity: productQuantityCounter, subscriptionDate: selectedDatesJsonString, deliveryTime: selectedDeliveryTime)
                
            }else {
                self.view.makeToast("Dates, Time slot selection & Quantity required", duration: 2.0, position: .bottom)
            }
            
        }
            
        else {
            
            if (selectedSubscriptionDates.count > 0 && timeSlotSelectionarray.count > 0) {
                
                add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coworkerEmployeeId, foodCourtProductId: editProductDetails.productID, productQuantity: productQuantityCounter, subscriptionDate: selectedDatesJsonString, deliveryTime: selectedDeliveryTime)
                
            }else {
                
                self.view.makeToast("Dates & Time slot selection Mandatory", duration: 2.0, position: .bottom)
            }
            
            
        }
        
    }
    
    @IBAction func calendarModeToggleBtnTapped(_ sender: UIButton) {
        
        if calendar.scope == .month {
            calendar.scope = .week
            calendarModeToggleBtn.setTitle("Monthly Mode", for: .normal)
        } else {
            calendar.scope = .month
            calendarModeToggleBtn.setTitle("Weekly Mode", for: .normal)
        }
        
        
    }
    
    //MARK: ADD & REMOVE PRODUCT FUNCTIONS
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        if selectedSubscriptionDates.count > 0 && timeSlotSelectionarray.count > 0 {
            //If product array is nil then goes to editproduct array
            if productDetails != nil {
                
                if productQuantityCounter < productDetails.productAllowedQtyInCart {
                    productQuantityCounter += 1
                    productQuantityLbl.text = "\(productQuantityCounter)"
                }else {
                    self.view.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
                }
            }
               //Search Product
            else if searchProductDetails != nil {
                
                guard let allowedQuantity = searchProductDetails?.productAllowedQtyInCart else {return}
                if productQuantityCounter < allowedQuantity {
                    productQuantityCounter += 1
                    productQuantityLbl.text = "\(productQuantityCounter)"
                }else {
                    self.view.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
                }
                
            }
            
            else {
                
                if productQuantityCounter < editProductDetails.productAllowedQtyInCart {
                    productQuantityCounter += 1
                    productQuantityLbl.text = "\(productQuantityCounter)"
                }else {
                    self.view.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
                }
            }
            //Removed Add Remove Api Call From this Position - Reference
            selectedDatesJsonString = getSelectedDatesJson()
            
        } else {
            let message = "Subscription dates & time are mandatory"
            self.view.makeToast(message, duration: 2.0, position: .bottom)
        }
        
        
    }
    
    @IBAction func removeButtonTapped(_ sender: UIButton) {
        
        if selectedSubscriptionDates.count > 0 && timeSlotSelectionarray.count > 0 {
            
            if productQuantityCounter > 0 {
                productQuantityCounter -= 1
                productQuantityLbl.text = "\(productQuantityCounter)"
            }else {
                self.view.makeToast("Lowest Count", duration: 1.0, position: .bottom)
            }
            
            selectedDatesJsonString = getSelectedDatesJson()
            
        } else {
            let message = "Subscription dates & time are mandatory"
            self.view.makeToast(message, duration: 2.0, position: .bottom)
        }
        
        
    }
    
}

//MARK: CALENDAR DELEGATE & DATA SOURCE IMPLEMENTATION
extension PreBookingSelectionVC: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        guard let endDate = Calendar.current.date(byAdding: .month, value: 1, to: Date(), wrappingComponents: false) else {return Date()}
        
        return endDate
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant =  bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let calendarDateString = formatter.string(from: date)
        
        if dateStringArray.contains(calendarDateString) {
            return 1
        } else {
            return 0
        }
        
    }
    
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        let selectedDate = formatter.string(from: date)
        
        if (dateStringArray.contains(selectedDate)) && (selectedSubscriptionDates.count <= 6) {
            return true
        } else {
            self.view.makeToast("Allowed to select only 7 days from available dates.", duration: 2.0, position: .bottom)
            return false
        }
        
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        //Allow only array containing dates to be selected
        
        let selectedDate = formatter.string(from: date)
        
        if productDetails != nil {
            
            if dateStringArray.contains(selectedDate) {
                selectedSubscriptionDates.append(selectedDate)
            } else {
                //Show Toast Message
                self.view.makeToast("Select from available dates list only.", duration: 2.0, position: .bottom)
            }
            
        }
        
        else if searchProductDetails != nil {
            
            if dateStringArray.contains(selectedDate) {
                selectedSubscriptionDates.append(selectedDate)
            } else {
                //Show Toast Message
                self.view.makeToast("Select from available dates list only.", duration: 2.0, position: .bottom)
            }
            
        }
        
        else {
            
            if selectedSubscriptionDates.contains(selectedDate) {
                return
            } else {
                selectedSubscriptionDates.append(selectedDate)
            }
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let deselectedDate = formatter.string(from: date)
        
        if let index = selectedSubscriptionDates.lastIndex(of: deselectedDate) {
            selectedSubscriptionDates.remove(at: index)
        }
        
    }
    
}

//MARK: TIMINGS COLLECTION VIEW IMPLEMENTATION
extension PreBookingSelectionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if productDetails != nil {
            return productDetails.foodCourtDeliveryTimeList.count
        }
        else if searchProductDetails != nil {
            return searchProductDetails?.foodCourtDeliveryTimeList?.count ?? 0
        }
        else {
            return editProductDetails.foodCourtDeliveryTimeList?.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if productDetails != nil {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreBookingTimingCell", for: indexPath) as? PreBookingTimingCell else {return UICollectionViewCell()}
            
            cell.configureCell(deliveryTimings: productDetails.foodCourtDeliveryTimeList[indexPath.item])
            
            return cell
        }
            
        else if searchProductDetails != nil {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreBookingTimingCell", for: indexPath) as? PreBookingTimingCell else {return UICollectionViewCell()}
            
            if let foodDeliverTimeList = searchProductDetails?.foodCourtDeliveryTimeList {
               cell.configureCell(deliveryTimings: foodDeliverTimeList[indexPath.item])
            }
            
            return cell
            
        }
        
        else {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreBookingTimingCell", for: indexPath) as? PreBookingTimingCell else {return UICollectionViewCell()}
            
            if let foodDeliveryTimeList = editProductDetails.foodCourtDeliveryTimeList {
                
                cell.configureCell(deliveryTimings: foodDeliveryTimeList[indexPath.item])
            }
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.frame.size.width
        let cellWidth = (collectionViewWidth - 30) / 2
        
        return CGSize(width: cellWidth, height: 50)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        isDeliverTimeSelected = true
        
        if productDetails != nil {
            
            selectedDeliveryTime = productDetails.foodCourtDeliveryTimeList[indexPath.item].deliveryTime
            
            if let selectedTime = timeSlotSelectionarray.lastIndex(of: selectedDeliveryTime) {
                timeSlotSelectionarray.remove(at: selectedTime)
            }else {
                timeSlotSelectionarray.append(selectedDeliveryTime)
            }
            
        }
        //Search product
        else if searchProductDetails != nil {
            
            selectedDeliveryTime = searchProductDetails?.foodCourtDeliveryTimeList?[indexPath.item].deliveryTime ?? ""
            
            if let selectedTime = timeSlotSelectionarray.lastIndex(of: selectedDeliveryTime) {
                timeSlotSelectionarray.remove(at: selectedTime)
            }else {
                timeSlotSelectionarray.append(selectedDeliveryTime)
            }
            
        }
        
        else {
            
            selectedDeliveryTime = editProductDetails.foodCourtDeliveryTimeList?[indexPath.item].deliveryTime ?? ""
            
            if let selectedTime = timeSlotSelectionarray.lastIndex(of: selectedDeliveryTime) {
                timeSlotSelectionarray.remove(at: selectedTime)
            }else {
                timeSlotSelectionarray.append(selectedDeliveryTime)
            }
            
            
        }
        
        //print(timeSlotSelectionarray)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if productDetails != nil {
            let deSelectedDeliveryTime = productDetails.foodCourtDeliveryTimeList[indexPath.item].deliveryTime
            
            if let deselectedTime = timeSlotSelectionarray.lastIndex(of: deSelectedDeliveryTime) {
                timeSlotSelectionarray.remove(at: deselectedTime)
            }
            
        }
        
        else if searchProductDetails != nil {
            
            let deSelectedDeliveryTime = searchProductDetails?.foodCourtDeliveryTimeList?[indexPath.item].deliveryTime
            
            if let deselectedTime = timeSlotSelectionarray.lastIndex(of: deSelectedDeliveryTime ?? "") {
                timeSlotSelectionarray.remove(at: deselectedTime)
            }
            
        }
        
        else {
            let deSelectedDeliveryTime = editProductDetails.foodCourtDeliveryTimeList?[indexPath.item].deliveryTime
            
            if let deselectedTime = timeSlotSelectionarray.lastIndex(of: deSelectedDeliveryTime ?? "") {
                timeSlotSelectionarray.remove(at: deselectedTime)
            }
            
        }
        
        //print(timeSlotSelectionarray)
        
    }
    
}

extension PreBookingSelectionVC {
    
    func getSelectedDatesJson() -> String {
        
        var returnedJsonString = ""
        selectedDatesDictArr.removeAll()
        
        selectedSubscriptionDates.forEach { (date) in
            selectedDatesDictArr.append(["subscription_date": date, "subscription_status": 1])
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: selectedDatesDictArr, options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            returnedJsonString = jsonString
            
        } catch {
            self.view.makeToast(error.localizedDescription, duration: 1.0, position: .bottom)
            print(error.localizedDescription)
        }
        
        return returnedJsonString
    }
    
}


//MARK: API IMPLEMENTATION
extension PreBookingSelectionVC {
    
    
    //ADD REMOVE FOODCOURT CART API
    
    func add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: Int, foodCourtProductId: Int, productQuantity: Int, subscriptionDate: String, deliveryTime: String) {
        
        FoodCourtServices.instance.addRemoveFoodcourtCartItem(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: productQuantity, subscriptionDate: subscriptionDate, deliveryTime: deliveryTime) { (returnedResponse, error) in
            
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        DispatchQueue.main.async {
                            //Update Count on Cart
                            self.getEmployeeDetails(employeeId: coWorkersEmployeeId)
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else {
                        //Failure
                        print(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count
                                DispatchQueue.main.async {
                                    self.topCartButton.setTitle("\(detail.itemsInCart)", for: .normal)
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
