//
//  FoodCategoriesVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class FoodCategoriesVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var categoryNameLbl: UILabel!
    
    //Variables
    var foodProductDetailsArr = [FoodCourtProductsDetails]()
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //Searching Variables
    var isSearching = false
    var foodProductDetailsFilterArray = [FoodCourtProductsDetails]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initialSetup()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    private func initialSetup() {
        
        getEmployeeDetails(employeeId: coWorkerEmployeeId)
        categoryNameLbl.text = foodProductDetailsArr.first?.foodCourtCategoryName
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: FoodCourtRegisteredXibs.IndividualProductCatCell, bundle: nil), forCellWithReuseIdentifier: FoodCourtRegisteredXibs.IndividualProductCatCell)
        
        collectionView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell, bundle: nil), forCellWithReuseIdentifier: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell)
        
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
    }
    
    //MARK: IB-ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cartBtnTapped(_ sender: UIButton) {
        
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        
        self.navigationController?.pushViewController(commonCartVc, animated: true)
        
    }

}

//MARK: COLLECTION VIEW IMPLEMENTATION
extension FoodCategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return isSearching ? foodProductDetailsFilterArray.count : foodProductDetailsArr.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isSearching {
            
            let product = foodProductDetailsFilterArray[indexPath.row]
            
            if product.isPrebookingCategory == 1 {
                //print("Requires Pre-Booking Cell")
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodCourtPrebookingCollCell", for: indexPath) as? FoodCourtPrebookingCollCell else {return UICollectionViewCell()}
                cell.configureCell(product: product, delegate: self, productIndex: indexPath.item)
                return cell
            } else {
                //print("Requires Normal Cell")
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodCourtRegisteredXibs.IndividualProductCatCell, for: indexPath) as? IndividualProductCatCell else {return UICollectionViewCell()}
                cell.configureCell(product: product, productInt: indexPath.row, delegate: self)
                return cell
            }
            
        } else {
            
            let product = foodProductDetailsArr[indexPath.row]
            
            if product.isPrebookingCategory == 1 {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodCourtPrebookingCollCell", for: indexPath) as? FoodCourtPrebookingCollCell else {return UICollectionViewCell()}
                cell.configureCell(product: product, delegate: self, productIndex: indexPath.item)
                return cell
            } else {
                //print("Required Normal Cell For Normal Load")
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodCourtRegisteredXibs.IndividualProductCatCell, for: indexPath) as? IndividualProductCatCell else {return UICollectionViewCell()}
                cell.configureCell(product: product, productInt: indexPath.row, delegate: self)
                return cell
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isSearching {
            let product = foodProductDetailsFilterArray[indexPath.item]
            return configureCollectionCellHeight(product: product, collectionView: collectionView)
            
        } else {
            
            let product = foodProductDetailsArr[indexPath.item]
            return configureCollectionCellHeight(product: product, collectionView: collectionView)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let foodProductVc = storyboard?.instantiateViewController(withIdentifier: "FoodItemDetailVC") as? FoodItemDetailVC else {return}
        
        if isSearching {
            foodProductVc.selectedFoodProductDetails = foodProductDetailsFilterArray[indexPath.row]
        } else {
            foodProductVc.selectedFoodProductDetails = foodProductDetailsArr[indexPath.row]
        }
        
        foodProductVc.delegate = self
        foodProductVc.modalPresentationStyle = .overCurrentContext
        foodProductVc.modalTransitionStyle = .crossDissolve
        navigationController?.present(foodProductVc, animated: true, completion: nil)
        
    }
    
}


//MARK: DELEGATE IMPLEMENTATION
extension FoodCategoriesVC: IndividualProductCatCellDelegate {
    
    func addBtnTapped(product: FoodCourtProductsDetails, productInt: Int, productQuantity: Int) {
        add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: product.productID, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
    }
    
    func minusBtnTapped(product: FoodCourtProductsDetails, productInt: Int, productQuantity: Int) {
        add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: product.productID, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
    }
    
    
}

//MARK: FOOD COURT PREBOOKING CELL DELEGATE IMPLEMENTATION
extension FoodCategoriesVC: FoodCourtPrebookingCollCellDelegate {
    
    func didTapBookButton(productDetail: FoodCourtProductsDetails, atTag tag: Int) {
        
        guard let preBookSelectionVc = storyboard?.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
        preBookSelectionVc.productDetails = productDetail
        self.navigationController?.pushViewController(preBookSelectionVc, animated: true)
 
    }
}

//MARK: TextFiled Delegate & Search Implementation
extension FoodCategoriesVC: UITextFieldDelegate {
    
    //Search Functionality Implementation & Filter
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        isSearching = false
        collectionView.reloadData()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if searchTextField.text != "" {
            
            isSearching = true
            
            foodProductDetailsFilterArray = foodProductDetailsArr.filter({$0.productName.lowercased().contains(textField.text!.lowercased())})
            
            collectionView.reloadData()
            
        } else {
            
            isSearching = false
            collectionView.reloadData()
            searchTextField.resignFirstResponder()
            
        }
        
        
    }
    
}

//MARK: FOOD ITEM DETAIL VIEW CONTROLLER DELEGATE
extension FoodCategoriesVC: FoodItemDetailVCDelegate {
    
    func addRemoveFoodCourtCartProduct(product: FoodCourtProductsDetails, productQuantity: Int) {
        add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: product.productID, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
    }
}


//MARK: API IMPLEMENTATION
extension FoodCategoriesVC {
   
    //ADD REMOVE FOODCOURT CART API
    
    func add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: Int, foodCourtProductId: Int, productQuantity: Int, subscriptionDate: String, deliveryTime: String) {
        
        FoodCourtServices.instance.addRemoveFoodcourtCartItem(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: productQuantity, subscriptionDate: subscriptionDate, deliveryTime: deliveryTime) { (returnedResponse, error) in
            
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        DispatchQueue.main.async {
                            //Update Count on Cart
                            self.getEmployeeDetails(employeeId: coWorkersEmployeeId)
                        }
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            print(returnedResponse.message)
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count
                                DispatchQueue.main.async {
                                    
                                    if detail.itemsInCart != 0 {
                                        self.cartBtn.setTitle("\(detail.itemsInCart)", for: .normal)
                                    } else {
                                        self.cartBtn.setTitle("", for: .normal)
                                    }
                                    
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

//MARK: Additional Methods for Simplification
extension FoodCategoriesVC {
    
    //This Method is to avoid duplication height code for collection view cell
    private func configureCollectionCellHeight(product: FoodCourtProductsDetails, collectionView: UICollectionView) -> CGSize {
        
        if product.isPrebookingCategory == 1 {
            let width = collectionView.frame.size.width
            let cellWidth = (width) / 2
            let cellHeight = (cellWidth * 1.5)
            return CGSize(width: cellWidth, height: cellHeight)
        } else {
            let width = collectionView.frame.size.width
            let cellWidth = (width - 15) / 2
            let cellHeight = (cellWidth * 1.3)
            return CGSize(width: cellWidth, height: cellHeight)
        }
        
    }
    
}
