//
//  ConvenienceCartTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 26/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol ConvenienceCartTblCellDelegate: class {
    
    func addButtonTappedConvCart(productDetails: ProductCartList, productIndex: Int, productQuantity: Int)
    
    func removeButtonTappedConvCart(productDetails: ProductCartList, productIndex: Int, productQuantity: Int)
}

class ConvenienceCartTblCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var itemImage: UIImageView!
    @IBOutlet private weak var itemTitleLbl: UILabel!
    @IBOutlet private weak var itemDescriptionLbl: UILabel!
    @IBOutlet private weak var itemPriceLbl: UILabel!
    @IBOutlet private weak var unitCountLbl: UILabel!
    
    //Delegate
    weak var delegate: ConvenienceCartTblCellDelegate?
    private var productDetails: ProductCartList!
    private var productIndex: Int!
    
    //Variables
    var counter = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        
        elementView.layer.cornerRadius = 10
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
        
        itemImage.layer.cornerRadius = 10
        itemImage.layer.borderColor = UIColor.black.cgColor
        itemImage.layer.borderWidth = 0.3
        
    }

    func configureCell(productDetails: ProductCartList, delegate: ConvenienceCartTblCellDelegate, productIndex: Int) {
        
        
        self.delegate = delegate
        self.productDetails = productDetails
        self.productIndex = productIndex
        
        guard let productImageUrl = URL(string: productDetails.productPhoto) else {return}
        itemImage.sd_setImage(with: productImageUrl)
        
        itemTitleLbl.text = productDetails.productName
        let productDescpritionTxt = productDetails.productDescription
        itemDescriptionLbl.text = productDescpritionTxt
        itemPriceLbl.text = "\(rupee) \(productDetails.productPrice)"
        unitCountLbl.text = "\(productDetails.productQty)"
        counter = productDetails.productQty
        
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        guard let productDetails = productDetails else {return}
        
        if counter < productDetails.productAllowedQtyInCart {
            counter += 1
            unitCountLbl.text = "\(counter)"
            delegate?.addButtonTappedConvCart(productDetails: productDetails, productIndex: productIndex, productQuantity: counter)
        }else {
            self.makeToast("Max Product Quantity Reached", duration: 1.0, position: .center)
        }
        
    }
    
    @IBAction func removeBtnTapped(_ sender: UIButton) {
        
        if counter > 0 {
            counter -= 1
            unitCountLbl.text = "\(counter)"
            delegate?.removeButtonTappedConvCart(productDetails: productDetails, productIndex: productIndex, productQuantity: counter)
        }else {
            
            self.makeToast("Lowest Count", duration: 1.0, position: .bottom)
        }
        
    }
    
}
