//
//  FoodCourtProducts.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// MARK: - FoodCourtProduct
struct FoodCourtProduct: Codable {
    let code: Int
    let status, message: String
    let foodCourtPrebookingDays: Int
    let results: [FoodCourtProductResult]
    
    enum CodingKeys: String, CodingKey {
        case code, status, message
        case foodCourtPrebookingDays = "food_court_prebooking_days"
        case results
    }
    
}

// MARK: - Result
struct FoodCourtProductResult: Codable {
    let vendorsName: String
    let searchAutocompleteData: [SearchAutocompleteData]
    let productData: [ProductData]
    
    enum CodingKeys: String, CodingKey {
        case vendorsName = "vendors_name"
        case searchAutocompleteData = "search_autocomplete_data"
        case productData = "product_data"
    }
}

// MARK: - ProductDatum
struct ProductData: Codable {
    let foodCourtCategoryID: Int
    let foodCourtCategoryName: String
    let isPrebookingCategory: Int
    let productsListArr: [FoodCourtProductsDetails]
    
    enum CodingKeys: String, CodingKey {
        case foodCourtCategoryID = "food_court_category_id"
        case foodCourtCategoryName = "food_court_category_name"
        case productsListArr = "products_list_arr"
        case isPrebookingCategory = "is_prebooking_category"
    }
}


// MARK: - ProductsListArr
 class FoodCourtProductsDetails: Codable {
    let productID: Int
    let foodCourtCategoryName: String
    let productName: String
    let productPhoto: String
    let productPrice, tax, productPriceWithTax: Int
    let priceStability: PriceStability
    let productUnit, productDescription: String
    let productAllowedQtyInCart: Int
    
    //PRE-BOOKING KEYS
    let isPrebookingCategory: Int
    let foodCourtDeliveryDatesList: [FoodCourtDeliveryDatesList]
    let foodCourtDeliveryTimeList: [FoodCourtDeliveryTimeList]
    
    var productCounter: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case foodCourtCategoryName = "food_court_category_name"
        case productName = "product_name"
        case productPhoto = "product_photo"
        case productPrice = "product_price"
        case tax
        case productPriceWithTax = "product_price_with_tax"
        case priceStability = "price_stability"
        case productUnit = "product_unit"
        case productDescription = "product_description"
        case productAllowedQtyInCart = "product_allowed_qty_in_cart"
        
        //PRE BOOKING CASES
        case isPrebookingCategory = "is_prebooking_category"
        case foodCourtDeliveryDatesList = "food_court_delivery_dates_list"
        case foodCourtDeliveryTimeList = "food_court_delivery_time_list"
    }
    
    enum PriceStability: String, Codable {
        case nochange = "nochange"
        case up = "up"
        case down = "down"
    }
    
    
}
    
    //PRE-BOOKING
    // MARK: - FoodCourtDeliveryDatesList
    struct FoodCourtDeliveryDatesList: Codable {
        let date: String
    }
    
    // MARK: - FoodCourtDeliveryTimeList
    struct FoodCourtDeliveryTimeList: Codable {
        let foodCourtProductsDeliveryTimingsID: Int
        let deliveryTime: String
        
        enum CodingKeys: String, CodingKey {
            case foodCourtProductsDeliveryTimingsID = "food_court_products_delivery_timings_id"
            case deliveryTime = "delivery_time"
        }
    }


// MARK: - SearchAutocompleteDatum
struct SearchAutocompleteData: Codable {
    
    let id: Int
    let name: String
    let vendorName: String?
    let venderId: Int? = -1
    let photo: String?
    let typeval: Int
    let typename: String
    let categoryID: Int?
    let categoryName: String?
    let categoryPhoto, productPhoto: String?
    let productPrice: Int?
    let priceStability, productUnit: String?
    let tax, productPriceWithTax: Int?
    let productDescription: String?
    let productAllowedQtyInCart, isPrebookingCategory: Int?
    let foodCourtDeliveryDatesList: [FoodCourtDeliveryDatesList]?
    let foodCourtDeliveryTimeList: [FoodCourtDeliveryTimeList]?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case vendorName = "vendor_name"
        case venderId = "food_court_details_id"
        case typeval, typename, photo
        case categoryID = "category_id"
        case categoryName = "category_name"
        case categoryPhoto = "category_photo"
        case productPhoto = "product_photo"
        case productPrice = "product_price"
        case priceStability = "price_stability"
        case productUnit = "product_unit"
        case tax
        case productPriceWithTax = "product_price_with_tax"
        case productDescription = "product_description"
        case productAllowedQtyInCart = "product_allowed_qty_in_cart"
        case isPrebookingCategory = "is_prebooking_category"
        case foodCourtDeliveryDatesList = "food_court_delivery_dates_list"
        case foodCourtDeliveryTimeList = "food_court_delivery_time_list"
    }
    
}
