//
//  EmployeeBookingServices.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class EmployeeBookingServices {
    static let instance = EmployeeBookingServices()
    
    //MARK: GET EMPLOYEE BOOKINGS SERVICE
    
    func getEmployeeBookings(coWorkerEmployeeId id: Int, bookingPeriodId: Int, completion:@escaping(EmployeeBookings?, Error?) ->()) {
        
        guard let url = URL(string: GET_EMPLOYEE_BOOKING_LIST_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(id)".data(using: String.Encoding.utf8)!)
        postData.append("&show_past_bookings=\(bookingPeriodId)".data(using: String.Encoding.utf8)!)
        
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(EmployeeBookings.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    //MARK: GET EMPLOYEE BOOKING DETAILS WITH ORDER ID
    
    func getEmployeeBookingDetails(byBookingNumber bookingNumber: String, coworkerEmployeeId: Int, andType type: String, completion: @escaping(EmployeeBookingOrderDetails?, Error?) ->()) {
        
        guard let url = URL(string: GET_EMPLOYEE_BOOKINGS_BY_ORDER_ID) else {return}
        
        let postData = NSMutableData(data: "booking_no=\(bookingNumber)".data(using: String.Encoding.utf8)!)
        postData.append("&co_works_employee_id=\(coworkerEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(EmployeeBookingOrderDetails.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    //MARK: EMPLOYEE ORDER CHANGE STATUS / CANCEL API
    func cancelChangeEmployeeBookingALLOrderStatus(withOrderId orderId: String, orderStatus status: Int, categoryType type: String, completion:@escaping(EmployeeBookingCancelOrder?,   Error?) ->()) {
        
        guard let url = URL(string: CANCEL_CHANGE_EMPLOYEE_BOOKING_ALL_ORDER_URL) else {return}
        
        let postData = NSMutableData(data: "common_order_id=\(orderId)".data(using: String.Encoding.utf8)!)
        postData.append("&order_status=\(status)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(EmployeeBookingCancelOrder.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
        
    }
    
    
}
