//
//  ExclusiveServiceImagesCollCell.swift
//  Orize App
//
//  Created by Apple on 14/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class ExclusiveServiceImagesCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    
    override var isSelected: Bool {
        
        didSet {
            
            if isSelected {
                
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.layer.borderColor = #colorLiteral(red: 0.01629221998, green: 0.3044524491, blue: 0.4406405687, alpha: 1)
                self.layer.borderWidth = 5.0
            }
            else {
                
                self.transform = CGAffineTransform.identity
                self.layer.borderColor = UIColor.clear.cgColor
                self.layer.borderWidth = 0.0
            }
            
        }
    }
    
    func configureCell(serviceDetail: ExclusiveServices, at index: Int) {
        
        guard let imageUrl = URL(string: serviceDetail.serviceImages[index]) else {return}
        self.imageView.sd_setImage(with: imageUrl, completed: .none)
        
    }

}
