//
//  EmployeeBookingsDetailsTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol EmployeeBookingDetailsTblCellDelegate: class {
    func trackItemsButtonTapped(orderDetails: OrderDetails, orderDetailTag: Int)
    func cancelOrderButtonTapped(orderDetails: OrderDetails, orderDetailTag: Int)
}

class EmployeeBookingsDetailsTblCell: UITableViewCell {
    
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productDescriptionLbl: UILabel!
    @IBOutlet private weak var quantityLbl: UILabel!
    @IBOutlet private weak var amountLbl: UILabel!
    @IBOutlet private weak var cancelBtn: UIButton!
    @IBOutlet private weak var trackBtn: UIButton!
    
    weak var delegate: EmployeeBookingDetailsTblCellDelegate?
    private var orderDetails: OrderDetails!
    private var orderDetailTag: Int!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        elementView.layer.cornerRadius = 10
        elementView.layer.borderWidth = 1.0
        elementView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        
        productImage.layer.cornerRadius = 10
        productImage.layer.borderWidth = 0.2
        productImage.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(orderDetails: OrderDetails, delegate: EmployeeBookingDetailsTblCellDelegate, orderDetailTag: Int) {
        
        self.orderDetails = orderDetails
        self.orderDetailTag = orderDetailTag
        self.delegate = delegate
        
        switch orderDetails.type {
            
        case .SportsBooking:
            
            guard let imageUrl = URL(string: orderDetails.photo) else {return}
            productImage.sd_setImage(with: imageUrl)
            productNameLbl.text = orderDetails.name
            productDescriptionLbl.text = orderDetails.orderDate
            quantityLbl.isHidden = true
            amountLbl.text = "Total - \(rupee) \(orderDetails.price)"
            
            cancelBtn.isHidden = orderDetails.isCancellable == false ? true : false
            trackBtn.isHidden = true
            
        default:
            
            guard let imageUrl = URL(string: orderDetails.photo) else {return}
            productImage.sd_setImage(with: imageUrl)
            productNameLbl.text = orderDetails.name
            productDescriptionLbl.text = orderDetails.description
            
            guard let quantity = orderDetails.quantity else {return}
            quantityLbl.text = "Quantity - \(quantity)"
            amountLbl.text = "Total - \(rupee) \(orderDetails.price)"
            
            cancelBtn.isHidden = orderDetails.isCancellable == false ? true : false
            
            //Track Button Hiding logic
            trackBtn.isHidden = orderDetails.type == .FoodCourt ? false : true
            
        }
        
        
    }
    
    @IBAction func trackItemBtnTapped(_ sender: UIButton) {
        delegate?.trackItemsButtonTapped(orderDetails: orderDetails, orderDetailTag: orderDetailTag)
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        delegate?.cancelOrderButtonTapped(orderDetails: orderDetails, orderDetailTag: orderDetailTag)
    }
    
}
