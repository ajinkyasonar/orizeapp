//
//  FavouriteServicesSelectionVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 14/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class FavouriteServicesSelectionVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Variables
    var servicesArray = [Services]()
    var previouslyFavouritedArray = [FavouriteService]()
    var coWorkerId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //Multiple Cell Selection Variables
    var selectedModuleArray = [Int]()
    var moduleIdsString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        getServicesList(withCoworkerId: coWorkerId)
        getFavourites(coworkerId: self.coWorkerId)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = true
        
        collectionView.register(UINib(nibName: HomePageRegisteredXibs.SelectedFavouriteCollCell, bundle: nil), forCellWithReuseIdentifier: HomePageRegisteredXibs.SelectedFavouriteCollCell)
        
    }
    
    //MARK: IB-ACTIONS
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callAddRemoveApiBtn(_ sender: UIButton) {
//        let uniqueSelectedModuleArray = selectedModuleArray.removingDuplicates()
//        moduleIdsString = (uniqueSelectedModuleArray.map{String($0)}).joined(separator: ",")
//        print(moduleIdsString)
        //addRemoveFavourites(coWorkerId: coWorkerId, moduleIds: moduleIdsString)
        
        //Append Items from Service Array And then remove duplicates
        
        for values in previouslyFavouritedArray {
            print(values.moduleID)
            selectedModuleArray.append(values.moduleID)
        }
        let unduplicatedFavArray = selectedModuleArray.removingDuplicates()
        print(unduplicatedFavArray)
        
    }

}

extension FavouriteServicesSelectionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return servicesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomePageRegisteredXibs.SelectedFavouriteCollCell, for: indexPath) as? SelectedFavouriteCollCell else {return UICollectionViewCell()}
        
        cell.configureCell(service: servicesArray[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.view.frame.width
        let cellWidth = (width) / 3
        let cellHeight = (cellWidth * 1.2)
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("Item Selected at Index: \(indexPath.item)")
        selectDeselectFavouriteCell(collectionView: collectionView, indexpath: indexPath)
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //print("Item Deselected at Index: \(indexPath.item)")
        selectDeselectFavouriteCell(collectionView: collectionView, indexpath: indexPath)
        
    }
    
    
    
}

//MARK: ADDITIONAL COLLECTION VIEW SELECTION METHODS
extension FavouriteServicesSelectionVC {
    
    //Select and Deselect Collection Cell
    
    func selectDeselectFavouriteCell(collectionView: UICollectionView, indexpath: IndexPath) {

        selectedModuleArray.removeAll()
        //print(selectedModuleArray)

        if let selectedItems = collectionView.indexPathsForSelectedItems {
            for index in selectedItems {
                
                if selectedModuleArray.contains(servicesArray[index.item].moduleID) {
                    guard let i = selectedModuleArray.lastIndex(of: servicesArray[index.item].moduleID) else {return}
                    selectedModuleArray.remove(at: selectedModuleArray[i])
                }else {
                    selectedModuleArray.append(servicesArray[index.item].moduleID)
                }
                
            }
        }
        print(selectedModuleArray)

    }
    
}


//MARK: API SERVICE IMPLEMENTATION
extension FavouriteServicesSelectionVC {
    
    //MARK: GET SERVICES LIST
    func getServicesList(withCoworkerId id: Int) {
        
        self.view.makeToastActivity(.center)
        
        HomePage_Services.instance.getHomepageServices(withCoworkerId: id) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.servicesArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                        }
                        
                        
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        }
                    }
                }
                
            }
            
        }
    }
    
    //Get Favourites Service
    func getFavourites(coworkerId: Int) {
        
        HomePage_Services.instance.getHomepageFavouritesService(withCoworkerId: coworkerId) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        self.previouslyFavouritedArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
            }
            
        }
        
    }
    
    
    
    
    //Add Remove Favourites Service
    func addRemoveFavourites(coWorkerId: Int, moduleIds: String) {
        
        HomePage_Services.instance.addRemoveFavourites(coworkerId: coWorkerId, moduleIds: moduleIds) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        DispatchQueue.main.async {
                            self.getServicesList(withCoworkerId: coWorkerId)
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
