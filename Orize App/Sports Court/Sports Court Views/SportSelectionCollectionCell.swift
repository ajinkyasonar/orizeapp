//
//  SportSelectionCollectionCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 08/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class SportSelectionCollectionCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = 10
        
    }
    
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var sportNameLbl: UILabel!
    
    
    func configureCell(data: SportsTypeListResult) {
        
        guard let sportImageUrl = URL(string: data.photo) else {return}
        sportImage.sd_setImage(with: sportImageUrl)
        
        sportNameLbl.text = data.name
    }
    
}
