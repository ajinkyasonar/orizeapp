//
//  FoodCourtVender.swift
//  Orize App
//
//  Created by Apple on 06/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// MARK: - VenderList
struct VenderList: Codable {
    let code: Int
    let status, message: String
    let results: [VenderDetail]
}

// MARK: - Result
struct VenderDetail: Codable {
    let foodCourtVendorID: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case foodCourtVendorID = "food_court_vendor_id"
        case name
    }
}
