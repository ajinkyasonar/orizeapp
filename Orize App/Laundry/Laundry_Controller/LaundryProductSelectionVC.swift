//
//  LaundryProductSelectionVC.swift
//  Orize App
//
//  Created by Apple on 22/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import Toast_Swift

protocol LaundryProductSelectionVCDelegate: class {
    func laundryPopupDismissed()
}

class LaundryProductSelectionVC: UIViewController {
    
    @IBOutlet weak var serviceTableView: UITableView!
    @IBOutlet weak var serviceTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var productDisplayImage: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var selectedProductQuantityLbl: UILabel!
    
    @IBOutlet weak var serviceInfoView: UIView!
    @IBOutlet weak var selectedServicesLbl: UILabel!
    @IBOutlet weak var totalCostLbl: UILabel!
    
    //Received Data
    var laundryProduct: LaundryProducts!
    var editLaundryProduct: ProductCartList!
    let coworkerId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //Variables
    var laundryServicesList = [LaundryServiceList]()
    var laundryServiceBooking = [LaundryServiceList]()
    var selectedServicesString = ""
    var totalServicePrice = 0
    var productCounter = 0
    
    
    //Delegate
    weak var delegate: LaundryProductSelectionVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        serviceTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    private func initialSetup() {
        
        //print("Normal Laundry Product: \(laundryProduct)")
        //print("Edit Laundry Product: \(editLaundryProduct)")
        
        serviceTableView.delegate = self
        serviceTableView.dataSource = self
        serviceTableView.register(UINib(nibName: LaundryXibs.LaundryServiceTblCell, bundle: nil), forCellReuseIdentifier: LaundryXibs.LaundryServiceTblCell)
        serviceTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        if laundryProduct != nil {
            setupNormalLaundryProduct(productDetails: laundryProduct)
        }else {
            setupEditLaundryProduct(productDetails: editLaundryProduct)
        }
        
        
        
    }
    
    func setupNormalLaundryProduct(productDetails: LaundryProducts) {
        guard let productImageUrl = URL(string: productDetails.productPhoto) else {return}
        productDisplayImage.sd_setImage(with: productImageUrl)
        productNameLbl.text = productDetails.productName
        selectedProductQuantityLbl.text = "\(productCounter)"
        laundryServicesList = productDetails.laundryServiceList
    }
    
    func setupEditLaundryProduct(productDetails: ProductCartList) {
        guard let productImageUrl = URL(string: productDetails.productPhoto) else {return}
        productDisplayImage.sd_setImage(with: productImageUrl)
        productNameLbl.text = productDetails.productName
        productCounter = productDetails.productQty
        selectedProductQuantityLbl.text = "\(productCounter)"
        totalServicePrice = editLaundryProduct.productPrice
        totalCostLbl.text = "\(rupee) \(totalServicePrice)"
        guard let laundryServiceList = productDetails.laundryServicesList else {return}
        laundryServicesList = laundryServiceList
        
        var selectedServices = [String]()
        
        for service in laundryServiceList {
            guard let cartId = service.cartId else {return}
            service.isSelected = cartId > 0
            
            if cartId > 0 {
                selectedServices.append(service.laundryServiceName)
            }
        }
        
        if !selectedServices.isEmpty {
            serviceInfoView.isHidden = false
            selectedServicesString = selectedServices.map({$0}).joined(separator: ",")
            selectedServicesLbl.text = selectedServicesString
            //print(selectedServicesString)
        }
        
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        serviceTableView.layer.removeAllAnimations()
        serviceTableViewHeightConstraint.constant = serviceTableView.contentSize.height
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        delegate?.laundryPopupDismissed()
    }
    
    
    @IBAction func addSelectedProductBtnTapped(_ sender: UIButton) {
        
        if laundryProduct != nil {
            performAddProduct(maxProductQuantity: laundryProduct.productAllowedQtyInCart)
        }else {
            performAddProduct(maxProductQuantity: editLaundryProduct.productAllowedQtyInCart)
        }
        
    }
    
    func performAddProduct(maxProductQuantity: Int) {
        
        if productCounter < maxProductQuantity {
            productCounter += 1
            self.selectedProductQuantityLbl.text = "\(productCounter)"
            
            //call update service
            if laundryProduct != nil {
                updateLaundryProductFromCart(coworkerId: coworkerId, laundryProductId: laundryProduct.productID, productQuantity: productCounter)
            }else {
                updateLaundryProductFromCart(coworkerId: coworkerId, laundryProductId: editLaundryProduct.productID, productQuantity: productCounter)
            }
            
        }else {
            self.view.makeToast("Max quantity reached", duration: 1.0, position: .center)
        }
        
    }
    
    @IBAction func removeSelectedProductBtnTapped(_ sender: UIButton) {
        
        if productCounter > 0 {
            productCounter -= 1
            self.selectedProductQuantityLbl.text = "\(productCounter)"
            //call update service
            if laundryProduct != nil {
                updateLaundryProductFromCart(coworkerId: coworkerId, laundryProductId: laundryProduct.productID, productQuantity: productCounter)
            }else {
                updateLaundryProductFromCart(coworkerId: coworkerId, laundryProductId: editLaundryProduct.productID, productQuantity: productCounter)
            }
            
            
        }else {
            self.view.makeToast("Least product quantity", duration: 1.0, position: .center)
        }
        
    }
    
}

extension LaundryProductSelectionVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return laundryServicesList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LaundryServiceTblCell", for: indexPath) as? LaundryServiceTblCell else {return UITableViewCell()}
        
        cell.configureCell(laundryService: laundryServicesList[indexPath.row], delegate: self, selectedServiceIndex: indexPath.row)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: Laundry Service TblCell Delegate IMPLEMENTATION
extension LaundryProductSelectionVC: LaundryServiceTblCellDelegate {
    
    
    func didTapAddRemoveServiceButton(at index: Int, serviceDetails: LaundryServiceList) {
        
        if productCounter > 0 {
            
            serviceDetails.isSelected = !serviceDetails.isSelected
            
            if serviceDetails.isSelected {
                
                if laundryProduct != nil {
                    addLaundryServices(coworkerId: coworkerId, laundryProductId: laundryProduct.productID, productQuantity: productCounter, laundryServiceId: serviceDetails.laundryServiceID)
                }else {
                    
                    print(serviceDetails.isSelected)
                    addLaundryServices(coworkerId: coworkerId, laundryProductId: editLaundryProduct.productID, productQuantity: productCounter, laundryServiceId: serviceDetails.laundryServiceID)
                }
                
                
            }
            else {
                
                if laundryProduct != nil {
                    if let matched = laundryServiceBooking.enumerated().first(where: {$0.element.laundryServiceID == serviceDetails.laundryServiceID}) {
                        print(laundryServiceBooking[matched.offset].laundryServiceName)
                        guard let cartid = laundryServiceBooking[matched.offset].cartId else {return}
                        removeLaundryServiceFromCart(cartId: cartid, coworkerId: coworkerId)
                    }
                }
                else {
                    print("Remove Logic: \(serviceDetails.isSelected)")
                    removeLaundryServiceFromCart(cartId: serviceDetails.cartId!, coworkerId: coworkerId)
                }
                
                
            }
            
            
        }else {
            self.view.makeToast("Product count should be more than 0", duration: 1.0, position: .center)
        }
        
    }
    
}

//MARK: API CALL IMPLEMENTATION
extension LaundryProductSelectionVC {
    
    //MARK: ADD LAUNDRY SERVICES TO CART API CALL
    
    func addLaundryServices(coworkerId: Int, laundryProductId: Int, productQuantity: Int, laundryServiceId: Int) {
        
        LaundryServices.instance.addLaundryServices(coworkedId: coworkerId, laundryProductId: laundryProductId, productQuantity: productQuantity, laundryServiceId: laundryServiceId) { [unowned self] (returnedResponse, error) in
            
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .center)
                }
                
            }
            else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        print(returnedResponse.message)
                        self.laundryServiceBooking.removeAll()
                        for result in returnedResponse.results {
                            self.totalServicePrice = result.subtotal
                            
                            if self.laundryProduct != nil {
                                if let matched = result.bookedLaundryProductList.enumerated().first(where: {$0.element.productID == self.laundryProduct.productID}) {
                                    self.laundryServiceBooking = result.bookedLaundryProductList[matched.offset].laundryService
                                    //print(self.laundryServiceBooking)
                                    let selectedServices = self.laundryServiceBooking.filter({$0.cartId ?? 0 > 0})
                                    print(selectedServices.map({$0.laundryServiceName}).joined(separator: ","))
                                    self.selectedServicesString = selectedServices.map({$0.laundryServiceName}).joined(separator: ",")
                                    
                                }
                            }else {
                                if let matched = result.bookedLaundryProductList.enumerated().first(where: {$0.element.productID == self.editLaundryProduct.productID}) {
                                    self.laundryServiceBooking = result.bookedLaundryProductList[matched.offset].laundryService
                                    //print(self.laundryServiceBooking)
                                    let selectedServices = self.laundryServiceBooking.filter({$0.cartId ?? 0 > 0})
                                    print(selectedServices.map({$0.laundryServiceName}).joined(separator: ","))
                                    self.selectedServicesString = selectedServices.map({$0.laundryServiceName}).joined(separator: ",")
                                    
                                }
                            }
                            
                        }
                        DispatchQueue.main.async {
                            self.serviceTableView.reloadData()
                            self.selectedServicesLbl.text = self.selectedServicesString
                            self.totalCostLbl.text = "\(rupee) \(self.totalServicePrice)"
                            self.serviceInfoView.isHidden = self.selectedServicesString == "" ? true : false
                        }
                        
                        self.getEmployeeDetails(employeeId: coworkerId)
                        
                    }
                    else {
                        //Failure
                        //print(returnedResponse.message)
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                    }
                    
                }
                
            }
            
            
            
        }
        
    }
    
    //MARK: REMOVE LAUNDRY SERVICE FROM CART API CALL
    
    func removeLaundryServiceFromCart(cartId: Int, coworkerId: Int) {
        
        LaundryServices.instance.removeLaundryServiceFromCart(cartId: cartId, coworkedId: coworkerId) { [unowned self] (returnedResponse, error) in
            
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .center)
                }
            }
            else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.message)
                        self.laundryServiceBooking.removeAll()
                        for result in returnedResponse.results {
                            self.totalServicePrice = result.subtotal
                            
                            if self.laundryProduct != nil {
                                if let matched = result.bookedLaundryProductList.enumerated().first(where: {$0.element.productID == self.laundryProduct.productID}) {
                                    self.laundryServiceBooking = result.bookedLaundryProductList[matched.offset].laundryService
                                    //print(self.laundryServiceBooking)
                                    let selectedServices = self.laundryServiceBooking.filter({$0.cartId ?? -1 != 0})
                                    print(selectedServices.map({$0.laundryServiceName}).joined(separator: ","))
                                    self.selectedServicesString = selectedServices.map({$0.laundryServiceName}).joined(separator: ",")
                                    
                                }
                                else {
                                    self.selectedServicesString = ""
                                }
                            }
                            else {
                                
                                if let matched = result.bookedLaundryProductList.enumerated().first(where: {$0.element.productID == self.editLaundryProduct.productID}) {
                                    self.laundryServiceBooking = result.bookedLaundryProductList[matched.offset].laundryService
                                    //print(self.laundryServiceBooking)
                                    let selectedServices = self.laundryServiceBooking.filter({$0.cartId ?? -1 != 0})
                                    print(selectedServices.map({$0.laundryServiceName}).joined(separator: ","))
                                    self.selectedServicesString = selectedServices.map({$0.laundryServiceName}).joined(separator: ",")
                                    
                                }
                                else {
                                    self.selectedServicesString = ""
                                }
                                
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.serviceTableView.reloadData()
                            self.selectedServicesLbl.text = self.selectedServicesString
                            self.totalCostLbl.text = "\(rupee) \(self.totalServicePrice)"
                            self.serviceInfoView.isHidden = self.selectedServicesString == "" ? true : false
                        }
                        
                        self.getEmployeeDetails(employeeId: coworkerId)
                        
                    }
                    else {
                        //Failure
                        //print(returnedResponse.message)
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    //MARK: UPDATE LAUNDRY PRODUCT FROM CART
    func updateLaundryProductFromCart(coworkerId: Int, laundryProductId: Int, productQuantity: Int) {
        
        LaundryServices.instance.updateLaundryProductFromCart(coworkerId: coworkerId, laundryProductId: laundryProductId, productQuantity: productQuantity) { (returnedResponse, error) in
            
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .center)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        for result in returnedResponse.results {
                            if !result.bookedLaundryProductList.isEmpty {
                                //print("Array is Full")
                                self.totalServicePrice = result.subtotal
                                //print("Total Price: \(self.totalServicePrice)")
                                DispatchQueue.main.async {
                                    self.totalCostLbl.text = "\(rupee) \(self.totalServicePrice)"
                                }
                            }else {
                                //print("Array is Empty")
                                DispatchQueue.main.async {
                                    self.serviceInfoView.isHidden = true
                                }
                                
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                                if self.productCounter == 0 {
                                    self.dismiss(animated: true, completion: nil)
                                    self.delegate?.laundryPopupDismissed()
                                }
                            })
                            
                            self.getEmployeeDetails(employeeId: coworkerId)
                            
                        }
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                debugPrint(error)
                DispatchQueue.main.async {
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count and post
                                //print(detail.itemsInCart)
                                NotificationCenter.default.post(name: .UPDATE_CART_COUNT_NOTIFICATION, object: nil, userInfo: ["cartCount": detail.itemsInCart])
                                
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
