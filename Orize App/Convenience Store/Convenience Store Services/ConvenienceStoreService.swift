//
//  ConvenienceStoreService.swift
//  Orize App
//
//  Created by Aditya Infotech on 29/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class ConvenienceStoreService {
    static let instance = ConvenienceStoreService()
    
    
    //MARK: API CALL TO GET CONVENIENCE STORE CATEGORY LIST
    
    func getConvenienceStoreCategoryList(completion:@escaping(ConvenienceStore?, Error?) -> ()) {
        guard let url = URL(string: CONVENIENCE_STORE_CATEGORY_LIST_URL) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedCategoryList = try decoder.decode(ConvenienceStore.self, from: data)
                    completion(returnedCategoryList, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
        }.resume()
        
    }
    
    // GET PRODUCT Details API
    
    func getConvenienceStoreProductsListWith(storeCategoryId: Int, completion:@escaping(_ success: Bool, ConvenienceStoreProducts?) -> ()) {
        
        let postData = NSMutableData(data: "store_category_id=\(storeCategoryId)".data(using: String.Encoding.utf8)!)
        guard let url = URL(string: CONVENIENCE_STORE_PRODUCT_LIST_URL) else {return}
        var urlrequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        urlrequest.httpMethod = "POST"
        urlrequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlrequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(false, nil)
                return
            }else {
                guard let data = data else {return completion(false, nil)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedProductsList = try decoder.decode(ConvenienceStoreProducts.self, from: data)
                    completion(true, returnedProductsList)
                
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(false, nil)
                    return
                }
            }
            
        }.resume()
        
    }
    
    
    
}
