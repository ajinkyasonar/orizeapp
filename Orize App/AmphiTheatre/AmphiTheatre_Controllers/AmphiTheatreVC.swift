//
//  AmphiTheatreVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import CarbonKit

class AmphiTheatreVC: UIViewController {
    
    @IBOutlet weak var logoAndRightButtonsView: UIView!
    
    //Variables
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var amphi_theatre_array = AmphiCategoryArray
    var items = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCarbonKit()
        
    }
    
    // IB-Actions: Actions Here
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension AmphiTheatreVC: CarbonTabSwipeNavigationDelegate {
    
    private func configureCarbonKit() {
        
        for allValues in amphi_theatre_array {
            items.append(allValues.categoryName)
        }
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: .top, relatedBy: .equal, toItem: logoAndRightButtonsView, attribute: .top, multiplier: 1.0, constant: 50))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        
        //Appearance of Menu
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        carbonTabSwipeNavigation?.setNormalColor(UIColor.white)
        carbonTabSwipeNavigation?.setSelectedColor(UIColor.white)
        carbonTabSwipeNavigation?.setIndicatorColor(UIColor.white)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            carbonTabSwipeNavigation?.setTabExtraWidth(30)
            carbonTabSwipeNavigation?.setTabBarHeight(60)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
        }else {
            carbonTabSwipeNavigation?.setTabBarHeight(50)
            carbonTabSwipeNavigation?.setTabExtraWidth(20)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
        }
        
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        
        switch index {
            
        case 0:
            print("Go To All Categories View Controller")
            
            guard let allEventViewController = storyboard?.instantiateViewController(withIdentifier: "AllEventsVC") as? AllEventsVC else {return UIViewController()}
            
            return allEventViewController
            
        default:
            print("Go to Individual Category View Controller")
            
            guard let individualEventVc = storyboard?.instantiateViewController(withIdentifier: "IndividualEventVC") as? IndividualEventVC else {return UIViewController()}
            
            DispatchQueue.global(qos: .userInteractive).async {
                // All the individual appending task done here.
                let selectedCategory = self.items[index]
                var selectedCategoryEvent = [AmphiEvent]()
                
                selectedCategoryEvent = amphiEventsData.filter({$0.eventType.rawValue.lowercased().contains(selectedCategory.lowercased())})
                
                individualEventVc.individualEventData = selectedCategoryEvent
                
            }
            
            
            return individualEventVc
        }
        
    }
    
    
}
