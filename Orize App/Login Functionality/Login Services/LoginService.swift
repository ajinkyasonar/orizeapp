//
//  LoginService.swift
//  Orize App
//
//  Created by Aditya Infotech on 29/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class LoginService {
    static let instance = LoginService()
    
    //Service Call For Login Api
    
    func loginUserwith(userName: String, password: String, firebaseToken: String, completion:@escaping(_ success: Bool, Login?) -> ()) {
        let postData = NSMutableData(data: "username=\(userName)".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(password)".data(using: String.Encoding.utf8)!)
        postData.append("&firebase_token=\(firebaseToken)".data(using: String.Encoding.utf8)!)
        
        guard let url = URL(string: LOGIN_URL) else {return}
        var urlrequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        urlrequest.httpMethod = "POST"
        urlrequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlrequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(false, nil)
                return
            }else {
                guard let data = data else {return completion(false, nil)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedLoginResponse = try decoder.decode(Login.self, from: data)
                    completion(true, returnedLoginResponse)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(false, nil)
                    return
                }
            }
            
        }.resume()
        
    }
    
    func getEmployeeDetails(employeeId: Int, completion: @escaping(EmployeeDetails?, Error?) ->()) {
        
        guard let url = URL(string: GET_EMPLOYEE_DETAILS_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(employeeId)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpBody = postData as Data
        urlRequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response , error) in
            
            if let error = error {
                debugPrint(error)
                completion(nil, error)
                return
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(EmployeeDetails.self, from: data)
                    completion(returnedResponse, nil)
                    
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
                
            }
            
        }
        
        task.resume()
        
    }
    
}
