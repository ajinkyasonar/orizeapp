//
//  FoodCourtVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift
import CarbonKit

class FoodCourtVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var logoAndRightButtonsView: UIView!
    @IBOutlet weak var navigationSearchBtn: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var venderUnavailableStack: UIStackView!
    
    //Variables
    var foodCourtCategories = [FoodCourtCategoriesDetails]()
    var foodCourtVenders = [VenderDetail]()
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var items = [String]()
    let coworkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        venderUnavailableStack.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getEmployeeDetails(employeeId: self.coworkerEmployeeId)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getVenderList()
        navigationSearchBtn.isHidden = true
        
    }
    
    //ACTIONS
    @IBAction func backButtontapped(_ sender: UIButton) {
        
        navigationController?.popToRootViewController(animated: false)
    }
    
    
    @IBAction func cartButtonTapped(_ sender: UIButton) {
        print("Cart Button Tapped")
        
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        
        self.navigationController?.pushViewController(commonCartVc, animated: true)
        
    }
    
    @IBAction func navSearchBtnTapped(_ sender: UIButton) {
        //To Be Implemented For Global Search Function
        
    }
    
    
}

//MARK: CARBON KIT Implementation
extension FoodCourtVC: CarbonTabSwipeNavigationDelegate {
    
    private func configureCarbonKit() {
        
        for allValues in self.foodCourtVenders {
            items.append(allValues.name)
        }
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: logoAndRightButtonsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 50))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view , attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        
        
        //Appearance of Menu
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        carbonTabSwipeNavigation?.setNormalColor(UIColor.white)
        carbonTabSwipeNavigation?.setSelectedColor(UIColor.white)
        carbonTabSwipeNavigation?.setIndicatorColor(UIColor.white)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            carbonTabSwipeNavigation?.setTabExtraWidth(30)
            carbonTabSwipeNavigation?.setTabBarHeight(60)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
        }else {
            carbonTabSwipeNavigation?.setTabBarHeight(50)
            carbonTabSwipeNavigation?.setTabExtraWidth(20)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
        }

        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        
        let foodCourtVenderId = self.foodCourtVenders[index].foodCourtVendorID
        
        guard let venderCategoryDetails = storyboard?.instantiateViewController(withIdentifier: "AllFoodCategoriesVC") as? VenderVC else {return UIViewController()}
        
        venderCategoryDetails.venderCategoryId = foodCourtVenderId
        
        venderCategoryDetails.delegate = self
        
        return venderCategoryDetails
  
    }
 
}

//MARK: ALL CATEGORY & INDIVIDUAL FOOD CATEGORY DELEGATE
extension FoodCourtVC: VenderVCDelegate {
    
    //Updates the Cart Item Count
    func updateFoodCartCountInRootVC(itemCount: Int) {
        //print(itemCount)
        
        //If Cart has 0 Item Count then Hide Number
        if itemCount > 0 {
            cartButton.setTitle("\(itemCount)", for: .normal)
        } else {
            cartButton.setTitle("", for: .normal)
        }
        
        
    }
    
    
    //Takes to the view controller at index passed in All Food Categories VC - Delegate Method
    func passCategoryIndexToRootVC(categoryIndex: Int) {
        
        carbonTabSwipeNavigation?.setCurrentTabIndex((UInt(categoryIndex)), withAnimation: true)
     
    }
    
}


//MARK: API SERVICES
extension FoodCourtVC {
    
    //MARK: FOOD COURT MODIFICATION -> VENDER LIST SERVICE
    
    func getVenderList() {
        
        self.view.makeToastActivity(.center)
        
        FoodCourtServices.instance.getFoodCourtVenderList { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        print(returnedResponse.results)
                        self.foodCourtVenders = returnedResponse.results
                        
                        if self.foodCourtVenders.isEmpty {
                            //print("There are no Venders in Food Court At this time")
                            DispatchQueue.main.async {
                                self.venderUnavailableStack.isHidden = false
                                self.view.hideToastActivity()
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                self.venderUnavailableStack.isHidden = true
                                self.configureCarbonKit()
                                self.view.hideToastActivity()
                            }
                        }
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }

    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                DispatchQueue.main.async {
                    debugPrint(error.localizedDescription)
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count
                                DispatchQueue.main.async {
                                    
                                    if detail.itemsInCart != 0 {
                                       self.cartButton.setTitle("\(detail.itemsInCart)", for: .normal)
                                    } else {
                                        self.cartButton.setTitle("", for: .normal)
                                    }
                                    
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
