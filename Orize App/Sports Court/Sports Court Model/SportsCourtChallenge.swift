//
//  SportsCourtChallenge.swift
//  Orize App
//
//  Created by Aditya Infotech on 30/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let sportsCourtChallenge = try? newJSONDecoder().decode(SportsCourtChallenge.self, from: jsonData)

import Foundation

struct SportsCourtChallenge: Codable {
    let code: Int
    let status, message: String
    let results: [SportsCourtChallengeResult]
}

struct SportsCourtChallengeResult: Codable {
    let smsGatewayResponse: String
    let smstxt: String
    
    enum CodingKeys: String, CodingKey {
        case smsGatewayResponse = "sms_gateway_response"
        case smstxt
    }
}
