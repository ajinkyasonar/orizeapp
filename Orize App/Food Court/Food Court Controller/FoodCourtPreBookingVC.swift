//
//  FoodCourtPreBookingVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 09/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class FoodCourtPreBookingVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    //Variables
    var foodCourtCategoryId: Int!
    var productDataArray = [FoodCourtProductsDetails]()
    
    //Searching Variables
    var isSearching = false
    var productDetailsFilterArray = [FoodCourtProductsDetails]()

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initialSetup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    private func initialSetup() {
        getProductList(withFoodCategoryId: foodCourtCategoryId)
        
        productCollectionView.dataSource = self
        productCollectionView.delegate = self
        
        productCollectionView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell, bundle: nil), forCellWithReuseIdentifier: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell)
        
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
    }

}

//MARK: COLLECTION VIEW IMPLEMENTATION
extension FoodCourtPreBookingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isSearching {
            return productDetailsFilterArray.count
        } else {
            return productDataArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell, for: indexPath) as? FoodCourtPrebookingCollCell else {return UICollectionViewCell()}
        
        if isSearching {
           cell.configureCell(product: productDetailsFilterArray[indexPath.item], delegate: self, productIndex: indexPath.item)
        } else {
            cell.configureCell(product: productDataArray[indexPath.item], delegate: self, productIndex: indexPath.item)
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width
        let cellWidth = (width - 5) / 2
        let cellHeight = (cellWidth * 1.3)
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}

//MARK: FOOD COURT PRE BOOKING COLLECTION CELL DELEGATE IMPLEMENTATION
extension FoodCourtPreBookingVC: FoodCourtPrebookingCollCellDelegate {
    
    func didTapBookButton(productDetail: FoodCourtProductsDetails, atTag tag: Int) {
        //This Takes to the Pre Booking Screen
        guard let preBookSelectionVc = storyboard?.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
        preBookSelectionVc.productDetails = productDetail
        self.navigationController?.pushViewController(preBookSelectionVc, animated: true)
        //print(productDetail)
    }
    
}

//MARK: TextFiled Delegate & Search Implementation
extension FoodCourtPreBookingVC: UITextFieldDelegate {
    
    //Search Functionality Implementation & Filter
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        isSearching = false
        productCollectionView.reloadData()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if searchTextField.text != "" {
            
            isSearching = true
            
            //Filter Search on Name.
            productDetailsFilterArray = productDataArray.filter({$0.productName.lowercased().contains(textField.text!.lowercased())})
            productCollectionView.reloadData()
            
        } else {
            
            isSearching = false
            productCollectionView.reloadData()
            searchTextField.resignFirstResponder()
            
        }
        
        
    }
    
}


//MARK: API IMPLEMENTATION
extension FoodCourtPreBookingVC {
    
    func getProductList(withFoodCategoryId id: Int) {
        
        productDataArray.removeAll()
        self.view.makeToastActivity(.center)
        
        FoodCourtServices.instance.getProductListForFoodCourt(withfoodCourtCategoryId: id) { (returnedResponse, error) in
            
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        returnedResponse.results.forEach({ (preBookingDetails) in
                            
                            preBookingDetails.productData.forEach({ (product) in
                                self.productDataArray = product.productsListArr
                            })
                            
                        })
                        
                        DispatchQueue.main.async {
                            self.productCollectionView.reloadData()
                            self.view.hideToastActivity()
                        }
                        
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            //print(returnedResponse.message)
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
        
}
