//
//  CancelCarpool.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let cancelCarpool = try? newJSONDecoder().decode(CancelCarpool.self, from: jsonData)

import Foundation

struct CancelCarpool: Codable {
    let code: Int
    let status, message: String
}
