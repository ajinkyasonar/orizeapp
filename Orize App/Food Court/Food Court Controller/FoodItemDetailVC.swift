//
//  FoodItemDetailVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 08/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

protocol FoodItemDetailVCDelegate: class {
    
    func addRemoveFoodCourtCartProduct(product: FoodCourtProductsDetails, productQuantity: Int)
    
    func addRemoveSearchFoodCourtCartProduct(searchProduct: SearchAutocompleteData, productQuantity: Int)
    
}

extension FoodItemDetailVCDelegate {
    
    func addRemoveFoodCourtCartProduct(product: FoodCourtProductsDetails, productQuantity: Int) {
        
    }
    
    func addRemoveSearchFoodCourtCartProduct(searchProduct: SearchAutocompleteData, productQuantity: Int) {
        
    }
    
}

class FoodItemDetailVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productDescriptionLbl: UILabel!
    @IBOutlet weak var counterDisplayLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var preBookBtn: UIButton!
    @IBOutlet weak var addRemoveStack: UIStackView!
    
    //Variables
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var selectedFoodProductDetails: FoodCourtProductsDetails!
    var searchFoodProductDetails: SearchAutocompleteData?
    var counter = 0
    
    //Delegate
    weak var delegate: FoodItemDetailVCDelegate?
    
    //Closure to navigate to pre booking selection view controller.
    var navigateToPreBookingSelection: ((_ searchedPrebookProduct: SearchAutocompleteData)->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        
    }
    
    private func setupInitialView() {
        
        if searchFoodProductDetails != nil {
            //User has navigated from search view controller
            print("User has navigated from Search View Controller")
            guard let searchFoodProductDetails = searchFoodProductDetails else {return}
            
            guard let imageUrl = URL(string: searchFoodProductDetails.productPhoto ?? "") else {return}
            imageView.sd_setImage(with: imageUrl)
            productNameLbl.text = searchFoodProductDetails.name
            productPriceLbl.text = "\(rupee) \(searchFoodProductDetails.productPriceWithTax ?? 0)"
            productDescriptionLbl.text = "\(searchFoodProductDetails.productDescription ?? "")"
            
            //Todo: Prebooking condition to be applied in this case.
            addRemoveStack.isHidden = searchFoodProductDetails.isPrebookingCategory == 1 ? true : false
            preBookBtn.isHidden = searchFoodProductDetails.isPrebookingCategory == 1 ? false : true
            
        } else {
            //User has navigated from all category view controller
            print("User has navigated from all view controller")
            guard let imageUrl = URL(string: selectedFoodProductDetails.productPhoto) else {return}
            imageView.sd_setImage(with: imageUrl)
            productNameLbl.text = selectedFoodProductDetails.productName
            productPriceLbl.text = "\(rupee) \(selectedFoodProductDetails.productPriceWithTax)"
            productDescriptionLbl.text = selectedFoodProductDetails.productDescription
            
            addRemoveStack.isHidden = selectedFoodProductDetails.isPrebookingCategory == 1 ? true : false
            preBookBtn.isHidden = selectedFoodProductDetails.isPrebookingCategory == 1 ? false : true
            
        }
        
    }
    
    //Actions
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func hiddenViewBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        
        if searchFoodProductDetails != nil {
            //Functionality for auto search
            print("Search Functionality")
            guard let searchFoodProductDetails = searchFoodProductDetails else {return}
            
            if counter == searchFoodProductDetails.productAllowedQtyInCart {
                self.view.makeToast("Max Count Reached", duration: 1.0, position: .center)
            } else {
                counter += 1
                counterDisplayLbl.text = "\(counter)"
            }
            delegate?.addRemoveSearchFoodCourtCartProduct(searchProduct: searchFoodProductDetails, productQuantity: counter)
            
            
        } else {
            //Functionality for normal search
            print("Normal Functionality")
            if counter == selectedFoodProductDetails.productAllowedQtyInCart {
                self.view.makeToast("Max Count Reached", duration: 1.0, position: .center)
            } else {
                counter += 1
                counterDisplayLbl.text = "\(counter)"
            }
            delegate?.addRemoveFoodCourtCartProduct(product: selectedFoodProductDetails, productQuantity: counter)
        }
        
    }
    
    @IBAction func removeBtnTapped(_ sender: UIButton) {
        
        if counter <= 0 {
            
            self.view.makeToast("Lowest Count Reached", duration: 1.0, position: .center)
            
        }else {
        
            counter -= 1
            counterDisplayLbl.text = "\(counter)"
        }

        if searchFoodProductDetails != nil {
            guard let searchFoodProductDetails = searchFoodProductDetails else {return}
            delegate?.addRemoveSearchFoodCourtCartProduct(searchProduct: searchFoodProductDetails, productQuantity: counter)
        } else {
            delegate?.addRemoveFoodCourtCartProduct(product: selectedFoodProductDetails, productQuantity: counter)
        }
        
    }
    
    @IBAction func preBookButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            
            if let searchFoodProductDetails = self.searchFoodProductDetails {
                self.navigateToPreBookingSelection?(searchFoodProductDetails)
            }
            
        }
        
        //Navigate to Pre Booking Selection View Controller
        guard let preBookSelectionVc = storyboard?.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
        preBookSelectionVc.searchProductDetails = self.searchFoodProductDetails
        self.dismiss(animated: true, completion: nil)
    }

}

