//
//  Health_Spa_Pass_Cell.swift
//  Orize App
//
//  Created by Apple on 26/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol Health_Spa_Pass_CellDelegate : class {
    func addPassButtonTapped(at index: Int, passDetails: HealthSpaPass)
    func removePassButtonTapped(at index: Int, passDetails: HealthSpaPass)
}

class Health_Spa_Pass_Cell: UITableViewCell {
    
    @IBOutlet weak var passDetailLbl: UILabel!
    @IBOutlet weak var passPriceLbl: UILabel!
    @IBOutlet weak var passUnitLbl: UILabel!
    
    var counter = 0
    
    weak var delegate: Health_Spa_Pass_CellDelegate?
    private var passIndex: Int!
    private var selectedPassDetails: HealthSpaPass!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(passDetails: HealthSpaPass, delegate: Health_Spa_Pass_CellDelegate, selectedIndex: Int) {
        
        self.delegate = delegate
        self.selectedPassDetails = passDetails
        self.passIndex = selectedIndex
        
        passDetailLbl.text = passDetails.timePeriod
        passPriceLbl.text = "\(rupee) \(passDetails.price)"
        passDetails.unitCount = 0
        passUnitLbl.text = "\(passDetails.unitCount)"
    }
    
    
    @IBAction func addPassBtnTapped(_ sender: UIButton) {
        delegate?.addPassButtonTapped(at: passIndex, passDetails: selectedPassDetails)
        passUnitLbl.text = "\(selectedPassDetails.unitCount)"
    }
    
    @IBAction func removePassBtnTapped(_ sender: UIButton) {
        delegate?.removePassButtonTapped(at: passIndex, passDetails: selectedPassDetails)
        passUnitLbl.text = "\(selectedPassDetails.unitCount)"
    }
    
}
