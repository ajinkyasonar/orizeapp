//
//  UpdateCarpool.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let editCarpool = try? newJSONDecoder().decode(EditCarpool.self, from: jsonData)

import Foundation

struct UpdateCarpool: Codable {
    let code: Int
    let status, message: String
    let results: [UpdateCarpoolResult]
}

struct UpdateCarpoolResult: Codable {
    let carPoolVehicleRequestID: Int
    
    enum CodingKeys: String, CodingKey {
        case carPoolVehicleRequestID = "car_pool_vehicle_request_id"
    }
}
