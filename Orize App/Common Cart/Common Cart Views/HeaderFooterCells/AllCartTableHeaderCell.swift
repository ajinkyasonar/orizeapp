//
//  AllCartTableHeaderCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 25/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol AllCartTableHeaderCellDelegate: class {
    func passModuleDetails(withIndex index: Int, moduleDetails: ModuleBooking)
}

class AllCartTableHeaderCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var categoryNameLbl: UILabel!
    
    weak var delegate: AllCartTableHeaderCellDelegate?
    private var moduleIndex: Int!
    private var moduleDetails: ModuleBooking!

    override func awakeFromNib() {
        super.awakeFromNib()
        elementView.layer.cornerRadius = 10
    }
    
    
    func configureCell(module: ModuleBooking, delegate: AllCartTableHeaderCellDelegate, moduleIndex: Int) {
        
        self.moduleDetails = module
        self.moduleIndex = moduleIndex
        self.delegate = delegate
        
        categoryNameLbl.text = module.moduleName.rawValue
        
    }
    
    
    @IBAction func viewAllButtonTapped(_ sender: UIButton) {
        delegate?.passModuleDetails(withIndex: moduleIndex, moduleDetails: moduleDetails)
    }
    
}
