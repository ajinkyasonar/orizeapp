//
//  FoodSearchVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 01/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol FoodSearchVCDelegate : class {
    
    func passVendorIndexToVenderVC(categoryIndex: Int)
    
    func passCategoryIdToVenderVC(categoryId: Int)
}

class FoodSearchVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var cartBarBtn: UIButton!
    
    //Variables
    let employeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var foodVenderId: Int!
    var venderDetails = [VenderDetail]()
    var productDetailsArr = [ProductData]()
    var productFilterArr = [ProductData]()
    var searchAutoCompleteData = [SearchAutocompleteData]()
    var searchResultFilterArray = [SearchAutocompleteData]()
    var isSearching = false
    
    //Closure for Search Product Passing to All View Controller
    var passSearchFoodProductDetails: ((_ searchProductDetails: SearchAutocompleteData, _ productQuantity: Int) -> ())?
    
    //Delegate
    weak var delegate: FoodSearchVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        
        searchResultFilterArray = searchAutoCompleteData
        productFilterArr = productDetailsArr
        
    }
    
    //Initial View Setup Function
    fileprivate func setupInitialView() {
        
        getEmployeeDetails(employeeId: employeeId)
        getProductList(withVenderCategoryId: foodVenderId)
        getVenderList()
        searchTextField.delegate = self
        searchTableView.delegate = self
        searchTableView.dataSource = self
        
        searchTableView.register(UINib(nibName: FoodCourtRegisteredXibs.Food_AllCategory_SearchTblCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.Food_AllCategory_SearchTblCell)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    //IB_ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cartBtnTapped(_ sender: UIButton) {
        
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        self.navigationController?.pushViewController(commonCartVc, animated: true)
    }
    
}

extension FoodSearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch isSearching {
            
        case true:
            return searchResultFilterArray.count
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch isSearching {
            
        case true:
            
            guard let cell = searchTableView.dequeueReusableCell(withIdentifier: FoodCourtRegisteredXibs.Food_AllCategory_SearchTblCell, for: indexPath) as? Food_AllCategory_SearchTblCell else {return UITableViewCell()}
            
            cell.configureCell(searchData: searchResultFilterArray[indexPath.row])
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let typeName = searchResultFilterArray[indexPath.row].typename
        
        switch typeName {
            
        case "vendor":
            
            //print("Vender Functionality")
            
            guard let venderName = searchResultFilterArray[indexPath.row].vendorName else {return}
            
            if let matchedVendorIndex = venderDetails.enumerated().first(where: {$0.element.name.lowercased() == venderName.lowercased()}) {
                
                //print(matchedVendorIndex.offset)
                
                delegate?.passVendorIndexToVenderVC(categoryIndex: matchedVendorIndex.offset)
                
                self.navigationController?.popViewController(animated: true)
                
            }
            break
            
        case "products":
            
            //print("Product Functionality")
            
            let selectedProduct = searchResultFilterArray[indexPath.row]
            
            guard let foodItemDetailVc = storyboard?.instantiateViewController(withIdentifier: "FoodItemDetailVC") as? FoodItemDetailVC else {return}
            
            //Currently Working Here...
            foodItemDetailVc.searchFoodProductDetails = selectedProduct
            
            foodItemDetailVc.navigateToPreBookingSelection = {(searchedPrebookProduct) -> () in
                //Closure that navigates to pre booking - was used bec food detail is presented. nav was required to push.
                guard let prebookSelectionVc = self.storyboard?.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
                prebookSelectionVc.searchProductDetails = searchedPrebookProduct
                self.navigationController?.pushViewController(prebookSelectionVc, animated: true)
            }
            
            foodItemDetailVc.delegate = self
            foodItemDetailVc.modalTransitionStyle = .crossDissolve
            foodItemDetailVc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(foodItemDetailVc, animated: true, completion: nil)
   
            break
            
        case "category":
            
            //print("Apply Logic For Category Selection")
            
            let selectedCategoryId = searchResultFilterArray[indexPath.row].id
            
            delegate?.passCategoryIdToVenderVC(categoryId: selectedCategoryId)
            
            self.navigationController?.popViewController(animated: true)
            
            break
            
        default:
            return
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}

//MARK: DELEGATE & PROTOCOL IMPLEMENTATION FOR FOOD ITEM DETAIL VIEW CONTROLLER
extension FoodSearchVC: FoodItemDetailVCDelegate {
    
    func addRemoveSearchFoodCourtCartProduct(searchProduct: SearchAutocompleteData, productQuantity: Int) {
        
        //This is only for normal food search products & not for pre-booking products
         add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: employeeId, foodCourtProductId: searchProduct.id, productQuantity: productQuantity, subscriptionDate: "", deliveryTime: "")
        
    }
    
}



//Search Functionality Implementation & Filter
extension FoodSearchVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        searchTableView.reloadData()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if searchTextField.text != "" {
            
            isSearching = true
            
            //Filter Search on Name.
            searchResultFilterArray = searchAutoCompleteData.filter({$0.name.lowercased().contains(textField.text!.lowercased())})
            searchTableView.reloadData()
            
        } else {
            isSearching = false
            searchTableView.reloadData()
            searchTextField.resignFirstResponder()
        }
        
        
    }
    
}

//MARK: API SERVICES
extension FoodSearchVC {
    
    //GET ALL PRODUCTS SERVICE WITH VENDER ID = 0
    func getProductList(withVenderCategoryId venderCategoryId: Int) {
        
        self.view.makeToastActivity(.center)
        
        FoodCourtServices.instance.getFoodCourtVenderDetails(with: venderCategoryId) { (returnedResponse, error) in
            
            if error != nil {
                
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        
                        for values in returnedResponse.results {
                            self.searchAutoCompleteData = values.searchAutocompleteData
                            self.productDetailsArr = values.productData
                        }
                        
                        DispatchQueue.main.async {
                            self.searchTableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        
                    } else {
                        //Failure
                        print(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    //GET VENDER LIST SERVICE
    func getVenderList() {
        
        FoodCourtServices.instance.getFoodCourtVenderList { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        self.venderDetails = returnedResponse.results
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    //GET EMPLOYEE DETAILS API SERVICE - FOR CART COUNT
    func getEmployeeDetails(employeeId: Int) {
        
        LoginService.instance.getEmployeeDetails(employeeId: employeeId) { (returnedResponse, error) in
            
            if let error = error {
                
                DispatchQueue.main.async {
                    debugPrint(error.localizedDescription)
                    self.view.makeToast(error.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        
                        returnedResponse.results.forEach({ (detail) in
                            
                            if detail.status == 1 {
                                //Continue further process - Get Cart Item Count
                                DispatchQueue.main.async {
                                    
                                    if detail.itemsInCart != 0 {
                                        self.cartBarBtn.setTitle("\(detail.itemsInCart)", for: .normal)
                                    } else {
                                        self.cartBarBtn.setTitle("", for: .normal)
                                    }
                                    
                                }
                            } else {
                                //Logout User due to inactivity
                                DispatchQueue.main.async {
                                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .center)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                    logoutUser()
                                })
                                
                            }
                            
                        })
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //ADD REMOVE FOODCOURT CART API
    
    func add_RemoveFoodCourtCartProduct(coWorkersEmployeeId: Int, foodCourtProductId: Int, productQuantity: Int, subscriptionDate: String, deliveryTime: String) {
        
        FoodCourtServices.instance.addRemoveFoodcourtCartItem(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: productQuantity, subscriptionDate: subscriptionDate, deliveryTime: deliveryTime) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        //print(returnedResponse.results)
                        DispatchQueue.main.async {
                            //Update Count on Cart
                            self.getEmployeeDetails(employeeId: coWorkersEmployeeId)
                        }
                    }else {
                        //Failure
                        print(returnedResponse.message)
                    }
                    
                }
                
            }
            
        }
        
    }
    

}
