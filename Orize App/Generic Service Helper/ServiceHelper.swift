//
//  ServiceHelper.swift
//  NestedTableViewDemo
//
//  Created by Ajinkya Sonar on 25/06/19.
//  Copyright © 2019 Ajinkya Sonar. All rights reserved.
//

import Foundation

class ServiceHelper {
    static let instance = ServiceHelper()
    
    
    //MARK: GENERIC POST API CALL
    
    func GenericPostService<T: Codable>(forUrl url: String, parameters: [String:Any]?, completion:@escaping(T) ->()) {
        
        guard let url = URL(string: url) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = parameters?.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                return
            }else {
                guard let data = data else {return}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(T.self, from: data)
                    completion(returnedResponse)
                }catch{
                    print(error.localizedDescription)
                    return
                }
                
            }
            
        }
        task.resume()
        
    }
    
    //MARK: GET GENERIC REQUEST SERVICE
    
    func GenericGetRequest<T: Codable>(forUrl url: String, completion:@escaping(T) ->()) {
        
        guard let url = URL(string: url) else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                return
            }else {
                
                guard let data = data else {return}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(T.self, from: data)
                    completion(returnedResponse)
                }catch{
                    debugPrint(error.localizedDescription)
                    return
                }
                
            }
            
            
        }
        task.resume()
        
    }
    
}



