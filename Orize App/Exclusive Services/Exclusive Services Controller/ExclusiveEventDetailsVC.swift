//
//  EventDetailsVC.swift
//  Orize App
//
//  Created by Apple on 19/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class ExclusiveEventDetailsVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var eventDescriptionLbl: UILabel!
    
    var selectedEvent: Event!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        
        guard let eventImageUrl = URL(string: selectedEvent.eventImage) else {return}
        backgroundImage.sd_setImage(with: eventImageUrl, completed: .none)
        
        eventImage.sd_setImage(with: eventImageUrl, completed: .none)
        eventImage.layer.masksToBounds = true
        eventImage.layer.cornerRadius = 8
        eventNameLbl.text = selectedEvent.eventName
        eventDescriptionLbl.text = selectedEvent.eventDescription
        
    }
    
    //MARK: IB-ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
