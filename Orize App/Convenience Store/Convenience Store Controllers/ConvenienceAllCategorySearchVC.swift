//
//  ConvenienceAllCategorySearchVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 15/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol ConvenienceAllCategorySearchVCDelegate : class {
    
    func passCategoryIndexToAllVC(categoryIndex: Int)
}

class ConvenienceAllCategorySearchVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchTableView: UITableView!
    
    //Variables
    var convenienceStoreCategory = [ConvenienceStoreResult]()
    var convenienceSearchAutocompleteArray: [SearchAutocompleteDatum]!
    var searchResultFilterArray = [SearchAutocompleteDatum]()
    var isSearching = false
    
    //Delegate
    weak var delegate: ConvenienceAllCategorySearchVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInitialView()
        searchResultFilterArray = convenienceSearchAutocompleteArray
        
    }
    
    func setupInitialView() {
        
        getConvenienceStoreCategoryList()
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTextField.delegate = self
        
        searchTableView.register(UINib(nibName: ConvinienceStoreRegisteredXibs.AllConvenienceSearchTblCell, bundle: nil), forCellReuseIdentifier: ConvinienceStoreRegisteredXibs.AllConvenienceSearchTblCell)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    //IB_ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ConvenienceAllCategorySearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch isSearching {
            
        case true:
            return searchResultFilterArray.count
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch isSearching {
            
        case true:
            
            guard let cell = searchTableView.dequeueReusableCell(withIdentifier: ConvinienceStoreRegisteredXibs.AllConvenienceSearchTblCell, for: indexPath) as? AllConvenienceSearchTblCell else {return UITableViewCell()}
            
            cell.configureCell(searchData: searchResultFilterArray[indexPath.row])
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //guard let categoryId = searchResultFilterArray[indexPath.row].categoryID else {return}
        
        guard let categoryName = searchResultFilterArray[indexPath.row].categoryName else {return}
        
        if let matchedCategoryIndex = convenienceStoreCategory.enumerated().first(where: {$0.element.name == categoryName}) {
            // do something with foo.offset and foo.element
            //print(matchedCategoryIndex.offset)
            delegate?.passCategoryIndexToAllVC(categoryIndex: matchedCategoryIndex.offset)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}

//Search Functionality Implementation & Filter
extension ConvenienceAllCategorySearchVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        searchTableView.reloadData()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if searchTextField.text != "" {
            
            isSearching = true
            
            //Filter Search on Name.
            searchResultFilterArray = convenienceSearchAutocompleteArray.filter({$0.name.lowercased().contains(textField.text!.lowercased())})
            searchTableView.reloadData()
            
        } else {
            isSearching = false
            searchTableView.reloadData()
            searchTextField.resignFirstResponder()
        }
        
        
    }
    
}

//MARK: API IMPLEMENTATION
extension ConvenienceAllCategorySearchVC {
    
    //Get Convenience Store Category List
    func getConvenienceStoreCategoryList() {
        
        ServiceHelper.instance.GenericGetRequest(forUrl: CONVENIENCE_STORE_CATEGORY_LIST_URL) { (categoryList: ConvenienceStore) in
            
            if categoryList.code == 0 {
                self.convenienceStoreCategory = categoryList.results
            }
            
        }
    }
    
}
