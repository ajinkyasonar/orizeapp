//
//  FindACarpoolVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 12/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class FindACarpoolVC: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var searchTextFieldMainView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var seatCounterLbl: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var findCarpoolTableView: UITableView!
    @IBOutlet weak var findCarpoolTableHeightConstraint: NSLayoutConstraint!
    
    //Variables
    var coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var carpoolVehiclesListArrayData = [ListAllVehiclesResult]()
    var seatCounter = 1
    
    //Searching Variables
    var isSearching = false
    var filteredCarpoolVehiclesListArrayData = [ListAllVehiclesResult]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupInitialView()
    }
    
    func setupInitialView() {
        
        getListOfAllCarpoolVehiclesService(coWorkerEmployeeId: coWorkerEmployeeId)
        searchTextFieldMainView.layer.borderWidth = 0.2
        searchTextFieldMainView.layer.borderColor = UIColor.black.cgColor
        searchTextField.delegate = self
        seatCounterLbl.text = "\(seatCounter)"
        minusButton.isEnabled = false
        
        findCarpoolTableView.delegate = self
        findCarpoolTableView.dataSource = self
        
        self.findCarpoolTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        findCarpoolTableView.layer.removeAllAnimations()
        findCarpoolTableHeightConstraint.constant = findCarpoolTableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    //MARK: ACTIONS HERE
    @IBAction func minusBtnTapped(_ sender: UIButton) {
        seatCounter -= 1
        seatCounterLbl.text = "\(seatCounter)"
        
        if seatCounter == 1 {
            minusButton.isEnabled = false
        } else {
            minusButton.isEnabled = true
            
        }
        
        searchCombinationLogic()
        
    }
    
    @IBAction func plusBtnTapped(_ sender: UIButton) {
        seatCounter += 1
        seatCounterLbl.text = "\(seatCounter)"
        
        if seatCounter == 1 {
            minusButton.isEnabled = false
        } else {
            minusButton.isEnabled = true
        }
        
        searchCombinationLogic()
        
    }
    
    @IBAction func backButtontapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

//MARK: Table View Implementation
extension FindACarpoolVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            return filteredCarpoolVehiclesListArrayData.count
        } else {
           return carpoolVehiclesListArrayData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isSearching {
            guard let cell = findCarpoolTableView.dequeueReusableCell(withIdentifier: "FindCarpoolTableCell", for: indexPath) as? FindCarpoolTableCell else {return UITableViewCell()}
            cell.configureCell(data: filteredCarpoolVehiclesListArrayData[indexPath.row])
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        } else {
            guard let cell = findCarpoolTableView.dequeueReusableCell(withIdentifier: "FindCarpoolTableCell", for: indexPath) as? FindCarpoolTableCell else {return UITableViewCell()}
            cell.configureCell(data: carpoolVehiclesListArrayData[indexPath.row])
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension FindACarpoolVC: FindCarpoolTableCellDelegate {
    
    func requestCarpoolBtnTapped(_ tag: Int) {
        
        if isSearching {
            let individualValues = filteredCarpoolVehiclesListArrayData[tag]
            requestCarpoolService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: individualValues.carPoolVehicleTravelID)
        } else {
            let individualValues = carpoolVehiclesListArrayData[tag]
            requestCarpoolService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: individualValues.carPoolVehicleTravelID)
        }
        
    }
    
}

//MARK: TEXT FIELD DELEGATE IMPLEMENTATION
extension FindACarpoolVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        searchCombinationLogic()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchTextField.text = ""
        searchCombinationLogic()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchTextField.text = ""
        searchTextField.resignFirstResponder()
        isSearching = false
        findCarpoolTableView.reloadData()
        self.view.endEditing(true)
    }
    
}

//MARK: API IMPLEMENTATION HERE
extension FindACarpoolVC {
    
    //MARK: GET LIST OF ALL CARPOOL VEHICLES API
    func getListOfAllCarpoolVehiclesService(coWorkerEmployeeId: Int) {
        
        self.view.makeToastActivity(.center)
        
        CarpoolServices.instance.findCarpoolGetAllVehicleListService(coWorkerEmployeeId: coWorkerEmployeeId) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.sync {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                if let returnedResponse = returnedResponse {
                    
                    self.carpoolVehiclesListArrayData = returnedResponse.results
                    
                    DispatchQueue.main.async {
                        
                        self.findCarpoolTableView.reloadData()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        self.view.hideToastActivity()
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    //MARK: Request carpool api for confirmation of previous booking implementation
    
    func requestCarpoolService(coWorkerEmployeeId: Int, carpoolVehicleTravelId: Int) {
        
        self.view.makeToastActivity(.center)
        
        CarpoolServices.instance.requestCarpoolService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    print(returnedResponse.code)
                    
                    
                    if returnedResponse.code == 0 {
                        
                        DispatchQueue.main.async {
                            
                            guard let carpoolConfirmationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CarpoolConfirmationVC") as? CarpoolConfirmationVC else {return}
                            carpoolConfirmationViewController.carpoolVehicleTravelId = carpoolVehicleTravelId
                            carpoolConfirmationViewController.requestCarpoolResultArray = returnedResponse.results
                            self.present(carpoolConfirmationViewController, animated: true, completion: nil)
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.findCarpoolTableView.reloadData()
                            self.view.hideToastActivity()
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

//MARK: ADDITIONAL METHODS INCLUDING SEARCH ON SEAT COUNT
extension FindACarpoolVC {
    
    func searchCombinationLogic() {
        
        //When Search bar and counter both are checked
        if (searchTextField.text != "") && (seatCounter > 1) {
            isSearching = true
            filteredCarpoolVehiclesListArrayData = carpoolVehiclesListArrayData.filter({$0.destination.lowercased().contains(searchTextField.text!.lowercased()) && $0.availableSeats >= seatCounter})
            findCarpoolTableView.reloadData()
            
          //When only Searchbar is checked
        } else if  (searchTextField.text != "") && (seatCounter == 1) {
            isSearching = true
            filteredCarpoolVehiclesListArrayData = carpoolVehiclesListArrayData.filter({$0.destination.lowercased().contains(searchTextField.text!.lowercased())})
            findCarpoolTableView.reloadData()
            
            //When only counter is checked
        } else if (searchTextField.text == "") && (seatCounter > 1) {
            isSearching = true
            filteredCarpoolVehiclesListArrayData = carpoolVehiclesListArrayData.filter({$0.availableSeats >= seatCounter})
            findCarpoolTableView.reloadData()
            
        } else {
            isSearching = false
            findCarpoolTableView.reloadData()
            searchTextField.resignFirstResponder()
        }
        
    }
    
}
