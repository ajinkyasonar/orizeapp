//
//  FirstViewController.swift
//  Orize App
//
//  Created by Aditya Infotech on 17/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginVC: UIViewController {
    
    //Outlets
    @IBOutlet private weak var usernameTxtField: UITextField!
    @IBOutlet private weak var passwordTxtField: UITextField!
    @IBOutlet private weak var rememberMeBtn: UIButton!
    @IBOutlet private weak var forgotPasswordBtn: UIButton!
    @IBOutlet private weak var loginBtn: UIButton!
    
    @IBOutlet private weak var mainStackView: UIStackView!
    @IBOutlet private weak var imageViewYConstraint: NSLayoutConstraint!
    
    
    //STORAGE VARIABLES
    var loginResultArray = [LoginResult]()
    
    var autoLoginCheckingDict = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        setupInitialView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //Configure TextField Method
    private func configureTextFields() {
        usernameTxtField.delegate = self
        passwordTxtField.delegate = self
    }
    
    private func setupInitialView() {
        rememberMeBtn.isSelected = false
        passwordTxtField.isSecureTextEntry = true
        
        mainStackView.alpha = 0.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.imageViewYConstraint.constant = -120
            
            UIView.animate(withDuration: 3.0,
                           delay: 0.0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 1.0,
                           options: .curveEaseIn,
                           animations: {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6, execute: {
                                self.fadeMainStack()
                            })
                            self.view.layoutIfNeeded()
            }, completion: nil)
            
        }
        
    }
    
    func fadeMainStack() {
        
        UIView.animate(withDuration: 1.0, animations: {
            self.mainStackView.alpha = 1.0
        }, completion: nil)
    }
    
    
    @IBAction func rememberMeButtonTapped(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isSelected ? sender.setImage(UIImage(named: "checked-box"), for: .normal) : sender.setImage(UIImage(named: "stop-button"), for: .normal)
        
    }
    
    @IBAction func forgotPasswordButtonTapped(sender: UIButton) {
        
    }
    
    //Login Button Action
    @IBAction func loginButtonTapped(sender: UIButton) {
        
        SVProgressHUD.show()
        
        if USERNAME == "" || PASSWORD == "" {
            //print("USER NAME AND PASSWORD REQUIRED")
            let alert = UIAlertController(title: "Invalid Credentails", message: "Username & Password are required", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            //self.present(alert, animated: true, completion: nil)
            self.present(alert, animated: true) {
                SVProgressHUD.dismiss()
            }
            
            
        }else {
            //print("MAKE LOGIN API CALL")
            
            LoginService.instance.loginUserwith(userName: USERNAME, password: PASSWORD, firebaseToken: FIREBASE_TOKEN) { (success, returnedLoginResponse) in
                
                if let returnedLoginResponse = returnedLoginResponse {
                    
                    if returnedLoginResponse.code != 0 {
                        //print("Wrong Credentials Entered")
                        
                        DispatchQueue.main.async {
                            
                            let alert = UIAlertController(title: "Wrong Credentails Entered", message: "Kindly Check Your Username & Password", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: {
                                SVProgressHUD.dismiss()
                            })
                        }
                        
                    }else {
                        //Check If remember button is selected for user defaults
                        self.loginResultArray = returnedLoginResponse.results
                        
                        for values in self.loginResultArray {
                            
                            DispatchQueue.main.async {
                                if self.rememberMeBtn.isSelected {
                                    UserDefaults.standard.set(values.code, forKey: "employeeCode")
                                    UserDefaults.standard.set(values.coWorksEmployeeID, forKey: CO_WORKERS_EMPLOYEE_ID)
                                    UserDefaults.standard.set(values.fullName, forKey: Co_Worker_EMPLOYEE_NAME)
                                    self.usernameTxtField.text = self.usernameTxtField.text
                                    self.passwordTxtField.text = self.passwordTxtField.text
                                    
                                }else {
                                    UserDefaults.standard.set(values.code, forKey: "employeeCode")
                                    UserDefaults.standard.set(values.coWorksEmployeeID, forKey: CO_WORKERS_EMPLOYEE_ID)
                                    UserDefaults.standard.set(values.fullName, forKey: "employeeName")
                                    self.usernameTxtField.text = ""
                                    self.passwordTxtField.text = ""
                                    
                                }
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            print("Take The User To Successful Login Screen")
                            SVProgressHUD.dismiss()
                            
                            guard let homeTabbar = HOMEPAGE_STORYBOARD.instantiateViewController(withIdentifier: "HomeTabController") as? HomeTabController else {return}
                            self.navigationController?.pushViewController(homeTabbar, animated: true)
                            
                        }
                        
                    }
                    
                }
                
                
            }
            
        }
        
    }
    
}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let usernameText = usernameTxtField.text else {return}
        guard let passwordText = passwordTxtField.text else {return}
        USERNAME = usernameText
        PASSWORD = passwordText
    }
    
    
}
