//
//  LaundryCategoryTblCell.swift
//  Orize App
//
//  Created by Apple on 20/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol LaundryCategoryTblCellDelegate: class {
    
    func didTapViewAllButton(at index: Int, categoryData: LaundryCategoryData)
    
}

class LaundryCategoryTblCell: UITableViewCell {
    
    @IBOutlet weak var elementView: UIView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    weak var delegate: LaundryCategoryTblCellDelegate?
    private var tappedIndex: Int!
    private var categoryData: LaundryCategoryData!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    func configureCell(categoryData: LaundryCategoryData, delegate: LaundryCategoryTblCellDelegate, selectedCategoryIndex index: Int) {
        
        self.delegate = delegate
        self.tappedIndex = index
        self.categoryData = categoryData
        
        categoryNameLbl.text = categoryData.laundryCategoryName
        
    }
    
    @IBAction func viewAllBtnTapped(_ sender: UIButton) {
        delegate?.didTapViewAllButton(at: tappedIndex, categoryData: categoryData)
    }
    
    func setupCollectionViewDataSoruceDelegate(dataSoruceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        
        productCollectionView.register(UINib(nibName: LaundryXibs.LaundryCategoryCollCell, bundle: nil), forCellWithReuseIdentifier: LaundryXibs.LaundryCategoryCollCell)
        
        productCollectionView.delegate = dataSoruceDelegate
        productCollectionView.dataSource = dataSoruceDelegate
        productCollectionView.tag = row
        productCollectionView.reloadData()
        
    }
    
}
