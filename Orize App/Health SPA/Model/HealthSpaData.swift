//
//  HealthSpaData.swift
//  Orize App
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

struct SliderImage {
    var imageUrl: String
}

var sliderImageArr = [SliderImage(imageUrl: "http://www.eliteinteriorconcepts.com/image/new%20folder/gym/3.jpg"),
                       SliderImage(imageUrl: "https://www.technogym.com/wpress/wp-content/uploads/2019/02/Wllness-Premium.jpg"),
                       SliderImage(imageUrl: "https://alfahiminteriors.com/wp-content/uploads/2016/11/AF-SPAID000002.jpg")]

class HealthSpaPass {
    let timePeriod: String
    let price: Int
    var unitCount: Int
    
    init(timePeriod: String, price: Int, unitCount: Int) {
        self.timePeriod = timePeriod
        self.price = price
        self.unitCount = unitCount
    }
    
}

var healthSpaPassesArray = [HealthSpaPass(timePeriod: "One Hour Access", price: 100, unitCount: 0),
                            HealthSpaPass(timePeriod: "One Day Access", price: 250, unitCount: 0),
                            HealthSpaPass(timePeriod: "Monthly Access", price: 1500, unitCount: 0)]
