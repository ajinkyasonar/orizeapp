//
//  calendarPopupVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 18/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Toast_Swift
import IQKeyboardManagerSwift

protocol CalendarPopupVCDelegate {
    func didSelectedDate(selectedDateByUser: String, selectedDateByUserForApi: String)
}

class CalendarPopupVC: UIViewController {
    
    @IBOutlet weak var popupView: UIView!
    
    //Calendar Outlets
    @IBOutlet weak var calendarCollectionView: JTAppleCalendarView!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    
    //Variables
    let formatter = DateFormatter()
    var selectedDateByUser = ""
    var delegate: CalendarPopupVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        initaialUISetup()
        
    }
    
    func initaialUISetup() {
        calendarCollectionView.ibCalendarDelegate = self
        calendarCollectionView.ibCalendarDataSource = self
        setupCalendarUI()
        popupView.layer.cornerRadius = 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    //ACTIONS
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: CALENDAR FUNCTIONS
    func setupCalendarUI() {
        calendarCollectionView.minimumLineSpacing = 0
        calendarCollectionView.minimumInteritemSpacing = 0
        
        calendarCollectionView.visibleDates { (visibleDates) in
            self.setupMonthYearInitiallyCalendar(from: visibleDates)
        }
        calendarCollectionView.scrollToDate(Date(), animateScroll: false)
        //calendarCollectionView.selectDates([Date()])
        
    }
    
    func setupMonthYearInitiallyCalendar(from visibleDate: DateSegmentInfo) {
        
        //Setup Month & Year label in beginning
        
        guard let date = visibleDate.monthDates.first?.date else {return}
        
        self.formatter.dateFormat = "yyyy"
        self.yearLbl.text = self.formatter.string(from: date)
        
        self.formatter.dateFormat = "MMMM"
        self.monthLbl.text = self.formatter.string(from: date)
        
    }
    
    
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        
        guard let validCell = view as? CalendarPopupCell else {return}
        
        if cellState.isSelected {
            validCell.dateLbl.textColor = UIColor.white
        } else {
            
            if cellState.dateBelongsTo == .thisMonth {
                validCell.dateLbl.textColor = UIColor.black
            } else {
                validCell.dateLbl.textColor = UIColor.lightGray
            }
            
        }
        
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState) {
        
        guard let validCell = view as? CalendarPopupCell else {return}
        
        if cellState.isSelected {
            validCell.selectedView.isHidden = false
        } else {
            validCell.selectedView.isHidden = true
        }
        
    }
    
    
}

//MARK: CALENDAR IMPLEMENTATION HERE
extension CalendarPopupVC: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = Date()
        let endDate = formatter.date(from: "2030 12 31")
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate!)
        return parameters
        
    }
    
    
}

extension CalendarPopupVC: JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        guard let cell = calendarCollectionView.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarPopupCell", for: indexPath) as? CalendarPopupCell else {return JTAppleCell()}
        cell.dateLbl.text = cellState.text
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        return cell
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        //Display Logic for Date on Confirm Date View Label
        formatter.dateFormat = "yyyy MM dd"
        let todaysDateString = formatter.string(from: Date())
        let selectedDateByUserString = formatter.string(from: date)
        
        if (selectedDateByUserString < todaysDateString) {
            
            //print("Cant Select Those Dates")
            self.view.makeToast("Cant select previous dates", duration: 1.0, position: .bottom)
            
        }else {
            
            formatter.dateFormat = "dd-MM-yyyy"
            let selectedDate = date
            let selectedDateString = formatter.string(from: selectedDate)
            
            let apiDateFormatter = DateFormatter()
            apiDateFormatter.dateFormat = "yyyy-MM-dd"
            let selectedDateForApiString = apiDateFormatter.string(from: selectedDate)
            
            delegate?.didSelectedDate(selectedDateByUser: selectedDateString, selectedDateByUserForApi: selectedDateForApiString)
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
        setupMonthYearInitiallyCalendar(from: visibleDates)
        
    }


}
