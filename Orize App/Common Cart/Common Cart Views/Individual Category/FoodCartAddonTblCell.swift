//
//  FoodCartAddonTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 01/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class FoodCartAddonTblCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productDescriptionLbl: UILabel!
    @IBOutlet private weak var productAmountLbl: UILabel!
    @IBOutlet private weak var productUnitCountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func removeButtonTapped(_ sender: UIButton) {
        
    }
    
}
