//
//  ExclusiveServiceDetailVC.swift
//  Orize App
//
//  Created by Apple on 14/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import FSCalendar
import Toast_Swift

class ExclusiveServiceDetailVC: UIViewController {
    
    @IBOutlet weak var mainDisplayImage: UIImageView!
    @IBOutlet weak var additionalImageCollectionView: UICollectionView!
    @IBOutlet weak var exclusiveServiceNameLbl: UILabel!
    @IBOutlet weak var exclusiveServicePriceLbl: UILabel!
    @IBOutlet weak var exclusiveServiceDescriptionLbl: UILabel!
    @IBOutlet weak var timeSelectionCollectionView: UICollectionView!
    @IBOutlet weak var timeSelectionCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addOnTableView: UITableView!
    @IBOutlet weak var addOnTableViewHeightConstraint: NSLayoutConstraint!
    
    //Calendar
    @IBOutlet weak var calendar: FSCalendar!
    
    var exclusiveServiceDetail: ExclusiveServices!
    let formatter = DateFormatter()
    var selectedTimingArray = [String]()
    var selectedDatesArray = [String]()
    
    //Flags
    var isTimeSelected = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        
        guard let mainImageUrl = URL(string: exclusiveServiceDetail.serviceImages.first ?? "") else {return}
        mainDisplayImage.sd_setImage(with: mainImageUrl, completed: .none)
        
        mainDisplayImage.layer.cornerRadius = 10
        mainDisplayImage.layer.masksToBounds = true
        mainDisplayImage.layer.borderWidth = 0.1
        mainDisplayImage.layer.borderColor = UIColor.black.cgColor
        
        additionalImageCollectionView.delegate = self
        additionalImageCollectionView.dataSource = self
        additionalImageCollectionView.register(UINib(nibName: ExclusiveServicesXibs.ExclusiveServiceImagesCollCell, bundle: nil), forCellWithReuseIdentifier: ExclusiveServicesXibs.ExclusiveServiceImagesCollCell)
        
        timeSelectionCollectionView.delegate = self
        timeSelectionCollectionView.dataSource = self
        timeSelectionCollectionView.allowsMultipleSelection = true
        
        addOnTableView.delegate = self
        addOnTableView.dataSource = self
        addOnTableView.register(UINib(nibName: ExclusiveServicesXibs.ExclusiveServiceAddonTblCell, bundle: nil), forCellReuseIdentifier: ExclusiveServicesXibs.ExclusiveServiceAddonTblCell)
        
        exclusiveServiceNameLbl.text = exclusiveServiceDetail.serviceName
        exclusiveServicePriceLbl.text = "\(rupee) \(exclusiveServiceDetail.servicePrice) per hour"
        exclusiveServiceDescriptionLbl.text = exclusiveServiceDetail.serviceDescription
        
        initialCalendarSetup()
        
    }
    
    fileprivate func initialCalendarSetup() {
        
        //Calendar
        calendar.delegate = self
        calendar.dataSource = self
        
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        calendar.scope = .month
        calendar.allowsMultipleSelection = true
        
        timeSelectionCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        addOnTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        timeSelectionCollectionView.layer.removeAllAnimations()
        timeSelectionCollectionViewHeightConstraint.constant = timeSelectionCollectionView.contentSize.height
        
        addOnTableView.layer.removeAllAnimations()
        addOnTableViewHeightConstraint.constant = addOnTableView.contentSize.height
        
        self.updateViewConstraints()
        self.view.layoutIfNeeded()
        
    }
    
    
    
    //MARK: IB-ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK: COLLETION VIEW IMPLEMENTATION
extension ExclusiveServiceDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == additionalImageCollectionView {
            
            return exclusiveServiceDetail.serviceImages.count
        }
        else {
            
            return exclusiveServiceDetail.serviceTimings.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == additionalImageCollectionView {
           
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExclusiveServicesXibs.ExclusiveServiceImagesCollCell, for: indexPath) as? ExclusiveServiceImagesCollCell else {return UICollectionViewCell()}
            
            cell.configureCell(serviceDetail: exclusiveServiceDetail, at: indexPath.item)
            
            return cell
            
        }
        else {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExclusiveTimingCollCell", for: indexPath) as? ExclusiveTimingCollCell else {return UICollectionViewCell()}
            
            cell.configureCell(eventDetail: exclusiveServiceDetail, at: indexPath.item)
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == additionalImageCollectionView {
           
            return CGSize(width: 100, height: 100)
        }
        else {
            
            let width = timeSelectionCollectionView.bounds.width
            
            return CGSize(width: (width / 2) - 20, height: 60)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView == timeSelectionCollectionView {
            
            return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
            
        }
        else {
            
            return UIEdgeInsets()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == additionalImageCollectionView {
            
            let selectedImage = exclusiveServiceDetail.serviceImages[indexPath.item]
            guard let selectedImageUrl = URL(string: selectedImage) else {return}
            mainDisplayImage.sd_setImage(with: selectedImageUrl, completed: .none)
            
        }
        else {
            
            let selectedTime = exclusiveServiceDetail.serviceTimings[indexPath.item]
            selectedTimingArray.append(selectedTime)
            print(selectedTimingArray)
            self.view.makeToast(selectedTimingArray.joined(separator: ","), duration: 1.0, position: .center)
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == timeSelectionCollectionView {
            
            let selectedTime = exclusiveServiceDetail.serviceTimings[indexPath.item]
            
            if let selectedIndex = selectedTimingArray.lastIndex(of: selectedTime) {
                selectedTimingArray.remove(at: selectedIndex)
            }
            
            print(selectedTimingArray)
            self.view.makeToast(selectedTimingArray.joined(separator: ","), duration: 1.0, position: .center)
            
        }
        
    }
    
    
}

//MARK: Calendar Implementation
extension ExclusiveServiceDetailVC: FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        guard let endDate = Calendar.current.date(byAdding: .month, value: 1, to: Date(), wrappingComponents: false) else {return Date()}
        
        return endDate
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let selectedDate = formatter.string(from: date)
        selectedDatesArray.append(selectedDate)
        print(selectedDatesArray)
        
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let deselectedDate = formatter.string(from: date)
        
        if let index = selectedDatesArray.lastIndex(of: deselectedDate) {
            selectedDatesArray.remove(at: index)
        }
        print(selectedDatesArray)
    }
    
}


//MARK: TABLE VIEW IMPLEMENTATION
extension ExclusiveServiceDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return addOnItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExclusiveServicesXibs.ExclusiveServiceAddonTblCell) as? ExclusiveServiceAddonTblCell else {return UITableViewCell()}
        
        cell.configureCell(delegate: self, addonDetail: addOnItemsArray[indexPath.row], at: indexPath.row)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    }
    
}

//MARK: EXCLUSIVE SERVICE ADD ON TABLE CELL DELEGATE IMPLEMENTATION
extension ExclusiveServiceDetailVC: ExclusiveServiceAddonTblCellDelegate {
    
    func didTapAddButton(addOnItem item: AddOnItems, at index: Int, itemCount: Int) {
        
        print("Selected Item: \(item) at index: \(index) and count is: \(itemCount)")
    }
    
    func didTapRemoveButton(addOnItem item: AddOnItems, at index: Int, itemCount: Int) {
        
        print("Selected Item: \(item) at index: \(index) and count is: \(itemCount)")
    }
    
    
}
