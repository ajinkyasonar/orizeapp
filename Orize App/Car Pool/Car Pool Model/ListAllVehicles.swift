//
//  ListAllVehicles.swift
//  Orize App
//
//  Created by Aditya Infotech on 20/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let listAllVehicles = try? newJSONDecoder().decode(ListAllVehicles.self, from: jsonData)

import Foundation

struct ListAllVehicles: Codable {
    let code: Int
    let status, message: String
    let results: [ListAllVehiclesResult]
}

struct ListAllVehiclesResult: Codable {
    let carPoolVehicleTravelID: Int
    let driverName, vehicleNo, destination, travelDateApp: String
    let travelTimeApp: String
    let availableSeats: Int
    let remarks: String
    
    enum CodingKeys: String, CodingKey {
        case carPoolVehicleTravelID = "car_pool_vehicle_travel_id"
        case driverName = "driver_name"
        case vehicleNo = "vehicle_no"
        case destination
        case travelDateApp = "travel_date_app"
        case travelTimeApp = "travel_time_app"
        case availableSeats = "available_seats"
        case remarks
    }
}

