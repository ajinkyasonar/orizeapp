//
//  CommonCartVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 30/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import CarbonKit
import Toast_Swift

class CommonCartVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var logoAndRightButtonsView: UIView!
    
    //Variables
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var bookingResultsArray = [BookingResults]()
    var otherModuleArray = [ModuleBooking]()
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var items = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getAllCartList(byEmployeeId: coWorkerEmployeeId)
        
    }
    
    //Actions
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

}

extension CommonCartVC: CarbonTabSwipeNavigationDelegate {
    
    private func configureCarbonKit() {
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: logoAndRightButtonsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 50))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation?.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view , attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        
        
        //Appearance of Menu
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        carbonTabSwipeNavigation?.setNormalColor(UIColor.white)
        carbonTabSwipeNavigation?.setSelectedColor(UIColor.white)
        carbonTabSwipeNavigation?.setIndicatorColor(UIColor.white)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            carbonTabSwipeNavigation?.setTabExtraWidth(30)
            carbonTabSwipeNavigation?.setTabBarHeight(60)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 25)!)
        }else {
            carbonTabSwipeNavigation?.setTabBarHeight(50)
            carbonTabSwipeNavigation?.setTabExtraWidth(20)
            carbonTabSwipeNavigation?.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
            carbonTabSwipeNavigation?.setSelectedColor(UIColor.white, font: UIFont(name: "Montserrat-Regular", size: 18)!)
        }
    
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let index = Int(index)
        
        if index == 0 {
            
            guard let allCartVc = storyboard?.instantiateViewController(withIdentifier: "AllCartVC") as? AllCartVC else {return UIViewController()}
            allCartVc.bookingResultsArray = bookingResultsArray
            allCartVc.delegate = self
            return allCartVc
            
        } else {
            
            guard let individualCategoryVc = storyboard?.instantiateViewController(withIdentifier: "IndividualCategoryCartVC") as? IndividualCategoryCartVC else {return UIViewController()}
            individualCategoryVc.moduleBooking = bookingResultsArray[0].data[index - 1]
            individualCategoryVc.categoryModuleId = bookingResultsArray[0].data[index - 1].moduleID
            return individualCategoryVc
            
        }
        
    }
    
}

//MARK: ALL CART VC Delegate Implementation
extension CommonCartVC: AllCartVCDelegate {
    
    func passModuleDataToRootVc(moduleDetails: ModuleBooking, moduleAt index: Int) {
        let carbonTabIndex = UInt(index)
        carbonTabSwipeNavigation?.setCurrentTabIndex(carbonTabIndex, withAnimation: true)
    }
    
}



//MARK: API IMPLEMENTATATION
extension CommonCartVC {
    
    func getAllCartList(byEmployeeId employeeId: Int) {
        
        bookingResultsArray.removeAll()
        otherModuleArray.removeAll()
        self.view.makeToastActivity(.center)
        
        CommonCartServices.instance.getAllCartList(byEmployeeId: employeeId) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.bookingResultsArray = returnedResponse.results
                        
                        //Append all the module names to carbonkit items
                        self.bookingResultsArray.forEach({ (detail) in
                            self.items.append(detail.moduleName)
                            
                            
                            self.otherModuleArray = detail.data
                            detail.data.forEach({ (detail) in
                                self.items.append(detail.moduleName.rawValue)
                            })
                        })
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.configureCarbonKit()
                        }
                        //print(self.bookingResultsArray)
                        //print(self.items)
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
    

