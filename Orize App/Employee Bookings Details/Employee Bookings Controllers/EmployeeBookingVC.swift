//
//  EmployeeBookingDetailsVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import CarbonKit
import Toast_Swift

class EmployeeBookingVC: UIViewController {
    
    //Outlets
    @IBOutlet private weak var logoAndRightButtonsView: UIView!
    @IBOutlet weak var staticBookingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //Variables
    var employeeBookingArray = [BookingDetails]()
    var coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    let presentBookingId = 0 //For Present Bookings Use 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getEmployeeBookings(coWorkerEmployeeId: coWorkerEmployeeId)
    }
    
    private func setupInitialView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: EmployeeBookingRegisteredXibs.EmployeeBookingsTblCell, bundle: nil), forCellReuseIdentifier: EmployeeBookingRegisteredXibs.EmployeeBookingsTblCell)
        
    }
    
    //IB-ACTIONS HERE
    @IBAction func pastBookingsBtnTapped(_ sender: UIButton) {
        guard let pastBookingVc = storyboard?.instantiateViewController(withIdentifier: "EmployeePastBookingsVC") as? EmployeePastBookingsVC else {return}
        self.navigationController?.pushViewController(pastBookingVc, animated: true)
    }
    
}

extension EmployeeBookingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employeeBookingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeBookingRegisteredXibs.EmployeeBookingsTblCell, for: indexPath) as? EmployeeBookingsTblCell else {return UITableViewCell()}
        
        cell.configureCell(bookingDetails: employeeBookingArray[indexPath.row], delegate: self, bookingDetailInt: indexPath.row)
        return cell
    }
    
    
}

//MARK: Employee Booking Cell Protocol Implementation
extension EmployeeBookingVC: EmployeeBookingsTblCellDelegate {
    
    func viewButtonTapped(bookingDetails: BookingDetails, tag: Int) {
        guard let bookingDetailsVc = storyboard?.instantiateViewController(withIdentifier: "EmployeeBookingDetailsVC") as? EmployeeBookingDetailsVC else {return}
        
        bookingDetailsVc.bookingNumber = bookingDetails.bookingNo
        bookingDetailsVc.orderType = bookingDetails.type
        
        navigationController?.pushViewController(bookingDetailsVc, animated: true)
        
    }
    
    func cancelButtonTapped(bookingDetails: BookingDetails, tag: Int) {
        //Present Booking Cancellation API Service
        //print(bookingDetails)
        let orderStatusValue = 3 // This is For Cancellation of Request
        cancelAllEmployeeAllOrdersStatus(withCommonOrderId: bookingDetails.bookingNo, orderStatusValue: orderStatusValue, categoryType: bookingDetails.type)
    }
    
}


//MARK: API SERVICES IMPLEMENTATION
extension EmployeeBookingVC {
    
    //Get All Employee Bookings Service
    func getEmployeeBookings(coWorkerEmployeeId: Int) {
        
        self.view.makeToastActivity(.center)
        
        EmployeeBookingServices.instance.getEmployeeBookings(coWorkerEmployeeId: coWorkerEmployeeId, bookingPeriodId: presentBookingId) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.employeeBookingArray = returnedResponse.results
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        //print(self.employeeBookingArray)
                    } else {
                        //Failure
                        debugPrint(returnedResponse.message)
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    //MARK: CHANGE ALL ORDER STATUS API SERVICE
    func cancelAllEmployeeAllOrdersStatus(withCommonOrderId orderId: String, orderStatusValue status: Int, categoryType type: String) {
        
        EmployeeBookingServices.instance.cancelChangeEmployeeBookingALLOrderStatus(withOrderId: orderId, orderStatus: status, categoryType: type) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.getEmployeeBookings(coWorkerEmployeeId: self.coWorkerEmployeeId)
                        }
                    }else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
            }
            
            
        }
        
    }
    
}
