//
//  IndividualEventVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 17/09/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class IndividualEventVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var individualEventData: [AmphiEvent]!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    fileprivate func initialSetup() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: AmphiTheatreXibs.AmphiAllEventsTblCell, bundle: nil), forCellReuseIdentifier: AmphiTheatreXibs.AmphiAllEventsTblCell)
    }

}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension IndividualEventVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return individualEventData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AmphiAllEventsTblCell") as? AmphiAllEventsTblCell else {return UITableViewCell()}
        
        cell.configureCell(event: individualEventData[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedEvent = individualEventData[indexPath.row]
        
        guard let amphiCartVc = storyboard?.instantiateViewController(withIdentifier: "AmphiTheatreCartVC") as? AmphiTheatreCartVC else {return}
        
        amphiCartVc.selectedAmphiEvent = selectedEvent
        
        navigationController?.pushViewController(amphiCartVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
