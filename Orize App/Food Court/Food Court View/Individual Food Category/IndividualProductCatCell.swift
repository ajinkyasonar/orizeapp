//
//  IndividualProductCatCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 28/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import Cosmos
import Toast_Swift

protocol IndividualProductCatCellDelegate {
    
    func addBtnTapped(product: FoodCourtProductsDetails, productInt: Int, productQuantity: Int)
    
    func minusBtnTapped(product: FoodCourtProductsDetails, productInt: Int, productQuantity: Int)
    
}

class IndividualProductCatCell: UICollectionViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    @IBOutlet private weak var unitLbl: UILabel!

    var counter = 0
    
    var delegate: IndividualProductCatCellDelegate?
    var productInt: Int!
    var productQuantity: Int!
    var product: FoodCourtProductsDetails!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        elementView.layer.cornerRadius = 10
        elementView.layer.borderWidth = 0.3
        elementView.layer.borderColor = UIColor.black.cgColor
    }
    
    func configureCell(product: FoodCourtProductsDetails, productInt: Int, delegate: IndividualProductCatCellDelegate) {
        
        self.product = product
        self.productInt = productInt
        self.delegate = delegate
        
        guard let productImageUrl = URL(string: product.productPhoto) else {return}
        productImage.sd_setImage(with: productImageUrl)
        
        productNameLbl.text = product.productName
        productPriceLbl.text = "\(rupee) \(product.productPriceWithTax)"
   
        counter = 0
        unitLbl.text = "\(counter)"
        
        
    }
    
    @IBAction func minusBtnTapped(_ sender: UIButton) {
        if counter > 0 {
            counter -= 1
            unitLbl.text = "\(counter)"
        }else {
            self.makeToast("Lowest Count", duration: 1.0, position: .bottom)
        }
        
        delegate?.minusBtnTapped(product: product, productInt: productInt, productQuantity: counter)
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        
        guard let product = product else {return}
        if counter < product.productAllowedQtyInCart {
            counter += 1
            unitLbl.text = "\(counter)"
        }else {
            self.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
        }
        
        delegate?.addBtnTapped(product: product, productInt: productInt, productQuantity: counter)
    }

}
