//
//  CarpoolServices.swift
//  Orize App
//
//  Created by Aditya Infotech on 19/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class CarpoolServices {
    static let instance = CarpoolServices()
    
    //MARK: LIST YOUR CAR API IMPLEMENTATION
    
    func listYourCarService(coWorkerEmployeeId: Int, driverName: String, vehicleNumber: String, destination: String, dateTimeString timing: String, numberOfSeats: Int, additionalRemarks: String, isSaveAllowedValue: Int, parkingLevel: String, completion:@escaping(ListYourCar?, Error?) -> ()) {
        
        guard let url = URL(string: LIST_YOUR_CAR_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkerEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&driver_name=\(driverName)".data(using: String.Encoding.utf8)!)
        postData.append("&vehicle_no=\(vehicleNumber)".data(using: String.Encoding.utf8)!)
        postData.append("&destination=\(destination)".data(using: String.Encoding.utf8)!)
        postData.append("&travel_datetime=\(timing)".data(using: String.Encoding.utf8)!)
        postData.append("&number_of_seats=\(numberOfSeats)".data(using: String.Encoding.utf8)!)
        postData.append("&remarks=\(additionalRemarks)".data(using: String.Encoding.utf8)!)
        postData.append("&is_save_allowed=\(isSaveAllowedValue)".data(using: String.Encoding.utf8)!)
        postData.append("&parking_level=\(parkingLevel)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedListYourCarResponse = try decoder.decode(ListYourCar.self, from: data)
                    completion(returnedListYourCarResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
        
    }
    
    //MARK: LIST YOUR CAR UPDATE URL IMPLEMENTATION
    
    func listYourCarUpdateService(coWorkerEmployeeId: Int, carpoolVehicleTravelId: Int ,driverName: String, vehicleNumber: String, destination: String, dateTimeString timing: String, numberOfSeats: Int, additionalRemarks: String, isSaveAllowedValue: Int, parkingLevel: String, completion:@escaping(ListYourCarUpdate?, Error?)->()) {
        
        guard let url = URL(string: LIST_YOUR_CAR_UPDATE_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkerEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&car_pool_vehicle_travel_id=\(carpoolVehicleTravelId)".data(using: String.Encoding.utf8)!)
        postData.append("&driver_name=\(driverName)".data(using: String.Encoding.utf8)!)
        postData.append("&vehicle_no=\(vehicleNumber)".data(using: String.Encoding.utf8)!)
        postData.append("&destination=\(destination)".data(using: String.Encoding.utf8)!)
        postData.append("&travel_datetime=\(timing)".data(using: String.Encoding.utf8)!)
        postData.append("&number_of_seats=\(numberOfSeats)".data(using: String.Encoding.utf8)!)
        postData.append("&remarks=\(additionalRemarks)".data(using: String.Encoding.utf8)!)
        postData.append("&is_save_allowed=\(isSaveAllowedValue)".data(using: String.Encoding.utf8)!)
        postData.append("&parking_level=\(parkingLevel)".data(using: String.Encoding.utf8)!)
        
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedListYourCarUpdateResponse = try decoder.decode(ListYourCarUpdate.self, from: data)
                    completion(returnedListYourCarUpdateResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
        
    }
    
    //MARK: LIST YOUR CAR CANCEL API IMPLEMENTATION
    
    func listYourCarCancelService(coWorkerEmployeeId: Int, carpoolVehicleTravelId: Int, completion:@escaping(ListYourCarCancel?, Error?)->()) {
        
        guard let url = URL(string: LIST_YOUR_CAR_CANCEL_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkerEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&car_pool_vehicle_travel_id=\(carpoolVehicleTravelId)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedListYourCarCancelResponse = try decoder.decode(ListYourCarCancel.self, from: data)
                    completion(returnedListYourCarCancelResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
        
        
    }
    
    //MARK: FIND & REQUEST CAR POOL IMPLEMENTATION
    
    //MARK: Get List of all Carpool Vehicles
    func findCarpoolGetAllVehicleListService(coWorkerEmployeeId: Int, completion:@escaping(ListAllVehicles?, Error?)->()) {
        
        guard let url = URL(string: FIND_CARPOOL_GET_ALL_VEHICLES_LIST_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 20)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkerEmployeeId)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "")
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedListOfAllVehiclesResponse = try decoder.decode(ListAllVehicles.self, from: data)
                    completion(returnedListOfAllVehiclesResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
        
        
    }
    
    //MARK: REQUEST CARPOOL API IMPLEMENTATION
    func requestCarpoolService(coWorkerEmployeeId: Int, carpoolVehicleTravelId: Int, completion:@escaping(RequestCarpool?, Error?)->()) {
        
        guard let url = URL(string: REQUEST_CARPOOL_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkerEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&car_pool_vehicle_travel_id=\(carpoolVehicleTravelId)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedRequestCarpoolResponse = try decoder.decode(RequestCarpool.self, from: data)
                    completion(returnedRequestCarpoolResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
        
    }
    
    //MARK: CANCEL CARPOOL API IMPLEMENTATION
    
    func cancelCarpoolService(coWorkersEmployeeId: Int, carpoolVehicleRequestId: Int, completion:@escaping(CancelCarpool?, Error?)->()) {
        
        guard let url = URL(string: CANCEL_CARPOOL_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&car_pool_vehicle_request_id=\(carpoolVehicleRequestId)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedCancelledCarpoolResponse = try decoder.decode(CancelCarpool.self, from: data)
                    completion(returnedCancelledCarpoolResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
            }.resume()
    }
    
    
    //MARK: UPDATE CARPOOL API IMPLEMENTATION
    func updateCarpoolService(coWorkerEmployeeId: Int, carpoolVehicleRequestId: Int, description: String, completion:@escaping(UpdateCarpool?, Error?)->()) {
        
        guard let url = URL(string: UPDATE_CARPOOL_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkerEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&car_pool_vehicle_request_id=\(carpoolVehicleRequestId)".data(using: String.Encoding.utf8)!)
        postData.append("&description=\(description)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                
                print(error.debugDescription)
                completion(nil, error)
                return
                
            } else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedUpdateCarpoolResponse = try decoder.decode(UpdateCarpool.self, from: data)
                    completion(returnedUpdateCarpoolResponse, nil)
                }catch{
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
            }
            
        }.resume()
        
    }
    
    
    //EMPLOYEE CARPOOL LISTING -> Your Carpool Listing API IMPLEMENTATION
    func getEmployeeCarpoolListingService(coWorkersEmployeeId: Int, completion:@escaping(EmployeeCarpoolListing?, Error?)->()) {
        
        guard let url = URL(string: EMPLOYEE_CARPOOL_LISTING_URL) else {return}
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedEmployeeVehicleListResponse = try decoder.decode(EmployeeCarpoolListing.self, from: data)
                    completion(returnedEmployeeVehicleListResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }.resume()
        
    }
    
    //MARK: REMOVE VEHICLE API SERVICE
    func removeVehicleService(coWorkersEmployeeId: Int, carpoolVehicleTravelId: Int, completion:@escaping(RemoveVehicle?, Error?)->()) {
        
        guard let url = URL(string: REMOVE_VEHICLE_URL) else {return}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&car_pool_vehicle_travel_id=\(carpoolVehicleTravelId)".data(using: String.Encoding.utf8)!)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedRemovedVehicleResponse = try decoder.decode(RemoveVehicle.self, from: data)
                    completion(returnedRemovedVehicleResponse, nil)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                
            }
            
        }.resume()
        
    }
    
}
