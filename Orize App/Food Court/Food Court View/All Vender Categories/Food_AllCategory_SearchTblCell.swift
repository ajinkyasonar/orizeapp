//
//  Food_AllCategory_SearchTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 01/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class Food_AllCategory_SearchTblCell: UITableViewCell {
    
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productCategoryLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        productNameLbl.text = "Hello"
        
    }
    
    func configureCell(searchData: SearchAutocompleteData) {
        
        productNameLbl.text = searchData.name
        productCategoryLbl.text = searchData.typename
        
        switch searchData.typename {
        case "products":
            guard let imageUrl = URL(string: searchData.productPhoto ?? "") else {return}
            productImage.sd_setImage(with: imageUrl)
            break
        default:
            guard let imageUrl = URL(string: searchData.photo ?? "") else {return}
            productImage.sd_setImage(with: imageUrl)
        }
        
    }
    
    
    
}
