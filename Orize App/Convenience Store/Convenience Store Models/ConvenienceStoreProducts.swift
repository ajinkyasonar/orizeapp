//
//  ConvenienceStoreProducts.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 01/02/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// MARK: - ConvenienceStoreProducts
struct ConvenienceStoreProducts: Codable {
    let code: Int
    let status, message: String
    let results: [ConvenienceStoreProductsResult]
}

// MARK: - Result
struct ConvenienceStoreProductsResult: Codable {
    let searchAutocompleteData: [SearchAutocompleteDatum]
    let productData: [ProductDatum]
    
    enum CodingKeys: String, CodingKey {
        case searchAutocompleteData = "search_autocomplete_data"
        case productData = "product_data"
    }
}

// MARK: - ProductDatum
struct ProductDatum: Codable {
    let storeCategoryID: Int
    let storeCategoryName: String
    let productsListArr: [ProductsListArr]
    
    enum CodingKeys: String, CodingKey {
        case storeCategoryID = "store_category_id"
        case storeCategoryName = "store_category_name"
        case productsListArr = "products_list_arr"
    }
}

// MARK: - ProductsListArr
struct ProductsListArr: Codable {
    let productID: Int
    let storeCategoryName, productName: String
    let productPhoto: String
    let productPrice: Int
    let productUnit, productDescription: String
    let productAllowedQtyInCart: Int
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case storeCategoryName = "store_category_name"
        case productName = "product_name"
        case productPhoto = "product_photo"
        case productPrice = "product_price"
        case productUnit = "product_unit"
        case productDescription = "product_description"
        case productAllowedQtyInCart = "product_allowed_qty_in_cart"
        
    }
}

// MARK: - SearchAutocompleteDatum
struct SearchAutocompleteDatum: Codable {
    let id: Int
    let name: String
    let categoryPhoto: String
    let typeval: Int
    let typename: String
    let categoryID: Int?
    let categoryName: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case categoryPhoto = "category_photo"
        case typeval, typename
        case categoryID = "category_id"
        case categoryName = "category_name"
    }
}
