//
//  SelectedSportBookingVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 09/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import JTAppleCalendar
import SDWebImage
import Toast_Swift


class SelectedSportBookingVC: UIViewController {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Calendar Outlets
    @IBOutlet weak var calendarCollectionView: JTAppleCalendarView!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    
    //Outlets
    @IBOutlet weak var gameTitleLbl: UILabel!
    @IBOutlet weak var gameImage: UIImageView!
    
    @IBOutlet weak var bookCourtBtn: UIButton!
    @IBOutlet weak var challengesBtn: UIButton!
    @IBOutlet weak var confirmSelectedDateLbl: UILabel!
    
    @IBOutlet weak var timingsCollectionView: UICollectionView!
    @IBOutlet weak var timingsCollectionViewHeightConstraint: NSLayoutConstraint!
    
    //POPUP VIEW OUTLETS
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var popupContentView: UIView!
    @IBOutlet weak var selectedGameLbl: UILabel!
    @IBOutlet weak var selectedDateLbl: UILabel!
    @IBOutlet weak var selectedTimeLbl: UILabel!
    @IBOutlet weak var sportsCourtBookingPriceDisplayLbl: UILabel!
    @IBOutlet weak var termsConditionsCheckboxBtn: UIButton!
    @IBOutlet weak var bookSportsCourtChallengeBtn: UIButton!
    
    //Hiding Views
    @IBOutlet weak var challengeSwitchStack: UIStackView!
    
    //Dimension changing constraints
    @IBOutlet weak var popupViewHeightConstraint: NSLayoutConstraint!
    
    //Variables
    let coWorkersEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    let formatter = DateFormatter()
    var selectedDateByUser = ""
    let rupeeSymbol = "\u{20B9}"
    var discountedPriceForRazorpay = Int()
    var selectedSportsTimingId = Int()
    var sportsCourtBookingId = Int()
    var bookingAmountValue = Int()
    var secondBookDiscountValue = Int()
    
    var sportsTimingsResultArray = [SportsTimingsResult]()
    var sportsAvailableDatesListArray = [SportsAvailabilityDatesList]()
    var challengeAvailableDatesListArray = [SportsAvailabilityDatesList]()
    var sportsTimingsArray = [SportsTimingsArray]()
    
    //Flags
    var categorySelectedByUserTag = 1
    var isChallengeSwitchValue = 1
    var termsAndConditionCheckboxBtnValue = 0
    
    //Variables to get
    var gameTitleString: String?
    var gameImageUrl: String?
    var sportsTypeId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    func initialUISetup() {
        
        getSelectedSportsTimingsService(withSportsTypeId: sportsTypeId ?? -1, andCoWorkerEmployeeId: coWorkersEmployeeId)
        calendarCollectionView.ibCalendarDelegate = self
        calendarCollectionView.ibCalendarDataSource = self
        setupCalendarUI()
        
        gameTitleLbl.text = gameTitleString ?? ""
        guard let gameImageUrl = URL(string: gameImageUrl ?? "") else {return}
        gameImage.sd_setImage(with: gameImageUrl)
        
        timingsCollectionView.delegate = self
        timingsCollectionView.dataSource = self
        
        self.timingsCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        setupInitialPopupView()
        
    }
    
    func setupInitialPopupView() {
        
        popupContentView.layer.cornerRadius = 5
        popupContentView.layer.borderWidth = 0.2
        popupContentView.layer.borderColor = UIColor.black.cgColor
        
        popupView.isHidden = true
    }
    
    //MARK: ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        timingsCollectionView.layer.removeAllAnimations()
        timingsCollectionViewHeightConstraint.constant = timingsCollectionView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        
    }
    
    @IBAction func bookCourtOrChallengesButtonTapped(_ sender: UIButton) {
        //Tag 1 - Book Button and Tag 2 - Challenges
        
        termsConditionsCheckboxBtn.isSelected = false
        termsConditionsCheckboxBtn.setImage(UIImage(named: "checkboxempty"), for: .normal)
        termsAndConditionCheckboxBtnValue = 0
        switch sender.tag {
        case 1:
            
            bookCourtBtn.backgroundColor = UIColor.white
            bookCourtBtn.layer.borderWidth = 2
            bookCourtBtn.layer.borderColor =  #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
            
            challengesBtn.backgroundColor =  #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 0.6)
            challengesBtn.layer.borderWidth = 0
            challengesBtn.layer.borderColor = UIColor.clear.cgColor
            
            categorySelectedByUserTag = sender.tag
            
            break
            
        case 2:
            
            challengesBtn.backgroundColor = UIColor.white
            challengesBtn.layer.borderWidth = 2
            challengesBtn.layer.borderColor =  #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
            
            bookCourtBtn.backgroundColor =  #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 0.6)
            bookCourtBtn.layer.borderWidth = 0
            bookCourtBtn.layer.borderColor = UIColor.clear.cgColor
            
            categorySelectedByUserTag = sender.tag
            
            break
            
        default:
            return
            
        }
        checkSelectedCategoryByUser()
    }
    
    
    //MARK: ACTIONS WITH POPUP VIEW
    
    @IBAction func popupCloseButtonTapped(_ sender: UIButton) {
        
        popupView.isHidden = true
        
    }
    
    //MARK: IS Challenge Switch Action
    @IBAction func isChallengeSwitchValueChanged(sender: UISwitch) {
        
        if sender.isOn {
            isChallengeSwitchValue = 1
        } else {
            isChallengeSwitchValue = 0
        }
        
        print(isChallengeSwitchValue)
    }
    
    //MARK: Terms & Conditions CheckBox Button Tapped
    @IBAction func termsConditionsCheckboxBtnTapped(_ sender: UIButton) {
        
        termsConditionsCheckboxBtn.isSelected = !termsConditionsCheckboxBtn.isSelected
        
        switch termsConditionsCheckboxBtn.isSelected {
        case true:
            sender.setImage(UIImage(named: "checked"), for: .normal)
            termsAndConditionCheckboxBtnValue = 1
        case false:
            sender.setImage(UIImage(named: "checkboxempty"), for: .normal)
            termsAndConditionCheckboxBtnValue = 0
        }
        
    }
    
    //MARK: TERMS & CONDITIONS Button Tapped
    @IBAction func termsConditionButtonTapped(_ sender: UIButton) {
        print("termsConditionButtonTapped")
    }
    
    //MARK: BOOK SPORTS COURT BUTTON TAPPED
    @IBAction func bookSportsCourtBtnTapped(_ sender: UIButton) {
        
        print(isChallengeSwitchValue)
        
        if self.termsAndConditionCheckboxBtnValue == 0 {
            
            self.view.makeToast("Kindly agree to terms & conditions to proceed by selecting the checkbox", duration: 3.0, position: .bottom)
            
        } else {
            
            //MARK: GO TO GATEWAY SELECTION SCREEN
            
            guard let gatewaySelectionVc = storyboard?.instantiateViewController(withIdentifier: "SportsGatewaySelectionVC") as? SportsGatewaySelectionVC else {return}
            gatewaySelectionVc.paymentGatewayDict = ["sportsTypeId": self.sportsTypeId ?? -1,
                                                     "sportsTimingId" : self.selectedSportsTimingId,
                                                     "isChallenge": isChallengeSwitchValue,
                                                     "bookingDate": self.selectedDateByUser,
                                                     "apiBookingAmount": self.bookingAmountValue,
                                                     "apiSecondBookDiscount": self.secondBookDiscountValue,
                                                     "discountedPriceToDisplay": self.discountedPriceForRazorpay,
                                                     "sportNameDisplay":self.gameTitleString ?? "",
                                                     "categoryTagSelectedByUser":self.categorySelectedByUserTag,
                                                     "sportsCourtBookingId": sportsCourtBookingId
            ]
            self.navigationController?.pushViewController(gatewaySelectionVc, animated: true)
            
        }
        
    }
    
    
    
    //MARK: CALENDAR FUNCTIONS
    
    func setupCalendarUI() {
        
        calendarCollectionView.minimumLineSpacing = 0
        calendarCollectionView.minimumInteritemSpacing = 0
        
        calendarCollectionView.visibleDates { (visibleDates) in
            self.setupMonthYearInitiallyCalendar(from: visibleDates)
        }
        calendarCollectionView.scrollToDate(Date(), animateScroll: false)
        calendarCollectionView.selectDates([Date()])
        
    }
    
    func setupMonthYearInitiallyCalendar(from visibleDate: DateSegmentInfo) {
        
        //Setup Month & Year label in beginning
        
        guard let date = visibleDate.monthDates.first?.date else {return}
        
        self.formatter.dateFormat = "yyyy"
        self.yearLbl.text = self.formatter.string(from: date)
        
        self.formatter.dateFormat = "MMMM"
        self.monthLbl.text = self.formatter.string(from: date)
        
    }
    
    
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        
        guard let validCell = view as? SportsCalendarCell else {return}
        
        if cellState.isSelected {
            validCell.dateLbl.textColor = UIColor.white
        } else {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd"
            let cellDates = dateFormat.string(from: cellState.date)
            
            if categorySelectedByUserTag == 1 {
                if sportsAvailableDatesListArray.contains(where: {$0.date == cellDates}) {
                    validCell.dateLbl.textColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
                }else {
                    validCell.dateLbl.textColor = UIColor.lightGray
                }
            }else {
                if challengeAvailableDatesListArray.contains(where: {$0.date == cellDates}) {
                    validCell.dateLbl.textColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
                }else {
                    validCell.dateLbl.textColor = UIColor.lightGray
                }
            }
            
        }
        
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState) {
        
        guard let validCell = view as? SportsCalendarCell else {return}
        
        if cellState.isSelected {
            validCell.selectedView.isHidden = false
        } else {
            validCell.selectedView.isHidden = true
        }
        
    }

    
}

//MARK: CALENDAR IMPLEMENTATION HERE
extension SelectedSportBookingVC: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: .month, value: 3, to: startDate, wrappingComponents: false)

        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate!)
        
        return parameters
        
    }
    
    
}

extension SelectedSportBookingVC: JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        guard let cell = calendarCollectionView.dequeueReusableJTAppleCell(withReuseIdentifier: "SportsCalendarCell", for: indexPath) as? SportsCalendarCell else {return JTAppleCell()}
        cell.dateLbl.text = cellState.text
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        return cell
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        //Display Logic for Date on Confirm Date View Label
        formatter.dateFormat = "MMMM d, yyyy"
        let selectedDate = cellState.date
        confirmSelectedDateLbl.text = formatter.string(from: selectedDate)
        
        //Get Selected Date in yyyy-MM-dd and then added timings to sportsTimingsArray
        formatter.dateFormat = "yyyy-MM-dd"
        selectedDateByUser = formatter.string(from: selectedDate)
        
        
        //CHECKS IF SELECTED DATE IS LESS THAN OR EQUAL TO CURRENT DATE - For Booking Confirmation
        let dateCheckingFormatter = DateFormatter()
        dateCheckingFormatter.dateFormat = "yyyy-MM-dd"
        
        let todaysDateString = dateCheckingFormatter.string(from: Date())
        let selectedDateByUserString = dateCheckingFormatter.string(from: cellState.date)
        
        if (selectedDateByUserString < todaysDateString) {
            //print("Cant Select Those Dates")
            sportsTimingsArray.removeAll()
            timingsCollectionView.reloadData()
            self.view.makeToast("Cant select previous dates", duration: 1.0, position: .bottom)
        } else {
            //print("Proceed")
            //print(categorySelectedByUserTag)
            checkSelectedCategoryByUser()
            
        }
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
        setupMonthYearInitiallyCalendar(from: visibleDates)
        
    }
    
}

//MARK: Timings Collection View Implementation
extension SelectedSportBookingVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sportsTimingsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = timingsCollectionView.dequeueReusableCell(withReuseIdentifier: "SelectedSportTimingsCollectionCell", for: indexPath) as? SelectedSportTimingsCollectionCell else {return UICollectionViewCell()}
        cell.configureCell(data: sportsTimingsArray[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 60)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedTimeToDisplay = sportsTimingsArray[indexPath.row].time
        bookingAmountValue = sportsTimingsResultArray[0].bookingAmount
        secondBookDiscountValue = sportsTimingsResultArray[0].secondBookDiscount
        selectedSportsTimingId = sportsTimingsArray[indexPath.row].sportsTimingsID
        sportsCourtBookingId = sportsTimingsArray[indexPath.row].sportsCourtBookingID ?? -1
        
        //Display Popup With Required Values
        
        if categorySelectedByUserTag == 1 {
            //Show Booking Popup
            
            showPopupForBookings(selectedSportName: gameTitleString ?? "", confirmSelectedDate: confirmSelectedDateLbl.text ?? "", selectedTimeSlot: selectedTimeToDisplay, sportsCourtBookingAmount: bookingAmountValue, secondBookDiscount: secondBookDiscountValue)
        }else {
            //Show Challenge Popup
            
            showPopupForChallenge(selectedSportName: gameTitleString ?? "", confirmSelectedDate: confirmSelectedDateLbl.text ?? "", selectedTimeSlot: selectedTimeToDisplay, sportsCourtBookingAmount: bookingAmountValue, secondBookDiscount: secondBookDiscountValue)
        }
        
        
    }
    
    
}


//MARK: ADDITIONAL FUNCTIONS
extension SelectedSportBookingVC {
    
    func defaultSelectBookButtonOnViewLoad() {
        
        bookCourtBtn.tag = 1
        bookCourtBtn.backgroundColor = UIColor.white
        bookCourtBtn.layer.borderWidth = 2
        bookCourtBtn.layer.borderColor = #colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1)
        
        challengesBtn.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.5)
        challengesBtn.layer.borderWidth = 0
        challengesBtn.layer.borderColor = UIColor.clear.cgColor
        
    }
    
    //MARK: POPULATE DATE IN ARRAYS ON DATE CLICK FUNCTION
    
    func checkSelectedCategoryByUser() {
        sportsTimingsArray.removeAll()
        //Check for Selected Category by User - Booking or Challenge and show result.
        switch categorySelectedByUserTag {
            
        case 1:
            
            if let matchedElement = sportsAvailableDatesListArray.enumerated().first(where: {$0.element.date == selectedDateByUser}) {
                // do something with foo.offset and foo.element
                sportsTimingsArray = sportsAvailableDatesListArray[matchedElement.offset].sportsTimingsArray
            } else {
                sportsTimingsArray.removeAll()
            }
            
            calendarCollectionView.reloadData()
            timingsCollectionView.reloadData()
            break
        default:
            
            if let matchedElement = challengeAvailableDatesListArray.enumerated().first(where: {$0.element.date == selectedDateByUser}) {
                sportsTimingsArray = challengeAvailableDatesListArray[matchedElement.offset].sportsTimingsArray
            } else {
                sportsTimingsArray.removeAll()
            }
            
            calendarCollectionView.reloadData()
            timingsCollectionView.reloadData()
            break
        }
        //print(sportsTimingsArray)
    }

    //MARK: BOOKING POPUP SETUP
    private func showPopupForBookings(selectedSportName: String, confirmSelectedDate: String, selectedTimeSlot: String, sportsCourtBookingAmount: Int, secondBookDiscount:Int) {
        
        popupViewHeightConstraint.constant = 330
        
        UIView.animate(withDuration: 0.5) {
            self.popupView.isHidden = false
            self.challengeSwitchStack.isHidden = false
            self.selectedGameLbl.text = selectedSportName
            self.selectedDateLbl.text = confirmSelectedDate
            self.selectedTimeLbl.text = selectedTimeSlot
            self.discountedPriceForRazorpay = sportsCourtBookingAmount - (sportsCourtBookingAmount * secondBookDiscount/100)
            self.sportsCourtBookingPriceDisplayLbl.text = "\(self.rupeeSymbol) \(self.discountedPriceForRazorpay)"
            self.bookSportsCourtChallengeBtn.setTitle("Book Sports Court", for: .normal)
            self.popupView.layoutIfNeeded()
        }
        
        
       
        
    }
    
    //MARK: CHALLENGE POPUP SETUP
    private func showPopupForChallenge(selectedSportName: String, confirmSelectedDate: String, selectedTimeSlot: String, sportsCourtBookingAmount: Int, secondBookDiscount:Int) {
        
        popupViewHeightConstraint.constant = 280
        self.challengeSwitchStack.isHidden = true
        
        UIView.animate(withDuration: 0.5) {
            self.popupView.isHidden = false
            
            self.selectedGameLbl.text = selectedSportName
            self.selectedDateLbl.text = confirmSelectedDate
            self.selectedTimeLbl.text = selectedTimeSlot
            self.discountedPriceForRazorpay = sportsCourtBookingAmount - (sportsCourtBookingAmount * secondBookDiscount/100)
            self.sportsCourtBookingPriceDisplayLbl.text = "\(self.rupeeSymbol) \(self.discountedPriceForRazorpay)"
            self.bookSportsCourtChallengeBtn.setTitle("Request Challenge", for: .normal)
            self.popupView.layoutIfNeeded()
        }
        
    }
    
}


//MARK: API IMPLEMENTATION HERE
extension SelectedSportBookingVC {
    
    //GET SELECTED SPORTS TIMINGS SERVICE
    func getSelectedSportsTimingsService(withSportsTypeId sportsTypeId: Int, andCoWorkerEmployeeId coWorkerEmployeeId: Int) {
        
        self.view.makeToastActivity(.center)
        
        SportsCourtServices.instance.getSportsTimings(withSportsTypeId: sportsTypeId, coWorkersEmployeeId: coWorkerEmployeeId) { (returnedResponse, error) in
            
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    //print(returnedResponse.results)
                    self.sportsTimingsResultArray = returnedResponse.results
                    
                    for data in self.sportsTimingsResultArray {
                        self.sportsAvailableDatesListArray = data.availabilityDatesList
                        
                    }
                    //print(self.sportsAvailableDatesListArray)
                    
                    for data in self.sportsTimingsResultArray {
                        self.challengeAvailableDatesListArray = data.challengesDatesList
                    }
                    //print(self.challengeAvailableDatesListArray)
                    
                    DispatchQueue.main.async {
                        
                        let dateCheckingFormatter = DateFormatter()
                        dateCheckingFormatter.dateFormat = "yyyy-MM-dd"
                        self.selectedDateByUser = dateCheckingFormatter.string(from: Date())
                        self.defaultSelectBookButtonOnViewLoad()
                        self.checkSelectedCategoryByUser()
                        self.calendarCollectionView.reloadData()
                        self.timingsCollectionView.reloadData()
                        self.view.hideToastActivity()
                        
                        
                    }
                }
                
            }
            
        }
        
    }
    
}


