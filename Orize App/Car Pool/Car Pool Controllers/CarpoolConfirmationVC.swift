//
//  CarpoolConfirmationVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 16/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class CarpoolConfirmationVC: UIViewController {
    
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var vehicleNumberLbl: UILabel!
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var additionalQuestionTextView: UITextView!
    
    //Hiding Outlets
    @IBOutlet weak var messageLblView: CustomView!
    
    
    //Variables
    var carpoolVehicleTravelId: Int?
    var requestCarpoolResultArray: [RequestCarpoolResult]?
    var coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    var carpoolVehicleRequestId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
    }
    
    func setupInitialUI() {
        additionalQuestionTextView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        additionalQuestionTextView.layer.cornerRadius = 5.0
        additionalQuestionTextView.layer.borderWidth = 0.3
        additionalQuestionTextView.delegate = self
        
        guard let requestCarpoolResultArray = requestCarpoolResultArray else {return}
        for data in requestCarpoolResultArray {
            setupUIAfterCarpoolRequestApi(data: data)
        }
        
    }
    
    func setupUIAfterCarpoolRequestApi(data: RequestCarpoolResult) {
        
        self.driverNameLbl.text = data.driverName
        self.vehicleNumberLbl.text = data.vehicleNo
        self.destinationLbl.text = data.destination
        self.dateLbl.text = data.travelDateApp
        self.timeLbl.text = data.travelTimeApp
        self.carpoolVehicleRequestId = data.carPoolVehicleRequestID
        self.messageLbl.text = data.remarks
        
        if data.remarks == "" {
            self.messageLblView.isHidden = true
        }else {
            self.messageLblView.isHidden = false
        }
        
    }
    
    //MARK ACTIONS HERE
    @IBAction func backButtonTapped(_ sender: UIButton) {
        print("Backk Button Tapped")
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateRequestButtonTapped(_ sender: UIButton) {
        print(carpoolVehicleRequestId)
        
        guard let description = messageLbl.text else {return}
        updateCarpoolService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleRequestId: carpoolVehicleRequestId, description: description)
        
    }
    
    @IBAction func cancelRequestButtonTapped(_ sender: UIButton) {
        
        //Cancel Carpool API Call
        cancelCarpoolService(coWorkersEmployeeId: coWorkerEmployeeId, carpoolVehicleRequestId: carpoolVehicleRequestId)
    }

}

//MARK: TEXT View Delegate Implementation
extension CarpoolConfirmationVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
}


//MARK: API IMPLEMENTATION HERE
extension CarpoolConfirmationVC {
    
    //MARK: CANCEL CARPOOL SERVICE
    func cancelCarpoolService(coWorkersEmployeeId: Int, carpoolVehicleRequestId: Int) {
        
        self.view.makeToastActivity(.center)
        
        CarpoolServices.instance.cancelCarpoolService(coWorkersEmployeeId: coWorkersEmployeeId, carpoolVehicleRequestId: carpoolVehicleRequestId) { (returnedResponse, error) in
            
            if error != nil {
                
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            }else {
                if let returnedResponse = returnedResponse {
                    
                    DispatchQueue.main.async {
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        self.view.hideToastActivity()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                            self.navigationController?.popToRootViewController(animated: true)
                            //self.dismiss(animated: true, completion: nil)
                        })
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //MARK: UPDATE CARPOOL SERVICE
    func updateCarpoolService(coWorkerEmployeeId: Int, carpoolVehicleRequestId: Int, description: String) {
        
        self.view.makeToastActivity(.center)
        
        CarpoolServices.instance.updateCarpoolService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleRequestId: carpoolVehicleRequestId, description: description) { (returnedResponse, error) in
            
            if error != nil {
                
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    DispatchQueue.main.async {
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        self.view.hideToastActivity()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                            self.navigationController?.popToRootViewController(animated: true)
                            //self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
                
            }
            
        }
        
    }
    
}
