//
//  HomePage_Favourite_List.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let homePageFavouriteList = try? newJSONDecoder().decode(HomePageFavouriteList.self, from: jsonData)

import Foundation

// MARK: - HomePageFavouriteList
struct HomePageFavouriteList: Codable {
    let code: Int
    let status, message: String
    let results: [FavouriteService]
}

// MARK: - Result
struct FavouriteService: Codable {
    let favouritesModulesID, moduleID: Int
    let name: String
    let photo: String
    
    enum CodingKeys: String, CodingKey {
        case favouritesModulesID = "favourites_modules_id"
        case moduleID = "module_id"
        case name, photo
    }
}


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let addRemoveFavourites = try? newJSONDecoder().decode(AddRemoveFavourites.self, from: jsonData)

// MARK: - AddRemoveFavourites
struct AddRemoveFavourites: Codable {
    let code: Int
    let status, message: String
    
}
