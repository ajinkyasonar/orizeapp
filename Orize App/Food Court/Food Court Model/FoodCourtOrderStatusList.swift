//
//  FoodCourtOrderStatusList.swift
//  Orize App
//
//  Created by Apple on 24/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// MARK: - FoodCourtOrderStatusList
struct FoodCourtOrderStatusList: Codable {
    let code: Int
    let status, message: String
    let results: [OrderStatusList]
}

// MARK: - Result
struct OrderStatusList: Codable {
    let id: Int
    let name: String
}



// MARK: - FoodCourtTrackOrder
struct FoodCourtTrackOrder: Codable {
    let code: Int
    let status, message: String
    let results: [FoodCourtOrder]
}

struct FoodCourtOrder: Codable {
    let id: Int
    let status, createdDate: String
    
    enum CodingKeys: String, CodingKey {
        case id, status
        case createdDate = "created_date"
    }
}
