//
//  FoodCourtServices.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

class FoodCourtServices {
    static let instance = FoodCourtServices()
    
    //MARK: GET FOOD COURT VENDER LIST SERVICE -> MODIFICATION
    
    func getFoodCourtVenderList(completion: @escaping(VenderList?, Error?) -> ()) {
        guard let url = URL(string: GET_FOODCOURT_VENDER_LIST_URL) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                completion(nil, error)
                return
            } else {
                let decoder = JSONDecoder()
                guard let data = data else {return completion(nil, error)}
                
                do{
                    let returnedResponse = try decoder.decode(VenderList.self, from: data)
                    completion(returnedResponse, nil)
                    
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
    }
    
    //MARK: GET FOOD COURT VENDER DETAILS SERVICE
    func getFoodCourtVenderDetails(with foodCourtVenderId: Int, completion:@escaping(FoodCourtProduct?, Error?) -> ()) {
        
        guard let url = URL(string: GET_FOODCOURT_VENDER_PRODUCTS_URL) else {return}
        
        let postData = NSMutableData(data: "food_court_vendors_id=\(foodCourtVenderId)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpBody = postData as Data
        urlRequest.httpMethod = "POST"
        
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(FoodCourtProduct.self, from: data)
                    completion(returnedResponse, nil)
                } catch{
                    print(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        
        task.resume()
        
        
    }
  
    
    //MARK: GET PRODUCT LIST FOR FOOD COURT SERVICE
    
    func getProductListForFoodCourt(withfoodCourtCategoryId foodCourtCategoryId: Int, completion: @escaping(FoodCourtProduct?, Error?)->()) {
        
        guard let url = URL(string: GET_FOODCOURT_PRODUCT_LIST_URL) else {return}
        
        let postData = NSMutableData(data: "food_court_category_id=\(foodCourtCategoryId)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(FoodCourtProduct.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
        
        
    }
    
    //MARK: ADD_REMOVE_FOODCOURT_CART_ITEM SERVICE IMPLEMENTATION
    
    func addRemoveFoodcourtCartItem(coWorkersEmployeeId: Int, foodCourtProductId productId: Int, productQuantity quantity: Int, subscriptionDate: String, deliveryTime: String, completion:@escaping(AddRemoveFoodCourtCart?, Error?) ->()) {
        
        guard let url = URL(string: ADD_REMOVE_FOODCOURT_CART_ITEM_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(coWorkersEmployeeId)".data(using: String.Encoding.utf8)!)
        postData.append("&food_court_products_id=\(productId)".data(using: String.Encoding.utf8)!)
        postData.append("&qty=\(quantity)".data(using: String.Encoding.utf8)!)
        postData.append("&subscription_date=\(subscriptionDate)".data(using: String.Encoding.utf8)!)
        postData.append("&delivery_time=\(deliveryTime)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(AddRemoveFoodCourtCart.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
        }
        task.resume()
        
    }
    
    //MARK: GET FOOD COURT ORDER STATUS LIST SERVICE
    func getFoodCourtOrderStatusList(completion: @escaping(FoodCourtOrderStatusList?, Error?) ->()) {
        
        guard let url = URL(string: GET_FOODCOURT_ORDER_STATUS_LIST_URL) else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            } else {
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(FoodCourtOrderStatusList.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
        }
        
        task.resume()
        
    }
    
    //MARK: FOOD COURT TRACK ORDER API SERVICE
    func foodCourtTrackOrder(coWorkerEmployeeId id: Int, foodCourtOrderDetailsId orderDetailsId: Int, completion: @escaping(FoodCourtTrackOrder?, Error?) ->()) {
        
        guard let url = URL(string: FOODCOURT_TRACK_ORDER_URL) else {return}
        
        let postData = NSMutableData(data: "co_works_employee_id=\(id)".data(using: String.Encoding.utf8)!)
        postData.append("&food_court_order_details_id=\(orderDetailsId)".data(using: String.Encoding.utf8)!)
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData as Data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                debugPrint(error.debugDescription)
                completion(nil, error)
                return
            }else {
                
                guard let data = data else {return completion(nil, error)}
                let decoder = JSONDecoder()
                
                do{
                    let returnedResponse = try decoder.decode(FoodCourtTrackOrder.self, from: data)
                    completion(returnedResponse, nil)
                }catch{
                    debugPrint(error)
                    completion(nil, error)
                    return
                }
                
            }
            
        }
        task.resume()
        
        
    }
    
}

//MARK: PREVIOUS COMMENTED CODE
/*
 
 //MARK: GET FOOD COURT CATEGORIES LIST SERVICE API
 func getFoodCourtCategories(completion: @escaping(FoodCourtCategories?, Error?) -> ()) {
 guard let url = URL(string: GET_FOODCOURT_CATEGORIES_LIST_URL) else {return}
 let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
 
 if error != nil {
 debugPrint(error.debugDescription)
 completion(nil, error)
 return
 } else {
 guard let data = data else {return completion(nil, error)}
 let decoder = JSONDecoder()
 
 do{
 let returnedResponse = try decoder.decode(FoodCourtCategories.self, from: data)
 completion(returnedResponse, nil)
 }catch{
 debugPrint(error.localizedDescription)
 completion(nil, error)
 return
 }
 }
 
 }
 task.resume()
 }
 
 
 
 */
