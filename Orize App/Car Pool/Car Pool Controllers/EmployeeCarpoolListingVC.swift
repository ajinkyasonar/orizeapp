//
//  EmployeeCarpoolListingVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 22/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class EmployeeCarpoolListingVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var employeeCarpoolTableView: UITableView!
    @IBOutlet weak var employeeCarpoolTableHeightConstraint: NSLayoutConstraint!
    
    //Variables
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var employeeCarpoolListingResultArray = [EmployeeCarpoolListingResult]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
    }
    
    func setupInitialUI() {
        
        getEmployeeCarpoolListing(coWorkerEmployeeId: coWorkerEmployeeId)
        employeeCarpoolTableView.delegate = self
        employeeCarpoolTableView.dataSource = self
        self.employeeCarpoolTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        employeeCarpoolTableView.layer.removeAllAnimations()
        employeeCarpoolTableHeightConstraint.constant = employeeCarpoolTableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    //ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        print("Employee CarPool Listing Back Button Tapped")
        navigationController?.popToRootViewController(animated: true)
    }

}

//MARK: Table View Implentation
extension EmployeeCarpoolListingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employeeCarpoolListingResultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = employeeCarpoolTableView.dequeueReusableCell(withIdentifier: "EmployeeCarpoolListingCell", for: indexPath) as? EmployeeCarpoolListingCell else {return UITableViewCell()}
        cell.configureCell(data: employeeCarpoolListingResultArray[indexPath.row])
        cell.delegate = self
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: Employee Carpool Listing Cell Delegate Implementation
extension EmployeeCarpoolListingVC: EmployeeCarpoolListingCellDelegate {
    
    func deleteButtonTapped(_ tag: Int) {
        
        let carpoolVehicleTravelId = employeeCarpoolListingResultArray[tag].carPoolVehicleTravelID
        removeVehicleService(coWorkersEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId)
        
    }
    
    func editButtonTapped(_ tag: Int) {
        
        guard let listYourCarViewController = storyboard?.instantiateViewController(withIdentifier: "ListYourCarVC") as? ListYourCarVC else {return}
        let individualValues = employeeCarpoolListingResultArray[tag]
        listYourCarViewController.isFromEditing = true
        listYourCarViewController.carpoolVehicleTravelId = individualValues.carPoolVehicleTravelID
        listYourCarViewController.isSaveAllowedValue = individualValues.isSaveAllowed
        listYourCarViewController.employeeCarpoolDictionary = ["driverName": individualValues.driverName, "vehicleNumber": individualValues.vehicleNo, "destination": individualValues.destination, "displayDate": individualValues.travelDateApp, "apiDate": individualValues.travelDateOriginal, "displayTime": individualValues.travelTimeApp, "apiTime": individualValues.travelTimeOriginal, "numberOfSeats": individualValues.numberOfSeats,"remarks": individualValues.remarks, "parkingLevel": individualValues.parkingLevel] as [String : AnyObject]
        //Todo: Parking level value is to be passed from this api - Remind Pramod to include the parking level value.
        
        navigationController?.pushViewController(listYourCarViewController, animated: true)
    }
    
}

//MARK: API IMPLEMENTATION HERE
extension EmployeeCarpoolListingVC {
    
    //MARK: GET EMPLOYEE CARPOOL LISTING API
    func getEmployeeCarpoolListing(coWorkerEmployeeId: Int) {
        
        self.view.makeToastActivity(.center)
        
        CarpoolServices.instance.getEmployeeCarpoolListingService(coWorkersEmployeeId: coWorkerEmployeeId) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                if let returnedResponse = returnedResponse {
                    
                    //print(returnedResponse.results)
                    self.employeeCarpoolListingResultArray = returnedResponse.results
                    
                    DispatchQueue.main.async {
                        self.employeeCarpoolTableView.reloadData()
                        self.view.hideToastActivity()
                    }
                }
                
            }
            
        }
        
    }
    
    //MARK: REMOVE VEHICLE API IMPLEMENTATION
    func removeVehicleService(coWorkersEmployeeId: Int, carpoolVehicleTravelId: Int) {
        
        CarpoolServices.instance.removeVehicleService(coWorkersEmployeeId: coWorkersEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                if let returnedResponse = returnedResponse {
                    
                    DispatchQueue.main.async {
                        self.getEmployeeCarpoolListing(coWorkerEmployeeId: coWorkersEmployeeId)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        })
                    }
                }
            }
            
        }
        
    }
    
}
