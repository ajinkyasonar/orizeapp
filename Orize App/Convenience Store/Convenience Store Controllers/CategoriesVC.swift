//
//  AllProductsVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 30/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol CategoriesVCDelegate: class {
    
    func sendCartCountToRootVC(cartCount: Int)
}

class CategoriesVC: UIViewController {
    
    //Outlets
    @IBOutlet private weak var searchBarAndImageViewOuterView: UIView!
    @IBOutlet private weak var searchBarTextField: UITextField!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //Required Variables
    var storeCategoryId: Int!
    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //Storage Variables
    var productDataArray = [ProductsListArr]()
    
    //Searching Variables
    var isSearching = false
    var productDataArrayFilterArray = [ProductsListArr]()
    
    //Delegate
    weak var delegate: CategoriesVCDelegate?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initialSetupOnViewLoad()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //Initial Setup On View Load
    
    func initialSetupOnViewLoad() {
        
        let storeCategoryId = self.storeCategoryId ?? 0
        getStoreProductList(with: storeCategoryId)
        searchBarAndImageViewOuterView.layer.borderWidth = 0.2
        searchBarAndImageViewOuterView.layer.borderColor = UIColor.black.cgColor
        collectionView.delegate = self
        collectionView.dataSource = self
        
        searchBarTextField.delegate = self
        searchBarTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }

}

//MARK: Fruits Collection View Implementation

extension CategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isSearching {
            return productDataArrayFilterArray.count
        } else {
            return productDataArray.count
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as? CategoryCollectionCell else {return UICollectionViewCell()}
        
        if isSearching {
            cell.configureCategoryCell(productData: productDataArrayFilterArray[indexPath.row], delegate: self, productIndex: indexPath.row)
        } else {
            cell.configureCategoryCell(productData: productDataArray[indexPath.row], delegate: self, productIndex: indexPath.row)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: (self.view.frame.width - 40)/3, height: 230)
        }else {
             return CGSize(width: (self.view.frame.width-30)/2, height: 230)
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var selectedItem: ProductsListArr!
        
        if isSearching {
            selectedItem = productDataArrayFilterArray[indexPath.row]
        } else {
            selectedItem = productDataArray[indexPath.row]
        }
        
        guard let selectedProductVc = storyboard?.instantiateViewController(withIdentifier: "SelectedProductVC") as? SelectedProductVC else {return}
        selectedProductVc.selectedProductDetails = selectedItem
        selectedProductVc.delegate = self
        navigationController?.present(selectedProductVc, animated: true, completion: nil)
        
    }


}

//MARK: Category Collection Cell Delegate Implementation
extension CategoriesVC: CategoryCollectionCellDelegate {
    
    //Here Implement Add And Remove Product Functionality
    
    func addProductBtnTapped(productData: ProductsListArr, productIndex: Int, productCount: Int) {
        
        addRemoveStoreCartProduct(employeeId: coWorkerEmployeeId, storeProductId: productData.productID, productQuantity: productCount)
    }
    
    func RemoveProductBtnTapped(productData: ProductsListArr, productIndex: Int, productCount: Int) {
        
        addRemoveStoreCartProduct(employeeId: coWorkerEmployeeId, storeProductId: productData.productID, productQuantity: productCount)
    }
    
}

//MARK: SELECTED PRODUCT VIEW CONTROLLER DELEGATE IMPLEMENTATION - WHEN TAPPED ON PRODUCT
extension CategoriesVC: SelectedProductVCDelegate {
    
    func addRemoveConvenienceCartProduct(product: ProductsListArr, productQuantity: Int) {
        addRemoveStoreCartProduct(employeeId: coWorkerEmployeeId, storeProductId: product.productID, productQuantity: productQuantity)
    }
    
}


//MARK: TextFiled Delegate & Search Implementation
extension CategoriesVC: UITextFieldDelegate {
    
    //Search Functionality Implementation & Filter
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        isSearching = false
        collectionView.reloadData()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if searchBarTextField.text != "" {
            
            isSearching = true
            
            //Filter Search on Name.
            productDataArrayFilterArray = productDataArray.filter({$0.productName.lowercased().contains(textField.text!.lowercased())})
            collectionView.reloadData()
            
        } else {
            
            isSearching = false
            collectionView.reloadData()
            searchBarTextField.resignFirstResponder()
            
        }
        
        
    }
    
}

//MARK: API IMPLEMENTATION
extension CategoriesVC {
    
    //API IMPLEMENTATION FOR GETTING Store Products
    func getStoreProductList(with storeCategoryId: Int) {
        //SVProgressHUD.show()
        
        productDataArray.removeAll()
        
        ConvenienceStoreService.instance.getConvenienceStoreProductsListWith(storeCategoryId: storeCategoryId) { (success, returnedData) in
            
            if let returnedData = returnedData {
                
                if returnedData.code == 0 {
                    self.productDataArray = returnedData.results[0].productData[0].productsListArr
                }
                
            }
            //print(self.fruitsDataArray)
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
               // SVProgressHUD.dismiss()
            }
            
        }
        
    }
    
    //Add Remove Category Store Cart Product API Implementation
    func addRemoveStoreCartProduct(employeeId: Int, storeProductId: Int, productQuantity: Int) {
        
        let params = ["co_works_employee_id": employeeId, "store_products_id": storeProductId, "qty": productQuantity]
        
        ServiceHelper.instance.GenericPostService(forUrl: ADD_REMOVE_STORE_CART_PRODUCT_URL, parameters: params) { (addRemoveResponse: ConvenienceStoreAddRemoveProduct) in
            
            DispatchQueue.main.async {
                self.view.makeToast(addRemoveResponse.message, duration: 1.0, position: .center)
                self.getEmployeeDetails(coWorkerEmployeeId: employeeId)
            }
            
        }
        
    }
    
    //Get EMPLOYEE DETAILS - This is To Get Total Cart Items with Employee Details
    func getEmployeeDetails(coWorkerEmployeeId: Int) {
        
        let params = ["co_works_employee_id": coWorkerEmployeeId]
        
        ServiceHelper.instance.GenericPostService(forUrl: GET_EMPLOYEE_DETAILS_URL, parameters: params) { (employeeDetails: EmployeeDetails) in
            
            if employeeDetails.code == 0 {
                //Send employee cart count to root view controller for cart display
                employeeDetails.results.forEach({ (detail) in
                    self.delegate?.sendCartCountToRootVC(cartCount: detail.itemsInCart)
                })
            }
            
        }
        
    }
    
    
}
