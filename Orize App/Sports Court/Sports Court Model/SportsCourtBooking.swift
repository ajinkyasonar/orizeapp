//
//  SportsCourtBooking.swift
//  Orize App
//
//  Created by Aditya Infotech on 30/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let sportsCourtBooking = try? newJSONDecoder().decode(SportsCourtBooking.self, from: jsonData)

import Foundation

struct SportsCourtBooking: Codable {
    let code: Int
    let status, message: String
    let results: [SportsCourtBookingResult]
}

struct SportsCourtBookingResult: Codable {
    let smsGatewayResponse: String
    let smstxt: String
    let id: Int
    
    enum CodingKeys: String, CodingKey {
        case smsGatewayResponse = "sms_gateway_response"
        case smstxt
        case id
    }
}
