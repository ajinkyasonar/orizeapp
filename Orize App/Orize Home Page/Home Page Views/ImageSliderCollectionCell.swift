//
//  ImageSliderCollectionCell.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 05/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class ImageSliderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageSliderView: UIImageView!
    
    func configureCell(banner: Banner) {
        guard let imageurl = URL(string: banner.filenameurl) else {return}
        imageSliderView.sd_setImage(with: imageurl)
    }
    
}
