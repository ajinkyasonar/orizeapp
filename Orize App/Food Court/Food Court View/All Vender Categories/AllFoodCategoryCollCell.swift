//
//  AllFoodCategoryCollCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 28/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Cosmos
import Toast_Swift

protocol AllFoodCategoryCollCellActionDelegate: class {
    
    func addBtnTapped(product: FoodCourtProductsDetails, categoryInt: Int, productInt: Int, productQuantity: Int)
    
    func minusBtnTapped(product: FoodCourtProductsDetails, categoryInt: Int, productInt: Int, productQuantity: Int)
    
}


class AllFoodCategoryCollCell: UICollectionViewCell {
    
    //Outlets
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    @IBOutlet private weak var unitLbl: UILabel!
    
    weak var delegate: AllFoodCategoryCollCellActionDelegate?
    private var product: FoodCourtProductsDetails!
    private var categoryInt: Int!
    private var productInt: Int!
    
    
    //Variables
    var counter = 0
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        elementView.layer.cornerRadius = 5
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
    }
    
    
    func configureCell(product: FoodCourtProductsDetails, categoryInt: Int, productInt: Int, delegate: AllFoodCategoryCollCellActionDelegate) {
        
        self.delegate = delegate
        self.product = product
        self.categoryInt = categoryInt
        self.productInt = productInt
        
        guard let productImageUrl = URL(string: product.productPhoto) else {return}
        productImage.sd_setImage(with: productImageUrl)
        
        productNameLbl.text = product.productName
        productPriceLbl.text = "\(rupee) \(product.productPriceWithTax)"
        unitLbl.text = "\(product.productCounter)"
        
    }
    
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        
        guard let product = product else {return}
        if counter < product.productAllowedQtyInCart {
            counter += 1
            product.productCounter = counter
            unitLbl.text = "\(product.productCounter)"
        }else {
            self.makeToast("Max Product Quantity Reached", duration: 1.0, position: .bottom)
        }
        
        delegate?.addBtnTapped(product: product, categoryInt: categoryInt, productInt: productInt, productQuantity: counter)
        
        
    }
    
    @IBAction func minusBtnTapped(_ sender: UIButton) {
        
        if counter > 0 {
            counter -= 1
            product.productCounter = counter
            unitLbl.text = "\(product.productCounter)"
        }else {
            self.makeToast("Lowest Count", duration: 1.0, position: .bottom)
        }
        
        delegate?.minusBtnTapped(product: product, categoryInt: categoryInt, productInt: productInt, productQuantity: counter)
        
    }

}
