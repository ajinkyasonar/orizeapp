//
//  Extenstions.swift
//  Orize App
//
//  Created by Aditya Infotech on 14/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

//MARK: Remove duplicates from Array extension
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

extension Array where Element: Equatable {
    mutating func addObjectIfNew(_ item: Element) {
        if !contains(item) {
            append(item)
        }
    }
}

//Generate Random String
func randomString(length: Int) -> String {
    let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return String((0..<length).map{ _ in letters.randomElement()! })
}

//Logout User
func logoutUser() {
    let defaults = UserDefaults.standard
    defaults.removeObject(forKey: "employeeCode")
    defaults.removeObject(forKey: CO_WORKERS_EMPLOYEE_ID)
    defaults.removeObject(forKey: Co_Worker_EMPLOYEE_NAME)
    
    guard let loginViewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
    let navigationVc = UINavigationController(rootViewController: loginViewController)
    let share = UIApplication.shared.delegate as? AppDelegate
    share?.window?.rootViewController = navigationVc
    share?.window?.makeKeyAndVisible()
}

extension Notification.Name {
    static let UPDATE_CART_COUNT_NOTIFICATION = Notification.Name("updateCartCount")
}

extension UIApplication {
    static var appVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}
