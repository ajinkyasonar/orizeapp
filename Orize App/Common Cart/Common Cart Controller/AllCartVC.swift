//
//  AllCartVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

protocol AllCartVCDelegate: class {
    func passModuleDataToRootVc(moduleDetails: ModuleBooking, moduleAt index: Int)
}

class AllCartVC: UIViewController {
    
    //Outlets
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var emptyCartView: UIView!
    
    @IBOutlet weak var allCartTableView: UITableView!
    @IBOutlet weak var allCartTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var subtotalAmountLbl: UILabel!
    @IBOutlet weak var cgstAmountLbl: UILabel!
    @IBOutlet weak var sgstAmountLbl: UILabel!
    @IBOutlet weak var grandTotalAmountLbl: UILabel!
    
    //Variables
    var bookingResultsArray = [BookingResults]()
    var totalBookingAmountValue = Int()
    let employeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //Delegate
    weak var delegate: AllCartVCDelegate?
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //setupInitialView()
        updateMainTableView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK: IB- ACTIONS PERFORMED HERE
    
    @IBAction func continuePaymentBtnTapped(_ sender: UIButton) {
     
        //Go to Payment Selection View Controller
        guard let paymentSelectionVc = storyboard?.instantiateViewController(withIdentifier: "CommonCartGatewaySelectionVC") as? CommonCartGatewaySelectionVC else {return}
        paymentSelectionVc.totalCartAmountValue = totalBookingAmountValue
        self.navigationController?.pushViewController(paymentSelectionVc, animated: true)
    }
    
}

//MARK: INITIAL VIEW SETUP FUNCTIONS
extension AllCartVC {
    
    private func setupInitialView() {
        //Setup Table View Here
        allCartTableView.delegate = self
        allCartTableView.dataSource = self
        
        allCartTableView.register(UINib(nibName: CommonCartRegisteredXibs.AllCartTableHeaderCell, bundle: nil), forCellReuseIdentifier: CommonCartRegisteredXibs.AllCartTableHeaderCell)
        
        allCartTableView.register(UINib(nibName: CommonCartRegisteredXibs.AllCartTblCell, bundle: nil), forCellReuseIdentifier: CommonCartRegisteredXibs.AllCartTblCell)
        
        allCartTableView.register(UINib(nibName: CommonCartRegisteredXibs.AllCartTableFooterCell, bundle: nil), forCellReuseIdentifier: CommonCartRegisteredXibs.AllCartTableFooterCell)
        
        self.allCartTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        allCartTableView.layer.removeAllAnimations()
        allCartTableViewHeightConstraint.constant = allCartTableView.contentSize.height
        
        self.updateViewConstraints()
        self.view.layoutIfNeeded()
        
    }
    
}

extension AllCartVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //print("Section Count: \(bookingResultsArray[0].data.count)")
        return bookingResultsArray[0].data.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Row Count: \(bookingResultsArray[0].data[section].productCartList.count)")
        return bookingResultsArray[0].data[section].productCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = allCartTableView.dequeueReusableCell(withIdentifier: CommonCartRegisteredXibs.AllCartTblCell, for: indexPath) as? AllCartTblCell else {return UITableViewCell()}
        
        cell.bookingArray = bookingResultsArray
        cell.tag = indexPath.section
        cell.categoryTableView.tag = indexPath.row
        cell.delegate = self //This Delegate is to update All Cart Table View
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
        /*
         let cell = tableView.dequeueReusableCell(withIdentifier: CommonCartRegisteredXibs.AllCartTblCell) as! AllCartTblCell
         cell.setNeedsLayout()
         cell.updateConstraints()
         return cell.contentView.bounds.height
         */
        
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableCell(withIdentifier: CommonCartRegisteredXibs.AllCartTableHeaderCell) as? AllCartTableHeaderCell else {return UIView()}
        
        headerView.configureCell(module: bookingResultsArray[0].data[section], delegate: self, moduleIndex: section)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CommonCartRegisteredXibs.AllCartTableHeaderCell) as? AllCartTableHeaderCell
        
        return cell?.bounds.height ?? 40
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let footerView = tableView.dequeueReusableCell(withIdentifier: CommonCartRegisteredXibs.AllCartTableFooterCell) as? AllCartTableFooterCell else {return UIView()}
        
        footerView.configureCell(module: bookingResultsArray[0].data[section])
        return footerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: CommonCartRegisteredXibs.AllCartTableFooterCell) as? AllCartTableFooterCell
        //        return cell?.bounds.height ?? 70
        return 60
    }
    
}


//MARK: TABLE VIEW HEADER CELL DELEGATE
extension AllCartVC: AllCartTableHeaderCellDelegate {
    
    func passModuleDetails(withIndex index: Int, moduleDetails: ModuleBooking) {
        
        delegate?.passModuleDataToRootVc(moduleDetails: moduleDetails, moduleAt: index + 1)
    }
    
}


//MARK: All Cart Table Cell Delegate
extension AllCartVC: AllCartTblCellDelegate {
    
    
    func editLaundryNavigation(productDetails: ProductCartList, productInt: Int) {
        guard let laundrySelectionvc = LAUNDRY_STORYBOARD.instantiateViewController(withIdentifier: "LaundryProductSelectionVC") as? LaundryProductSelectionVC else {return}
        laundrySelectionvc.editLaundryProduct = productDetails
        laundrySelectionvc.modalTransitionStyle = .crossDissolve
        laundrySelectionvc.modalPresentationStyle = .overCurrentContext
        laundrySelectionvc.delegate = self
        navigationController?.present(laundrySelectionvc, animated: true)
    }
    
    
    func PreBookingNavigation(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        //This Is To Navigate To Pre Booking SELECTION VIEW CONTROLLER
        guard let preBookingSelectionVc = FOODCOURT_STORYBOARD.instantiateViewController(withIdentifier: "PreBookingSelectionVC") as? PreBookingSelectionVC else {return}
        preBookingSelectionVc.editProductDetails = productDetails
        self.navigationController?.pushViewController(preBookingSelectionVc, animated: true)
    }
    
    
    func updateMainTableView() {
        
        bookingResultsArray.removeAll()
        
        self.view.makeToastActivity(.center)
        
        CommonCartServices.instance.getAllCartList(byEmployeeId: employeeId) { (returnedResponse, error) in
            
            if error != nil {
                debugPrint(error?.localizedDescription ?? "")
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    print(error.debugDescription)
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            } else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0 {
                        //Success
                        self.bookingResultsArray = returnedResponse.results
                        
                        
                        self.bookingResultsArray.forEach({ (detail) in
                            
                            self.totalBookingAmountValue = detail.allTotalamount
                            
                            DispatchQueue.main.async {
                                //All taxes amount here to display
                                self.subtotalAmountLbl.text = "\(rupee) \(detail.allSubtotal)"
                                self.cgstAmountLbl.text = "\(rupee) \(detail.totalCgstAmount)"
                                self.sgstAmountLbl.text = "\(rupee) \(detail.totalSgstAmount)"
                                self.grandTotalAmountLbl.text = "\(rupee) \(detail.allTotalamount)"
                            }
                            
                            if detail.data.count == 0 {
                                //Show Empty Cart Screen
                                DispatchQueue.main.async {
                                    self.mainContentView.isHidden = true
                                    self.emptyCartView.isHidden = false
                                }
                            }else {
                                //Show Main Content View
                                DispatchQueue.main.async {
                                    self.mainContentView.isHidden = false
                                    self.emptyCartView.isHidden = true
                                }
                            }
                            
                        })
                        
                        DispatchQueue.main.async {
                            self.setupInitialView()
                            self.allCartTableView.reloadData()
                            self.view.hideToastActivity()
                            
                        }
                        //print(self.bookingResultsArray)
                        //print(self.bookingResultsArray.count)
                        //print(self.items)
                        
                    } else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
}

//MARK: LAUNDRY PRODUCT SELECTION VIEW CONTROLLER DELEGATE
extension AllCartVC: LaundryProductSelectionVCDelegate {
    
    func laundryPopupDismissed() {
        updateMainTableView()
    }
}
