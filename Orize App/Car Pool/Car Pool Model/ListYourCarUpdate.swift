//
//  ListYourCarUpdate.swift
//  Orize App
//
//  Created by Aditya Infotech on 19/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let listYourCarUpdate = try? newJSONDecoder().decode(ListYourCarUpdate.self, from: jsonData)

import Foundation

struct ListYourCarUpdate: Codable {
    let code: Int
    let status, message: String
}
