//
//  ConvenienceStoreAddRemoveProduct.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

//   let convenienceStoreAddRemoveProduct = try? newJSONDecoder().decode(ConvenienceStoreAddRemoveProduct.self, from: jsonData)

import Foundation

// MARK: - ConvenienceStoreAddRemoveProduct - To Add And Remove Products From Convenience Store
struct ConvenienceStoreAddRemoveProduct: Codable {
    let code: Int
    let status, message: String
    let results: [StoreResult]
}

// MARK: - Result
struct StoreResult: Codable {
    let subtotal, tax, totalamount: Int
}
