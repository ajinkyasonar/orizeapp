//
//  SportsTimingsList.swift
//  Orize App
//
//  Created by Aditya Infotech on 25/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let sportsTimings = try? newJSONDecoder().decode(SportsTimings.self, from: jsonData)

import Foundation

struct SportsTimings: Codable {
    let code: Int
    let status, message: String
    let results: [SportsTimingsResult]
}

struct SportsTimingsResult: Codable {
    let sportsDatesID: Int
    let sportsTypePhoto: String
    let sportsTypeID: Int
    let bookingAmount: Int
    let secondBookDiscount: Int
    let sportsTypeName, availabilityDays: String
    let availabilityDatesList, challengesDatesList: [SportsAvailabilityDatesList]
    
    enum CodingKeys: String, CodingKey {
        case sportsDatesID = "sports_dates_id"
        case sportsTypePhoto = "sports_type_photo"
        case sportsTypeID = "sports_type_id"
        case bookingAmount = "booking_amount"
        case secondBookDiscount = "second_book_discount"
        case sportsTypeName = "sports_type_name"
        case availabilityDays = "availability_days"
        case availabilityDatesList = "availability_dates_list"
        case challengesDatesList = "challenges_dates_list"
    }
}

struct SportsAvailabilityDatesList: Codable {
    let date: String
    let sportsTimingsArray: [SportsTimingsArray]
    
    enum CodingKeys: String, CodingKey {
        case date
        case sportsTimingsArray = "sports_timings_array"
    }
}

struct SportsTimingsArray: Codable {
    let sportsTimingsID: Int
    let time: String
    let bookingStatus: Int
    let sportsCourtBookingID: Int?
    
    enum CodingKeys: String, CodingKey {
        case sportsTimingsID = "sports_timings_id"
        case time
        case bookingStatus = "booking_status"
        case sportsCourtBookingID = "sports_court_booking_id"
    }
}

