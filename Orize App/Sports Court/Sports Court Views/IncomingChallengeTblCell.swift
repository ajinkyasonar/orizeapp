//
//  IncomingChallengeTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 11/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol IncomingChallengeTblCellDelegate {
    
    func didTapAcceptButton(_ tag: Int)
    func didTapDeclineButton(_ tag: Int)
}

class IncomingChallengeTblCell: UITableViewCell {
    
    @IBOutlet weak var allElementView: UIView!
    @IBOutlet weak var gameNameLbl: UILabel!
    @IBOutlet weak var playerCountLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeSlotLbl: UILabel!
    
    @IBOutlet weak var challengerNameContactView: UIView!
    @IBOutlet weak var challengerNameLbl: UILabel!
    @IBOutlet weak var challengerContactNumberLbl: UILabel!
    
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var declineBtn: UIButton!
    
    var delegate: IncomingChallengeTblCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        allElementView.layer.cornerRadius = 5
        challengerNameContactView.layer.cornerRadius = 5
        acceptBtn.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func configureCell(data: RequestDetails) {
        gameNameLbl.text = data.sportsTypeName
        playerCountLbl.text = "\(data.playersCapacity)/6 PlAYERS"
        dateLbl.text = data.bookingDate
        timeSlotLbl.text = data.bookingTime
        challengerNameLbl.text = data.challengerName ?? ""
        challengerContactNumberLbl.text = data.contactNumber
        
    }
    
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        delegate?.didTapAcceptButton(self.tag)
    }
    
    @IBAction func declineBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeclineButton(self.tag)
    }
    

}
