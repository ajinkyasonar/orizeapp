//
//  AllCartTableFooterCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 26/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class AllCartTableFooterCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var subtotalCategoryLbl: UILabel!
    @IBOutlet private weak var subTotalAmountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        elementView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        elementView.layer.cornerRadius = 10
        elementView.layer.borderWidth = 0.2
        elementView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
    
    func configureCell(module: ModuleBooking) {
        subtotalCategoryLbl.text = "\(module.moduleName.rawValue) Subtotal"
        subTotalAmountLbl.text = "\(rupee) \(module.totalamount)"
    }
    
}
