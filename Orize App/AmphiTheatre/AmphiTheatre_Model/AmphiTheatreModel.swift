//
//  AmphiTheatreModel.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

struct AmphiCategories {
    
    let categoryName: String
    let id: Int
    
    init(categoryName: String, id: Int) {
        self.categoryName = categoryName
        self.id = id
    }
    
}

let AmphiCategoryArray = [
    AmphiCategories(categoryName: "All Events", id: 1),
    AmphiCategories(categoryName: "Movies", id: 2),
    AmphiCategories(categoryName: "Sports", id: 3),
    AmphiCategories(categoryName: "Special", id: 4)
]

//Events Model
struct AmphiEvent {
    
    enum EventType: String {
        case Sports
        case Movies
        case Special
    }
    
    let eventCategoryId: Int
    let eventType: EventType
    let eventImage: String
    let eventName: String
    let eventMessage: String
    let playingSlot: String
    let eventDescription: String
    var isSpecialEvent: Bool
    var passDetails: [PassDetails]?
    
    init(eventCategoryId: Int, eventType: EventType, eventImage: String, eventName: String, eventMessage: String, playingSlot: String, eventDescription: String, isSpecialEvent: Bool, passDetails: [PassDetails] = []) {
        
        self.eventCategoryId = eventCategoryId
        self.eventType = eventType
        self.eventName = eventName
        self.eventImage = eventImage
        self.eventMessage = eventMessage
        self.playingSlot = playingSlot
        self.eventDescription = eventDescription
        self.isSpecialEvent = isSpecialEvent
        self.passDetails = passDetails
        
        
    }
    
}

struct PassDetails {
    let passType: String
    let passPrice: Int
    let allowedPassQuantity: Int
    
    enum PassType {
        case MoviePass
        case SportsPass
    }
    
    enum MoviePass: String {
        case Hourly
        case Monthly
        case Yearly
    }
    
    enum SportsPass: String {
        case Minutes
    }
    
//    enum PassType: String {
//        case Hourly
//        case Monthly
//        case Yearly
//    }
    
}

let passDetailsArray = [
    
        PassDetails(passType: PassDetails.MoviePass.Hourly.rawValue, passPrice: 300, allowedPassQuantity: 5),
        
        PassDetails(passType: PassDetails.MoviePass.Monthly.rawValue, passPrice: 600, allowedPassQuantity: 5),
        
        PassDetails(passType: PassDetails.MoviePass.Yearly.rawValue, passPrice: 1000, allowedPassQuantity: 5),
        
        PassDetails(passType: PassDetails.SportsPass.Minutes.rawValue, passPrice: 50, allowedPassQuantity: 10)
        
    ]


let amphiEventsData = [
    
    AmphiEvent(eventCategoryId: 3, eventType: AmphiEvent.EventType.Sports ,eventImage: "http://13.234.40.117/qhospitality/images/amphitheatre/ipl_image.jpg", eventName: "IPL Season Match Screening", eventMessage: "Food & beverages will be available.", playingSlot: "Playing Tonight:", eventDescription: "Mumbai Indians v/s Rajasthan Royals", isSpecialEvent: false, passDetails: passDetailsArray),
        
    AmphiEvent(eventCategoryId: 2, eventType: AmphiEvent.EventType.Movies ,eventImage: "http://13.234.40.117/qhospitality/images/amphitheatre/gold_movie.jpg", eventName: "Friday Movie Night", eventMessage: "Come watch a movie at our open air amphitheatre", playingSlot: "Playing Tonight", eventDescription: "Gold, starring Akshay Kumar", isSpecialEvent: false, passDetails: passDetailsArray),
        
    AmphiEvent(eventCategoryId: 4, eventType: AmphiEvent.EventType.Special, eventImage: "http://13.234.40.117/qhospitality/images/amphitheatre/apple_music_event.jpg", eventName: "Apple Music Festival", eventMessage: "Watch new WWDC streaming live in our 3D theatre", playingSlot: "Afternoon", eventDescription: "Food, Beverages & 3D glasses provided", isSpecialEvent: true)

    ]
