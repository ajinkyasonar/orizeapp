//
//  HealthSpaPaymentDetailsVC.swift
//  Orize App
//
//  Created by Apple on 28/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class HealthSpaPaymentDetailsVC: UIViewController {
    
    //Received Values
    var purchasedPasses: [HealthSpaPass]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //IB-ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}
