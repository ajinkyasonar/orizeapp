//
//  AllCartTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol AllCartTblCellDelegate: class {
    
    func updateMainTableView()
    
    func PreBookingNavigation(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) ////Navigate To Pre Booking SELECTION VIEW CONTROLLER as it cant be done with cell
    
    func editLaundryNavigation(productDetails: ProductCartList, productInt: Int)
}

class AllCartTblCell: UITableViewCell {
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    var coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    var bookingArray: [BookingResults]! {
        
        didSet {
            
            self.categoryTableView.delegate = self
            self.categoryTableView.dataSource = self
            self.categoryTableView.reloadData()
            
            self.categoryTableView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCartTblCell, bundle: nil), forCellReuseIdentifier: FoodCourtRegisteredXibs.FoodCartTblCell)
            
            self.categoryTableView.register(UINib(nibName: ConvinienceStoreRegisteredXibs.ConvenienceCartTblCell, bundle: nil), forCellReuseIdentifier: ConvinienceStoreRegisteredXibs.ConvenienceCartTblCell)
            
            self.categoryTableView.register(UINib(nibName: "LaundryCartTblCell", bundle: nil), forCellReuseIdentifier: "LaundryCartTblCell")
            
            
        }
    }
    
    //Delegate
    weak var delegate: AllCartTblCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}

extension AllCartTblCell: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Row Count: \(bookingResultsArray[0].data[section].productCartList.count)")
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch bookingArray[0].data[self.tag].moduleName {
            
        case .FOODCOURT:
            
            //let moduleName = bookingArray[0].data[categoryTableView.tag].moduleName
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FoodCourtRegisteredXibs.FoodCartTblCell, for: indexPath) as? FoodCartTblCell else {return UITableViewCell()}
            
            //Ascending Sorted Array
            let sortedProductList = bookingArray[0].data[self.tag].productCartList.sorted()
            
            //let productCartList = bookingArray[0].data[self.tag].productCartList[categoryTableView.tag]
            
            let productCartList = sortedProductList[categoryTableView.tag]
            
            cell.configureCell(productDetails: productCartList, delegate: self, categoryInt: self.tag, productInt: categoryTableView.tag)
            
            return cell
            
        case .CONVENIENCE_STORE:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ConvinienceStoreRegisteredXibs.ConvenienceCartTblCell, for: indexPath) as? ConvenienceCartTblCell else {return UITableViewCell()}
            
            //Ascending Sorted Array
            let sortedProductList = bookingArray[0].data[self.tag].productCartList.sorted()
            
            let productCartList = sortedProductList[categoryTableView.tag]
            
            cell.configureCell(productDetails: productCartList, delegate: self, productIndex: categoryTableView.tag)
            
            return cell
            
        case .LAUNDRY:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LaundryCartTblCell", for: indexPath) as? LaundryCartTblCell else {return UITableViewCell()}
            
            //Ascending Sorted Array
            let sortedProductList = bookingArray[0].data[self.tag].productCartList.sorted()
            
            //let productCartList = bookingArray[0].data[self.tag].productCartList[categoryTableView.tag]
            
            let productCartList = sortedProductList[categoryTableView.tag]
            
            cell.configureCell(productDetails: productCartList, delegate: self, productIndex: categoryTableView.tag)
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}



//MARK: FOOD CART TABLE CELL DELEGATE IMPLEMENTATION
extension AllCartTblCell: FoodCartTblCellDelegate {
    
    
    func preBookingEditButtonTapped(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        //This Is To Navigate To Pre Booking SELECTION VIEW CONTROLLER
        delegate?.PreBookingNavigation(productDetails: productDetails, productInt: productInt, ProductQuantity: ProductQuantity)
    }
    
    func addButtonTappedFoodCart(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        //print(productDetails)
        addRemoveFoodCourtItemService(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: productDetails.productID, productQuantity: ProductQuantity, subscriptionDate: "", deliveryTime: "")
        //print(ProductQuantity)
    }
    
    func removeButtonTappedFoodCart(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int) {
        //print(productDetails)
        addRemoveFoodCourtItemService(coWorkersEmployeeId: coWorkerEmployeeId, foodCourtProductId: productDetails.productID, productQuantity: ProductQuantity, subscriptionDate: "", deliveryTime: "")
        //print(productDetails.productQty)
    }
    
    
}

//MARK: CONVENIENCE CART TABLE VIEW CELL DELEGATE IMPLEMENTATION
extension AllCartTblCell: ConvenienceCartTblCellDelegate {
    
    
    func addButtonTappedConvCart(productDetails: ProductCartList, productIndex: Int, productQuantity: Int) {
        //This Will Take Convenience Cart Add Remove Service
        addRemoveProductsFromConvenienceCart(coWorkersEmployeeId: coWorkerEmployeeId, storeProductId: productDetails.productID, productQuantity: productQuantity)
        
    }
    
    func removeButtonTappedConvCart(productDetails: ProductCartList, productIndex: Int, productQuantity: Int) {
        //This Will Take Convenience Cart Add Remove Service
        addRemoveProductsFromConvenienceCart(coWorkersEmployeeId: coWorkerEmployeeId, storeProductId: productDetails.productID, productQuantity: productQuantity)
        
    }
    
    
}

//MARK: LAUNDRY CART TABLE VIEW CELL DELEGATE IMPLEMENTATION
extension AllCartTblCell: LaundryCartTblCellDelegate {
    
    func editBtnTapped(at index: Int, productDetails: ProductCartList) {
        delegate?.editLaundryNavigation(productDetails: productDetails, productInt: index)
    }

}


//MARK: API SERVICE IMPLEMENTATION
extension AllCartTblCell {
    
    //MARK: ADD REMOVE CART PRODUCT From Food Court Cart API SERVICE
    
    func addRemoveFoodCourtItemService(coWorkersEmployeeId: Int, foodCourtProductId: Int, productQuantity: Int, subscriptionDate: String, deliveryTime: String) {
        
        FoodCourtServices.instance.addRemoveFoodcourtCartItem(coWorkersEmployeeId: coWorkersEmployeeId, foodCourtProductId: foodCourtProductId, productQuantity: productQuantity, subscriptionDate: subscriptionDate, deliveryTime: deliveryTime) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    
                    self.contentView.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
                
            }else {
                
                if returnedResponse != nil {
                    
                    DispatchQueue.main.async {
                        
                        self.delegate?.updateMainTableView()
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //MARK: ADD REMOVE PRODUCTS FROM CONVENIENCE CART API SERVICE
    
    func addRemoveProductsFromConvenienceCart(coWorkersEmployeeId: Int, storeProductId: Int, productQuantity: Int) {
        
        let params = ["co_works_employee_id": coWorkersEmployeeId, "store_products_id": storeProductId, "qty": productQuantity]
        
        ServiceHelper.instance.GenericPostService(forUrl: ADD_REMOVE_STORE_CART_PRODUCT_URL, parameters: params) { (returnedResponse: ConvenienceStoreAddRemoveProduct) in
            
            if returnedResponse.code != 0 {
                //Failure
                DispatchQueue.main.async {
                    self.contentView.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                }
            } else {
                DispatchQueue.main.async {
                    self.delegate?.updateMainTableView()
                }
            }
            
        }
        
    }
    
}



