//
//  RazorpayOrderId.swift
//  Orize App
//
//  Created by Aditya Infotech on 03/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//


//   let razorpayOrderID = try? newJSONDecoder().decode(RazorpayOrderID.self, from: jsonData)

import Foundation

import Foundation

// MARK: - RazorpayOrderID
struct RazorpayOrderID: Codable {
    let code: Int
    let status, message: String
    let results: [RazorpayOrderDetails]
}

// MARK: - Result
struct RazorpayOrderDetails: Codable {
    
    let razorpayOrderID: String
    
    enum CodingKeys: String, CodingKey {
        case razorpayOrderID = "razorpay_order_id"
    }
}
