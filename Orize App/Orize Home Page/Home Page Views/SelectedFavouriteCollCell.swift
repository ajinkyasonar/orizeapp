//
//  SelectedFavouriteCollCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 14/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class SelectedFavouriteCollCell: UICollectionViewCell {
    
    @IBOutlet private weak var outerView: UIView!
    @IBOutlet private weak var favImage: UIImageView!
    @IBOutlet private weak var favTitle: UILabel!
    @IBOutlet private weak var checkmarkImage: UIImageView!
    
    override var isSelected: Bool {
        
        didSet {
            if self.isSelected {
                checkmarkImage.isHidden = false
            }else {
                checkmarkImage.isHidden = true
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        checkmarkImage.isHidden = true
    }
    
    func configureCell(service: Services) {
        guard let imageUrl = URL(string: service.photo) else {return}
        favImage.sd_setImage(with: imageUrl)
        favTitle.text = service.name
        
        if service.isFavourite {
            outerView.layer.borderColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
            outerView.layer.borderWidth = 2.0
        }else {
            outerView.layer.borderColor = UIColor.gray.cgColor
            outerView.layer.borderWidth = 0.5
        }
        
    }
    
    

}
