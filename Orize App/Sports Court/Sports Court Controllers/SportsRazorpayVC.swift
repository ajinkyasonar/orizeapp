//
//  SportsRazorpayVC.swift
//  Orize App
//
//  Created by Aditya Infotech on 09/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Razorpay
import Toast_Swift


class SportsRazorpayVC: UIViewController, RazorpayPaymentCompletionProtocol {
    
    //Variables
    var razorpay: Razorpay!
    var totalAmount = Double()
    var imageStr = String()
    var isFromPaymetSuccessfull = false
    
    //Variables Required
    var coWrokersEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    var razorPayKey = GatewayKeys.RAZORPAY_KEY
    
    var apiBookingAmount = -1
    var apiSecondBookDiscount = -1
    var discountedPriceToDisplay = -1
    var sportsTypeId = -1
    var bookingDate = ""
    var sportsTimingId = -1
    var isChallenge = -1
    var sportNameDisplay = ""
    var categoryTagSelectedByUser = -1
    
    var sportsCourtBookingId = -1
    
    var orderId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalAmount = Double(discountedPriceToDisplay) * 100
        
        imageStr = "https://img.etimg.com/thumb/msid-66686984,width-300,imgsize-12508,resizemode-4/razorpay.jpg"
        razorpay = Razorpay.initWithKey(razorPayKey, andDelegate: self)
        
        showPaymentForm(amount: Double(totalAmount), image: imageStr, name: "Orize", description: sportNameDisplay)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func showPaymentForm(amount: Double, image:String, name: String, description: String)
    {
        let options: [String:Any] = [
            "amount" : amount, //mandatory in paise
            "description": description,
            "image": image,
            "currency": "INR",
            "name": name,
            "order_id": orderId
        ]
        
        razorpay.open(options)
    }
    
    //FAILURE
    func onPaymentError(_ code: Int32, description str: String) {
        
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle:UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        { action -> Void in
            
            self.navigationController?.popViewController(animated: true)
        })
        self.present(alertController, animated: true, completion: nil)
        
        isFromPaymetSuccessfull = false
    }
    
    
    //SUCCESS
    public func onPaymentSuccess(_ payment_id: String)
        
    {
        
        self.view.makeToastActivity(.center)
        
        let alertController = UIAlertController(title: "Message", message: "Payment Successful", preferredStyle:UIAlertController.Style.alert)//Payment Id \(payment_id)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        { action -> Void in
            
            //print("Is Challenge Value \(self.isChallenge)")
            
            self.isFromPaymetSuccessfull = true
            
            // CALL BOOKING APIS Here HERE if categorySelectedtag = 1 - booking if 2 - challenge
            
            if self.categoryTagSelectedByUser == 1 {
                self.sportsCourtBookingService(coWorkersEmployeeId: self.coWrokersEmployeeId, sportsTypeId: self.sportsTypeId, bookingDate: self.bookingDate, sportsTimingsId: self.sportsTimingId, isChallenge: self.isChallenge, paymentGatewayResponse: payment_id, paymentGateway: 2, bookingAmount: self.apiBookingAmount, secondBookDiscount: self.apiSecondBookDiscount)
            } else {

                self.sportsCourtChallengeService(sportsCourtBookingId: self.sportsCourtBookingId, coWorkersEmployeeId: self.coWrokersEmployeeId, bookingDate: self.bookingDate, paymentGatewayResponse: payment_id, paymentGateway: 2, bookingAmount: self.apiBookingAmount, secondBookDiscount: self.apiSecondBookDiscount)
            }
            
            
            
        })
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
}

extension SportsRazorpayVC {
    
    //SPORTS COURT BOOKING SERVICE
    func sportsCourtBookingService(coWorkersEmployeeId: Int, sportsTypeId: Int, bookingDate: String, sportsTimingsId: Int, isChallenge: Int, paymentGatewayResponse: String, paymentGateway: Int, bookingAmount: Int, secondBookDiscount: Int) {
        
        self.view.makeToastActivity(.center)
        
        SportsCourtServices.instance.bookSportsCourtService(coWorkersEmployeeId: coWorkersEmployeeId, sportsTypeId: sportsTypeId, bookingDate: bookingDate, sportsTimingsId: sportsTimingsId, isChallenge: isChallenge, paymentGatewayResponse: paymentGatewayResponse, paymentGateway: paymentGateway, bookingAmount: bookingAmount, secondBookDiscount: secondBookDiscount) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    //Failure
                    if returnedResponse.code != 0 {
                        print(returnedResponse.message)
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                            self.view.hideToastActivity()
                        }
                        
                    } else {
                        //Success
                        //print(returnedResponse.message)
                        //print(returnedResponse.results)
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                guard let sportsCourtvc = self.storyboard?.instantiateViewController(withIdentifier: "SportsCourtVC") as? SportsCourtVC else {return}
                                self.navigationController?.pushViewController(sportsCourtvc, animated: true)
                            })
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //MARK: CHALLENGES API
    func sportsCourtChallengeService(sportsCourtBookingId: Int, coWorkersEmployeeId: Int, bookingDate: String, paymentGatewayResponse: String, paymentGateway: Int, bookingAmount: Int, secondBookDiscount: Int) {
        
        self.view.makeToastActivity(.center)
        print(bookingDate)
        
        SportsCourtServices.instance.sportsCourtRequestChallengeService(sportsCourtBookingId: sportsCourtBookingId, coWorkersEmployeeId: coWorkersEmployeeId, bookingDate: bookingDate, paymentGatewayResponse: paymentGatewayResponse, paymentGateway: paymentGateway, bookingAmount: bookingAmount, secondBookDiscount: secondBookDiscount) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code != 0 {
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                            self.view.hideToastActivity()
                        }
                        
                    } else {
                        print(returnedResponse.message)
                        print(returnedResponse.results)
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .center)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                guard let sportsCourtvc = self.storyboard?.instantiateViewController(withIdentifier: "SportsCourtVC") as? SportsCourtVC else {return}
                                self.navigationController?.pushViewController(sportsCourtvc, animated: true)
                            })
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
