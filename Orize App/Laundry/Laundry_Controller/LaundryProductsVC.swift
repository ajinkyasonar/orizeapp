//
//  LaundryProductsVC.swift
//  Orize App
//
//  Created by Apple on 21/11/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import Toast_Swift

class LaundryProductsVC: UIViewController {
    
    //Received Data
    var laundryCategoryData: LaundryCategoryData!
    
    //Variables
    let coWorkerId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
    
    //IB-Outlets
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var productCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    private func initialSetup() {
        
        categoryNameLbl.text = laundryCategoryData.laundryCategoryName
        productCollectionView.delegate = self
        productCollectionView.dataSource = self
        productCollectionView.register(UINib(nibName: LaundryXibs.LaundryCategoryCollCell, bundle: nil), forCellWithReuseIdentifier: LaundryXibs.LaundryCategoryCollCell)
    }
    
    //IB-ACTIONS
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cartBtnTapped(_ sender: UIButton) {
        guard let commonCartVc = COMMONCART_STORYBOARD.instantiateViewController(withIdentifier: "CommonCartVC") as? CommonCartVC else {return}
        self.navigationController?.pushViewController(commonCartVc, animated: true)
    }

}

//MARK: PRODUCT COLLECTION VIEW IMPLEMENTATION
extension LaundryProductsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return laundryCategoryData.productsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LaundryCategoryCollCell", for: indexPath) as? LaundryCategoryCollCell else {return UICollectionViewCell()}
        
        let products = laundryCategoryData.productsListArr[indexPath.item]
        
        cell.configureCell(productDetails: products, delegate: self, selectedProductIndex: indexPath.item)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let cellWidth = (width - 10) / 2
        let cellHeight = cellWidth * 1.2
        return CGSize(width: cellWidth, height: cellHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedProduct = laundryCategoryData.productsListArr[indexPath.item]
        guard let productSelectionvc = storyboard?.instantiateViewController(withIdentifier: "LaundryProductSelectionVC") as? LaundryProductSelectionVC else {return}
        productSelectionvc.laundryProduct = selectedProduct
        productSelectionvc.modalPresentationStyle = .overCurrentContext
        productSelectionvc.modalTransitionStyle = .crossDissolve
        navigationController?.present(productSelectionvc, animated: true)
        
    }
    
}

//MARK: Laundry Collection Cell Delegate Implementation
extension LaundryProductsVC: LaundryCategoryCollCellDelegate {
    
    //This implementation is currently hidden in laundry module no add & remove buttons in cell
    
    func didTapAddBtn(at index: Int, productDetails: LaundryProducts, productQuantity: Int) {
        print(productDetails)
    }
    
    func didTapRemoveBtn(at index: Int, productDetails: LaundryProducts, productQuantity: Int) {
        print(productDetails)
    }
    
    
}
