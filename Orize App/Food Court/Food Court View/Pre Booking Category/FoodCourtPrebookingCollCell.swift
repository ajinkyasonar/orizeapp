//
//  FoodCourtPrebookingCollCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 09/07/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol FoodCourtPrebookingCollCellDelegate: class {
    func didTapBookButton(productDetail: FoodCourtProductsDetails, atTag tag: Int)
}

class FoodCourtPrebookingCollCell: UICollectionViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var productNameLbl: UILabel!
    @IBOutlet private weak var productPriceLbl: UILabel!
    
    //Variables
    weak var delegate: FoodCourtPrebookingCollCellDelegate?
    private var productDetail: FoodCourtProductsDetails!
    private var productIndex: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        elementView.layer.cornerRadius = 5
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
    }
    
    func configureCell(product: FoodCourtProductsDetails, delegate: FoodCourtPrebookingCollCellDelegate, productIndex: Int) {
        
        self.delegate = delegate
        self.productDetail = product
        self.productIndex = productIndex
        
        guard let productImageUrl = URL(string: product.productPhoto) else {return}
        productImage.sd_setImage(with: productImageUrl)
        
        productNameLbl.text = product.productName
        productPriceLbl.text = "\(rupee) \(product.productPriceWithTax)"
        
    }
    
    @IBAction func bookButtonTapped(_ sender: UIButton) {
        delegate?.didTapBookButton(productDetail: productDetail, atTag: productIndex)
    }

}
