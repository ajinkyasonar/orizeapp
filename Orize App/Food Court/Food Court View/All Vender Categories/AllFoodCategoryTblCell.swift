//
//  AllFoodCategoryTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol AllFoodCategoryTblCellDelegate: class {
    
    func viewAllButtonTapped(atIndex index: Int, productDataDetails: ProductData)
    
}

class AllFoodCategoryTblCell: UITableViewCell {
    
    @IBOutlet private weak var foodCategoryNameLbl: UILabel!
    @IBOutlet private weak var productCollectionView: UICollectionView!
    
    weak var delegate: AllFoodCategoryTblCellDelegate?
    private var viewButtonIndex: Int!
    private var productDataDetail: ProductData!
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    func configureCell(productData: ProductData, viewButtonIndex: Int, delegate: AllFoodCategoryTblCellDelegate) {
        
        self.viewButtonIndex = viewButtonIndex
        self.productDataDetail = productData
        self.delegate = delegate
        
        foodCategoryNameLbl.text = productData.foodCourtCategoryName
    }
    
    @IBAction func viewAllBtnTapped(_ sender: UIButton) {
        
        delegate?.viewAllButtonTapped(atIndex: viewButtonIndex, productDataDetails: productDataDetail)
        
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        
        productCollectionView.register(UINib(nibName: FoodCourtRegisteredXibs.AllFoodCategoryCollCell, bundle: nil), forCellWithReuseIdentifier: FoodCourtRegisteredXibs.AllFoodCategoryCollCell)
        
        productCollectionView.register(UINib(nibName: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell, bundle: nil), forCellWithReuseIdentifier: FoodCourtRegisteredXibs.FoodCourtPrebookingCollCell)
        
        productCollectionView.delegate = dataSourceDelegate
        productCollectionView.dataSource = dataSourceDelegate
        productCollectionView.tag = row
        productCollectionView.reloadData()
        
    }
    
}

