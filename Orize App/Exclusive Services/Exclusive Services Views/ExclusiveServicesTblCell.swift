//
//  ExclusiveServicesTblCell.swift
//  Orize App
//
//  Created by Apple on 12/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol ExclusiveServicesTblCellDelegate: class {
    func didTapViewButton(selectedServiceDetail detail: ExclusiveServices, at index: Int)
}

class ExclusiveServicesTblCell: UITableViewCell {
    
    @IBOutlet private weak var serviceImage: UIImageView!
    @IBOutlet private weak var serviceNameLbl: UILabel!
    @IBOutlet private weak var serviceDescriptionLbl: UILabel!
    
    private weak var delegate: ExclusiveServicesTblCellDelegate?
    private var selectedServiceDetail: ExclusiveServices!
    private var selectedServiceIndex: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(delegate: ExclusiveServicesTblCellDelegate, serviceDetail: ExclusiveServices, selectedIndex: Int) {
        
        self.delegate = delegate
        self.selectedServiceDetail = serviceDetail
        self.selectedServiceIndex = selectedIndex
        
        guard let imageUrl = URL(string: serviceDetail.serviceImages.first ?? "") else {return}
        serviceImage.sd_setImage(with: imageUrl, completed: .none)
        
        serviceNameLbl.text = serviceDetail.serviceName
        serviceDescriptionLbl.text = serviceDetail.serviceDescription
    }
    
    @IBAction func viewButtonTapped(_ sender: UIButton) {
        delegate?.didTapViewButton(selectedServiceDetail: selectedServiceDetail, at: selectedServiceIndex)
    }
    
}
