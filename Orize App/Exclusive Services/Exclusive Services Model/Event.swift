//
//  Event.swift
//  Orize App
//
//  Created by Apple on 12/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

struct Event {
    let eventImage: String
    let eventName: String
    let eventDescription: String
}

var eventArray = [
    
    Event(eventImage: "http://13.234.40.117/qhospitality/images/exclusive/event1-min.jpg", eventName: "Lady In Red, Canvas Painting Workshop", eventDescription: "Have you touched your paint-brushes for years? If not, then Paintologys Paint-party is the perfect way to get back to your long-lost hobby or to indulge in painting for fun!, Paintology is known to indulge people into painting for fun, and to de-stress one's mind and soul, for which you dont need to know painting! Yes!! ABSOLUTELY, NO PAINTING EXPERIENCE IS NEEDED! Paint this beautiful galaxy starry night painting with Paintology on a CIRCULAR canvas with all the various painting techniques and fun-loving methods."),
    
    Event(eventImage: "http://13.234.40.117/qhospitality/images/exclusive/event2-min.jpg", eventName: "Creative Veins, Performing Arts School", eventDescription: "Music Jammin is a community formed in 2012 to provide a platform to Artists. The objective of the community is to meet,motivate,support,network and provide the next step in the journey. The community now has more than 10,000 members and has been proud of various collaborations including the 100 member ed Music Band of India's Got talent. So if you are a Musician, dancer, actor, stand up comedian or poet you are now welcome to come together and showcase your talent."),
    
    Event(eventImage: "http://13.234.40.117/qhospitality/images/exclusive/event3-min.jpg", eventName: "The Pool Party Pod, An Entertainment Event", eventDescription: "In Oshiwara, Andheri, Raheja Classique Club is a good option if you’re looking to throw a pool party, as it is a private club and has a big lagoon-shaped swimming pool. Although it like most clubs requires membership, you can as a non-member rent out its facilities with the reference of a member {which they can help you to get}. You have to pay for the catering and ask for pool services separately, which isn’t exorbitant. For snacks, it costs somewhere around INR 650 per plate, {plus taxes}, and the swimming rentals are close to INR 220 per adult. You can contact them for details at 022 6695 5550."),
    
    Event(eventImage: "http://13.234.40.117/qhospitality/images/exclusive/event4-min.jpg", eventName: "Groove to the music of DJ Amann Nagpal this Pre Independence Day", eventDescription: "Commercial night with Dj Aman Nagpal this Friday. Friday gonna be LIT , coz the tiger is back to roar , #itstigertime with #DJAmanNagpal !! Reservations:- 9920190005 #linkingroad #CommercialNight #alcohol #newplaceintown #followit #cocktail #mocktail #rooftop #sociallife #bombay #mumbaidiaries #bar #cafe #lounge #Bandra.")

]
