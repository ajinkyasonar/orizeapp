//
//  CustomTextFieldInsets.swift
//  Orize App
//
//  Created by Aditya Infotech on 17/01/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class CustomTextFieldInsets: UITextField {
    
    private var offSet: CGFloat = 20
    private var padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    
    override func awakeFromNib() {
        setupView()
        super.awakeFromNib()
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    func setupView() {
        let placeholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.137254902, green: 0.3137254902, blue: 0.431372549, alpha: 1)])
        self.attributedPlaceholder = placeholder
    }
}


