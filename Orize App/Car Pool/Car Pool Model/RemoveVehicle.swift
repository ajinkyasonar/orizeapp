//
//  RemoveVehicle.swift
//  Orize App
//
//  Created by Aditya Infotech on 22/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let removeVehicle = try? newJSONDecoder().decode(RemoveVehicle.self, from: jsonData)

import Foundation

struct RemoveVehicle: Codable {
    let code: Int
    let status, message: String
    
}
