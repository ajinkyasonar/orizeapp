//
//  HomePage_Services.swift
//  Orize App
//
//  Created by Aditya Infotech on 13/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let homePageServices = try? newJSONDecoder().decode(HomePageServices.self, from: jsonData)

import Foundation

// MARK: - HomePageServices
struct HomePageServicesList: Codable {
    let code: Int
    let status, message: String
    let results: [Services]
}

// MARK: - Result
struct Services: Codable {
    let moduleID: Int
    let name: String
    let photo: String
    let isFavourite: Bool
    
    enum CodingKeys: String, CodingKey {
        case moduleID = "module_id"
        case name, photo
        case isFavourite = "is_favourite"
    }
}
