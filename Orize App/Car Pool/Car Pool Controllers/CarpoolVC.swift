//
//  CarpoolVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 12/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class CarpoolVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var customNavigationView: UIView!
    @IBOutlet weak var findCarpoolButton: UIButton!
    @IBOutlet weak var listYourCarButton: UIButton!
    @IBOutlet weak var yourCarpoolListingButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        
    }
    
    func setupInitialView() {
        customNavigationView.layer.borderWidth = 0.2
        customNavigationView.layer.borderColor = UIColor.black.cgColor
        
        findCarpoolButton.layer.cornerRadius = 10
        findCarpoolButton.layer.borderWidth = 0.2
        findCarpoolButton.layer.borderColor = UIColor.black.cgColor
        
        listYourCarButton.layer.cornerRadius = 10
        listYourCarButton.layer.borderWidth = 0.2
        listYourCarButton.layer.borderColor = UIColor.black.cgColor
        
        yourCarpoolListingButton.layer.cornerRadius = 10
    }
    
    //MARK: ACTIONS HERE
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func findCarpoolBtnTapped(_ sender: UIButton) {
        
        guard let findCarpoolViewController = storyboard?.instantiateViewController(withIdentifier: "FindACarpoolVC") as? FindACarpoolVC else {return}
        self.navigationController?.pushViewController(findCarpoolViewController, animated: true)
        
    }
    
    @IBAction func listYourCarBtnTapped(_ sender: UIButton) {
        
        guard let listYourCarViewController = storyboard?.instantiateViewController(withIdentifier: "ListYourCarVC") as? ListYourCarVC else {return}
        self.navigationController?.pushViewController(listYourCarViewController, animated: true)
    }
    
    @IBAction func yourCarpoolListingBtnTapped(_ sender: UIButton) {
        
        guard let employeeCarpoolListingViewController = storyboard?.instantiateViewController(withIdentifier: "EmployeeCarpoolListingVC") as? EmployeeCarpoolListingVC else {return}
        self.navigationController?.pushViewController(employeeCarpoolListingViewController, animated: true)
    }

}
