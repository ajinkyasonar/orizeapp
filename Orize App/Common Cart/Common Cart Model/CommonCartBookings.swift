//
//  CommonCartBookings.swift
//  Orize App
//
//  Created by Aditya Infotech on 24/06/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let commonCartBookings = try? newJSONDecoder().decode(CommonCartBookings.self, from: jsonData)

import Foundation

// MARK: - Empty
struct CommonCartBookings: Codable {
    let code: Int
    let status, message: String
    let results: [BookingResults]
}

// MARK: - Result
struct BookingResults: Codable {
    let moduleID: Int
    let moduleName: String
    let allSubtotal, allTotalamount, carttotalitemcount, actualAmount: Int
    let totalSgstAmount, totalCgstAmount, allTax: Double
    let data: [ModuleBooking]
    
    enum CodingKeys: String, CodingKey {
        case moduleID = "module_id"
        case moduleName = "module_name"
        case allSubtotal = "all_subtotal"
        case allTax = "all_tax"
        case allTotalamount = "all_totalamount"
        case carttotalitemcount, data
        
        case actualAmount = "actualamount"
        case totalSgstAmount = "totalsgstamount"
        case totalCgstAmount = "totalcgstamount"
        
    }
}

// MARK: - Datum
struct ModuleBooking: Codable {
    
    
    let moduleID: Int
    let moduleName: ModuleNames
    let subtotal, totalamount: Int
    let actualAmount: Int?
    let totalSgstAmount, totalCgstAmount, tax: Double?
    let productCartList: [ProductCartList]
    
    enum CodingKeys: String, CodingKey {
        case moduleID = "module_id"
        case moduleName = "module_name"
        case subtotal, tax, totalamount
        case productCartList = "product_cart_list"
        
        case actualAmount = "actualamount"
        case totalSgstAmount = "totalsgstamount"
        case totalCgstAmount = "totalcgstamount"
    }
}

// MARK: - ProductCartList
struct ProductCartList: Codable, Comparable {
    
    static func == (lhs: ProductCartList, rhs: ProductCartList) -> Bool {
        return lhs.productName.lowercased() < rhs.productName.lowercased()
    }
    
    static func < (lhs: ProductCartList, rhs: ProductCartList) -> Bool {
        return lhs.productName.lowercased() < rhs.productName.lowercased()
    }
    
    let storeCartID: Int?
    let productID: Int
    let productName: String
    let productPhoto: String
    let productDescription: String
    let productPrice, productQty, productAllowedQtyInCart: Int
    let foodCourtCartID: Int?
    let sgst, cgst, tax: Double?
    let productPriceWithTax, isPrebookingCategory: Int?
    let subscriptionDate: [SubscriptionDate]?
    let deliveryTime: String?
    let foodCourtDeliveryDatesList: [FoodCourtDeliveryDatesList]?
    let foodCourtDeliveryTimeList: [FoodCourtDeliveryTimeList]?
    let laundryServicesList: [LaundryServiceList]?

    
    enum CodingKeys: String, CodingKey {
        case storeCartID = "store_cart_id"
        case productID = "product_id"
        case productName = "product_name"
        case productPhoto = "product_photo"
        case productDescription = "product_description"
        case productPrice = "product_price"
        case productQty = "product_qty"
        case productAllowedQtyInCart = "product_allowed_qty_in_cart"
        case foodCourtCartID = "food_court_cart_id"
        case sgst, cgst, tax
        case productPriceWithTax = "product_price_with_tax"
        case isPrebookingCategory = "is_prebooking_category"
        case subscriptionDate = "subscription_date"
        case deliveryTime = "delivery_time"
        case foodCourtDeliveryDatesList = "food_court_delivery_dates_list"
        case foodCourtDeliveryTimeList = "food_court_delivery_time_list"
        case laundryServicesList = "laundry_service"
        
    }
    
    
}

enum ModuleNames: String, Codable {
    case CONVENIENCE_STORE = "Convenience Store"
    case FOODCOURT = "Food Court"
    case LAUNDRY = "Laundry"
}

// MARK: - SubscriptionDate
struct SubscriptionDate: Codable {
    let subscriptionDate: String
    
    enum CodingKeys: String, CodingKey {
        case subscriptionDate = "subscription_date"
    }
}
