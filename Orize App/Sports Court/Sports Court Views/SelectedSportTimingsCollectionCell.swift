//
//  SelectedSportTimingsCollectionCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 10/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

class SelectedSportTimingsCollectionCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        timingLabel.layer.borderWidth = 0.2
        timingLabel.layer.borderColor = UIColor.black.cgColor
        timingLabel.layer.cornerRadius = 5
        
        staticWaitListLbl.layer.borderWidth = 0.2
        staticWaitListLbl.layer.borderColor = UIColor.black.cgColor
        staticWaitListLbl.layer.cornerRadius = 5
        
        
    }
    
    @IBOutlet weak var timingLabel: UILabel!
    @IBOutlet weak var staticWaitListLbl: UILabel!
    
    func configureCell(data: SportsTimingsArray) {
        
        timingLabel.text = data.time
        staticWaitListLbl.isHidden =  data.bookingStatus == 1 ? true : false
        timingLabel.backgroundColor = data.bookingStatus == 1 ? UIColor.white : UIColor.lightGray
        self.isUserInteractionEnabled = data.bookingStatus == 1 ? true : false
    }
    
    
}
