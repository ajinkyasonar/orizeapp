//
//  ServicesCollectionViewCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 06/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class ServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var servicesImage: UIImageView!
    @IBOutlet weak var servicesTitleLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1.0
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.6)
        
        self.layer.shadowRadius = 5.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        
        
    }
    
    func configureCell(service: Services) {
        guard let imageurl = URL(string: service.photo) else {return}
        servicesImage.sd_setImage(with: imageurl)
        servicesTitleLbl.text = service.name
    }
    
}
