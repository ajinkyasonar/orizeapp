//
//  ListYourCarVC.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 16/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class ListYourCarVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoBackButton: UIButton!
    
    @IBOutlet weak var driverNameTextField: UITextField!
    @IBOutlet weak var carNumberTextField: UITextField!
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var parkingLevelTextField: UITextField!
    
    @IBOutlet weak var numberOfSeatsLbl: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    
    @IBOutlet weak var additionalRemarksTextView: UITextView!
    
    @IBOutlet weak var saveCarDetailsBtn: UIButton!
    
    private var timePicker: UIDatePicker?
    
    
    //Hiding Views
    
    
    @IBOutlet weak var checkBoxSaveButtonView: UIStackView!
    @IBOutlet weak var numberOfSeatsWithButtonsView: UIView!
    @IBOutlet weak var listYourCarStaticTopView: UIView!
    @IBOutlet weak var enterYourCarpoolDetailsStaticLbl: UILabel!
    @IBOutlet weak var additionalRemarkTextView: UITextView!
    @IBOutlet weak var submitDetailsButton: CustomButton!
    
    
    @IBOutlet weak var confirmationMessageStack: UIStackView!
    @IBOutlet weak var confirmationMessageLblView: CustomView!
    @IBOutlet weak var confirmationMessageLbl: UILabel!
    @IBOutlet weak var editRequestButton: CustomButton!
    @IBOutlet weak var cancelRequestButton: CustomButton!
    
    @IBOutlet weak var confirmationNumberOfSeatsView: UIView!
    @IBOutlet weak var confirmationNumberOfSeatsLbl: UILabel!
    
    //Variables
    var seatCounter = 1
    var listYourCarArrayData = [ListYourCarResult]()
    
    //Variables For API Calls
    let apiFormatter = DateFormatter()
    var dateForApi = ""
    var timeForApi = ""
    var isSaveAllowedValue = 0
    
    //Variables For Editing
    var isFromEditing = false
    var carpoolVehicleTravelId = 0
    
    //Data From Employee Carpool View Controller for Editing
    var employeeCarpoolDictionary: [String: AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
    }
    
    func setupInitialUI() {
        saveCarDetailsBtn.setImage(UIImage(named: "checkboxempty"), for: .normal)
        saveCarDetailsBtn.isSelected = false
        
        driverNameTextField.delegate = self
        carNumberTextField.delegate = self
        destinationTextField.delegate = self
        dateTextField.delegate = self
        timeTextField.delegate = self
        parkingLevelTextField.delegate = self
        additionalRemarksTextView.delegate = self
        
        numberOfSeatsLbl.text = "\(seatCounter)"
        minusButton.isEnabled = false
        
        setTodaysDateToDateTextField()
        
        addTimePickerToTimeTextFieldView()
        
        confirmationMessageStack.isHidden = true
        confirmationMessageLblView.isHidden = true
        editRequestButton.isHidden = true
        cancelRequestButton.isHidden = true
        confirmationNumberOfSeatsView.isHidden = true
        logoBackButton.isHidden = true
        
        if isFromEditing {
            guard let employeeDict = employeeCarpoolDictionary else {return}
            setupInitialViewOnEmpleeCarListing(employeeDictionary: employeeDict)
        }
        
        
    }
    
    func setTodaysDateToDateTextField() {
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MM-yyyy"
        let todaysDateString = formatter.string(from: date)
        dateTextField.text = todaysDateString
        
        //Todays Date to pass for Api Call
        apiFormatter.dateFormat = "yyyy-MM-dd"
        dateForApi = apiFormatter.string(from: date)
        
    }
    
    //MARK: ACTIONS HERE
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoBackButtonTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func saveCarDetailsBtnTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        switch sender.isSelected {
        case true:
            
            sender.setImage(UIImage(named: "checked"), for: .normal)
            isSaveAllowedValue = 1
            
        case false:
            
            sender.setImage(UIImage(named: "checkboxempty"), for: .normal)
            isSaveAllowedValue = 0
        }
        
    }
    
    //MARK: ACTIONS HERE
    @IBAction func minusBtnTapped(_ sender: UIButton) {
        
        if seatCounter == 1 {
            minusButton.isEnabled = false
        } else {
            minusButton.isEnabled = true
            seatCounter -= 1
            numberOfSeatsLbl.text = "\(seatCounter)"
        }
        
    }
    
    @IBAction func plusBtnTapped(_ sender: UIButton) {
        seatCounter += 1
        numberOfSeatsLbl.text = "\(seatCounter)"
        
        if seatCounter == 1 {
            minusButton.isEnabled = false
        } else {
            minusButton.isEnabled = true
        }
        
    }
    
    @IBAction func submitDetailsBtnTapped(_ sender: UIButton) {
        
        switch isFromEditing {
            
        case true:
            //print("Just Present the Next Screen")
            
            let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
            let requiredTimeString = "\(timeForApi):00"
            let completeDateTimeStringForApi = "\(dateForApi) \(requiredTimeString)"
            //print(completeDateTimeStringForApi)
            UIChangesOnSubmitBtnTap()
            guard let driverName = driverNameTextField.text else {return}
            guard let carNumber = carNumberTextField.text else {return}
            guard let destination = destinationTextField.text else {return}
            guard let date = dateTextField.text else {return}
            guard let time = timeTextField.text else {return}
            guard let parkingLevel = parkingLevelTextField.text else {return}
            
            if driverName == "" {
                self.view.makeToast("Please enter driver name", duration: 2.0, position: .bottom)
            }else if carNumber == ""{
                self.view.makeToast("Please enter vehicle number", duration: 2.0, position: .bottom)
            }else if destination == ""{
                self.view.makeToast("Please enter destination", duration: 2.0, position: .bottom)
            }else if date == ""{
                self.view.makeToast("Please enter date", duration: 2.0, position: .bottom)
            }else if time == ""{
                self.view.makeToast("Please enter time", duration: 2.0, position: .bottom)
            }else{
                
                listYourVehicleUpdateService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId, driverName: driverName, vehicleNumber: carNumber, destination: destination, dateTimeString: completeDateTimeStringForApi, numberOfSeats: seatCounter, additionalRemarks: additionalRemarksTextView.text, isSaveAllowedValue: isSaveAllowedValue, parkingLevel: parkingLevel)
            }
            
        default:
            
            guard let driverName = driverNameTextField.text else {return}
            guard let carNumber = carNumberTextField.text else {return}
            guard let destination = destinationTextField.text else {return}
            guard let date = dateTextField.text else {return}
            guard let time = timeTextField.text else {return}
            guard let parkingLevel = parkingLevelTextField.text else {return}
            
            if driverName == "" {
                self.view.makeToast("Please enter driver name", duration: 2.0, position: .bottom)
            }else if carNumber == ""{
                self.view.makeToast("Please enter vehicle number", duration: 2.0, position: .bottom)
            }else if destination == ""{
                self.view.makeToast("Please enter destination", duration: 2.0, position: .bottom)
            }else if date == ""{
                self.view.makeToast("Please enter date", duration: 2.0, position: .bottom)
            }else if time == ""{
                self.view.makeToast("Please enter time", duration: 2.0, position: .bottom)
            }else if parkingLevel == "" {
                self.view.makeToast("Please enter parking level", duration: 2.0, position: .bottom)
            }else{
                //print("Hit API Call")
                
                switch carpoolVehicleTravelId {
                    
                case 0:
                    
                    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
                    let requiredTimeString = "\(timeForApi):00"
                    let completeDateTimeStringForApi = "\(dateForApi) \(requiredTimeString)"
                    
                    print(completeDateTimeStringForApi)
                    
                    //Call Add Service
                    listYourVehicleService(coWorkerEmployeeId: coWorkerEmployeeId, driverName: driverName, vehicleNumber: carNumber, destination: destination, dateTimeString: completeDateTimeStringForApi, numberOfSeats: seatCounter, additionalRemarks: additionalRemarksTextView.text, isSaveAllowedValue: isSaveAllowedValue, parkingLevel: parkingLevel)
                    
                default:
                    
                    print("Call Update API")
                    
                    let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
                    let requiredTimeString = "\(timeForApi):00"
                    let completeDateTimeStringForApi = "\(dateForApi) \(requiredTimeString)"
                    
                    print(completeDateTimeStringForApi)
                    
                    listYourVehicleUpdateService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId, driverName: driverName, vehicleNumber: carNumber, destination: destination, dateTimeString: completeDateTimeStringForApi, numberOfSeats: seatCounter, additionalRemarks: additionalRemarksTextView.text, isSaveAllowedValue: isSaveAllowedValue, parkingLevel: parkingLevel)
                    
                }
                
                
            }
            
        }
        
        
    }
    
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        
        self.confirmationMessageStack.isHidden = true
        self.confirmationMessageLblView.isHidden = true
        self.editRequestButton.isHidden = true
        self.cancelRequestButton.isHidden = true
        self.confirmationNumberOfSeatsView.isHidden = true
        self.logoBackButton.isHidden = true
        
        self.checkBoxSaveButtonView.isHidden = false
        self.numberOfSeatsWithButtonsView.isHidden = false
        self.listYourCarStaticTopView.isHidden = false
        self.enterYourCarpoolDetailsStaticLbl.isHidden = false
        self.additionalRemarkTextView.isHidden = false
        self.submitDetailsButton.isHidden = false
        
        self.driverNameTextField.isEnabled = true
        self.carNumberTextField.isEnabled = true
        self.destinationTextField.isEnabled = true
        self.dateTextField.isEnabled = true
        self.timeTextField.isEnabled = true
        self.parkingLevelTextField.isEnabled = true
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        
        //Call Cancel Carpool API
        let coWorkerEmployeeId = UserDefaults.standard.integer(forKey: CO_WORKERS_EMPLOYEE_ID)
        listYourVehicleCancelService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId)
        
    }
    
    
}

extension ListYourCarVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField {
        case dateTextField:
            
            guard let popupCalendarViewController = COMMOM_ELEMENT_STORYBOARD.instantiateViewController(withIdentifier: "CalendarPopupVC") as? CalendarPopupVC else {return}
            popupCalendarViewController.modalTransitionStyle = .crossDissolve
            popupCalendarViewController.modalPresentationStyle = .overCurrentContext
            popupCalendarViewController.delegate = self
            self.present(popupCalendarViewController, animated: true, completion: nil)
            
        default:
            return
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignFirstResponder()
        self.view.endEditing(true)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text != "" {
            textField.resignFirstResponder()
            
        }
        
    }
    
}

//MARK: TEXT View Delegate Implementation
extension ListYourCarVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    
}

//MARK: Calendar Popup Delegate Implementation
extension ListYourCarVC: CalendarPopupVCDelegate {
    
    func didSelectedDate(selectedDateByUser: String, selectedDateByUserForApi: String) {
        dateTextField.text = selectedDateByUser
        dateForApi = selectedDateByUserForApi
        
    }
    
}

//MARK: TIME PICKER FUNCTIONALITY
extension ListYourCarVC {
    
    func addTimePickerToTimeTextFieldView() {
        
        timePicker = UIDatePicker()
        timePicker?.datePickerMode = .time
        timeTextField.inputView = timePicker
        
        timePicker?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        timePicker?.setValue(#colorLiteral(red: 0.03921568627, green: 0.2352941176, blue: 0.3647058824, alpha: 1), forKeyPath: "textColor")
        
        timePicker?.addTarget(self, action: #selector(timeSelectedByUser(_:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        view.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func timeSelectedByUser(_ sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        timeTextField.text = formatter.string(from: sender.date)
        
        //Convert Time for API - 24 hours format
        apiFormatter.dateFormat = "HH:mm"
        timeForApi = apiFormatter.string(from: sender.date)
        
    }
    
    @objc func viewTapped(_ gestureRecognizer: UIGestureRecognizer) {
        view.endEditing(true)
    }
    
}

//MARK: API CALL IMPLEMENTATIONS HERE
extension ListYourCarVC {
    
    //List your Vehicle api
    
    func listYourVehicleService(coWorkerEmployeeId: Int, driverName: String, vehicleNumber: String, destination: String, dateTimeString: String, numberOfSeats: Int, additionalRemarks: String, isSaveAllowedValue: Int, parkingLevel: String) {
        
        SVProgressHUD.show()
        
        CarpoolServices.instance.listYourCarService(coWorkerEmployeeId: coWorkerEmployeeId, driverName: driverName, vehicleNumber: vehicleNumber, destination: destination, dateTimeString: dateTimeString, numberOfSeats: numberOfSeats, additionalRemarks: additionalRemarks, isSaveAllowedValue: isSaveAllowedValue, parkingLevel: parkingLevel) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    self.listYourCarArrayData = returnedResponse.results
                    
                    for values in self.listYourCarArrayData
                    {
                        self.carpoolVehicleTravelId = values.carPoolVehicleTravelID
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.confirmationMessageStack.isHidden = false
                        self.confirmationMessageLblView.isHidden = false
                        self.editRequestButton.isHidden = false
                        self.cancelRequestButton.isHidden = false
                        self.confirmationNumberOfSeatsView.isHidden = false
                        self.logoBackButton.isHidden = false
                        
                        self.checkBoxSaveButtonView.isHidden = true
                        self.numberOfSeatsWithButtonsView.isHidden = true
                        self.listYourCarStaticTopView.isHidden = true
                        self.enterYourCarpoolDetailsStaticLbl.isHidden = true
                        self.additionalRemarkTextView.isHidden = true
                        self.submitDetailsButton.isHidden = true
                        
                        self.driverNameTextField.isEnabled = false
                        self.carNumberTextField.isEnabled = false
                        self.destinationTextField.isEnabled = false
                        self.dateTextField.isEnabled = false
                        self.timeTextField.isEnabled = false
                        self.parkingLevelTextField.isEnabled = false
                        
                        self.confirmationNumberOfSeatsLbl.text = "\(self.seatCounter)"
                        self.confirmationMessageLbl.text = self.additionalRemarksTextView.text
                        
                        self.scrollView.setContentOffset(.zero, animated: false)
                        
                        SVProgressHUD.dismiss()
                        
                    }
                    //print(self.listYourCarArrayData)
                }
                
            }
            
            
            
        }
        
    }
    
    //List your Vehicle update api
    func listYourVehicleUpdateService(coWorkerEmployeeId: Int, carpoolVehicleTravelId: Int, driverName: String, vehicleNumber: String, destination: String, dateTimeString: String, numberOfSeats: Int, additionalRemarks: String, isSaveAllowedValue: Int, parkingLevel: String) {
        
        SVProgressHUD.show()
        
        CarpoolServices.instance.listYourCarUpdateService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId, driverName: driverName, vehicleNumber: vehicleNumber, destination: destination, dateTimeString: dateTimeString, numberOfSeats: numberOfSeats, additionalRemarks: additionalRemarks, isSaveAllowedValue: isSaveAllowedValue, parkingLevel: parkingLevel) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                    SVProgressHUD.dismiss()
                }
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    DispatchQueue.main.async {
                        
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        
                        self.confirmationMessageStack.isHidden = false
                        self.confirmationMessageLblView.isHidden = false
                        self.editRequestButton.isHidden = false
                        self.cancelRequestButton.isHidden = false
                        self.confirmationNumberOfSeatsView.isHidden = false
                        self.logoBackButton.isHidden = false
                        
                        self.checkBoxSaveButtonView.isHidden = true
                        self.numberOfSeatsWithButtonsView.isHidden = true
                        self.listYourCarStaticTopView.isHidden = true
                        self.enterYourCarpoolDetailsStaticLbl.isHidden = true
                        self.additionalRemarkTextView.isHidden = true
                        self.submitDetailsButton.isHidden = true
                        
                        self.driverNameTextField.isEnabled = false
                        self.carNumberTextField.isEnabled = false
                        self.destinationTextField.isEnabled = false
                        self.dateTextField.isEnabled = false
                        self.timeTextField.isEnabled = false
                        self.parkingLevelTextField.isEnabled = false
                        
                        self.confirmationNumberOfSeatsLbl.text = "\(self.seatCounter)"
                        self.confirmationMessageLbl.text = self.additionalRemarksTextView.text
                        
                        self.scrollView.setContentOffset(.zero, animated: false)
                        
                        SVProgressHUD.dismiss()
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //List Your Vehicle Cancel Api
    
    func listYourVehicleCancelService(coWorkerEmployeeId: Int, carpoolVehicleTravelId: Int) {
        
        SVProgressHUD.show()
        
        CarpoolServices.instance.listYourCarCancelService(coWorkerEmployeeId: coWorkerEmployeeId, carpoolVehicleTravelId: carpoolVehicleTravelId) { (returnedResponse, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription, duration: 2.0, position: .bottom)
                    SVProgressHUD.dismiss()
                }
                
            }else {
                
                if let returnedResponse = returnedResponse {
                    
                    if returnedResponse.code == 0
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            
                            SVProgressHUD.dismiss()
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                guard let carpoolViewController = self.storyboard?.instantiateViewController(withIdentifier: "CarpoolVC") as? CarpoolVC else {return}
                                self.navigationController?.pushViewController(carpoolViewController, animated: true)
                                
                            })
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

//MARK: IMPLEMENTATIONS ONLY WHEN ISEDITING IS TRUE -> EMPLOYEE CARPOOL LISTING EDIT
extension ListYourCarVC {
    
    func setupInitialViewOnEmpleeCarListing(employeeDictionary: [String: AnyObject]) {
        
        self.driverNameTextField.text = employeeDictionary["driverName"] as? String
        self.carNumberTextField.text = employeeDictionary["vehicleNumber"] as? String
        self.destinationTextField.text = employeeDictionary["destination"] as? String
        self.dateTextField.text = employeeDictionary["displayDate"] as? String
        self.timeTextField.text = employeeDictionary["displayTime"] as? String
        self.seatCounter = employeeDictionary["numberOfSeats"] as? Int ?? 0
        self.numberOfSeatsLbl.text = "\(seatCounter)"
        self.additionalRemarkTextView.text = employeeDictionary["remarks"] as? String
        self.parkingLevelTextField.text = employeeDictionary["parkingLevel"] as? String
        
        timeForApi = employeeDictionary["apiTime"] as! String
        dateForApi = employeeDictionary["apiDate"] as! String
        
        if isSaveAllowedValue == 1 {
            saveCarDetailsBtn.setImage(UIImage(named: "checked"), for: .normal)
            
        } else {
            saveCarDetailsBtn.setImage(UIImage(named: "checkboxempty"), for: .normal)
        }
        
        
    }
    
    //When isEditing is True
    func UIChangesOnSubmitBtnTap() {
        
        self.confirmationMessageStack.isHidden = false
        self.confirmationMessageLblView.isHidden = false
        self.editRequestButton.isHidden = false
        self.cancelRequestButton.isHidden = false
        self.confirmationNumberOfSeatsView.isHidden = false
        self.logoBackButton.isHidden = false
        
        self.checkBoxSaveButtonView.isHidden = true
        self.numberOfSeatsWithButtonsView.isHidden = true
        self.listYourCarStaticTopView.isHidden = true
        self.enterYourCarpoolDetailsStaticLbl.isHidden = true
        self.additionalRemarkTextView.isHidden = true
        self.submitDetailsButton.isHidden = true
        
        self.driverNameTextField.isEnabled = false
        self.carNumberTextField.isEnabled = false
        self.destinationTextField.isEnabled = false
        self.dateTextField.isEnabled = false
        self.timeTextField.isEnabled = false
        self.parkingLevelTextField.isEnabled = false
        
        self.confirmationNumberOfSeatsLbl.text = "\(self.seatCounter)"
        self.confirmationMessageLbl.text = self.additionalRemarksTextView.text
        
    }
    
}
