//
//  CustomView.swift
//  Orize App
//
//  Created by Ajinkya Sonar on 16/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

@IBDesignable
class CustomView: UIView {

    @IBInspectable var borderWidth: CGFloat = 0.0 {
        
        didSet {
           self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
}
