//
//  SentChallengeTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 11/04/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit

protocol SentChallengeTblCellDelegate {
    func didTapCancelChallengeRequestBtn(_ tag: Int)
}

class SentChallengeTblCell: UITableViewCell {
    
    @IBOutlet weak var elementView: UIView!
    @IBOutlet weak var gameNameLbl: UILabel!
    @IBOutlet weak var playerNumberLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeSlotLbl: UILabel!
    @IBOutlet weak var contactNumberBtn: UIButton!
    @IBOutlet weak var cancelChallengeRequestBtn: UIButton!
    
    var delegate: SentChallengeTblCellDelegate?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        elementView.layer.cornerRadius = 5
        cancelChallengeRequestBtn.layer.cornerRadius = 5
        contactNumberBtn.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func configureCell(data: RequestDetails) {
        
        gameNameLbl.text = data.sportsTypeName
        playerNumberLbl.text = "\(data.playersCapacity)/6 PlAYERS"
        dateLbl.text = data.bookingDate
        timeSlotLbl.text = data.bookingTime
        contactNumberBtn.setTitle(data.contactNumber, for: .normal)
        
    }

    
    //ACTIONS
    @IBAction func cancelChallengeBtnTapped(_ sender: UIButton) {
        delegate?.didTapCancelChallengeRequestBtn(self.tag)
    }
    
}
