//
//  EventCollectionCell.swift
//  Orize App
//
//  Created by Apple on 12/08/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol EventCollectionCellDelegate: class {
    
    func didTapJoinEventBtn(eventDetail: Event, at index: Int)
    
    func didTapEventImage(eventDetail: Event, at index: Int)
}

class EventCollectionCell: UICollectionViewCell {
    
    @IBOutlet private weak var eventImage: UIImageView!
    @IBOutlet private weak var eventNameLbl: UILabel!
    let eventImageTapGesture = UITapGestureRecognizer()
    
    private weak var delegate: EventCollectionCellDelegate?
    private var selectedEventDetail: Event!
    private var eventIndex: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(delegate: EventCollectionCellDelegate, eventDetail: Event, selectedIndex: Int) {
        
        self.delegate = delegate
        self.selectedEventDetail = eventDetail
        self.eventIndex = selectedIndex
        
        guard let imageUrl = URL(string: eventDetail.eventImage) else {return}
        eventImage.sd_setImage(with: imageUrl, completed: .none)
        
        eventImage.isUserInteractionEnabled = true
        eventImage.addGestureRecognizer(eventImageTapGesture)
        eventImageTapGesture.addTarget(self, action: #selector(eventImageTapped))
        
        eventNameLbl.text = eventDetail.eventName
    }
    
    @IBAction func joinEventBtnTapped(_ sender: UIButton) {
        delegate?.didTapJoinEventBtn(eventDetail: selectedEventDetail, at: eventIndex)
    }
    
    @objc func eventImageTapped() {
        delegate?.didTapEventImage(eventDetail: selectedEventDetail, at: eventIndex)
    }

}
