//
//  FoodCartTblCell.swift
//  Orize App
//
//  Created by Aditya Infotech on 30/05/19.
//  Copyright © 2019 Aditya Infotech. All rights reserved.
//

import UIKit
import SDWebImage

protocol FoodCartTblCellDelegate: class {
    
    func addButtonTappedFoodCart(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int)
    
    func removeButtonTappedFoodCart(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int)
    
    func preBookingEditButtonTapped(productDetails: ProductCartList, productInt: Int, ProductQuantity: Int)
}

class FoodCartTblCell: UITableViewCell {
    
    @IBOutlet private weak var elementView: UIView!
    @IBOutlet private weak var itemImage: UIImageView!
    @IBOutlet private weak var itemTitleLbl: UILabel!
    @IBOutlet private weak var unitCountLbl: UILabel!
    
    @IBOutlet private weak var itemPriceLbl: UILabel!
    @IBOutlet private weak var sGstLbl: UILabel!
    @IBOutlet private weak var cGstLbl: UILabel!
    @IBOutlet private weak var itemTotalPriceWithTaxLbl: UILabel!
    
    
    //Hiding Outlets
    @IBOutlet private weak var addRemoveBtnStack: UIStackView!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var preBookedLbl: UILabel!
    
    weak var delegate: FoodCartTblCellDelegate?
    private var productDetails: ProductCartList!
    private var productInt: Int!
    private var productQuantity: Int!
    
    //Variables
    var counter = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        elementView.layer.cornerRadius = 10
        elementView.layer.borderColor = UIColor.black.cgColor
        elementView.layer.borderWidth = 0.3
        
        itemImage.layer.cornerRadius = 10
        itemImage.layer.borderColor = UIColor.black.cgColor
        itemImage.layer.borderWidth = 0.3
        
    }
    
    
    func configureCell(productDetails: ProductCartList, delegate: FoodCartTblCellDelegate, categoryInt: Int, productInt: Int) {
        
        
        if productDetails.isPrebookingCategory == 1 {
            
            addRemoveBtnStack.isHidden = true
            editButton.isHidden = false
            preBookedLbl.isHidden = false
        }else {
            addRemoveBtnStack.isHidden = false
            editButton.isHidden = true
            preBookedLbl.isHidden = true
        }
        
        
        self.delegate = delegate
        self.productDetails = productDetails
        self.productInt = productInt
        
        guard let productImageUrl = URL(string: productDetails.productPhoto) else {return}
        itemImage.sd_setImage(with: productImageUrl)
        
        itemTitleLbl.text = productDetails.productName
        itemPriceLbl.text = "\(rupee) \(productDetails.productPrice) x \(productDetails.productQty)"
        cGstLbl.text = "\(rupee) \(productDetails.cgst ?? 0)"
        sGstLbl.text = "\(rupee) \(productDetails.sgst ?? 0)"
        itemTotalPriceWithTaxLbl.text = "\(rupee) \(productDetails.productPriceWithTax ?? -1)"
        unitCountLbl.text = "\(productDetails.productQty)"
        counter = productDetails.productQty
        
    }
    
    
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        guard let productDetails = productDetails else {return}
        
        if counter < productDetails.productAllowedQtyInCart {
            counter += 1
            unitCountLbl.text = "\(counter)"
            delegate?.addButtonTappedFoodCart(productDetails: productDetails, productInt: productInt, ProductQuantity: counter)
        }else {
            self.makeToast("Max Product Quantity Reached", duration: 1.0, position: .center)
        }
        
        
    }
    
    @IBAction func removeBtnTapped(_ sender: UIButton) {
        
        if counter > 0 {
            counter -= 1
            unitCountLbl.text = "\(counter)"
            delegate?.removeButtonTappedFoodCart(productDetails: productDetails, productInt: productInt, ProductQuantity: counter)
        }else {
            
            self.makeToast("Lowest Count", duration: 1.0, position: .bottom)
        }
        
        
    }
    
    @IBAction func preBookingEditBtnTapped(_ sender: UIButton) {
        delegate?.preBookingEditButtonTapped(productDetails: productDetails, productInt: productInt, ProductQuantity: counter)
    }
    
}
